concertoPanel.factory('UserTestCollectionService', function (BaseCollectionService) {
    let collectionService = Object.create(BaseCollectionService);
    collectionService.collectionPath = Paths.USERTEST_COLLECTION;
    collectionService.userRoleRequired = "role_super_admin";
    return collectionService;
});