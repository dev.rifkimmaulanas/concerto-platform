<?php

namespace Concerto\PanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Concerto\PanelBundle\Service\UserTestService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class UserTestController extends ASectionController
{

    const ENTITY_NAME = "UserTest";

    public function __construct(EngineInterface $templating, UserTestService $service, TranslatorInterface $translator)
    {
        parent::__construct($templating, $service, $translator);

        $this->entityName = self::ENTITY_NAME;
    }

    /**
     * @Route("/UserTest/fetch/{object_id}/{format}", name="User_test_object", defaults={"format":"json"})
     * @param $object_id
     * @param string $format
     * @return Response
     */
    public function objectAction($object_id, $format = "json")
    {
        return parent::objectAction($object_id, $format);
    }

    /**
     * @Route("/UserTest/collection/{format}", name="User_test_collection", defaults={"format":"json"})
     * @param string $format
     * @return Response
     */
    public function collectionAction($format = "json")
    {
        return parent::collectionAction($format);
    }

    /**
     * @Route("/UserTest/form/{action}", name="User_test_form", defaults={"action":"edit"})
     * @param string $action
     * @param array $params
     * @return Response
     */
    public function formAction($action = "edit", $params = array())
    {
        return parent::formAction($action, $params);
    }

    /**
     * @Route("/UserTest/{object_id}/save", name="User_test_save", methods={"POST"})
     * @param Request $request
     * @param $object_id
     * @return Response
     */
    public function saveAction(Request $request, $object_id)
    {
        $result = $this->service->save(
            $object_id,
            "private",
            "1",
            $this->service->get(""),
            "",
            $request->get("username")."@gmail.com",
            $request->get("username"),
            $request->get("password"),
            $request->get("password"),
            1,
            1,
            1,
            1,
            1,
            1
        );
        return $this->getSaveResponse($result);
    }

    /**
     * @Route("/UserTest/{object_ids}/delete", name="User_test_delete", methods={"POST"})
     * @param Request $request
     * @param $object_ids
     * @return Response
     */
    public function deleteAction(Request $request, $object_ids)
    {
        return parent::deleteAction($request, $object_ids);
    }
}
