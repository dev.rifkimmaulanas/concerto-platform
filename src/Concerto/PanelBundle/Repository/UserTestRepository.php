<?php

namespace Concerto\PanelBundle\Repository;

use Concerto\PanelBundle\Entity\UserTest;

/**
 * UserTestRepository
 */
class UserTestRepository extends AEntityRepository {
    
    public function findAllExcept(UserTest $user) {
        $query = $this->getEntityManager()->createQuery(
                        'SELECT u
            FROM ConcertoPanelBundle:User u
            WHERE u.id != :id'
                )->setParameter('id', $user->getId());
        return $query->getResult();
    }
}
