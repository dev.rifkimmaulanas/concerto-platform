<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_wdt']), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_search_results']), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler']), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_router']), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception']), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception_css']), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_twig_error_test']), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        elseif (0 === strpos($pathinfo, '/api')) {
            if (0 === strpos($pathinfo, '/api/check')) {
                // concerto_api_checks_health
                if ('/api/check/health' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\APIBundle\\Controller\\ChecksController::healthAction',  '_route' => 'concerto_api_checks_health',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_concerto_api_checks_health;
                    }

                    return $ret;
                }
                not_concerto_api_checks_health:

                // concerto_api_checks_sessioncount
                if ('/api/check/session_count' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\APIBundle\\Controller\\ChecksController::sessionCountAction',  '_route' => 'concerto_api_checks_sessioncount',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_concerto_api_checks_sessioncount;
                    }

                    return $ret;
                }
                not_concerto_api_checks_sessioncount:

                // concerto_api_checks_prometheus
                if ('/api/check/prometheus' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\APIBundle\\Controller\\ChecksController::prometheusAction',  '_route' => 'concerto_api_checks_prometheus',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_concerto_api_checks_prometheus;
                    }

                    return $ret;
                }
                not_concerto_api_checks_prometheus:

            }

            elseif (0 === strpos($pathinfo, '/api/data')) {
                // concerto_api_datarecord_datacollection
                if (preg_match('#^/api/data/(?P<table_id>[^/]++)$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_datarecord_datacollection']), array (  '_controller' => 'Concerto\\APIBundle\\Controller\\DataRecordController::dataCollectionAction',));
                    if (!in_array($canonicalMethod, ['GET', 'POST', 'PUT'])) {
                        $allow = array_merge($allow, ['GET', 'POST', 'PUT']);
                        goto not_concerto_api_datarecord_datacollection;
                    }

                    return $ret;
                }
                not_concerto_api_datarecord_datacollection:

                // concerto_api_datarecord_dataobject
                if (preg_match('#^/api/data/(?P<table_id>[^/]++)/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_datarecord_dataobject']), array (  '_controller' => 'Concerto\\APIBundle\\Controller\\DataRecordController::dataObjectAction',));
                    if (!in_array($canonicalMethod, ['GET', 'POST', 'PUT', 'DELETE'])) {
                        $allow = array_merge($allow, ['GET', 'POST', 'PUT', 'DELETE']);
                        goto not_concerto_api_datarecord_dataobject;
                    }

                    return $ret;
                }
                not_concerto_api_datarecord_dataobject:

            }

            elseif (0 === strpos($pathinfo, '/api/saml')) {
                // concerto_api_saml_login
                if ('/api/saml/login' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\APIBundle\\Controller\\SamlController::loginAction',  '_route' => 'concerto_api_saml_login',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_concerto_api_saml_login;
                    }

                    return $ret;
                }
                not_concerto_api_saml_login:

                // concerto_api_saml_logout
                if ('/api/saml/logout' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\APIBundle\\Controller\\SamlController::logoutAction',  '_route' => 'concerto_api_saml_logout',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_concerto_api_saml_logout;
                    }

                    return $ret;
                }
                not_concerto_api_saml_logout:

                // concerto_api_saml_acs
                if ('/api/saml/acs' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\APIBundle\\Controller\\SamlController::acsAction',  '_route' => 'concerto_api_saml_acs',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_concerto_api_saml_acs;
                    }

                    return $ret;
                }
                not_concerto_api_saml_acs:

                // concerto_api_saml_sls
                if ('/api/saml/sls' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\APIBundle\\Controller\\SamlController::slsAction',  '_route' => 'concerto_api_saml_sls',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_concerto_api_saml_sls;
                    }

                    return $ret;
                }
                not_concerto_api_saml_sls:

                // concerto_api_saml_metadata
                if ('/api/saml/metadata' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\APIBundle\\Controller\\SamlController::metadataAction',  '_route' => 'concerto_api_saml_metadata',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_concerto_api_saml_metadata;
                    }

                    return $ret;
                }
                not_concerto_api_saml_metadata:

            }

            elseif (0 === strpos($pathinfo, '/api/runner/test')) {
                // concerto_api_testrunner_startnewsession
                if (preg_match('#^/api/runner/test/(?P<test_slug>[^/]++)/session/start(?:/(?P<params>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_testrunner_startnewsession']), array (  'test_name' => NULL,  'params' => '{}',  'debug' => false,  '_controller' => 'Concerto\\APIBundle\\Controller\\TestRunnerController::startNewSessionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_concerto_api_testrunner_startnewsession;
                    }

                    return $ret;
                }
                not_concerto_api_testrunner_startnewsession:

                // concerto_api_testrunner_startnewsession_1
                if (0 === strpos($pathinfo, '/api/runner/test_n') && preg_match('#^/api/runner/test_n/(?P<test_name>[^/]++)/session/start(?:/(?P<params>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_testrunner_startnewsession_1']), array (  'test_slug' => NULL,  'params' => '{}',  'debug' => false,  'test_name' => NULL,  '_controller' => 'Concerto\\APIBundle\\Controller\\TestRunnerController::startNewSessionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_concerto_api_testrunner_startnewsession_1;
                    }

                    return $ret;
                }
                not_concerto_api_testrunner_startnewsession_1:

                // concerto_api_testrunner_startnewsession_2
                if (preg_match('#^/api/runner/test/(?P<test_slug>[^/]++)/run(?:/(?P<params>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_testrunner_startnewsession_2']), array (  'test_name' => NULL,  'params' => '{}',  'debug' => false,  '_controller' => 'Concerto\\APIBundle\\Controller\\TestRunnerController::startNewSessionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_concerto_api_testrunner_startnewsession_2;
                    }

                    return $ret;
                }
                not_concerto_api_testrunner_startnewsession_2:

                if (0 === strpos($pathinfo, '/api/runner/test/session')) {
                    // concerto_api_testrunner_submittosession
                    if (preg_match('#^/api/runner/test/session/(?P<session_hash>[^/]++)/submit$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_testrunner_submittosession']), array (  '_controller' => 'Concerto\\APIBundle\\Controller\\TestRunnerController::submitToSessionAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_concerto_api_testrunner_submittosession;
                        }

                        return $ret;
                    }
                    not_concerto_api_testrunner_submittosession:

                    // concerto_api_testrunner_backgroundworker
                    if (preg_match('#^/api/runner/test/session/(?P<session_hash>[^/]++)/worker$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_testrunner_backgroundworker']), array (  '_controller' => 'Concerto\\APIBundle\\Controller\\TestRunnerController::backgroundWorkerAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_concerto_api_testrunner_backgroundworker;
                        }

                        return $ret;
                    }
                    not_concerto_api_testrunner_backgroundworker:

                    // concerto_api_testrunner_killsession
                    if (preg_match('#^/api/runner/test/session/(?P<session_hash>[^/]++)/kill$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_testrunner_killsession']), array (  '_controller' => 'Concerto\\APIBundle\\Controller\\TestRunnerController::killSessionAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_concerto_api_testrunner_killsession;
                        }

                        return $ret;
                    }
                    not_concerto_api_testrunner_killsession:

                    // concerto_api_testrunner_keepalivesession
                    if (preg_match('#^/api/runner/test/session/(?P<session_hash>[^/]++)/keepalive$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_testrunner_keepalivesession']), array (  '_controller' => 'Concerto\\APIBundle\\Controller\\TestRunnerController::keepAliveSessionAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_concerto_api_testrunner_keepalivesession;
                        }

                        return $ret;
                    }
                    not_concerto_api_testrunner_keepalivesession:

                    // concerto_api_testrunner_uploadfile
                    if (preg_match('#^/api/runner/test/session/(?P<session_hash>[^/]++)/upload$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'concerto_api_testrunner_uploadfile']), array (  '_controller' => 'Concerto\\APIBundle\\Controller\\TestRunnerController::uploadFileAction',));
                        if (!in_array($requestMethod, ['POST', 'OPTIONS'])) {
                            $allow = array_merge($allow, ['POST', 'OPTIONS']);
                            goto not_concerto_api_testrunner_uploadfile;
                        }

                        return $ret;
                    }
                    not_concerto_api_testrunner_uploadfile:

                }

            }

        }

        elseif (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/Administration')) {
                if (0 === strpos($pathinfo, '/admin/AdministrationSetting/map')) {
                    // AdministrationSetting_map
                    if ('/admin/AdministrationSetting/map' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::settingsMapAction',  '_route' => 'AdministrationSetting_map',);
                    }

                    // AdministrationSetting_map_update
                    if ('/admin/AdministrationSetting/map/update' === $pathinfo) {
                        $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::updateSettingsMapAction',  '_route' => 'AdministrationSetting_map_update',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_AdministrationSetting_map_update;
                        }

                        return $ret;
                    }
                    not_AdministrationSetting_map_update:

                }

                // AdministrationSetting_session_count_clear
                if ('/admin/AdministrationSetting/SessionCount/clear' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::clearSessionCountAction',  '_route' => 'AdministrationSetting_session_count_clear',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_AdministrationSetting_session_count_clear;
                    }

                    return $ret;
                }
                not_AdministrationSetting_session_count_clear:

                if (0 === strpos($pathinfo, '/admin/Administration/Messages')) {
                    // Administration_messages_collection
                    if ('/admin/Administration/Messages/collection' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::messagesCollectionAction',  '_route' => 'Administration_messages_collection',);
                    }

                    // Administration_messages_delete
                    if (preg_match('#^/admin/Administration/Messages/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'Administration_messages_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::deleteMessageAction',));
                    }

                    // Administration_messages_clear
                    if ('/admin/Administration/Messages/clear' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::clearMessagesAction',  '_route' => 'Administration_messages_clear',);
                    }

                }

                // Administration_session_count_collection
                if (0 === strpos($pathinfo, '/admin/Administration/SessionCount') && preg_match('#^/admin/Administration/SessionCount/(?P<filter>[^/]++)/collection$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'Administration_session_count_collection']), array (  'filter' => '{}',  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::sessionCountCollectionAction',));
                }

                if (0 === strpos($pathinfo, '/admin/Administration/ScheduledTask')) {
                    // Administration_tasks_collection
                    if ('/admin/Administration/ScheduledTask/collection' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::tasksCollectionAction',  '_route' => 'Administration_tasks_collection',);
                    }

                    // Administration_tasks_content_import
                    if ('/admin/Administration/ScheduledTask/content_import' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::taskContentImportAction',  '_route' => 'Administration_tasks_content_import',);
                    }

                    // Administration_tasks_package_install
                    if ('/admin/Administration/ScheduledTask/package_install' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::taskPackageInstallAction',  '_route' => 'Administration_tasks_package_install',);
                    }

                    if (0 === strpos($pathinfo, '/admin/Administration/ScheduledTask/git_')) {
                        // Administration_tasks_git_pull
                        if ('/admin/Administration/ScheduledTask/git_pull' === $pathinfo) {
                            return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::taskGitPullAction',  '_route' => 'Administration_tasks_git_pull',);
                        }

                        // Administration_tasks_git_enable
                        if ('/admin/Administration/ScheduledTask/git_enable' === $pathinfo) {
                            return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::taskGitEnableAction',  '_route' => 'Administration_tasks_git_enable',);
                        }

                        // Administration_tasks_git_update
                        if ('/admin/Administration/ScheduledTask/git_update' === $pathinfo) {
                            return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::taskGitUpdateAction',  '_route' => 'Administration_tasks_git_update',);
                        }

                        // Administration_tasks_git_reset
                        if ('/admin/Administration/ScheduledTask/git_reset' === $pathinfo) {
                            return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::taskGitResetAction',  '_route' => 'Administration_tasks_git_reset',);
                        }

                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/Administration/api_clients')) {
                    // Administration_api_clients_collection
                    if ('/admin/Administration/api_clients/collection' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::apiClientCollectionAction',  '_route' => 'Administration_api_clients_collection',);
                    }

                    // Administration_api_clients_delete
                    if (preg_match('#^/admin/Administration/api_clients/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'Administration_api_clients_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::deleteApiClientAction',));
                    }

                    // Administration_api_clients_clear
                    if ('/admin/Administration/api_clients/clear' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::clearApiClientAction',  '_route' => 'Administration_api_clients_clear',);
                    }

                    // Administration_api_clients_add
                    if ('/admin/Administration/api_clients/add' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::addApiClientAction',  '_route' => 'Administration_api_clients_add',);
                    }

                }

                // Administration_packages_status
                if ('/admin/Administration/packages/status' === $pathinfo) {
                    return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::packagesStatusAction',  '_route' => 'Administration_packages_status',);
                }

                // Administration_content_export
                if (0 === strpos($pathinfo, '/admin/Administration/content/export') && preg_match('#^/admin/Administration/content/export(?:/(?P<instructions>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'Administration_content_export']), array (  'instructions' => '[]',  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::exportContentAction',));
                }

                // Administration_user
                if ('/admin/Administration/user' === $pathinfo) {
                    return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::getAuthUserAction',  '_route' => 'Administration_user',);
                }

                if (0 === strpos($pathinfo, '/admin/Administration/git')) {
                    // Administration_git_disable
                    if ('/admin/Administration/git/disable' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::disableGitAction',  '_route' => 'Administration_git_disable',);
                    }

                    // Administration_git_diff
                    if (0 === strpos($pathinfo, '/admin/Administration/git/diff') && preg_match('#^/admin/Administration/git/diff/(?P<sha>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'Administration_git_diff']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::gitDiffAction',));
                    }

                    // Administration_git_status
                    if ('/admin/Administration/git/status' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::gitStatusAction',  '_route' => 'Administration_git_status',);
                    }

                    // Administration_git_commit
                    if ('/admin/Administration/git/commit' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::gitCommitAction',  '_route' => 'Administration_git_commit',);
                    }

                    // Administration_git_push
                    if ('/admin/Administration/git/push' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\AdministrationController::gitPushAction',  '_route' => 'Administration_git_push',);
                    }

                }

            }

            elseif (0 === strpos($pathinfo, '/admin/DataTable')) {
                // DataTable_object
                if (0 === strpos($pathinfo, '/admin/DataTable/fetch') && preg_match('#^/admin/DataTable/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::objectAction',));
                }

                // DataTable_collection
                if (0 === strpos($pathinfo, '/admin/DataTable/collection') && preg_match('#^/admin/DataTable/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::collectionAction',));
                }

                // DataTable_toggleLock
                if (preg_match('#^/admin/DataTable/(?P<object_id>[^/]++)/toggleLock$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_toggleLock']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::toggleLock',));
                }

                // DataTable_form
                if (0 === strpos($pathinfo, '/admin/DataTable/form') && preg_match('#^/admin/DataTable/form(?:/(?P<action>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_form']), array (  'action' => 'edit',  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::formAction',));
                }

                // DataTable_save
                if (preg_match('#^/admin/DataTable/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::saveAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_save;
                    }

                    return $ret;
                }
                not_DataTable_save:

                // DataTable_copy
                if (preg_match('#^/admin/DataTable/(?P<object_id>[^/]++)/copy$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_copy']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::copyAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_copy;
                    }

                    return $ret;
                }
                not_DataTable_copy:

                // DataTable_delete
                if (preg_match('#^/admin/DataTable/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::deleteAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_delete;
                    }

                    return $ret;
                }
                not_DataTable_delete:

                // DataTable_columns_collection
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/columns/collection$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_columns_collection']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::columnsCollectionAction',));
                }

                // DataTable_column_object
                if (0 === strpos($pathinfo, '/admin/DataTable/fetch') && preg_match('#^/admin/DataTable/fetch/(?P<table_id>[^/]++)/column/(?P<column_name>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_column_object']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::fetchColumnAction',));
                }

                // DataTable_data_collection
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/data/collection(?:/(?P<prefixed>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_data_collection']), array (  'prefixed' => 0,  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::dataCollectionAction',));
                }

                // DataTable_data_collection_csv
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/data/csv/(?P<name>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_data_collection_csv']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::streamedCsvDataCollectionAction',));
                }

                // DataTable_data_section
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/data/section$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_data_section']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::dataSectionAction',));
                }

                // DataTable_column_delete
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/column/(?P<column_names>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_column_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::deleteColumnAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_column_delete;
                    }

                    return $ret;
                }
                not_DataTable_column_delete:

                // DataTable_row_delete
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/row/(?P<row_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_row_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::deleteRowAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_row_delete;
                    }

                    return $ret;
                }
                not_DataTable_row_delete:

                // DataTable_truncate
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/truncate$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_truncate']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::truncateAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_truncate;
                    }

                    return $ret;
                }
                not_DataTable_truncate:

                // DataTable_deleteAll
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/deleteAll$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_deleteAll']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::deleteAllAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_deleteAll;
                    }

                    return $ret;
                }
                not_DataTable_deleteAll:

                // DataTable_column_save
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/column/(?P<column_name>[^/]++)/save$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_column_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::saveColumnAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_column_save;
                    }

                    return $ret;
                }
                not_DataTable_column_save:

                // DataTable_row_insert
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/row/insert$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_row_insert']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::insertRowAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_row_insert;
                    }

                    return $ret;
                }
                not_DataTable_row_insert:

                // DataTable_row_update
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/row/(?P<row_id>[^/]++)/update(?:/(?P<prefixed>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_row_update']), array (  'prefixed' => 0,  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::updateRowAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_row_update;
                    }

                    return $ret;
                }
                not_DataTable_row_update:

                // DataTable_export
                if (preg_match('#^/admin/DataTable/(?P<instructions>[^/]++)/export(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_export']), array (  'format' => 'yml',  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::exportAction',));
                }

                // DataTable_export_instructions
                if (preg_match('#^/admin/DataTable/(?P<object_ids>[^/]++)/instructions/export$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_export_instructions']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::exportInstructionsAction',));
                }

                if (0 === strpos($pathinfo, '/admin/DataTable/import')) {
                    // DataTable_import
                    if ('/admin/DataTable/import' === $pathinfo) {
                        $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::scheduleImportAction',  '_route' => 'DataTable_import',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_DataTable_import;
                        }

                        return $ret;
                    }
                    not_DataTable_import:

                    // DataTable_pre_import_status
                    if ('/admin/DataTable/import/status' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::preImportStatusAction',  '_route' => 'DataTable_pre_import_status',);
                    }

                }

                // DataTable_csv_import
                if (preg_match('#^/admin/DataTable/(?P<table_id>[^/]++)/csv/(?P<restructure>[^/]++)/(?P<header>[^/]++)/(?P<delimiter>[^/]++)/(?P<enclosure>[^/]++)/import$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'DataTable_csv_import']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DataTableController::importCsvAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_DataTable_csv_import;
                    }

                    return $ret;
                }
                not_DataTable_csv_import:

            }

            elseif (0 === strpos($pathinfo, '/admin/dialog')) {
                // Dialog_rdoc
                if ('/admin/dialog/r_documentation_generation_help.html' === $pathinfo) {
                    return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DialogController::rDocumentationWindowAction',  '_route' => 'Dialog_rdoc',);
                }

                // Dialog_generic
                if (preg_match('#^/admin/dialog/(?P<template_name>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'Dialog_generic']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DialogController::genericWindowAction',));
                }

                // Dialog_root
                if ('/admin/dialog' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\DialogController::genericWindowAction',  '_route' => 'Dialog_root',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_Dialog_root;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'Dialog_root'));
                    }

                    return $ret;
                }
                not_Dialog_root:

            }

            elseif (0 === strpos($pathinfo, '/admin/file')) {
                // FileBrowser_list
                if ('/admin/file/list' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::listAction',  '_route' => 'FileBrowser_list',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_FileBrowser_list;
                    }

                    return $ret;
                }
                not_FileBrowser_list:

                // FileBrowser_upload
                if ('/admin/file/upload' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::uploadAction',  '_route' => 'FileBrowser_upload',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_FileBrowser_upload;
                    }

                    return $ret;
                }
                not_FileBrowser_upload:

                // FileBrowser_rename
                if ('/admin/file/rename' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::renameAction',  '_route' => 'FileBrowser_rename',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_FileBrowser_rename;
                    }

                    return $ret;
                }
                not_FileBrowser_rename:

                if (0 === strpos($pathinfo, '/admin/file/co')) {
                    // FileBrowser_copy
                    if ('/admin/file/copy' === $pathinfo) {
                        $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::copyAction',  '_route' => 'FileBrowser_copy',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_FileBrowser_copy;
                        }

                        return $ret;
                    }
                    not_FileBrowser_copy:

                    // FileBrowser_content
                    if ('/admin/file/content' === $pathinfo) {
                        $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::getContentAction',  '_route' => 'FileBrowser_content',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_FileBrowser_content;
                        }

                        return $ret;
                    }
                    not_FileBrowser_content:

                    // FileBrowser_compress
                    if ('/admin/file/compress' === $pathinfo) {
                        $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::compressAction',  '_route' => 'FileBrowser_compress',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_FileBrowser_compress;
                        }

                        return $ret;
                    }
                    not_FileBrowser_compress:

                }

                // FileBrowser_create_directory
                if ('/admin/file/create_directory' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::createDirectoryAction',  '_route' => 'FileBrowser_create_directory',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_FileBrowser_create_directory;
                    }

                    return $ret;
                }
                not_FileBrowser_create_directory:

                // FileBrowser_move
                if ('/admin/file/move' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::moveAction',  '_route' => 'FileBrowser_move',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_FileBrowser_move;
                    }

                    return $ret;
                }
                not_FileBrowser_move:

                // FileBrowser_delete
                if ('/admin/file/delete' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::deleteAction',  '_route' => 'FileBrowser_delete',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_FileBrowser_delete;
                    }

                    return $ret;
                }
                not_FileBrowser_delete:

                if (0 === strpos($pathinfo, '/admin/file/download')) {
                    // FileBrowser_download
                    if ('/admin/file/download' === $pathinfo) {
                        $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::downloadAction',  '_route' => 'FileBrowser_download',);
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_FileBrowser_download;
                        }

                        return $ret;
                    }
                    not_FileBrowser_download:

                    // FileBrowser_download_multiple
                    if ('/admin/file/download_multiple' === $pathinfo) {
                        $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::downloadMultipleAction',  '_route' => 'FileBrowser_download_multiple',);
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_FileBrowser_download_multiple;
                        }

                        return $ret;
                    }
                    not_FileBrowser_download_multiple:

                }

                // FileBrowser_edit
                if ('/admin/file/edit' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::editAction',  '_route' => 'FileBrowser_edit',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_FileBrowser_edit;
                    }

                    return $ret;
                }
                not_FileBrowser_edit:

                // FileBrowser_extract
                if ('/admin/file/extract' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::extractAction',  '_route' => 'FileBrowser_extract',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_FileBrowser_extract;
                    }

                    return $ret;
                }
                not_FileBrowser_extract:

                // FileBrowser_permissions
                if ('/admin/file/permissions' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::permissionsAction',  '_route' => 'FileBrowser_permissions',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_FileBrowser_permissions;
                    }

                    return $ret;
                }
                not_FileBrowser_permissions:

                // FileBrowser_browser
                if ('/admin/file/browser' === $pathinfo) {
                    return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\FileBrowserController::fileBrowserAction',  '_route' => 'FileBrowser_browser',);
                }

            }

            // index
            if ('/admin' === $pathinfo) {
                return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\PanelController::indexAction',  '_route' => 'index',);
            }

            // breadcrumbs_template
            if ('/admin/breadcrumbs' === $pathinfo) {
                return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\PanelController::breadcrumbsAction',  '_route' => 'breadcrumbs_template',);
            }

            if (0 === strpos($pathinfo, '/admin/lo')) {
                // locale_change
                if (0 === strpos($pathinfo, '/admin/locale') && preg_match('#^/admin/locale/(?P<locale>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'locale_change']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\PanelController::changeLocaleAction',));
                }

                // login_check
                if ('/admin/login_check' === $pathinfo) {
                    return ['_route' => 'login_check'];
                }

                // logout
                if ('/admin/logout' === $pathinfo) {
                    return ['_route' => 'logout'];
                }

            }

            // disable_mfa
            if ('/admin/disable_mfa' === $pathinfo) {
                return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\PanelController::disableMFA',  '_route' => 'disable_mfa',);
            }

            // enable_mfa
            if ('/admin/enable_mfa' === $pathinfo) {
                return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\PanelController::enableMFA',  '_route' => 'enable_mfa',);
            }

            if (0 === strpos($pathinfo, '/admin/Test')) {
                // Test_object
                if (0 === strpos($pathinfo, '/admin/Test/fetch') && preg_match('#^/admin/Test/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::objectAction',));
                }

                // Test_collection
                if (0 === strpos($pathinfo, '/admin/Test/collection') && preg_match('#^/admin/Test/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::collectionAction',));
                }

                // Test_toggleLock
                if (preg_match('#^/admin/Test/(?P<object_id>[^/]++)/toggleLock$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_toggleLock']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::toggleLock',));
                }

                // Test_form
                if (0 === strpos($pathinfo, '/admin/Test/form') && preg_match('#^/admin/Test/form(?:/(?P<action>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_form']), array (  'action' => 'edit',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::formAction',));
                }

                // Test_save
                if (preg_match('#^/admin/Test/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::saveAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_Test_save;
                    }

                    return $ret;
                }
                not_Test_save:

                // Test_copy
                if (preg_match('#^/admin/Test/(?P<object_id>[^/]++)/copy$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_copy']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::copyAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_Test_copy;
                    }

                    return $ret;
                }
                not_Test_copy:

                // Test_delete
                if (preg_match('#^/admin/Test/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::deleteAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_Test_delete;
                    }

                    return $ret;
                }
                not_Test_delete:

                // Test_export
                if (preg_match('#^/admin/Test/(?P<instructions>[^/]++)/export(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_export']), array (  'format' => 'yml',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::exportAction',));
                }

                // Test_export_instructions
                if (preg_match('#^/admin/Test/(?P<object_ids>[^/]++)/instructions/export$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_export_instructions']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::exportInstructionsAction',));
                }

                if (0 === strpos($pathinfo, '/admin/Test/import')) {
                    // Test_import
                    if ('/admin/Test/import' === $pathinfo) {
                        $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::scheduleImportAction',  '_route' => 'Test_import',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_Test_import;
                        }

                        return $ret;
                    }
                    not_Test_import:

                    // Test_pre_import_status
                    if ('/admin/Test/import/status' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::preImportStatusAction',  '_route' => 'Test_pre_import_status',);
                    }

                }

                // Test_add_node
                if (preg_match('#^/admin/Test/(?P<object_id>[^/]++)/node/add$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_add_node']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::addNodeAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_Test_add_node;
                    }

                    return $ret;
                }
                not_Test_add_node:

                // Test_add_connection
                if (preg_match('#^/admin/Test/(?P<object_id>[^/]++)/connection/add$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_add_connection']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::addNodeConnectionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_Test_add_connection;
                    }

                    return $ret;
                }
                not_Test_add_connection:

                // Test_move_node
                if ('/admin/Test/node/move' === $pathinfo) {
                    $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::moveNodeAction',  '_route' => 'Test_move_node',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_Test_move_node;
                    }

                    return $ret;
                }
                not_Test_move_node:

                // Test_paste_nodes
                if (preg_match('#^/admin/Test/(?P<object_id>[^/]++)/node/paste$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'Test_paste_nodes']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestController::pasteNodesAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_Test_paste_nodes;
                    }

                    return $ret;
                }
                not_Test_paste_nodes:

                if (0 === strpos($pathinfo, '/admin/TestNode')) {
                    if (0 === strpos($pathinfo, '/admin/TestNodeConnection')) {
                        // TestNodeConnection_object
                        if (0 === strpos($pathinfo, '/admin/TestNodeConnection/fetch') && preg_match('#^/admin/TestNodeConnection/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodeConnection_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeConnectionController::objectAction',));
                        }

                        // TestNodeConnection_collection_by_flow_test
                        if (0 === strpos($pathinfo, '/admin/TestNodeConnection/flow') && preg_match('#^/admin/TestNodeConnection/flow/(?P<test_id>[^/]++)/collection$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodeConnection_collection_by_flow_test']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeConnectionController::collectionByFlowTestAction',));
                        }

                        // TestNodeConnection_collection
                        if (0 === strpos($pathinfo, '/admin/TestNodeConnection/collection') && preg_match('#^/admin/TestNodeConnection/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodeConnection_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeConnectionController::collectionAction',));
                        }

                        // TestNodeConnection_delete
                        if (preg_match('#^/admin/TestNodeConnection/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodeConnection_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeConnectionController::deleteAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestNodeConnection_delete;
                            }

                            return $ret;
                        }
                        not_TestNodeConnection_delete:

                        // TestNodeConnection_save
                        if (preg_match('#^/admin/TestNodeConnection/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodeConnection_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeConnectionController::saveAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestNodeConnection_save;
                            }

                            return $ret;
                        }
                        not_TestNodeConnection_save:

                    }

                    // TestNode_object
                    if (0 === strpos($pathinfo, '/admin/TestNode/fetch') && preg_match('#^/admin/TestNode/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNode_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeController::objectAction',));
                    }

                    // TestNode_collection_by_flow_test
                    if (0 === strpos($pathinfo, '/admin/TestNode/flow') && preg_match('#^/admin/TestNode/flow/(?P<test_id>[^/]++)/collection$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNode_collection_by_flow_test']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeController::collectionByFlowTestAction',));
                    }

                    // TestNode_collection
                    if (0 === strpos($pathinfo, '/admin/TestNode/collection') && preg_match('#^/admin/TestNode/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNode_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeController::collectionAction',));
                    }

                    // TestNode_delete
                    if (preg_match('#^/admin/TestNode/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNode_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeController::deleteAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestNode_delete;
                        }

                        return $ret;
                    }
                    not_TestNode_delete:

                    // TestNode_save
                    if (preg_match('#^/admin/TestNode/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNode_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeController::saveAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestNode_save;
                        }

                        return $ret;
                    }
                    not_TestNode_save:

                    // TestNode_expose_ports
                    if (preg_match('#^/admin/TestNode/(?P<object_id>[^/]++)/ports/expose$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNode_expose_ports']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeController::exposePorts',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestNode_expose_ports;
                        }

                        return $ret;
                    }
                    not_TestNode_expose_ports:

                    // TestNode_add_dynamic_port
                    if (preg_match('#^/admin/TestNode/(?P<object_id>[^/]++)/port/(?P<type>[^/]++)/add$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNode_add_dynamic_port']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodeController::addDynamicPort',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestNode_add_dynamic_port;
                        }

                        return $ret;
                    }
                    not_TestNode_add_dynamic_port:

                    if (0 === strpos($pathinfo, '/admin/TestNodePort')) {
                        // TestNodePort_object
                        if (0 === strpos($pathinfo, '/admin/TestNodePort/fetch') && preg_match('#^/admin/TestNodePort/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodePort_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodePortController::objectAction',));
                        }

                        // TestNodePort_collection
                        if (0 === strpos($pathinfo, '/admin/TestNodePort/collection') && preg_match('#^/admin/TestNodePort/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodePort_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodePortController::collectionAction',));
                        }

                        // TestNodePort_delete
                        if (preg_match('#^/admin/TestNodePort/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodePort_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodePortController::deleteAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestNodePort_delete;
                            }

                            return $ret;
                        }
                        not_TestNodePort_delete:

                        // TestNodePort_save
                        if (preg_match('#^/admin/TestNodePort/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodePort_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodePortController::saveAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestNodePort_save;
                            }

                            return $ret;
                        }
                        not_TestNodePort_save:

                        // TestNodePort_save_collection
                        if ('/admin/TestNodePort/save' === $pathinfo) {
                            $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodePortController::saveCollectionAction',  '_route' => 'TestNodePort_save_collection',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestNodePort_save_collection;
                            }

                            return $ret;
                        }
                        not_TestNodePort_save_collection:

                        // TestNodePort_hide
                        if (preg_match('#^/admin/TestNodePort/(?P<object_id>[^/]++)/hide$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestNodePort_hide']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestNodePortController::hideAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestNodePort_hide;
                            }

                            return $ret;
                        }
                        not_TestNodePort_hide:

                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/TestSessionLog')) {
                    // TestSessionLog_collection
                    if ('/admin/TestSessionLog/collection' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestSessionLogController::collectionAction',  '_route' => 'TestSessionLog_collection',);
                    }

                    // TestSessionLog_collection_by_test
                    if (0 === strpos($pathinfo, '/admin/TestSessionLog/Test') && preg_match('#^/admin/TestSessionLog/Test/(?P<test_id>[^/]++)/collection$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestSessionLog_collection_by_test']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestSessionLogController::collectionByTestAction',));
                    }

                    // TestSessionLog_delete
                    if (preg_match('#^/admin/TestSessionLog/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestSessionLog_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestSessionLogController::deleteAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestSessionLog_delete;
                        }

                        return $ret;
                    }
                    not_TestSessionLog_delete:

                    // TestSessionLog_clear
                    if (0 === strpos($pathinfo, '/admin/TestSessionLog/Test') && preg_match('#^/admin/TestSessionLog/Test/(?P<test_id>[^/]++)/clear$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestSessionLog_clear']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestSessionLogController::clearAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestSessionLog_clear;
                        }

                        return $ret;
                    }
                    not_TestSessionLog_clear:

                }

                elseif (0 === strpos($pathinfo, '/admin/TestVariable')) {
                    // TestVariable_object
                    if (0 === strpos($pathinfo, '/admin/TestVariable/fetch') && preg_match('#^/admin/TestVariable/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestVariable_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestVariableController::objectAction',));
                    }

                    if (0 === strpos($pathinfo, '/admin/TestVariable/Test')) {
                        // TestVariable_by_test_collection
                        if (preg_match('#^/admin/TestVariable/Test/(?P<test_id>[^/]++)/collection$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestVariable_by_test_collection']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestVariableController::collectionByTestAction',));
                        }

                        // TestVariable_parameters_collection
                        if (preg_match('#^/admin/TestVariable/Test/(?P<test_id>[^/]++)/parameters/collection$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestVariable_parameters_collection']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestVariableController::parametersCollectionAction',));
                        }

                        // TestVariable_returns_collection
                        if (preg_match('#^/admin/TestVariable/Test/(?P<test_id>[^/]++)/returns/collection$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestVariable_returns_collection']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestVariableController::returnsCollectionAction',));
                        }

                        // TestVariable_branches_collection
                        if (preg_match('#^/admin/TestVariable/Test/(?P<test_id>[^/]++)/branches/collection$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestVariable_branches_collection']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestVariableController::branchesCollectionAction',));
                        }

                    }

                    // TestVariable_save
                    if (preg_match('#^/admin/TestVariable/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestVariable_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestVariableController::saveAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestVariable_save;
                        }

                        return $ret;
                    }
                    not_TestVariable_save:

                    // TestVariable_delete
                    if (preg_match('#^/admin/TestVariable/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestVariable_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestVariableController::deleteAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestVariable_delete;
                        }

                        return $ret;
                    }
                    not_TestVariable_delete:

                }

                elseif (0 === strpos($pathinfo, '/admin/TestWizard')) {
                    // TestWizard_object
                    if (0 === strpos($pathinfo, '/admin/TestWizard/fetch') && preg_match('#^/admin/TestWizard/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizard_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::objectAction',));
                    }

                    // TestWizard_collection
                    if (0 === strpos($pathinfo, '/admin/TestWizard/collection') && preg_match('#^/admin/TestWizard/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizard_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::collectionAction',));
                    }

                    // TestWizard_toggleLock
                    if (preg_match('#^/admin/TestWizard/(?P<object_id>[^/]++)/toggleLock$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizard_toggleLock']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::toggleLock',));
                    }

                    // TestWizard_form
                    if (0 === strpos($pathinfo, '/admin/TestWizard/form') && preg_match('#^/admin/TestWizard/form(?:/(?P<action>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizard_form']), array (  'action' => 'edit',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::formAction',));
                    }

                    // TestWizard_save
                    if (preg_match('#^/admin/TestWizard/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizard_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::saveAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestWizard_save;
                        }

                        return $ret;
                    }
                    not_TestWizard_save:

                    // TestWizard_copy
                    if (preg_match('#^/admin/TestWizard/(?P<object_id>[^/]++)/copy$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizard_copy']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::copyAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestWizard_copy;
                        }

                        return $ret;
                    }
                    not_TestWizard_copy:

                    // TestWizard_delete
                    if (preg_match('#^/admin/TestWizard/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizard_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::deleteAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_TestWizard_delete;
                        }

                        return $ret;
                    }
                    not_TestWizard_delete:

                    // TestWizard_export
                    if (preg_match('#^/admin/TestWizard/(?P<instructions>[^/]++)/export(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizard_export']), array (  'format' => 'yml',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::exportAction',));
                    }

                    // TestWizard_export_instructions
                    if (preg_match('#^/admin/TestWizard/(?P<object_ids>[^/]++)/instructions/export$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizard_export_instructions']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::exportInstructionsAction',));
                    }

                    if (0 === strpos($pathinfo, '/admin/TestWizard/import')) {
                        // TestWizard_import
                        if ('/admin/TestWizard/import' === $pathinfo) {
                            $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::scheduleImportAction',  '_route' => 'TestWizard_import',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestWizard_import;
                            }

                            return $ret;
                        }
                        not_TestWizard_import:

                        // TestWizard_pre_import_status
                        if ('/admin/TestWizard/import/status' === $pathinfo) {
                            return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardController::preImportStatusAction',  '_route' => 'TestWizard_pre_import_status',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/TestWizardParam')) {
                        // TestWizardParam_object
                        if (0 === strpos($pathinfo, '/admin/TestWizardParam/fetch') && preg_match('#^/admin/TestWizardParam/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardParam_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardParamController::objectAction',));
                        }

                        // TestWizardParam_collection
                        if (0 === strpos($pathinfo, '/admin/TestWizardParam/collection') && preg_match('#^/admin/TestWizardParam/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardParam_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardParamController::collectionAction',));
                        }

                        if (0 === strpos($pathinfo, '/admin/TestWizardParam/TestWizard')) {
                            // TestWizardParam_collection_by_wizard
                            if (preg_match('#^/admin/TestWizardParam/TestWizard/(?P<wizard_id>[^/]++)/collection$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardParam_collection_by_wizard']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardParamController::collectionByWizardAction',));
                            }

                            // TestWizardParam_collection_by_wizard_and_type
                            if (preg_match('#^/admin/TestWizardParam/TestWizard/(?P<wizard_id>[^/]++)/type/(?P<type>[^/]++)/collection$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardParam_collection_by_wizard_and_type']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardParamController::collectionByWizardAndTypeAction',));
                            }

                        }

                        // TestWizardParam_delete
                        if (preg_match('#^/admin/TestWizardParam/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardParam_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardParamController::deleteAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestWizardParam_delete;
                            }

                            return $ret;
                        }
                        not_TestWizardParam_delete:

                        // TestWizardParam_save
                        if (preg_match('#^/admin/TestWizardParam/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardParam_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardParamController::saveAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestWizardParam_save;
                            }

                            return $ret;
                        }
                        not_TestWizardParam_save:

                        // TestWizardParam_clear
                        if (0 === strpos($pathinfo, '/admin/TestWizardParam/TestWizard') && preg_match('#^/admin/TestWizardParam/TestWizard/(?P<wizard_id>[^/]++)/clear$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardParam_clear']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardParamController::clearAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestWizardParam_clear;
                            }

                            return $ret;
                        }
                        not_TestWizardParam_clear:

                    }

                    elseif (0 === strpos($pathinfo, '/admin/TestWizardStep')) {
                        // TestWizardStep_object
                        if (0 === strpos($pathinfo, '/admin/TestWizardStep/fetch') && preg_match('#^/admin/TestWizardStep/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardStep_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardStepController::objectAction',));
                        }

                        // TestWizardStep_collection_by_wizard
                        if (0 === strpos($pathinfo, '/admin/TestWizardStep/TestWizard') && preg_match('#^/admin/TestWizardStep/TestWizard/(?P<wizard_id>[^/]++)/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardStep_collection_by_wizard']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardStepController::collectionByWizardAction',));
                        }

                        // TestWizardStep_collection
                        if (0 === strpos($pathinfo, '/admin/TestWizardStep/collection') && preg_match('#^/admin/TestWizardStep/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardStep_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardStepController::collectionAction',));
                        }

                        // TestWizardStep_delete
                        if (preg_match('#^/admin/TestWizardStep/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardStep_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardStepController::deleteAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestWizardStep_delete;
                            }

                            return $ret;
                        }
                        not_TestWizardStep_delete:

                        // TestWizardStep_save
                        if (preg_match('#^/admin/TestWizardStep/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardStep_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardStepController::saveAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestWizardStep_save;
                            }

                            return $ret;
                        }
                        not_TestWizardStep_save:

                        // TestWizardStep_clear
                        if (0 === strpos($pathinfo, '/admin/TestWizardStep/TestWizard') && preg_match('#^/admin/TestWizardStep/TestWizard/(?P<wizard_id>[^/]++)/clear$#sD', $pathinfo, $matches)) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'TestWizardStep_clear']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\TestWizardStepController::clearAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_TestWizardStep_clear;
                            }

                            return $ret;
                        }
                        not_TestWizardStep_clear:

                    }

                }

            }

            elseif (0 === strpos($pathinfo, '/admin/User')) {
                // User_object
                if (0 === strpos($pathinfo, '/admin/User/fetch') && preg_match('#^/admin/User/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'User_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserController::objectAction',));
                }

                // User_form
                if (0 === strpos($pathinfo, '/admin/User/form') && preg_match('#^/admin/User/form(?:/(?P<action>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'User_form']), array (  'action' => 'edit',  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserController::formAction',));
                }

                // User_collection
                if (0 === strpos($pathinfo, '/admin/User/collection') && preg_match('#^/admin/User/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'User_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserController::collectionAction',));
                }

                // User_save
                if (preg_match('#^/admin/User/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'User_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserController::saveAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_User_save;
                    }

                    return $ret;
                }
                not_User_save:

                // User_delete
                if (preg_match('#^/admin/User/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'User_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserController::deleteAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_User_delete;
                    }

                    return $ret;
                }
                not_User_delete:

                if (0 === strpos($pathinfo, '/admin/UserTest')) {
                    // User_test_object
                    if (0 === strpos($pathinfo, '/admin/UserTest/fetch') && preg_match('#^/admin/UserTest/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'User_test_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserTestController::objectAction',));
                    }

                    // User_test_form
                    if (0 === strpos($pathinfo, '/admin/UserTest/form') && preg_match('#^/admin/UserTest/form(?:/(?P<action>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'User_test_form']), array (  'action' => 'edit',  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserTestController::formAction',));
                    }

                    // User_test_collection
                    if (0 === strpos($pathinfo, '/admin/UserTest/collection') && preg_match('#^/admin/UserTest/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'User_test_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserTestController::collectionAction',));
                    }

                    // User_test_save
                    if (preg_match('#^/admin/UserTest/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'User_test_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserTestController::saveAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_User_test_save;
                        }

                        return $ret;
                    }
                    not_User_test_save:

                    // User_test_delete
                    if (preg_match('#^/admin/UserTest/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'User_test_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\UserTestController::deleteAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_User_test_delete;
                        }

                        return $ret;
                    }
                    not_User_test_delete:

                }

            }

            elseif (0 === strpos($pathinfo, '/admin/ViewTemplate')) {
                // ViewTemplate_object
                if (0 === strpos($pathinfo, '/admin/ViewTemplate/fetch') && preg_match('#^/admin/ViewTemplate/fetch/(?P<object_id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_object']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::objectAction',));
                }

                // ViewTemplate_collection
                if (0 === strpos($pathinfo, '/admin/ViewTemplate/collection') && preg_match('#^/admin/ViewTemplate/collection(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_collection']), array (  'format' => 'json',  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::collectionAction',));
                }

                // ViewTemplate_toggleLock
                if (preg_match('#^/admin/ViewTemplate/(?P<object_id>[^/]++)/toggleLock$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_toggleLock']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::toggleLock',));
                }

                // ViewTemplate_form
                if (0 === strpos($pathinfo, '/admin/ViewTemplate/form') && preg_match('#^/admin/ViewTemplate/form(?:/(?P<action>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_form']), array (  'action' => 'edit',  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::formAction',));
                }

                // ViewTemplate_save
                if (preg_match('#^/admin/ViewTemplate/(?P<object_id>[^/]++)/save$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_save']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::saveAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_ViewTemplate_save;
                    }

                    return $ret;
                }
                not_ViewTemplate_save:

                // ViewTemplate_copy
                if (preg_match('#^/admin/ViewTemplate/(?P<object_id>[^/]++)/copy$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_copy']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::copyAction',));
                }

                // ViewTemplate_delete
                if (preg_match('#^/admin/ViewTemplate/(?P<object_ids>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_delete']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::deleteAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_ViewTemplate_delete;
                    }

                    return $ret;
                }
                not_ViewTemplate_delete:

                // ViewTemplate_export
                if (preg_match('#^/admin/ViewTemplate/(?P<instructions>[^/]++)/export(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_export']), array (  'format' => 'yml',  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::exportAction',));
                }

                // ViewTemplate_export_instructions
                if (preg_match('#^/admin/ViewTemplate/(?P<object_ids>[^/]++)/instructions/export$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_export_instructions']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::exportInstructionsAction',));
                }

                if (0 === strpos($pathinfo, '/admin/ViewTemplate/import')) {
                    // ViewTemplate_import
                    if ('/admin/ViewTemplate/import' === $pathinfo) {
                        $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::scheduleImportAction',  '_route' => 'ViewTemplate_import',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_ViewTemplate_import;
                        }

                        return $ret;
                    }
                    not_ViewTemplate_import:

                    // ViewTemplate_pre_import_status
                    if ('/admin/ViewTemplate/import/status' === $pathinfo) {
                        return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::preImportStatusAction',  '_route' => 'ViewTemplate_pre_import_status',);
                    }

                }

            }

            elseif (0 === strpos($pathinfo, '/admin/test')) {
                // test_runner_test_resume_debug
                if (0 === strpos($pathinfo, '/admin/test/session') && preg_match('#^/admin/test/session/(?P<existing_session_hash>[^/]++)/debug$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_test_resume_debug']), array (  'debug' => true,  'protected' => true,  'existing_session_hash' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startTestAction',));
                }

                // test_runner_test_start_debug
                if (preg_match('#^/admin/test/(?P<test_slug>[^/]++)/debug(?:/(?P<params>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_test_start_debug']), array (  'params' => '{}',  'debug' => true,  'protected' => true,  'test_slug' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startTestAction',));
                }

                // test_runner_resume_debug
                if (preg_match('#^/admin/test/(?P<test_slug>[^/]++)/session/(?P<existing_session_hash>[^/]++)/debug(?:/(?P<params>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_resume_debug']), array (  'params' => '{}',  'debug' => true,  'protected' => true,  'test_slug' => NULL,  'existing_session_hash' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startTestAction',));
                }

                // test_runner_protected_test_resume
                if (0 === strpos($pathinfo, '/admin/test/session') && preg_match('#^/admin/test/session(?:/(?P<existing_session_hash>[^/]++))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_protected_test_resume']), array (  'protected' => true,  'existing_session_hash' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startTestAction',));
                }

                // test_runner_protected_test_start
                if (preg_match('#^/admin/test(?:/(?P<test_slug>[^/]++)(?:/(?P<params>[^/]++))?)?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_protected_test_start']), array (  'params' => '{}',  'protected' => true,  'test_slug' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startTestAction',));
                }

                // test_runner_test_protected_start_name
                if (0 === strpos($pathinfo, '/admin/test_n') && preg_match('#^/admin/test_n(?:/(?P<test_name>[^/]++)(?:/(?P<params>[^/]++))?)?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_test_protected_start_name']), array (  'params' => '{}',  'protected' => true,  'test_name' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startTestAction',));
                }

                // test_runner_session_start_debug
                if (preg_match('#^/admin/test/(?P<test_slug>[^/]++)/start_session/debug(?:/(?P<params>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_session_start_debug']), array (  'params' => '{}',  'debug' => true,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startNewSessionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_test_runner_session_start_debug;
                    }

                    return $ret;
                }
                not_test_runner_session_start_debug:

                // test_runner_protected_session_start
                if (preg_match('#^/admin/test/(?P<test_slug>[^/]++)/start_session(?:/(?P<params>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_protected_session_start']), array (  'params' => '{}',  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startNewSessionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_test_runner_protected_session_start;
                    }

                    return $ret;
                }
                not_test_runner_protected_session_start:

            }

        }

        // fos_oauth_server_token
        if ('/oauth/v2/token' === $pathinfo) {
            $ret = array (  '_controller' => 'fos_oauth_server.controller.token:tokenAction',  '_route' => 'fos_oauth_server_token',);
            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                $allow = array_merge($allow, ['GET', 'POST']);
                goto not_fos_oauth_server_token;
            }

            return $ret;
        }
        not_fos_oauth_server_token:

        // fos_oauth_server_authorize
        if ('/oauth/v2/auth' === $pathinfo) {
            $ret = array (  '_controller' => 'fos_oauth_server.controller.authorize:authorizeAction',  '_route' => 'fos_oauth_server_authorize',);
            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                $allow = array_merge($allow, ['GET', 'POST']);
                goto not_fos_oauth_server_authorize;
            }

            return $ret;
        }
        not_fos_oauth_server_authorize:

        // home
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\HomeController::indexAction',  '_route' => 'home',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_home;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'home'));
            }

            return $ret;
        }
        not_home:

        // login
        if ('/login' === $pathinfo) {
            return array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\PanelController::loginAction',  '_route' => 'login',);
        }

        if (0 === strpos($pathinfo, '/ViewTemplate')) {
            // ViewTemplate_content
            if (preg_match('#^/ViewTemplate/(?P<id>[^/]++)/content$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_content']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::contentAction',));
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_ViewTemplate_content;
                }

                return $ret;
            }
            not_ViewTemplate_content:

            // ViewTemplate_css
            if (preg_match('#^/ViewTemplate/(?P<id>[^/]++)/css$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_css']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::cssAction',));
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_ViewTemplate_css;
                }

                return $ret;
            }
            not_ViewTemplate_css:

            // ViewTemplate_js
            if (preg_match('#^/ViewTemplate/(?P<id>[^/]++)/js$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_js']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::jsAction',));
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_ViewTemplate_js;
                }

                return $ret;
            }
            not_ViewTemplate_js:

            // ViewTemplate_html
            if (preg_match('#^/ViewTemplate/(?P<id>[^/]++)/html$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ViewTemplate_html']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\ViewTemplateController::htmlAction',));
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_ViewTemplate_html;
                }

                return $ret;
            }
            not_ViewTemplate_html:

        }

        elseif (0 === strpos($pathinfo, '/test')) {
            // test_runner_test_resume
            if (0 === strpos($pathinfo, '/test/session') && preg_match('#^/test/session(?:/(?P<existing_session_hash>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_test_resume']), array (  'existing_session_hash' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startTestAction',));
            }

            // test_runner_test_start
            if (preg_match('#^/test(?:/(?P<test_slug>[^/]++)(?:/(?P<params>[^/]++))?)?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_test_start']), array (  'params' => '{}',  'test_slug' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startTestAction',));
            }

            // test_runner_test_start_name
            if (0 === strpos($pathinfo, '/test_n') && preg_match('#^/test_n(?:/(?P<test_name>[^/]++)(?:/(?P<params>[^/]++))?)?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_test_start_name']), array (  'params' => '{}',  'test_name' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startTestAction',));
            }

            // test_runner_session_start
            if (preg_match('#^/test/(?P<test_slug>[^/]++)/start_session(?:/(?P<params>[^/]++))?$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_session_start']), array (  'params' => '{}',  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startNewSessionAction',));
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_test_runner_session_start;
                }

                return $ret;
            }
            not_test_runner_session_start:

            // test_runner_session_start_name
            if (0 === strpos($pathinfo, '/test_n') && preg_match('#^/test_n/(?P<test_name>[^/]++)/start_session(?:/(?P<params>[^/]++))?$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_session_start_name']), array (  'params' => '{}',  'test_name' => NULL,  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::startNewSessionAction',));
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_test_runner_session_start_name;
                }

                return $ret;
            }
            not_test_runner_session_start_name:

            if (0 === strpos($pathinfo, '/test/session')) {
                // test_runner_session_resume
                if (preg_match('#^/test/session/(?P<session_hash>[^/]++)/resume$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_session_resume']), array (  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::resumeSessionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_test_runner_session_resume;
                    }

                    return $ret;
                }
                not_test_runner_session_resume:

                // test_runner_session_submit
                if (preg_match('#^/test/session/(?P<session_hash>[^/]++)/submit$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_session_submit']), array (  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::submitToSessionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_test_runner_session_submit;
                    }

                    return $ret;
                }
                not_test_runner_session_submit:

                // test_runner_worker
                if (preg_match('#^/test/session/(?P<session_hash>[^/]++)/worker$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_worker']), array (  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::backgroundWorkerAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_test_runner_worker;
                    }

                    return $ret;
                }
                not_test_runner_worker:

                // test_runner_session_kill
                if (preg_match('#^/test/session/(?P<session_hash>[^/]++)/kill$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_session_kill']), array (  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::killSessionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_test_runner_session_kill;
                    }

                    return $ret;
                }
                not_test_runner_session_kill:

                // test_runner_session_keepalive
                if (preg_match('#^/test/session/(?P<session_hash>[^/]++)/keepalive$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_session_keepalive']), array (  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::keepAliveSessionAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_test_runner_session_keepalive;
                    }

                    return $ret;
                }
                not_test_runner_session_keepalive:

                // test_runner_upload_file
                if (preg_match('#^/test/session/(?P<session_hash>[^/]++)/upload$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_upload_file']), array (  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::uploadFileAction',));
                    if (!in_array($requestMethod, ['POST', 'OPTIONS'])) {
                        $allow = array_merge($allow, ['POST', 'OPTIONS']);
                        goto not_test_runner_upload_file;
                    }

                    return $ret;
                }
                not_test_runner_upload_file:

                // test_runner_log_error
                if (preg_match('#^/test/session/(?P<session_hash>[^/]++)/log$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'test_runner_log_error']), array (  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::logErrorAction',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_test_runner_log_error;
                    }

                    return $ret;
                }
                not_test_runner_log_error:

            }

        }

        // files_protected
        if (0 === strpos($pathinfo, '/files/protected') && preg_match('#^/files/protected/(?P<name>.+)$#sD', $pathinfo, $matches)) {
            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'files_protected']), array (  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::getProtectedFile',));
            if (!in_array($canonicalMethod, ['GET'])) {
                $allow = array_merge($allow, ['GET']);
                goto not_files_protected;
            }

            return $ret;
        }
        not_files_protected:

        // files_session
        if (0 === strpos($pathinfo, '/files/session') && preg_match('#^/files/session/(?P<name>.+)$#sD', $pathinfo, $matches)) {
            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'files_session']), array (  '_controller' => 'Concerto\\TestBundle\\Controller\\TestRunnerController::getSessionFile',));
            if (!in_array($canonicalMethod, ['GET'])) {
                $allow = array_merge($allow, ['GET']);
                goto not_files_session;
            }

            return $ret;
        }
        not_files_session:

        // remove_trailing_slash
        if (preg_match('#^/(?P<url>.*/)$#sD', $pathinfo, $matches)) {
            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'remove_trailing_slash']), array (  '_controller' => 'Concerto\\PanelBundle\\Controller\\RedirectController::removeTrailingSlashAction',));
            if (!in_array($canonicalMethod, ['GET'])) {
                $allow = array_merge($allow, ['GET']);
                goto not_remove_trailing_slash;
            }

            return $ret;
        }
        not_remove_trailing_slash:

        if (0 === strpos($pathinfo, '/admin/2fa')) {
            // 2fa_login
            if ('/admin/2fa' === $pathinfo) {
                return array (  '_controller' => 'scheb_two_factor.form_controller:form',  '_route' => '2fa_login',);
            }

            // 2fa_login_check
            if ('/admin/2fa_check' === $pathinfo) {
                return ['_route' => '2fa_login_check'];
            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
