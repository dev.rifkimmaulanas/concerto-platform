<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'Concerto\PanelBundle\Service\TestWizardStepService' shared autowired service.

include_once $this->targetDirs[3].'\\src\\Concerto\\PanelBundle\\Service\\ASectionService.php';
include_once $this->targetDirs[3].'\\src\\Concerto\\PanelBundle\\Service\\TestWizardStepService.php';

return $this->services['Concerto\\PanelBundle\\Service\\TestWizardStepService'] = new \Concerto\PanelBundle\Service\TestWizardStepService(${($_ = isset($this->services['Concerto\\PanelBundle\\Repository\\TestWizardStepRepository']) ? $this->services['Concerto\\PanelBundle\\Repository\\TestWizardStepRepository'] : $this->load('getTestWizardStepRepositoryService.php')) && false ?: '_'}, ${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->getValidatorService()) && false ?: '_'}, ${($_ = isset($this->services['Concerto\\PanelBundle\\Repository\\TestWizardRepository']) ? $this->services['Concerto\\PanelBundle\\Repository\\TestWizardRepository'] : $this->load('getTestWizardRepositoryService.php')) && false ?: '_'}, ${($_ = isset($this->services['security.authorization_checker']) ? $this->services['security.authorization_checker'] : $this->getSecurity_AuthorizationCheckerService()) && false ?: '_'}, ${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : ($this->services['security.token_storage'] = new \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage())) && false ?: '_'}, ${($_ = isset($this->services['Concerto\\PanelBundle\\Service\\AdministrationService']) ? $this->services['Concerto\\PanelBundle\\Service\\AdministrationService'] : $this->load('getAdministrationServiceService.php')) && false ?: '_'}, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : $this->getLoggerService()) && false ?: '_'});
