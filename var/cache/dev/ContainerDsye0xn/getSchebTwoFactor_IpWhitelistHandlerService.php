<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'scheb_two_factor.ip_whitelist_handler' shared service.

include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Handler\\AuthenticationHandlerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Handler\\IpWhitelistHandler.php';
include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\IpWhitelist\\IpWhitelistProviderInterface.php';
include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\IpWhitelist\\DefaultIpWhitelistProvider.php';

return $this->services['scheb_two_factor.ip_whitelist_handler'] = new \Scheb\TwoFactorBundle\Security\TwoFactor\Handler\IpWhitelistHandler(${($_ = isset($this->services['scheb_two_factor.trusted_device_handler']) ? $this->services['scheb_two_factor.trusted_device_handler'] : $this->load('getSchebTwoFactor_TrustedDeviceHandlerService.php')) && false ?: '_'}, ${($_ = isset($this->services['scheb_two_factor.default_ip_whitelist_provider']) ? $this->services['scheb_two_factor.default_ip_whitelist_provider'] : ($this->services['scheb_two_factor.default_ip_whitelist_provider'] = new \Scheb\TwoFactorBundle\Security\TwoFactor\IpWhitelist\DefaultIpWhitelistProvider([]))) && false ?: '_'});
