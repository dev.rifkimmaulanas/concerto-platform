<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'scheb_two_factor.security.google_authenticator' shared service.

include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Provider\\Google\\GoogleAuthenticatorInterface.php';
include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Provider\\Google\\GoogleAuthenticator.php';
include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Provider\\Google\\GoogleTotpFactory.php';

return $this->services['scheb_two_factor.security.google_authenticator'] = new \Scheb\TwoFactorBundle\Security\TwoFactor\Provider\Google\GoogleAuthenticator(${($_ = isset($this->services['scheb_two_factor.security.google_totp_factory']) ? $this->services['scheb_two_factor.security.google_totp_factory'] : ($this->services['scheb_two_factor.security.google_totp_factory'] = new \Scheb\TwoFactorBundle\Security\TwoFactor\Provider\Google\GoogleTotpFactory(NULL, NULL, 6))) && false ?: '_'}, 1);
