<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'Concerto\PanelBundle\Repository\TestSessionRepository' shared autowired service.

include_once $this->targetDirs[3].'\\vendor\\doctrine\\persistence\\lib\\Doctrine\\Persistence\\ObjectRepository.php';
include_once $this->targetDirs[3].'\\vendor\\doctrine\\collections\\lib\\Doctrine\\Common\\Collections\\Selectable.php';
include_once $this->targetDirs[3].'\\vendor\\doctrine\\orm\\lib\\Doctrine\\ORM\\EntityRepository.php';
include_once $this->targetDirs[3].'\\src\\Concerto\\PanelBundle\\Repository\\AEntityRepository.php';
include_once $this->targetDirs[3].'\\src\\Concerto\\PanelBundle\\Repository\\TestSessionRepository.php';

return $this->services['Concerto\\PanelBundle\\Repository\\TestSessionRepository'] = ${($_ = isset($this->services['doctrine.orm.default_entity_manager']) ? $this->services['doctrine.orm.default_entity_manager'] : $this->load('getDoctrine_Orm_DefaultEntityManagerService.php')) && false ?: '_'}->getRepository('Concerto\\PanelBundle\\Entity\\TestSession');
