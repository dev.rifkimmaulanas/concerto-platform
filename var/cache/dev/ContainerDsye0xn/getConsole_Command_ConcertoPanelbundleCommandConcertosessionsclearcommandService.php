<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'console.command.concerto_panelbundle_command_concertosessionsclearcommand' shared autowired service.

include_once $this->targetDirs[3].'\\vendor\\symfony\\symfony\\src\\Symfony\\Component\\Console\\Command\\Command.php';
include_once $this->targetDirs[3].'\\src\\Concerto\\PanelBundle\\Command\\ConcertoSessionsClearCommand.php';
include_once $this->targetDirs[3].'\\src\\Concerto\\PanelBundle\\Service\\MaintenanceService.php';

return $this->services['console.command.concerto_panelbundle_command_concertosessionsclearcommand'] = new \Concerto\PanelBundle\Command\ConcertoSessionsClearCommand(new \Concerto\PanelBundle\Service\MaintenanceService($this->getParameter('administration')));
