<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'security.authentication.csrf_token_validator.two_factor.admin_area' shared service.

include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Csrf\\CsrfTokenValidator.php';
include_once $this->targetDirs[3].'\\vendor\\symfony\\symfony\\src\\Symfony\\Component\\Security\\Csrf\\CsrfTokenManagerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Csrf\\NullCsrfTokenManager.php';

return $this->services['security.authentication.csrf_token_validator.two_factor.admin_area'] = new \Scheb\TwoFactorBundle\Security\TwoFactor\Csrf\CsrfTokenValidator(${($_ = isset($this->services['scheb_two_factor.null_csrf_token_manager']) ? $this->services['scheb_two_factor.null_csrf_token_manager'] : ($this->services['scheb_two_factor.null_csrf_token_manager'] = new \Scheb\TwoFactorBundle\Security\TwoFactor\Csrf\NullCsrfTokenManager())) && false ?: '_'}, ['auth_form_path' => '2fa_login', 'check_path' => '2fa_login_check', 'post_only' => false, 'always_use_default_target_path' => false, 'default_target_path' => '/', 'success_handler' => NULL, 'failure_handler' => NULL, 'authentication_required_handler' => NULL, 'auth_code_parameter_name' => '_auth_code', 'trusted_parameter_name' => '_trusted', 'multi_factor' => false, 'prepare_on_login' => false, 'prepare_on_access_denied' => false, 'csrf_token_generator' => NULL, 'csrf_parameter' => '_csrf_token', 'csrf_token_id' => 'two_factor', 'provider' => NULL]);
