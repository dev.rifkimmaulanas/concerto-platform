<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'scheb_two_factor.security.google.provider' shared service.

include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Provider\\TwoFactorProviderInterface.php';
include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Provider\\Google\\GoogleAuthenticatorTwoFactorProvider.php';

return $this->services['scheb_two_factor.security.google.provider'] = new \Scheb\TwoFactorBundle\Security\TwoFactor\Provider\Google\GoogleAuthenticatorTwoFactorProvider(${($_ = isset($this->services['scheb_two_factor.security.google_authenticator']) ? $this->services['scheb_two_factor.security.google_authenticator'] : $this->load('getSchebTwoFactor_Security_GoogleAuthenticatorService.php')) && false ?: '_'}, ${($_ = isset($this->services['scheb_two_factor.security.google.form_renderer']) ? $this->services['scheb_two_factor.security.google.form_renderer'] : $this->load('getSchebTwoFactor_Security_Google_FormRendererService.php')) && false ?: '_'});
