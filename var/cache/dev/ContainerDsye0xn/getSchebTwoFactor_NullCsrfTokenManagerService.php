<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'scheb_two_factor.null_csrf_token_manager' shared service.

include_once $this->targetDirs[3].'\\vendor\\symfony\\symfony\\src\\Symfony\\Component\\Security\\Csrf\\CsrfTokenManagerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\scheb\\two-factor-bundle\\Security\\TwoFactor\\Csrf\\NullCsrfTokenManager.php';

return $this->services['scheb_two_factor.null_csrf_token_manager'] = new \Scheb\TwoFactorBundle\Security\TwoFactor\Csrf\NullCsrfTokenManager();
