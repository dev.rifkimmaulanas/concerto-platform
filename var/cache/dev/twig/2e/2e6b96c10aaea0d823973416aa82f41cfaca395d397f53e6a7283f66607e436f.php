<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle::list.html.twig */
class __TwigTemplate_4bc8ae5b21889c08a3bbba103344cc9b418d52e4deab7105708de0c1c690921f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::list.html.twig"));

        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle::list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "    <div class=\"center\">
        <button ng-click=\"fetchAllCollections();\" class=\"btn btn-default btn-sm btn-list-refresh\">";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.refresh", [], "panel"), "html", null, true);
        echo "</button>
        <button ng-click=\"add();\" class=\"btn btn-success btn-sm\">";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.add", [], "panel"), "html", null, true);
        echo "</button>
        <button ng-click=\"import();\" class=\"btn btn-success btn-sm\" ng-if=\"exportable\">";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.import", [], "panel"), "html", null, true);
        echo "</button>
        <button ng-click=\"exportSelected();\" class=\"btn btn-default btn-sm\" ng-if=\"exportable\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.export.checked", [], "panel"), "html", null, true);
        echo "</button>
        <button ng-click=\"gridService.downloadList(collectionGridApi);\" class=\"btn btn-default btn-sm\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.download", [], "panel"), "html", null, true);
        echo "</button>
        <button ng-click=\"deleteSelected();\" class=\"btn btn-danger btn-sm\" ng-disabled=\"collectionFilter.starterContent && !administrationSettingsService.starterContentEditable\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.delete.checked", [], "panel"), "html", null, true);
        echo "</button><br/>
    </div>
    <div class=\"center\" style=\"margin-top: 16px;\">
        <div class=\"btn-group\">
            <label class=\"btn btn-default btn-sm\" ng-model=\"collectionFilter.starterContent\" uib-btn-radio=\"false\" ng-click=\"refreshGrid()\">";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.filter.user_made", [], "panel"), "html", null, true);
        echo "</label>
            <label class=\"btn btn-default btn-sm\" ng-model=\"collectionFilter.starterContent\" uib-btn-radio=\"true\" ng-click=\"refreshGrid()\">";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.filter.starter_content", [], "panel"), "html", null, true);
        echo "</label>
        </div>
    </div>
    <div ui-grid=\"collectionOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid collectionTable\"></div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle::list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 16,  95 => 15,  88 => 11,  84 => 10,  80 => 9,  76 => 8,  72 => 7,  68 => 6,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"panel\" %}

{% block content %}
    <div class=\"center\">
        <button ng-click=\"fetchAllCollections();\" class=\"btn btn-default btn-sm btn-list-refresh\">{{ \"list.button.refresh\"|trans }}</button>
        <button ng-click=\"add();\" class=\"btn btn-success btn-sm\">{{ \"list.button.add\"|trans }}</button>
        <button ng-click=\"import();\" class=\"btn btn-success btn-sm\" ng-if=\"exportable\">{{ \"list.button.import\"|trans }}</button>
        <button ng-click=\"exportSelected();\" class=\"btn btn-default btn-sm\" ng-if=\"exportable\">{{ \"list.button.export.checked\"|trans }}</button>
        <button ng-click=\"gridService.downloadList(collectionGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans }}</button>
        <button ng-click=\"deleteSelected();\" class=\"btn btn-danger btn-sm\" ng-disabled=\"collectionFilter.starterContent && !administrationSettingsService.starterContentEditable\">{{ \"list.button.delete.checked\"|trans }}</button><br/>
    </div>
    <div class=\"center\" style=\"margin-top: 16px;\">
        <div class=\"btn-group\">
            <label class=\"btn btn-default btn-sm\" ng-model=\"collectionFilter.starterContent\" uib-btn-radio=\"false\" ng-click=\"refreshGrid()\">{{ \"list.filter.user_made\"|trans }}</label>
            <label class=\"btn btn-default btn-sm\" ng-model=\"collectionFilter.starterContent\" uib-btn-radio=\"true\" ng-click=\"refreshGrid()\">{{ \"list.filter.starter_content\"|trans }}</label>
        </div>
    </div>
    <div ui-grid=\"collectionOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid collectionTable\"></div>
{% endblock %}
", "ConcertoPanelBundle::list.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/list.html.twig");
    }
}
