<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle::header.html.twig */
class __TwigTemplate_c28b5f372d7168b87ed03fcc18372c54f2285c73e7613b6c010886b2b05225bc extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::header.html.twig"));

        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle::header.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "    ";
        $context["googleAuthenticatorEnabled"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 5, $this->source); })()), "user", [], "any", false, false, false, 5), "isGoogleAuthenticatorEnabled", [], "any", false, false, false, 5)) ? ("true") : ("false"));
        // line 6
        echo "
    <table ng-controller=\"HeaderController\" ng-init=\"mfaEnabled = ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["googleAuthenticatorEnabled"]) || array_key_exists("googleAuthenticatorEnabled", $context) ? $context["googleAuthenticatorEnabled"] : (function () { throw new RuntimeError('Variable "googleAuthenticatorEnabled" does not exist.', 7, $this->source); })()), "html", null, true);
        echo ";\">
        <tr>
            <td><img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/concertopanel/images/symbol.png"), "html", null, true);
        echo "\"/></td>
            <td style=\"font-weight: bold;\">v";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 10, $this->source); })()), "html", null, true);
        echo "</td>
            <td style=\"width:100%; text-align: right; padding-right: 15px;\">
                ";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header.logged.user", ["{0}" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 12, $this->source); })()), "user", [], "any", false, false, false, 12), "username", [], "any", false, false, false, 12)], "panel"), "html", null, true);
        echo "
            </td>
            <td>
                <button ng-click=\"disableMFA()\" class=\"btn btn-default\" ng-show=\"mfaEnabled\"><i class=\"glyphicon glyphicon-qrcode\"></i> ";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header.disableMFA", [], "panel"), "html", null, true);
        echo "</button>
                <button ng-click=\"enableMFA()\" class=\"btn btn-default\" ng-show=\"!mfaEnabled\"><i class=\"glyphicon glyphicon-qrcode\"></i> ";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header.enableMFA", [], "panel"), "html", null, true);
        echo "</button>
            </td>
            <td>
                <div class=\"btn-group\" uib-dropdown>
                    <button type=\"button\" class=\"btn btn-default\" uib-dropdown-toggle>
                        <img src=\"";
        // line 21
        echo twig_escape_filter($this->env, (($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/concertopanel/images/") . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 21, $this->source); })()), "request", [], "any", false, false, false, 21), "locale", [], "any", false, false, false, 21)) . ".png"), "html", null, true);
        echo "\"/> ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["available_locales"]) || array_key_exists("available_locales", $context) ? $context["available_locales"] : (function () { throw new RuntimeError('Variable "available_locales" does not exist.', 21, $this->source); })()), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 21, $this->source); })()), "request", [], "any", false, false, false, 21), "locale", [], "any", false, false, false, 21), [], "array", false, false, false, 21), "html", null, true);
        echo " <span class=\"caret\"></span>
                    </button>
                    <ul class=\"dropdown-menu\" role=\"menu\">
                        ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["available_locales"]) || array_key_exists("available_locales", $context) ? $context["available_locales"] : (function () { throw new RuntimeError('Variable "available_locales" does not exist.', 24, $this->source); })())));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 25
            echo "                            <li><a href=\"\" ng-click=\"changeLocale('";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "')\"><img src=\"";
            echo twig_escape_filter($this->env, (($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/concertopanel/images/") . $context["key"]) . ".png"), "html", null, true);
            echo "\"/> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["available_locales"]) || array_key_exists("available_locales", $context) ? $context["available_locales"] : (function () { throw new RuntimeError('Variable "available_locales" does not exist.', 25, $this->source); })()), $context["key"], [], "array", false, false, false, 25), "html", null, true);
            echo "</a></li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "                    </ul>
                </div>
            </td>
            <td>
                <button ng-click=\"logout()\" class=\"btn btn-default\"><i class=\"glyphicon glyphicon-user\"></i> ";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header.logout", [], "panel"), "html", null, true);
        echo "</button>
            </td>
        </tr>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle::header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 31,  128 => 27,  115 => 25,  111 => 24,  103 => 21,  95 => 16,  91 => 15,  85 => 12,  80 => 10,  76 => 9,  71 => 7,  68 => 6,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"panel\" %}

{% block content %}
    {% set googleAuthenticatorEnabled = app.user.isGoogleAuthenticatorEnabled ? \"true\" : \"false\" %}

    <table ng-controller=\"HeaderController\" ng-init=\"mfaEnabled = {{ googleAuthenticatorEnabled }};\">
        <tr>
            <td><img src=\"{{ asset('bundles/concertopanel/images/symbol.png') }}\"/></td>
            <td style=\"font-weight: bold;\">v{{ version }}</td>
            <td style=\"width:100%; text-align: right; padding-right: 15px;\">
                {{ 'header.logged.user'|trans({'{0}':app.user.username}) }}
            </td>
            <td>
                <button ng-click=\"disableMFA()\" class=\"btn btn-default\" ng-show=\"mfaEnabled\"><i class=\"glyphicon glyphicon-qrcode\"></i> {{ 'header.disableMFA'|trans }}</button>
                <button ng-click=\"enableMFA()\" class=\"btn btn-default\" ng-show=\"!mfaEnabled\"><i class=\"glyphicon glyphicon-qrcode\"></i> {{ 'header.enableMFA'|trans }}</button>
            </td>
            <td>
                <div class=\"btn-group\" uib-dropdown>
                    <button type=\"button\" class=\"btn btn-default\" uib-dropdown-toggle>
                        <img src=\"{{ asset(\"bundles/concertopanel/images/\") ~ app.request.locale ~ \".png\" }}\"/> {{ available_locales[app.request.locale] }} <span class=\"caret\"></span>
                    </button>
                    <ul class=\"dropdown-menu\" role=\"menu\">
                        {% for key in available_locales|keys %}
                            <li><a href=\"\" ng-click=\"changeLocale('{{ key }}')\"><img src=\"{{ asset(\"bundles/concertopanel/images/\") ~ key ~ \".png\" }}\"/> {{ available_locales[key] }}</a></li>
                        {% endfor %}
                    </ul>
                </div>
            </td>
            <td>
                <button ng-click=\"logout()\" class=\"btn btn-default\"><i class=\"glyphicon glyphicon-user\"></i> {{ 'header.logout'|trans }}</button>
            </td>
        </tr>
    </table>
{% endblock %}
", "ConcertoPanelBundle::header.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/header.html.twig");
    }
}
