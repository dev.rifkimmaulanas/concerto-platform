<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:ViewTemplate:source_section.html.twig */
class __TwigTemplate_ce203b77aa832b6799efd37e9c066e1ed3dfcbc98252fd99b34a324f4ef95df8 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "
    ";
        // line 6
        $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 6, "73972667")->display(twig_array_merge($context, ["internal" => true, "accordion_group" => "source"]));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:source_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 6,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"ViewTemplate\" %}

{% block content %}

    {% embed \"ConcertoPanelBundle::form_v_accordion.html.twig\" with {'internal': true, 'accordion_group': 'source'} %}
        {% trans_default_domain \"ViewTemplate\" %}

        {% block legend %}{{ 'source.fieldset.legend'|trans }}{% endblock %}
        {% block legend_tooltip %}{{ 'source.fieldset.legend.tooltip'|trans }}{% endblock %}
        {% block elements %}
            <table style=\"width:100%;\">

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.head'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.head.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.head\" ui-codemirror-opts=\"headCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.css'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.css.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.css\" ui-codemirror-opts=\"cssCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.js'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.js.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.js\" ui-codemirror-opts=\"jsCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.html'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.html.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <textarea name=\"templateHtml\" ckeditor=\"htmlEditorOptions\" ng-model=\"object.html\" ng-disabled=\"!isEditable()\"></textarea>
                    {% endblock %}
                {% endembed %}
            </table>
        {% endblock %}
    {% endembed %}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/source_section.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:source_section.html.twig */
class __TwigTemplate_ce203b77aa832b6799efd37e9c066e1ed3dfcbc98252fd99b34a324f4ef95df8___73972667 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'legend_tooltip' => [$this, 'block_legend_tooltip'],
            'elements' => [$this, 'block_elements'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "ConcertoPanelBundle::form_v_accordion.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_accordion.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 6);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.fieldset.legend", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_legend_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.fieldset.legend.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 12
        echo "            <table style=\"width:100%;\">

                ";
        // line 14
        $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 14, "2030422359")->display($context);
        // line 23
        echo "
                ";
        // line 24
        $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 24, "1966819047")->display($context);
        // line 33
        echo "
                ";
        // line 34
        $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 34, "2023028089")->display($context);
        // line 43
        echo "
                ";
        // line 44
        $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 44, "915837900")->display($context);
        // line 52
        echo "            </table>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:source_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 52,  259 => 44,  256 => 43,  254 => 34,  251 => 33,  249 => 24,  246 => 23,  244 => 14,  240 => 12,  231 => 11,  213 => 10,  195 => 9,  68 => 6,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"ViewTemplate\" %}

{% block content %}

    {% embed \"ConcertoPanelBundle::form_v_accordion.html.twig\" with {'internal': true, 'accordion_group': 'source'} %}
        {% trans_default_domain \"ViewTemplate\" %}

        {% block legend %}{{ 'source.fieldset.legend'|trans }}{% endblock %}
        {% block legend_tooltip %}{{ 'source.fieldset.legend.tooltip'|trans }}{% endblock %}
        {% block elements %}
            <table style=\"width:100%;\">

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.head'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.head.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.head\" ui-codemirror-opts=\"headCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.css'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.css.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.css\" ui-codemirror-opts=\"cssCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.js'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.js.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.js\" ui-codemirror-opts=\"jsCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.html'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.html.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <textarea name=\"templateHtml\" ckeditor=\"htmlEditorOptions\" ng-model=\"object.html\" ng-disabled=\"!isEditable()\"></textarea>
                    {% endblock %}
                {% endembed %}
            </table>
        {% endblock %}
    {% endembed %}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/source_section.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:source_section.html.twig */
class __TwigTemplate_ce203b77aa832b6799efd37e9c066e1ed3dfcbc98252fd99b34a324f4ef95df8___2030422359 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 14
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 14);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 16
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.field.head", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 17
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.field.head.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 18
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 19
        echo "                        ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle::code_mirror_header.html.twig");
        echo "
                        <ui-codemirror ng-model=\"object.head\" ui-codemirror-opts=\"headCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:source_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  435 => 19,  426 => 18,  408 => 17,  390 => 16,  368 => 14,  261 => 52,  259 => 44,  256 => 43,  254 => 34,  251 => 33,  249 => 24,  246 => 23,  244 => 14,  240 => 12,  231 => 11,  213 => 10,  195 => 9,  68 => 6,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"ViewTemplate\" %}

{% block content %}

    {% embed \"ConcertoPanelBundle::form_v_accordion.html.twig\" with {'internal': true, 'accordion_group': 'source'} %}
        {% trans_default_domain \"ViewTemplate\" %}

        {% block legend %}{{ 'source.fieldset.legend'|trans }}{% endblock %}
        {% block legend_tooltip %}{{ 'source.fieldset.legend.tooltip'|trans }}{% endblock %}
        {% block elements %}
            <table style=\"width:100%;\">

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.head'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.head.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.head\" ui-codemirror-opts=\"headCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.css'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.css.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.css\" ui-codemirror-opts=\"cssCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.js'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.js.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.js\" ui-codemirror-opts=\"jsCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.html'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.html.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <textarea name=\"templateHtml\" ckeditor=\"htmlEditorOptions\" ng-model=\"object.html\" ng-disabled=\"!isEditable()\"></textarea>
                    {% endblock %}
                {% endembed %}
            </table>
        {% endblock %}
    {% endembed %}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/source_section.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:source_section.html.twig */
class __TwigTemplate_ce203b77aa832b6799efd37e9c066e1ed3dfcbc98252fd99b34a324f4ef95df8___1966819047 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 24
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 24);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 26
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.field.css", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 27
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.field.css.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 28
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 29
        echo "                        ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle::code_mirror_header.html.twig");
        echo "
                        <ui-codemirror ng-model=\"object.css\" ui-codemirror-opts=\"cssCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:source_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  612 => 29,  603 => 28,  585 => 27,  567 => 26,  545 => 24,  435 => 19,  426 => 18,  408 => 17,  390 => 16,  368 => 14,  261 => 52,  259 => 44,  256 => 43,  254 => 34,  251 => 33,  249 => 24,  246 => 23,  244 => 14,  240 => 12,  231 => 11,  213 => 10,  195 => 9,  68 => 6,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"ViewTemplate\" %}

{% block content %}

    {% embed \"ConcertoPanelBundle::form_v_accordion.html.twig\" with {'internal': true, 'accordion_group': 'source'} %}
        {% trans_default_domain \"ViewTemplate\" %}

        {% block legend %}{{ 'source.fieldset.legend'|trans }}{% endblock %}
        {% block legend_tooltip %}{{ 'source.fieldset.legend.tooltip'|trans }}{% endblock %}
        {% block elements %}
            <table style=\"width:100%;\">

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.head'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.head.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.head\" ui-codemirror-opts=\"headCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.css'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.css.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.css\" ui-codemirror-opts=\"cssCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.js'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.js.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.js\" ui-codemirror-opts=\"jsCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.html'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.html.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <textarea name=\"templateHtml\" ckeditor=\"htmlEditorOptions\" ng-model=\"object.html\" ng-disabled=\"!isEditable()\"></textarea>
                    {% endblock %}
                {% endembed %}
            </table>
        {% endblock %}
    {% endembed %}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/source_section.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:source_section.html.twig */
class __TwigTemplate_ce203b77aa832b6799efd37e9c066e1ed3dfcbc98252fd99b34a324f4ef95df8___2023028089 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 34
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 34);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 36
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.field.js", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 37
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.field.js.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 38
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 39
        echo "                        ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle::code_mirror_header.html.twig");
        echo "
                        <ui-codemirror ng-model=\"object.js\" ui-codemirror-opts=\"jsCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:source_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  789 => 39,  780 => 38,  762 => 37,  744 => 36,  722 => 34,  612 => 29,  603 => 28,  585 => 27,  567 => 26,  545 => 24,  435 => 19,  426 => 18,  408 => 17,  390 => 16,  368 => 14,  261 => 52,  259 => 44,  256 => 43,  254 => 34,  251 => 33,  249 => 24,  246 => 23,  244 => 14,  240 => 12,  231 => 11,  213 => 10,  195 => 9,  68 => 6,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"ViewTemplate\" %}

{% block content %}

    {% embed \"ConcertoPanelBundle::form_v_accordion.html.twig\" with {'internal': true, 'accordion_group': 'source'} %}
        {% trans_default_domain \"ViewTemplate\" %}

        {% block legend %}{{ 'source.fieldset.legend'|trans }}{% endblock %}
        {% block legend_tooltip %}{{ 'source.fieldset.legend.tooltip'|trans }}{% endblock %}
        {% block elements %}
            <table style=\"width:100%;\">

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.head'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.head.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.head\" ui-codemirror-opts=\"headCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.css'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.css.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.css\" ui-codemirror-opts=\"cssCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.js'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.js.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.js\" ui-codemirror-opts=\"jsCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.html'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.html.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <textarea name=\"templateHtml\" ckeditor=\"htmlEditorOptions\" ng-model=\"object.html\" ng-disabled=\"!isEditable()\"></textarea>
                    {% endblock %}
                {% endembed %}
            </table>
        {% endblock %}
    {% endembed %}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/source_section.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:source_section.html.twig */
class __TwigTemplate_ce203b77aa832b6799efd37e9c066e1ed3dfcbc98252fd99b34a324f4ef95df8___915837900 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 44
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", 44);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 46
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.field.html", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 47
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("source.field.html.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 48
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 49
        echo "                        <textarea name=\"templateHtml\" ckeditor=\"htmlEditorOptions\" ng-model=\"object.html\" ng-disabled=\"!isEditable()\"></textarea>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:source_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  966 => 49,  957 => 48,  939 => 47,  921 => 46,  899 => 44,  789 => 39,  780 => 38,  762 => 37,  744 => 36,  722 => 34,  612 => 29,  603 => 28,  585 => 27,  567 => 26,  545 => 24,  435 => 19,  426 => 18,  408 => 17,  390 => 16,  368 => 14,  261 => 52,  259 => 44,  256 => 43,  254 => 34,  251 => 33,  249 => 24,  246 => 23,  244 => 14,  240 => 12,  231 => 11,  213 => 10,  195 => 9,  68 => 6,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"ViewTemplate\" %}

{% block content %}

    {% embed \"ConcertoPanelBundle::form_v_accordion.html.twig\" with {'internal': true, 'accordion_group': 'source'} %}
        {% trans_default_domain \"ViewTemplate\" %}

        {% block legend %}{{ 'source.fieldset.legend'|trans }}{% endblock %}
        {% block legend_tooltip %}{{ 'source.fieldset.legend.tooltip'|trans }}{% endblock %}
        {% block elements %}
            <table style=\"width:100%;\">

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.head'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.head.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.head\" ui-codemirror-opts=\"headCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.css'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.css.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.css\" ui-codemirror-opts=\"cssCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.js'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.js.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\") }}
                        <ui-codemirror ng-model=\"object.js\" ui-codemirror-opts=\"jsCodeOptions\" ui-refresh='codemirrorForceRefresh'></ui-codemirror>
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                    {% trans_default_domain \"ViewTemplate\" %}
                    {% block label %}{{ 'source.field.html'|trans }}{% endblock %}
                    {% block tooltip %}{{ 'source.field.html.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <textarea name=\"templateHtml\" ckeditor=\"htmlEditorOptions\" ng-model=\"object.html\" ng-disabled=\"!isEditable()\"></textarea>
                    {% endblock %}
                {% endembed %}
            </table>
        {% endblock %}
    {% endembed %}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:source_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/source_section.html.twig");
    }
}
