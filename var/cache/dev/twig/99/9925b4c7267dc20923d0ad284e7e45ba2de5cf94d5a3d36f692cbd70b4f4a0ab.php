<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Test:flow_section.html.twig */
class __TwigTemplate_8f66e246ae32da424fe095bf5cd13cdd6f524632ca2f1ccd527e498fec57b0bd extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:flow_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:flow_section.html.twig"));

        // line 2
        echo "
<div id=\"flow\" flow-logic ng-class=\"{'flow-maximized':maximized}\">
    <div id=\"flow-menu-wrapper\">
        <div id=\"flow-menu\" class=\"panel panel-default\">
            <i class='glyphicon glyphicon-fullscreen' tooltip-append-to-body='true' uib-tooltip-html='\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.maximize", [], "Test"), "html", null, true);
        echo "\"' style=\"cursor: pointer;\" ng-click=\"toggleMaximize();\"></i><br/>
            <i class='glyphicon glyphicon-screenshot' tooltip-append-to-body='true' uib-tooltip-html='\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.reset_view", [], "Test"), "html", null, true);
        echo "\"' style=\"cursor: pointer;\" ng-click=\"resetView();\"></i><br/>
            <i class='glyphicon glyphicon-plus' tooltip-append-to-body='true' uib-tooltip-html='\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.zoom_in", [], "Test"), "html", null, true);
        echo "\"' style=\"cursor: pointer;\" ng-click=\"setZoom(1);\"></i><br/>
            <i class='glyphicon glyphicon-minus' tooltip-append-to-body='true' uib-tooltip-html='\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.zoom_out", [], "Test"), "html", null, true);
        echo "\"' style=\"cursor: pointer;\" ng-click=\"setZoom(-1);\"></i>
        </div>
    </div>
    <div id=\"flowContainerScroll\" oncontextmenu=\"return false;\">
        <div id=\"flowContainerWrapper\">
            <div id=\"flowContainer\" context-menu=\"onFlowCtxOpened()\" data-target=\"menu-flow\" tabindex=\"0\" context-menu-disabled=\"disableContextMenu || !isEditable()\" ng-class=\"{'cursor-resize':rectangleSelectionActive, 'cursor-move': movingActive}\">
                <div id=\"selection-rectangle\" hidden></div>
            </div>
        </div>
    </div>

    <div class=\"dropdown ctx-menu\" id=\"menu-node\">
        <ul class=\"dropdown-menu ctx-menu-ul\" role=\"menu\">
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"editNodeTitle(collectionService.getNode(nodeContext.id))\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.rename_node", [], "Test"), "html", null, true);
        echo "</li>
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"copyNode(nodeContext.id)\" ng-show=\"selectedNodeIds.length <= 1\">";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.copy_node", [], "Test"), "html", null, true);
        echo "</li>
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"copyNode(nodeContext.id)\" ng-show=\"selectedNodeIds.length > 1\">";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.copy_nodes", [], "Test"), "html", null, true);
        echo "</li>            
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"removeNode(nodeContext.id)\" ng-show=\"selectedNodeIds.length <= 1\">";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.remove_node", [], "Test"), "html", null, true);
        echo "</li>
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"removeNode(nodeContext.id)\" ng-show=\"selectedNodeIds.length > 1\">";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.remove_nodes", [], "Test"), "html", null, true);
        echo "</li>
        </ul>
    </div>

    <div class=\"dropdown ctx-menu\" id=\"menu-flow\" context-menu-margin-bottom=\"10\">
        <ul class=\"dropdown-menu ctx-menu-ul\" role=\"menu\">
            <div style=\"margin: 3px 20px 10px 10px;\">
                <table style=\"width:100%;\">
                    <tr>
                        <td style=\"width:16px;\"><i class=\"glyphicon glyphicon-search\"></i></td>
                        <td><input type=\"text\" ng-model=\"nodeNameFilter\" ctx-no-close-on-click class='form-control' /></td>
                    </tr>
                </table>
            </div>

            <uib-accordion close-others=\"true\">
                <uib-accordion-group ng-init=\"isOpen = \$first\" is-open=\"isOpen\" ng-repeat=\"tag in collectionService.getUniqueTags() | orderBy: 'name'\" panel-class=\"panel-default\">
                    <uib-accordion-heading>
                        <div ctx-no-close-on-click>";
        // line 44
        echo "{{tag | capitalize}} ({{ (collectionService.getTaggedCollection(tag) | filter:excludeSelfFilter | filter: { 'name': nodeNameFilter }).length}})";
        echo "</div>
                    </uib-accordion-heading>

                    <li ng-repeat=\"test in collectionService.getTaggedCollection(tag) | filter:excludeSelfFilter | filter: { 'name': nodeNameFilter } | orderBy: 'name' as tests track by test.id\" 
                        class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"addNewNode(0, test.id)\">
                        <i class='glyphicon glyphicon-question-sign' tooltip-append-to-body='true' uib-tooltip-html='test.description'></i>
                        ";
        // line 50
        echo "{{test.name}}";
        echo "
                    </li>

                </uib-accordion-group>

                <uib-accordion-group panel-class=\"panel-default\">
                    <uib-accordion-heading>
                        <div ctx-no-close-on-click>";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.tags.all", [], "Test"), "html", null, true);
        echo " ";
        echo "({{ (collectionService.collection | filter:excludeSelfFilter | filter: { 'name': nodeNameFilter }).length}})";
        echo "</div>
                    </uib-accordion-heading>

                    <li ng-repeat=\"test in collectionService.collection | filter:excludeSelfFilter| filter: { 'name': nodeNameFilter } | orderBy: 'name' as tests track by test.id\" 
                        class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"addNewNode(0, test.id)\">
                        <i class='glyphicon glyphicon-question-sign' tooltip-append-to-body='true' uib-tooltip-html='test.description'></i>
                        ";
        // line 63
        echo "{{test.name}}";
        echo "
                    </li>

                </uib-accordion-group>
            </uib-accordion>

            <li class=\"ctx-element ctx-action\" ng-class=\"{'ctx-action-active':copiedNodes.length > 0, 'ctx-action-inactive':copiedNodes.length === 0}\" ng-click=\"pasteNodes()\">";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.paste_nodes", [], "Test"), "html", null, true);
        echo "</li>
        </ul>
    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:flow_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 69,  142 => 63,  131 => 57,  121 => 50,  112 => 44,  91 => 26,  87 => 25,  83 => 24,  79 => 23,  75 => 22,  59 => 9,  55 => 8,  51 => 7,  47 => 6,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Test\" %}

<div id=\"flow\" flow-logic ng-class=\"{'flow-maximized':maximized}\">
    <div id=\"flow-menu-wrapper\">
        <div id=\"flow-menu\" class=\"panel panel-default\">
            <i class='glyphicon glyphicon-fullscreen' tooltip-append-to-body='true' uib-tooltip-html='\"{{ \"flow.buttons.maximize\"|trans }}\"' style=\"cursor: pointer;\" ng-click=\"toggleMaximize();\"></i><br/>
            <i class='glyphicon glyphicon-screenshot' tooltip-append-to-body='true' uib-tooltip-html='\"{{ \"flow.buttons.reset_view\"|trans }}\"' style=\"cursor: pointer;\" ng-click=\"resetView();\"></i><br/>
            <i class='glyphicon glyphicon-plus' tooltip-append-to-body='true' uib-tooltip-html='\"{{ \"flow.buttons.zoom_in\"|trans }}\"' style=\"cursor: pointer;\" ng-click=\"setZoom(1);\"></i><br/>
            <i class='glyphicon glyphicon-minus' tooltip-append-to-body='true' uib-tooltip-html='\"{{ \"flow.buttons.zoom_out\"|trans }}\"' style=\"cursor: pointer;\" ng-click=\"setZoom(-1);\"></i>
        </div>
    </div>
    <div id=\"flowContainerScroll\" oncontextmenu=\"return false;\">
        <div id=\"flowContainerWrapper\">
            <div id=\"flowContainer\" context-menu=\"onFlowCtxOpened()\" data-target=\"menu-flow\" tabindex=\"0\" context-menu-disabled=\"disableContextMenu || !isEditable()\" ng-class=\"{'cursor-resize':rectangleSelectionActive, 'cursor-move': movingActive}\">
                <div id=\"selection-rectangle\" hidden></div>
            </div>
        </div>
    </div>

    <div class=\"dropdown ctx-menu\" id=\"menu-node\">
        <ul class=\"dropdown-menu ctx-menu-ul\" role=\"menu\">
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"editNodeTitle(collectionService.getNode(nodeContext.id))\">{{ \"flow.buttons.rename_node\"|trans }}</li>
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"copyNode(nodeContext.id)\" ng-show=\"selectedNodeIds.length <= 1\">{{ \"flow.buttons.copy_node\"|trans }}</li>
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"copyNode(nodeContext.id)\" ng-show=\"selectedNodeIds.length > 1\">{{ \"flow.buttons.copy_nodes\"|trans }}</li>            
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"removeNode(nodeContext.id)\" ng-show=\"selectedNodeIds.length <= 1\">{{ \"flow.buttons.remove_node\"|trans }}</li>
            <li class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"removeNode(nodeContext.id)\" ng-show=\"selectedNodeIds.length > 1\">{{ \"flow.buttons.remove_nodes\"|trans }}</li>
        </ul>
    </div>

    <div class=\"dropdown ctx-menu\" id=\"menu-flow\" context-menu-margin-bottom=\"10\">
        <ul class=\"dropdown-menu ctx-menu-ul\" role=\"menu\">
            <div style=\"margin: 3px 20px 10px 10px;\">
                <table style=\"width:100%;\">
                    <tr>
                        <td style=\"width:16px;\"><i class=\"glyphicon glyphicon-search\"></i></td>
                        <td><input type=\"text\" ng-model=\"nodeNameFilter\" ctx-no-close-on-click class='form-control' /></td>
                    </tr>
                </table>
            </div>

            <uib-accordion close-others=\"true\">
                <uib-accordion-group ng-init=\"isOpen = \$first\" is-open=\"isOpen\" ng-repeat=\"tag in collectionService.getUniqueTags() | orderBy: 'name'\" panel-class=\"panel-default\">
                    <uib-accordion-heading>
                        <div ctx-no-close-on-click>{%verbatim%}{{tag | capitalize}} ({{ (collectionService.getTaggedCollection(tag) | filter:excludeSelfFilter | filter: { 'name': nodeNameFilter }).length}}){%endverbatim%}</div>
                    </uib-accordion-heading>

                    <li ng-repeat=\"test in collectionService.getTaggedCollection(tag) | filter:excludeSelfFilter | filter: { 'name': nodeNameFilter } | orderBy: 'name' as tests track by test.id\" 
                        class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"addNewNode(0, test.id)\">
                        <i class='glyphicon glyphicon-question-sign' tooltip-append-to-body='true' uib-tooltip-html='test.description'></i>
                        {% verbatim %}{{test.name}}{% endverbatim %}
                    </li>

                </uib-accordion-group>

                <uib-accordion-group panel-class=\"panel-default\">
                    <uib-accordion-heading>
                        <div ctx-no-close-on-click>{{ \"flow.tags.all\"|trans }} {%verbatim%}({{ (collectionService.collection | filter:excludeSelfFilter | filter: { 'name': nodeNameFilter }).length}}){%endverbatim%}</div>
                    </uib-accordion-heading>

                    <li ng-repeat=\"test in collectionService.collection | filter:excludeSelfFilter| filter: { 'name': nodeNameFilter } | orderBy: 'name' as tests track by test.id\" 
                        class=\"ctx-element ctx-action ctx-action-active\" ng-click=\"addNewNode(0, test.id)\">
                        <i class='glyphicon glyphicon-question-sign' tooltip-append-to-body='true' uib-tooltip-html='test.description'></i>
                        {% verbatim %}{{test.name}}{% endverbatim %}
                    </li>

                </uib-accordion-group>
            </uib-accordion>

            <li class=\"ctx-element ctx-action\" ng-class=\"{'ctx-action-active':copiedNodes.length > 0, 'ctx-action-inactive':copiedNodes.length === 0}\" ng-click=\"pasteNodes()\">{{ \"flow.buttons.paste_nodes\"|trans }}</li>
        </ul>
    </div>
</div>", "ConcertoPanelBundle:Test:flow_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/flow_section.html.twig");
    }
}
