<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle::code_mirror_header.html.twig */
class __TwigTemplate_37cc97b4ee513501afd78f563631fe4261ea5988b98305ac9e3bb9c356ab81d1 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::code_mirror_header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::code_mirror_header.html.twig"));

        // line 2
        echo "<div>
    ";
        // line 3
        if (((isset($context["mode"]) || array_key_exists("mode", $context)) && ((isset($context["mode"]) || array_key_exists("mode", $context) ? $context["mode"] : (function () { throw new RuntimeError('Variable "mode" does not exist.', 3, $this->source); })()) == "R"))) {
            // line 4
            echo "        ";
            // line 5
            echo "        <div class=\"codemirrorCompletionAlert\" ng-if=\"!RDocumentation.functionIndex[0]\">
            <uib-alert type=\"danger\">
                <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                ";
            // line 8
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("code.documentation.error.info", [], "panel"), "html", null, true);
            echo "
                <button ng-click=\"RDocumentation.showDocumentationHelp()\" class=\"btn-sm btn-primary\">
                    ";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("code.documentation.error.solve", [], "panel"), "html", null, true);
            echo "
                </button>
            </uib-alert>
        </div>
    ";
        }
        // line 15
        echo "    <div class=\"code-mirror-info\">
        <span><b>F11:</b> ";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("code.F11", [], "panel"), "html", null, true);
        echo "</span>
        <span>, <b>Shift + Tab:</b> ";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("code.shift.tab", [], "panel"), "html", null, true);
        echo "</span>
        ";
        // line 18
        if (((isset($context["mode"]) || array_key_exists("mode", $context)) && ((isset($context["mode"]) || array_key_exists("mode", $context) ? $context["mode"] : (function () { throw new RuntimeError('Variable "mode" does not exist.', 18, $this->source); })()) == "R"))) {
            // line 19
            echo "            <span ng-if=\"RDocumentation.functionIndex[0]\">, <b>Ctrl + Space:</b> ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("code.ctrl.space", [], "panel"), "html", null, true);
            echo "</span>
        ";
        }
        // line 21
        echo "    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle::code_mirror_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 21,  79 => 19,  77 => 18,  73 => 17,  69 => 16,  66 => 15,  58 => 10,  53 => 8,  48 => 5,  46 => 4,  44 => 3,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"panel\" %}
<div>
    {% if mode is defined and mode == 'R' %}
        {# twig if is there since it might be included outside R-codemirror. ng-if is there as this alert needs to be displayed only when there is an error #}
        <div class=\"codemirrorCompletionAlert\" ng-if=\"!RDocumentation.functionIndex[0]\">
            <uib-alert type=\"danger\">
                <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                {{ 'code.documentation.error.info'|trans }}
                <button ng-click=\"RDocumentation.showDocumentationHelp()\" class=\"btn-sm btn-primary\">
                    {{ 'code.documentation.error.solve' | trans }}
                </button>
            </uib-alert>
        </div>
    {% endif %}
    <div class=\"code-mirror-info\">
        <span><b>F11:</b> {{ 'code.F11'|trans }}</span>
        <span>, <b>Shift + Tab:</b> {{ 'code.shift.tab'|trans }}</span>
        {% if mode is defined and mode == 'R' %}
            <span ng-if=\"RDocumentation.functionIndex[0]\">, <b>Ctrl + Space:</b> {{ 'code.ctrl.space'|trans }}</span>
        {% endif %}
    </div>
</div>", "ConcertoPanelBundle::code_mirror_header.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/code_mirror_header.html.twig");
    }
}
