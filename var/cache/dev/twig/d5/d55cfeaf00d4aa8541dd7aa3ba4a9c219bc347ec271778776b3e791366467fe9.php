<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle::form_h.html.twig */
class __TwigTemplate_d60a46590667aa8a585dbdf554af30f76704cfcc02832a26cbbec8eb6641bf8c extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'top' => [$this, 'block_top'],
            'legend' => [$this, 'block_legend'],
            'elements' => [$this, 'block_elements'],
            'sections' => [$this, 'block_sections'],
            'floatingBarButtons' => [$this, 'block_floatingBarButtons'],
            'floatingBarExtraButtons' => [$this, 'block_floatingBarExtraButtons'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::form_h.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::form_h.html.twig"));

        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle::form_h.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "    ";
        if ((isset($context["isAddDialog"]) || array_key_exists("isAddDialog", $context) ? $context["isAddDialog"] : (function () { throw new RuntimeError('Variable "isAddDialog" does not exist.', 5, $this->source); })())) {
            // line 6
            echo "        <div class=\"modal-body\">
    ";
        }
        // line 8
        echo "    ";
        $this->displayBlock('top', $context, $blocks);
        // line 9
        echo "    ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle::form_validation.html.twig");
        echo "
    <uib-accordion close-others=\"false\">
        <uib-accordion-group is-open=\"tabAccordion.form.open\" is-disabled=\"tabAccordion.form.disabled\">
            <uib-accordion-heading>
                ";
        // line 13
        if ((((isset($context["isAddDialog"]) || array_key_exists("isAddDialog", $context) ? $context["isAddDialog"] : (function () { throw new RuntimeError('Variable "isAddDialog" does not exist.', 13, $this->source); })()) == false) && (isset($context["exportable"]) || array_key_exists("exportable", $context) ? $context["exportable"] : (function () { throw new RuntimeError('Variable "exportable" does not exist.', 13, $this->source); })()))) {
            // line 14
            echo "                    <i class=\"glyphicon glyphicon-comment\" ng-click=\"editDescription()\" uib-tooltip-html=\"'";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("description.tooltip", [], "panel"), "html", null, true);
            echo "'\"></i>
                ";
        }
        // line 16
        echo "                ";
        $this->displayBlock('legend', $context, $blocks);
        // line 17
        echo "                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.form.open, 'glyphicon-chevron-right': !tabAccordion.form.open}\"></i>
            </uib-accordion-heading>
            ";
        // line 19
        $this->displayBlock('elements', $context, $blocks);
        // line 21
        echo "            <div style=\"clear: left;\"></div>
        </uib-accordion-group>
        ";
        // line 23
        if (((isset($context["isAddDialog"]) || array_key_exists("isAddDialog", $context) ? $context["isAddDialog"] : (function () { throw new RuntimeError('Variable "isAddDialog" does not exist.', 23, $this->source); })()) == false)) {
            // line 24
            echo "            ";
            $this->displayBlock('sections', $context, $blocks);
            // line 25
            echo "        ";
        }
        // line 26
        echo "    </uib-accordion>
    ";
        // line 27
        if ((isset($context["isAddDialog"]) || array_key_exists("isAddDialog", $context) ? $context["isAddDialog"] : (function () { throw new RuntimeError('Variable "isAddDialog" does not exist.', 27, $this->source); })())) {
            // line 28
            echo "        </div>
        <div class=\"modal-footer\">
            ";
            // line 30
            if (((((isset($context["defaultButtons"]) || array_key_exists("defaultButtons", $context))) ? (_twig_default_filter((isset($context["defaultButtons"]) || array_key_exists("defaultButtons", $context) ? $context["defaultButtons"] : (function () { throw new RuntimeError('Variable "defaultButtons" does not exist.', 30, $this->source); })()), "true")) : ("true")) == "true")) {
                // line 31
                echo "                <button class=\"btn btn-primary\" ng-click=\"save()\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.save", [], "panel"), "html", null, true);
                echo "</button>
                <button class=\"btn btn-warning\" ng-click=\"cancel()\">";
                // line 32
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.cancel", [], "panel"), "html", null, true);
                echo "</button>
            ";
            }
            // line 34
            echo "        </div>
    ";
        }
        // line 36
        echo "
    ";
        // line 37
        if (((isset($context["isAddDialog"]) || array_key_exists("isAddDialog", $context) ? $context["isAddDialog"] : (function () { throw new RuntimeError('Variable "isAddDialog" does not exist.', 37, $this->source); })()) == false)) {
            // line 38
            echo "
        <div class=\"formFloatingBar\" align=\"right\">
            ";
            // line 40
            $this->displayBlock('floatingBarButtons', $context, $blocks);
            // line 54
            echo "        </div>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 8
    public function block_top($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 19
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 20
        echo "            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 24
    public function block_sections($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 40
    public function block_floatingBarButtons($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarButtons"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarButtons"));

        // line 41
        echo "                ";
        if (((((isset($context["defaultButtons"]) || array_key_exists("defaultButtons", $context))) ? (_twig_default_filter((isset($context["defaultButtons"]) || array_key_exists("defaultButtons", $context) ? $context["defaultButtons"] : (function () { throw new RuntimeError('Variable "defaultButtons" does not exist.', 41, $this->source); })()), "true")) : ("true")) == "true")) {
            // line 42
            echo "                    <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.save", [], "panel"), "html", null, true);
            echo "</button>
                    <button class='btn btn-warning' ng-click=\"cancel();\">";
            // line 43
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.cancel", [], "panel"), "html", null, true);
            echo "</button>
                    <button class='btn btn-success' ng-click=\"saveNew();\">";
            // line 44
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.save.new", [], "panel"), "html", null, true);
            echo "</button>
                    ";
            // line 45
            if ((isset($context["exportable"]) || array_key_exists("exportable", $context) ? $context["exportable"] : (function () { throw new RuntimeError('Variable "exportable" does not exist.', 45, $this->source); })())) {
                // line 46
                echo "                        <button class=\"btn btn-default\" ng-click=\"exportObject();\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.export", [], "panel"), "html", null, true);
                echo "</button>
                        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>";
                // line 47
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.lock", [], "panel"), "html", null, true);
                echo "</button>
                        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>";
                // line 48
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.unlock", [], "panel"), "html", null, true);
                echo "</button>
                    ";
            }
            // line 50
            echo "                    <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.delete", [], "panel"), "html", null, true);
            echo "</button>
                ";
        }
        // line 52
        echo "                ";
        $this->displayBlock('floatingBarExtraButtons', $context, $blocks);
        // line 53
        echo "            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 52
    public function block_floatingBarExtraButtons($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarExtraButtons"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarExtraButtons"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle::form_h.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  296 => 52,  286 => 53,  283 => 52,  277 => 50,  272 => 48,  268 => 47,  263 => 46,  261 => 45,  257 => 44,  253 => 43,  248 => 42,  245 => 41,  236 => 40,  219 => 24,  209 => 20,  200 => 19,  183 => 16,  166 => 8,  154 => 54,  152 => 40,  148 => 38,  146 => 37,  143 => 36,  139 => 34,  134 => 32,  129 => 31,  127 => 30,  123 => 28,  121 => 27,  118 => 26,  115 => 25,  112 => 24,  110 => 23,  106 => 21,  104 => 19,  100 => 17,  97 => 16,  91 => 14,  89 => 13,  81 => 9,  78 => 8,  74 => 6,  71 => 5,  62 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"panel\" %}

{% block content %}
    {% if isAddDialog %}
        <div class=\"modal-body\">
    {% endif %}
    {% block top %}{% endblock %}
    {{ include(\"ConcertoPanelBundle::form_validation.html.twig\") }}
    <uib-accordion close-others=\"false\">
        <uib-accordion-group is-open=\"tabAccordion.form.open\" is-disabled=\"tabAccordion.form.disabled\">
            <uib-accordion-heading>
                {% if isAddDialog == false and exportable %}
                    <i class=\"glyphicon glyphicon-comment\" ng-click=\"editDescription()\" uib-tooltip-html=\"'{{ 'description.tooltip'|trans }}'\"></i>
                {% endif %}
                {% block legend %}{% endblock %}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.form.open, 'glyphicon-chevron-right': !tabAccordion.form.open}\"></i>
            </uib-accordion-heading>
            {% block elements %}
            {% endblock %}
            <div style=\"clear: left;\"></div>
        </uib-accordion-group>
        {% if isAddDialog == false %}
            {% block sections %}{% endblock %}
        {% endif %}
    </uib-accordion>
    {% if isAddDialog %}
        </div>
        <div class=\"modal-footer\">
            {% if defaultButtons|default('true') == 'true' %}
                <button class=\"btn btn-primary\" ng-click=\"save()\">{{ 'form.button.save'|trans }}</button>
                <button class=\"btn btn-warning\" ng-click=\"cancel()\">{{ 'form.button.cancel'|trans }}</button>
            {% endif %}
        </div>
    {% endif %}

    {% if isAddDialog == false %}

        <div class=\"formFloatingBar\" align=\"right\">
            {% block floatingBarButtons %}
                {% if defaultButtons|default('true') == 'true' %}
                    <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans }}</button>
                    <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans }}</button>
                    <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans }}</button>
                    {% if exportable %}
                        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans }}</button>
                        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans }}</button>
                        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans }}</button>
                    {% endif %}
                    <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans }}</button>
                {% endif %}
                {% block floatingBarExtraButtons %}{% endblock %}
            {% endblock %}
        </div>
    {% endif %}
{% endblock %}", "ConcertoPanelBundle::form_h.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/form_h.html.twig");
    }
}
