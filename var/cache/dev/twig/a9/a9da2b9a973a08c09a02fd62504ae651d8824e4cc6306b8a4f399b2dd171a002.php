<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_10_definer.html.twig */
class __TwigTemplate_a94a93677e528a28b8dcca461ef256a749f3681ae6128c2c9066e0b2a62fc5ca extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        // line 1
        echo "<script type=\"text/ng-template\" id=\"type_10_definer.html\">
    <div ng-controller=\"WizardParamDefiner10Controller\">
    ";
        // line 3
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", 3, "209381743")->display($context);
        // line 35
        echo "    </div>
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 35,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_10_definer.html\">
    <div ng-controller=\"WizardParamDefiner10Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block definition %}
            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{testWizardParamService.getDefinerTitle(param)}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.definer.titles.list.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.form.field.type'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.form.field.type.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"param.definition.element.type\" ng-options=\"type.id as type.label for type in typesCollection | orderBy:'label'\" style=\"width:100%;\" class='form-control'>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr ng-if=\"typesCollection[param.definition.element.type].definer\">{% endblock %}
                            {% block label %}{{ 'param.form.field.definition'|trans }}{% endblock%}
                            {% block tooltip %}{{ 'param.form.field.definition.tooltip'|trans }}{% endblock%}
                            {% block control %}
                            <i class='glyphicon glyphicon-align-justify clickable' ng-click='launchDefinitionDialog(param.definition.element)' uib-tooltip-html='\"{{ 'param.form.field.definition.icon.tooltip'|trans }}\"' tooltip-append-to-body='true'></i>
                            <span class=\"wizardParamSummary\">{%verbatim%}{{testWizardParamService.getDefinerSummary(param.definition.element)}}{%endverbatim%}</span>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_10_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_10_definer.html.twig */
class __TwigTemplate_a94a93677e528a28b8dcca461ef256a749f3681ae6128c2c9066e0b2a62fc5ca___209381743 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'definition' => [$this, 'block_definition'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "ConcertoPanelBundle:TestWizard:type_base_definer.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_base_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_definition($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "definition"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "definition"));

        // line 6
        echo "            ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", 6, "2135507738")->display($context);
        // line 33
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 33,  164 => 6,  155 => 5,  133 => 3,  47 => 35,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_10_definer.html\">
    <div ng-controller=\"WizardParamDefiner10Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block definition %}
            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{testWizardParamService.getDefinerTitle(param)}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.definer.titles.list.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.form.field.type'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.form.field.type.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"param.definition.element.type\" ng-options=\"type.id as type.label for type in typesCollection | orderBy:'label'\" style=\"width:100%;\" class='form-control'>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr ng-if=\"typesCollection[param.definition.element.type].definer\">{% endblock %}
                            {% block label %}{{ 'param.form.field.definition'|trans }}{% endblock%}
                            {% block tooltip %}{{ 'param.form.field.definition.tooltip'|trans }}{% endblock%}
                            {% block control %}
                            <i class='glyphicon glyphicon-align-justify clickable' ng-click='launchDefinitionDialog(param.definition.element)' uib-tooltip-html='\"{{ 'param.form.field.definition.icon.tooltip'|trans }}\"' tooltip-append-to-body='true'></i>
                            <span class=\"wizardParamSummary\">{%verbatim%}{{testWizardParamService.getDefinerSummary(param.definition.element)}}{%endverbatim%}</span>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_10_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_10_definer.html.twig */
class __TwigTemplate_a94a93677e528a28b8dcca461ef256a749f3681ae6128c2c9066e0b2a62fc5ca___2135507738 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'legend_tooltip' => [$this, 'block_legend_tooltip'],
            'elements' => [$this, 'block_elements'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 6
        return "ConcertoPanelBundle::form_v_panel.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_panel.html.twig", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", 6);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo "{{testWizardParamService.getDefinerTitle(param)}}";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.list.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 11
        echo "                    ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", 11, "823820332")->display($context);
        // line 20
        echo "
                    ";
        // line 21
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", 21, "1577421706")->display($context);
        // line 31
        echo "                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  329 => 31,  327 => 21,  324 => 20,  321 => 11,  312 => 10,  294 => 9,  276 => 8,  254 => 6,  167 => 33,  164 => 6,  155 => 5,  133 => 3,  47 => 35,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_10_definer.html\">
    <div ng-controller=\"WizardParamDefiner10Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block definition %}
            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{testWizardParamService.getDefinerTitle(param)}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.definer.titles.list.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.form.field.type'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.form.field.type.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"param.definition.element.type\" ng-options=\"type.id as type.label for type in typesCollection | orderBy:'label'\" style=\"width:100%;\" class='form-control'>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr ng-if=\"typesCollection[param.definition.element.type].definer\">{% endblock %}
                            {% block label %}{{ 'param.form.field.definition'|trans }}{% endblock%}
                            {% block tooltip %}{{ 'param.form.field.definition.tooltip'|trans }}{% endblock%}
                            {% block control %}
                            <i class='glyphicon glyphicon-align-justify clickable' ng-click='launchDefinitionDialog(param.definition.element)' uib-tooltip-html='\"{{ 'param.form.field.definition.icon.tooltip'|trans }}\"' tooltip-append-to-body='true'></i>
                            <span class=\"wizardParamSummary\">{%verbatim%}{{testWizardParamService.getDefinerSummary(param.definition.element)}}{%endverbatim%}</span>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_10_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_10_definer.html.twig */
class __TwigTemplate_a94a93677e528a28b8dcca461ef256a749f3681ae6128c2c9066e0b2a62fc5ca___823820332 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 11
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", 11);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 13
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 16
        echo "                            <select ng-model=\"param.definition.element.type\" ng-options=\"type.id as type.label for type in typesCollection | orderBy:'label'\" style=\"width:100%;\" class='form-control'>
                            </select>
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  483 => 16,  474 => 15,  456 => 14,  438 => 13,  416 => 11,  329 => 31,  327 => 21,  324 => 20,  321 => 11,  312 => 10,  294 => 9,  276 => 8,  254 => 6,  167 => 33,  164 => 6,  155 => 5,  133 => 3,  47 => 35,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_10_definer.html\">
    <div ng-controller=\"WizardParamDefiner10Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block definition %}
            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{testWizardParamService.getDefinerTitle(param)}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.definer.titles.list.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.form.field.type'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.form.field.type.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"param.definition.element.type\" ng-options=\"type.id as type.label for type in typesCollection | orderBy:'label'\" style=\"width:100%;\" class='form-control'>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr ng-if=\"typesCollection[param.definition.element.type].definer\">{% endblock %}
                            {% block label %}{{ 'param.form.field.definition'|trans }}{% endblock%}
                            {% block tooltip %}{{ 'param.form.field.definition.tooltip'|trans }}{% endblock%}
                            {% block control %}
                            <i class='glyphicon glyphicon-align-justify clickable' ng-click='launchDefinitionDialog(param.definition.element)' uib-tooltip-html='\"{{ 'param.form.field.definition.icon.tooltip'|trans }}\"' tooltip-append-to-body='true'></i>
                            <span class=\"wizardParamSummary\">{%verbatim%}{{testWizardParamService.getDefinerSummary(param.definition.element)}}{%endverbatim%}</span>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_10_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_10_definer.html.twig */
class __TwigTemplate_a94a93677e528a28b8dcca461ef256a749f3681ae6128c2c9066e0b2a62fc5ca___1577421706 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'row' => [$this, 'block_row'],
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 21
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", 21);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 23
    public function block_row($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "row"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "row"));

        echo "<tr ng-if=\"typesCollection[param.definition.element.type].definer\">";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 24
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.definition", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.definition.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 26
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 27
        echo "                            <i class='glyphicon glyphicon-align-justify clickable' ng-click='launchDefinitionDialog(param.definition.element)' uib-tooltip-html='\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.definition.icon.tooltip", [], "TestWizard"), "html", null, true);
        echo "\"' tooltip-append-to-body='true'></i>
                            <span class=\"wizardParamSummary\">";
        // line 28
        echo "{{testWizardParamService.getDefinerSummary(param.definition.element)}}";
        echo "</span>
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  663 => 28,  658 => 27,  649 => 26,  631 => 25,  613 => 24,  595 => 23,  573 => 21,  483 => 16,  474 => 15,  456 => 14,  438 => 13,  416 => 11,  329 => 31,  327 => 21,  324 => 20,  321 => 11,  312 => 10,  294 => 9,  276 => 8,  254 => 6,  167 => 33,  164 => 6,  155 => 5,  133 => 3,  47 => 35,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_10_definer.html\">
    <div ng-controller=\"WizardParamDefiner10Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block definition %}
            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{testWizardParamService.getDefinerTitle(param)}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.definer.titles.list.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.form.field.type'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.form.field.type.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"param.definition.element.type\" ng-options=\"type.id as type.label for type in typesCollection | orderBy:'label'\" style=\"width:100%;\" class='form-control'>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr ng-if=\"typesCollection[param.definition.element.type].definer\">{% endblock %}
                            {% block label %}{{ 'param.form.field.definition'|trans }}{% endblock%}
                            {% block tooltip %}{{ 'param.form.field.definition.tooltip'|trans }}{% endblock%}
                            {% block control %}
                            <i class='glyphicon glyphicon-align-justify clickable' ng-click='launchDefinitionDialog(param.definition.element)' uib-tooltip-html='\"{{ 'param.form.field.definition.icon.tooltip'|trans }}\"' tooltip-append-to-body='true'></i>
                            <span class=\"wizardParamSummary\">{%verbatim%}{{testWizardParamService.getDefinerSummary(param.definition.element)}}{%endverbatim%}</span>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_10_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_10_definer.html.twig");
    }
}
