<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Test:wizard_section.html.twig */
class __TwigTemplate_3deb2ca656b06c424c7523b7664175d8f04b7490aedb00833bf1b924ba980d60 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:wizard_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:wizard_section.html.twig"));

        // line 2
        echo "
<div class=\"center\">
    <button ng-disabled=\"!isEditable()\" class=\"btn btn-primary\" ng-click=\"convertToR()\">";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("logic.convert.buttons.convert", [], "Test"), "html", null, true);
        echo "</button>
</div>

";
        // line 7
        echo twig_include($this->env, $context, "ConcertoPanelBundle:TestWizard:carousel.html.twig", ["wizard_mode" => "prod"]);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:wizard_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 7,  45 => 4,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Test\" %}

<div class=\"center\">
    <button ng-disabled=\"!isEditable()\" class=\"btn btn-primary\" ng-click=\"convertToR()\">{{ \"logic.convert.buttons.convert\"|trans }}</button>
</div>

{{ include(\"ConcertoPanelBundle:TestWizard:carousel.html.twig\", {'wizard_mode':'prod'}) }}", "ConcertoPanelBundle:Test:wizard_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/wizard_section.html.twig");
    }
}
