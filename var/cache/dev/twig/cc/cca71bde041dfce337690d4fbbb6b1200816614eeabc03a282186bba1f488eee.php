<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle::breadcrumbs.html.twig */
class __TwigTemplate_eeef1ea7cee5e42a8f4fc5ac8f137ce26ec0a226295396f98948ac7d08ef8e38 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::breadcrumbs.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::breadcrumbs.html.twig"));

        // line 9
        echo "
<ul class=\"breadcrumb c-breadcrumb\">
    <li ng-repeat=\"step in steps\" ng-switch=\"\$last || !!step.abstract\" ng-class=\"{active: \$last}\">
        <a ng-switch-when=\"false\" href=\"{{step.ncyBreadcrumbLink}}\">{{step.ncyBreadcrumbLabel}}</a>
        <span ng-switch-when=\"true\">{{step.ncyBreadcrumbLabel}}</span>
        <span class=\"divider\" ng-hide=\"\$last\">/</span>
    </li>
</ul>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle::breadcrumbs.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  41 => 9,);
    }

    public function getSourceContext()
    {
        return new Source("{% verbatim %}
<ul class=\"breadcrumb c-breadcrumb\">
    <li ng-repeat=\"step in steps\" ng-switch=\"\$last || !!step.abstract\" ng-class=\"{active: \$last}\">
        <a ng-switch-when=\"false\" href=\"{{step.ncyBreadcrumbLink}}\">{{step.ncyBreadcrumbLabel}}</a>
        <span ng-switch-when=\"true\">{{step.ncyBreadcrumbLabel}}</span>
        <span class=\"divider\" ng-hide=\"\$last\">/</span>
    </li>
</ul>
{% endverbatim %}", "ConcertoPanelBundle::breadcrumbs.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/breadcrumbs.html.twig");
    }
}
