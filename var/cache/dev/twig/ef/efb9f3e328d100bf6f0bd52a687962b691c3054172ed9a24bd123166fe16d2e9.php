<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Administration:form.html.twig */
class __TwigTemplate_7578fc9444906c0d8e1191d4b60aa0fe21219203bf54373855f586af4893851f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'elements' => [$this, 'block_elements'],
            'sections' => [$this, 'block_sections'],
            'floatingBarExtraButtons' => [$this, 'block_floatingBarExtraButtons'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "ConcertoPanelBundle::form_h.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        // line 2
        $context["class_name"] = "Administration";
        // line 4
        $context["exportable"] = false;
        // line 5
        $context["isAddDialog"] = false;
        // line 6
        $context["defaultButtons"] = "false";
        // line 1
        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h.html.twig", "ConcertoPanelBundle:Administration:form.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        // line 9
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title", [], "Administration"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 12
        echo "    ";
        $this->loadTemplate("ConcertoPanelBundle:Administration:form.html.twig", "ConcertoPanelBundle:Administration:form.html.twig", 12, "470260012")->display($context);
        // line 23
        echo "
    ";
        // line 24
        $this->loadTemplate("ConcertoPanelBundle:Administration:form.html.twig", "ConcertoPanelBundle:Administration:form.html.twig", 24, "1654394563")->display($context);
        // line 32
        echo "
    ";
        // line 33
        $this->loadTemplate("ConcertoPanelBundle:Administration:form.html.twig", "ConcertoPanelBundle:Administration:form.html.twig", 33, "1010180148")->display($context);
        // line 41
        echo "
    ";
        // line 42
        $this->loadTemplate("ConcertoPanelBundle:Administration:form.html.twig", "ConcertoPanelBundle:Administration:form.html.twig", 42, "718451237")->display($context);
        // line 53
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 56
    public function block_sections($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        // line 57
        echo "    <uib-accordion-group is-open=\"tabAccordion.content.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.fieldset.legend.tooltip", [], "Administration"), "html", null, true);
        echo "'\"></i>
            ";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.fieldset.legend", [], "Administration"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.content.open, 'glyphicon-chevron-right': !tabAccordion.content.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 63
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Administration:content_section.html.twig", ["class_name" => "Administration"]);
        echo "
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.messages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.fieldset.legend.tooltip", [], "Administration"), "html", null, true);
        echo "'\"></i>
            ";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.fieldset.legend", [], "Administration"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.messages.open, 'glyphicon-chevron-right': !tabAccordion.messages.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 71
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Administration:messages_section.html.twig", ["class_name" => "Administration"]);
        echo "
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.usageCharts.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("usage_charts.fieldset.legend.tooltip", [], "Administration"), "html", null, true);
        echo "'\"></i>
            ";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("usage_charts.fieldset.legend", [], "Administration"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.usageCharts.open, 'glyphicon-chevron-right': !tabAccordion.usageCharts.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 79
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Administration:usage_charts_section.html.twig", ["class_name" => "Administration"]);
        echo "
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.packages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 83
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.fieldset.legend.tooltip", [], "Administration"), "html", null, true);
        echo "'\"></i>
            ";
        // line 84
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.fieldset.legend", [], "Administration"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.packages.open, 'glyphicon-chevron-right': !tabAccordion.packages.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 87
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Administration:packages_section.html.twig", ["class_name" => "Administration"]);
        echo "
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.apiClients.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.fieldset.legend.tooltip", [], "Administration"), "html", null, true);
        echo "'\"></i>
            ";
        // line 92
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.fieldset.legend", [], "Administration"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.apiClients.open, 'glyphicon-chevron-right': !tabAccordion.apiClients.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 95
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Administration:api_clients_section.html.twig", ["class_name" => "Administration"]);
        echo "
    </uib-accordion-group>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 99
    public function block_floatingBarExtraButtons($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarExtraButtons"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarExtraButtons"));

        // line 100
        echo "    <button class='btn btn-success' ng-click=\"persistSettings();\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.save", [], "panel"), "html", null, true);
        echo "</button>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  241 => 100,  232 => 99,  219 => 95,  213 => 92,  209 => 91,  202 => 87,  196 => 84,  192 => 83,  185 => 79,  179 => 76,  175 => 75,  168 => 71,  162 => 68,  158 => 67,  151 => 63,  145 => 60,  141 => 59,  137 => 57,  128 => 56,  117 => 53,  115 => 42,  112 => 41,  110 => 33,  107 => 32,  105 => 24,  102 => 23,  99 => 12,  90 => 11,  77 => 9,  68 => 8,  57 => 1,  55 => 6,  53 => 5,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Administration\" %}
{% trans_default_domain \"Administration\" %}
{% set exportable = false %}
{% set isAddDialog = false %}
{% set defaultButtons = 'false' %}

{% block legend %}
    {{ \"form.title\"|trans }}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.home_test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.home_test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"exposedSettingsMap.home_test_id\" style=\"width:100%;\" class='form-control'>
                <option value=\"0\">{{ 'none.choosen'|trans({}, \"panel\") }}</option>
                <option value=\"{% verbatim %}{{ test.id }}{% endverbatim %}\" ng-repeat=\"test in testCollectionService.collection | orderBy: 'name'\">{% verbatim %}{{ test.name }}{% endverbatim %}</option>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.api_enabled'|trans }}{% endblock %}
        {% block tooltip %}{{ 'form.settings.api_enabled.tooltip'|trans }}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"exposedSettingsMap.api_enabled_overridable === 'false'\" ng-model=\"exposedSettingsMap.api_enabled\" ng-true-value=\"'true'\" ng-false-value=\"'false'\">
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_limit'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_limit.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"exposedSettingsMap.session_limit_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.session_limit\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_runner_service'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_runner_service.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"exposedSettingsMap.session_runner_service_overridable === 'false'\" ng-model=\"exposedSettingsMap.session_runner_service\" style=\"width:100%;\" class='form-control'>
                <option value=\"PersistantSessionRunnerService\">Persistent</option>
                <option value=\"SerializedSessionRunnerService\">Serialized</option>
            </select>
        {% endblock %}
    {% endembed %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.content.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'content.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'content.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.content.open, 'glyphicon-chevron-right': !tabAccordion.content.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:content_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.messages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'messages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'messages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.messages.open, 'glyphicon-chevron-right': !tabAccordion.messages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:messages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.usageCharts.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'usage_charts.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'usage_charts.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.usageCharts.open, 'glyphicon-chevron-right': !tabAccordion.usageCharts.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:usage_charts_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.packages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'packages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'packages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.packages.open, 'glyphicon-chevron-right': !tabAccordion.packages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:packages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.apiClients.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'api_clients.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'api_clients.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.apiClients.open, 'glyphicon-chevron-right': !tabAccordion.apiClients.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:api_clients_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarExtraButtons %}
    <button class='btn btn-success' ng-click=\"persistSettings();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:Administration:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/form.html.twig");
    }
}


/* ConcertoPanelBundle:Administration:form.html.twig */
class __TwigTemplate_7578fc9444906c0d8e1191d4b60aa0fe21219203bf54373855f586af4893851f___470260012 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Administration:form.html.twig", 12);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 14
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.settings.home_test", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.settings.home_test.tooltip", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 17
        echo "            <select ng-model=\"exposedSettingsMap.home_test_id\" style=\"width:100%;\" class='form-control'>
                <option value=\"0\">";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                <option value=\"";
        // line 19
        echo "{{ test.id }}";
        echo "\" ng-repeat=\"test in testCollectionService.collection | orderBy: 'name'\">";
        echo "{{ test.name }}";
        echo "</option>
            </select>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  470 => 19,  466 => 18,  463 => 17,  454 => 16,  436 => 15,  418 => 14,  396 => 12,  241 => 100,  232 => 99,  219 => 95,  213 => 92,  209 => 91,  202 => 87,  196 => 84,  192 => 83,  185 => 79,  179 => 76,  175 => 75,  168 => 71,  162 => 68,  158 => 67,  151 => 63,  145 => 60,  141 => 59,  137 => 57,  128 => 56,  117 => 53,  115 => 42,  112 => 41,  110 => 33,  107 => 32,  105 => 24,  102 => 23,  99 => 12,  90 => 11,  77 => 9,  68 => 8,  57 => 1,  55 => 6,  53 => 5,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Administration\" %}
{% trans_default_domain \"Administration\" %}
{% set exportable = false %}
{% set isAddDialog = false %}
{% set defaultButtons = 'false' %}

{% block legend %}
    {{ \"form.title\"|trans }}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.home_test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.home_test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"exposedSettingsMap.home_test_id\" style=\"width:100%;\" class='form-control'>
                <option value=\"0\">{{ 'none.choosen'|trans({}, \"panel\") }}</option>
                <option value=\"{% verbatim %}{{ test.id }}{% endverbatim %}\" ng-repeat=\"test in testCollectionService.collection | orderBy: 'name'\">{% verbatim %}{{ test.name }}{% endverbatim %}</option>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.api_enabled'|trans }}{% endblock %}
        {% block tooltip %}{{ 'form.settings.api_enabled.tooltip'|trans }}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"exposedSettingsMap.api_enabled_overridable === 'false'\" ng-model=\"exposedSettingsMap.api_enabled\" ng-true-value=\"'true'\" ng-false-value=\"'false'\">
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_limit'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_limit.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"exposedSettingsMap.session_limit_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.session_limit\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_runner_service'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_runner_service.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"exposedSettingsMap.session_runner_service_overridable === 'false'\" ng-model=\"exposedSettingsMap.session_runner_service\" style=\"width:100%;\" class='form-control'>
                <option value=\"PersistantSessionRunnerService\">Persistent</option>
                <option value=\"SerializedSessionRunnerService\">Serialized</option>
            </select>
        {% endblock %}
    {% endembed %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.content.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'content.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'content.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.content.open, 'glyphicon-chevron-right': !tabAccordion.content.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:content_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.messages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'messages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'messages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.messages.open, 'glyphicon-chevron-right': !tabAccordion.messages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:messages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.usageCharts.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'usage_charts.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'usage_charts.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.usageCharts.open, 'glyphicon-chevron-right': !tabAccordion.usageCharts.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:usage_charts_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.packages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'packages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'packages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.packages.open, 'glyphicon-chevron-right': !tabAccordion.packages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:packages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.apiClients.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'api_clients.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'api_clients.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.apiClients.open, 'glyphicon-chevron-right': !tabAccordion.apiClients.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:api_clients_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarExtraButtons %}
    <button class='btn btn-success' ng-click=\"persistSettings();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:Administration:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/form.html.twig");
    }
}


/* ConcertoPanelBundle:Administration:form.html.twig */
class __TwigTemplate_7578fc9444906c0d8e1191d4b60aa0fe21219203bf54373855f586af4893851f___1654394563 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 24
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Administration:form.html.twig", 24);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 26
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.settings.api_enabled", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 27
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.settings.api_enabled.tooltip", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 28
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 29
        echo "            <input type=\"checkbox\" ng-disabled=\"exposedSettingsMap.api_enabled_overridable === 'false'\" ng-model=\"exposedSettingsMap.api_enabled\" ng-true-value=\"'true'\" ng-false-value=\"'false'\">
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  694 => 29,  685 => 28,  667 => 27,  649 => 26,  627 => 24,  470 => 19,  466 => 18,  463 => 17,  454 => 16,  436 => 15,  418 => 14,  396 => 12,  241 => 100,  232 => 99,  219 => 95,  213 => 92,  209 => 91,  202 => 87,  196 => 84,  192 => 83,  185 => 79,  179 => 76,  175 => 75,  168 => 71,  162 => 68,  158 => 67,  151 => 63,  145 => 60,  141 => 59,  137 => 57,  128 => 56,  117 => 53,  115 => 42,  112 => 41,  110 => 33,  107 => 32,  105 => 24,  102 => 23,  99 => 12,  90 => 11,  77 => 9,  68 => 8,  57 => 1,  55 => 6,  53 => 5,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Administration\" %}
{% trans_default_domain \"Administration\" %}
{% set exportable = false %}
{% set isAddDialog = false %}
{% set defaultButtons = 'false' %}

{% block legend %}
    {{ \"form.title\"|trans }}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.home_test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.home_test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"exposedSettingsMap.home_test_id\" style=\"width:100%;\" class='form-control'>
                <option value=\"0\">{{ 'none.choosen'|trans({}, \"panel\") }}</option>
                <option value=\"{% verbatim %}{{ test.id }}{% endverbatim %}\" ng-repeat=\"test in testCollectionService.collection | orderBy: 'name'\">{% verbatim %}{{ test.name }}{% endverbatim %}</option>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.api_enabled'|trans }}{% endblock %}
        {% block tooltip %}{{ 'form.settings.api_enabled.tooltip'|trans }}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"exposedSettingsMap.api_enabled_overridable === 'false'\" ng-model=\"exposedSettingsMap.api_enabled\" ng-true-value=\"'true'\" ng-false-value=\"'false'\">
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_limit'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_limit.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"exposedSettingsMap.session_limit_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.session_limit\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_runner_service'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_runner_service.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"exposedSettingsMap.session_runner_service_overridable === 'false'\" ng-model=\"exposedSettingsMap.session_runner_service\" style=\"width:100%;\" class='form-control'>
                <option value=\"PersistantSessionRunnerService\">Persistent</option>
                <option value=\"SerializedSessionRunnerService\">Serialized</option>
            </select>
        {% endblock %}
    {% endembed %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.content.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'content.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'content.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.content.open, 'glyphicon-chevron-right': !tabAccordion.content.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:content_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.messages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'messages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'messages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.messages.open, 'glyphicon-chevron-right': !tabAccordion.messages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:messages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.usageCharts.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'usage_charts.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'usage_charts.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.usageCharts.open, 'glyphicon-chevron-right': !tabAccordion.usageCharts.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:usage_charts_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.packages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'packages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'packages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.packages.open, 'glyphicon-chevron-right': !tabAccordion.packages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:packages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.apiClients.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'api_clients.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'api_clients.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.apiClients.open, 'glyphicon-chevron-right': !tabAccordion.apiClients.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:api_clients_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarExtraButtons %}
    <button class='btn btn-success' ng-click=\"persistSettings();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:Administration:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/form.html.twig");
    }
}


/* ConcertoPanelBundle:Administration:form.html.twig */
class __TwigTemplate_7578fc9444906c0d8e1191d4b60aa0fe21219203bf54373855f586af4893851f___1010180148 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 33
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Administration:form.html.twig", 33);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 35
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.settings.session_limit", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.settings.session_limit.tooltip", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 37
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 38
        echo "            <input ng-disabled=\"exposedSettingsMap.session_limit_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.session_limit\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  914 => 38,  905 => 37,  887 => 36,  869 => 35,  847 => 33,  694 => 29,  685 => 28,  667 => 27,  649 => 26,  627 => 24,  470 => 19,  466 => 18,  463 => 17,  454 => 16,  436 => 15,  418 => 14,  396 => 12,  241 => 100,  232 => 99,  219 => 95,  213 => 92,  209 => 91,  202 => 87,  196 => 84,  192 => 83,  185 => 79,  179 => 76,  175 => 75,  168 => 71,  162 => 68,  158 => 67,  151 => 63,  145 => 60,  141 => 59,  137 => 57,  128 => 56,  117 => 53,  115 => 42,  112 => 41,  110 => 33,  107 => 32,  105 => 24,  102 => 23,  99 => 12,  90 => 11,  77 => 9,  68 => 8,  57 => 1,  55 => 6,  53 => 5,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Administration\" %}
{% trans_default_domain \"Administration\" %}
{% set exportable = false %}
{% set isAddDialog = false %}
{% set defaultButtons = 'false' %}

{% block legend %}
    {{ \"form.title\"|trans }}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.home_test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.home_test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"exposedSettingsMap.home_test_id\" style=\"width:100%;\" class='form-control'>
                <option value=\"0\">{{ 'none.choosen'|trans({}, \"panel\") }}</option>
                <option value=\"{% verbatim %}{{ test.id }}{% endverbatim %}\" ng-repeat=\"test in testCollectionService.collection | orderBy: 'name'\">{% verbatim %}{{ test.name }}{% endverbatim %}</option>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.api_enabled'|trans }}{% endblock %}
        {% block tooltip %}{{ 'form.settings.api_enabled.tooltip'|trans }}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"exposedSettingsMap.api_enabled_overridable === 'false'\" ng-model=\"exposedSettingsMap.api_enabled\" ng-true-value=\"'true'\" ng-false-value=\"'false'\">
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_limit'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_limit.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"exposedSettingsMap.session_limit_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.session_limit\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_runner_service'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_runner_service.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"exposedSettingsMap.session_runner_service_overridable === 'false'\" ng-model=\"exposedSettingsMap.session_runner_service\" style=\"width:100%;\" class='form-control'>
                <option value=\"PersistantSessionRunnerService\">Persistent</option>
                <option value=\"SerializedSessionRunnerService\">Serialized</option>
            </select>
        {% endblock %}
    {% endembed %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.content.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'content.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'content.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.content.open, 'glyphicon-chevron-right': !tabAccordion.content.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:content_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.messages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'messages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'messages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.messages.open, 'glyphicon-chevron-right': !tabAccordion.messages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:messages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.usageCharts.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'usage_charts.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'usage_charts.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.usageCharts.open, 'glyphicon-chevron-right': !tabAccordion.usageCharts.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:usage_charts_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.packages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'packages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'packages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.packages.open, 'glyphicon-chevron-right': !tabAccordion.packages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:packages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.apiClients.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'api_clients.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'api_clients.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.apiClients.open, 'glyphicon-chevron-right': !tabAccordion.apiClients.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:api_clients_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarExtraButtons %}
    <button class='btn btn-success' ng-click=\"persistSettings();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:Administration:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/form.html.twig");
    }
}


/* ConcertoPanelBundle:Administration:form.html.twig */
class __TwigTemplate_7578fc9444906c0d8e1191d4b60aa0fe21219203bf54373855f586af4893851f___718451237 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 42
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Administration:form.html.twig", 42);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 44
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.settings.session_runner_service", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 45
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.settings.session_runner_service.tooltip", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 46
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 47
        echo "            <select ng-disabled=\"exposedSettingsMap.session_runner_service_overridable === 'false'\" ng-model=\"exposedSettingsMap.session_runner_service\" style=\"width:100%;\" class='form-control'>
                <option value=\"PersistantSessionRunnerService\">Persistent</option>
                <option value=\"SerializedSessionRunnerService\">Serialized</option>
            </select>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1134 => 47,  1125 => 46,  1107 => 45,  1089 => 44,  1067 => 42,  914 => 38,  905 => 37,  887 => 36,  869 => 35,  847 => 33,  694 => 29,  685 => 28,  667 => 27,  649 => 26,  627 => 24,  470 => 19,  466 => 18,  463 => 17,  454 => 16,  436 => 15,  418 => 14,  396 => 12,  241 => 100,  232 => 99,  219 => 95,  213 => 92,  209 => 91,  202 => 87,  196 => 84,  192 => 83,  185 => 79,  179 => 76,  175 => 75,  168 => 71,  162 => 68,  158 => 67,  151 => 63,  145 => 60,  141 => 59,  137 => 57,  128 => 56,  117 => 53,  115 => 42,  112 => 41,  110 => 33,  107 => 32,  105 => 24,  102 => 23,  99 => 12,  90 => 11,  77 => 9,  68 => 8,  57 => 1,  55 => 6,  53 => 5,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Administration\" %}
{% trans_default_domain \"Administration\" %}
{% set exportable = false %}
{% set isAddDialog = false %}
{% set defaultButtons = 'false' %}

{% block legend %}
    {{ \"form.title\"|trans }}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.home_test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.home_test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"exposedSettingsMap.home_test_id\" style=\"width:100%;\" class='form-control'>
                <option value=\"0\">{{ 'none.choosen'|trans({}, \"panel\") }}</option>
                <option value=\"{% verbatim %}{{ test.id }}{% endverbatim %}\" ng-repeat=\"test in testCollectionService.collection | orderBy: 'name'\">{% verbatim %}{{ test.name }}{% endverbatim %}</option>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.api_enabled'|trans }}{% endblock %}
        {% block tooltip %}{{ 'form.settings.api_enabled.tooltip'|trans }}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"exposedSettingsMap.api_enabled_overridable === 'false'\" ng-model=\"exposedSettingsMap.api_enabled\" ng-true-value=\"'true'\" ng-false-value=\"'false'\">
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_limit'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_limit.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"exposedSettingsMap.session_limit_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.session_limit\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Administration\" %}
        {% block label %}{{ 'form.settings.session_runner_service'|trans }}{% endblock %}
        {% block tooltip %}{{'form.settings.session_runner_service.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"exposedSettingsMap.session_runner_service_overridable === 'false'\" ng-model=\"exposedSettingsMap.session_runner_service\" style=\"width:100%;\" class='form-control'>
                <option value=\"PersistantSessionRunnerService\">Persistent</option>
                <option value=\"SerializedSessionRunnerService\">Serialized</option>
            </select>
        {% endblock %}
    {% endembed %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.content.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'content.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'content.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.content.open, 'glyphicon-chevron-right': !tabAccordion.content.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:content_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.messages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'messages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'messages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.messages.open, 'glyphicon-chevron-right': !tabAccordion.messages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:messages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.usageCharts.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'usage_charts.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'usage_charts.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.usageCharts.open, 'glyphicon-chevron-right': !tabAccordion.usageCharts.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:usage_charts_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.packages.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'packages.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'packages.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.packages.open, 'glyphicon-chevron-right': !tabAccordion.packages.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:packages_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
    <uib-accordion-group is-open=\"tabAccordion.apiClients.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'api_clients.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'api_clients.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.apiClients.open, 'glyphicon-chevron-right': !tabAccordion.apiClients.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:Administration:api_clients_section.html.twig\", {'class_name':\"Administration\"}) }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarExtraButtons %}
    <button class='btn btn-success' ng-click=\"persistSettings();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:Administration:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/form.html.twig");
    }
}
