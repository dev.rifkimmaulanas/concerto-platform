<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Administration:api_clients_section.html.twig */
class __TwigTemplate_4277ae233dd2738f8bf05e87444a3612b7e7995ab5d4820044dbe1e0304e6866 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:api_clients_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:api_clients_section.html.twig"));

        // line 2
        echo "
<uib-alert type=\"warning\" ng-if=\"exposedSettingsMap['api_enabled'] !== 'true'\">
    <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("alerts.api_disabled", [], "Administration"), "html", null, true);
        echo "
</uib-alert>

<div class=\"center\">
    <button ng-click=\"refreshApiClients();\" class=\"btn btn-default btn-sm btn-list-refresh\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.list.buttons.refresh", [], "Administration"), "html", null, true);
        echo "</button>
    <button ng-click=\"addApiClient();\" class=\"btn btn-success btn-sm\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.list.buttons.add", [], "Administration"), "html", null, true);
        echo "</button>
    <button ng-click=\"deleteSelectedApiClients();\" class=\"btn btn-danger btn-sm\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.list.buttons.delete_selected", [], "Administration"), "html", null, true);
        echo "</button>
    <button ng-click=\"deleteAllApiClients();\" class=\"btn btn-danger btn-sm\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.list.buttons.clear", [], "Administration"), "html", null, true);
        echo "</button>
</div>
<div ui-grid=\"apiClientsOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid collectionTable\"></div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:api_clients_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 12,  61 => 11,  57 => 10,  53 => 9,  46 => 5,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Administration\" %}

<uib-alert type=\"warning\" ng-if=\"exposedSettingsMap['api_enabled'] !== 'true'\">
    <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
    {{ 'alerts.api_disabled'|trans }}
</uib-alert>

<div class=\"center\">
    <button ng-click=\"refreshApiClients();\" class=\"btn btn-default btn-sm btn-list-refresh\">{{ \"api_clients.list.buttons.refresh\"|trans }}</button>
    <button ng-click=\"addApiClient();\" class=\"btn btn-success btn-sm\">{{ \"api_clients.list.buttons.add\"|trans }}</button>
    <button ng-click=\"deleteSelectedApiClients();\" class=\"btn btn-danger btn-sm\">{{ \"api_clients.list.buttons.delete_selected\"|trans }}</button>
    <button ng-click=\"deleteAllApiClients();\" class=\"btn btn-danger btn-sm\">{{ \"api_clients.list.buttons.clear\"|trans }}</button>
</div>
<div ui-grid=\"apiClientsOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid collectionTable\"></div>", "ConcertoPanelBundle:Administration:api_clients_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/api_clients_section.html.twig");
    }
}
