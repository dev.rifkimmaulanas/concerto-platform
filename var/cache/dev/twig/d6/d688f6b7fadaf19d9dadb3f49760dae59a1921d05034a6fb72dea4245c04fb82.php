<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_12_setter.html.twig */
class __TwigTemplate_eddbcbe31d12c55f2a3ba949c580811aed32255dd9a60d5bbd0be9c5ed1122fe extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        // line 1
        echo "<script type=\"text/ng-template\" id=\"type_12_setter.html\">
    <div ng-controller=\"WizardParamSetter12Controller\">
    ";
        // line 3
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", 3, "1219520617")->display($context);
        // line 47
        echo "    </div>
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_12_setter.html\">
    <div ng-controller=\"WizardParamSetter12Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block setter %}

            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{title}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.setter.titles.column_map.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.setter.column_map.table'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.setter.column_map.table.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.table\"
                                    ng-change=\"onColumnMapTableChange()\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"table in dataTableCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" ng-value=\"table.name\" ng-bind=\"table.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr class=\"formRowV\" ng-repeat=\"col in param.definition.cols\">{% endblock%}
                        {% block label %}{%verbatim%}{{col.label}}{%endverbatim%}{% endblock%}
                        {% block tooltip %}{%verbatim%}{{col.tooltip}}{%endverbatim%}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.columns[col.name]\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"column in dataTableCollectionService.getBy('name', output.table).columns | orderBy: 'name'\" ng-value=\"column.name\" ng-bind=\"column.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_12_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_12_setter.html.twig */
class __TwigTemplate_eddbcbe31d12c55f2a3ba949c580811aed32255dd9a60d5bbd0be9c5ed1122fe___1219520617 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'setter' => [$this, 'block_setter'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_base_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_setter($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        // line 6
        echo "
            ";
        // line 7
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", 7, "1529673763")->display($context);
        // line 45
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 45,  179 => 7,  176 => 6,  167 => 5,  145 => 3,  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_12_setter.html\">
    <div ng-controller=\"WizardParamSetter12Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block setter %}

            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{title}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.setter.titles.column_map.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.setter.column_map.table'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.setter.column_map.table.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.table\"
                                    ng-change=\"onColumnMapTableChange()\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"table in dataTableCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" ng-value=\"table.name\" ng-bind=\"table.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr class=\"formRowV\" ng-repeat=\"col in param.definition.cols\">{% endblock%}
                        {% block label %}{%verbatim%}{{col.label}}{%endverbatim%}{% endblock%}
                        {% block tooltip %}{%verbatim%}{{col.tooltip}}{%endverbatim%}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.columns[col.name]\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"column in dataTableCollectionService.getBy('name', output.table).columns | orderBy: 'name'\" ng-value=\"column.name\" ng-bind=\"column.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_12_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_12_setter.html.twig */
class __TwigTemplate_eddbcbe31d12c55f2a3ba949c580811aed32255dd9a60d5bbd0be9c5ed1122fe___1529673763 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'legend_tooltip' => [$this, 'block_legend_tooltip'],
            'elements' => [$this, 'block_elements'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 7
        return "ConcertoPanelBundle::form_v_panel.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_panel.html.twig", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", 7);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo "{{title}}";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_legend_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.column_map.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 12
        echo "                    ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", 12, "1755005674")->display($context);
        // line 27
        echo "
                    ";
        // line 28
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", 28, "829929540")->display($context);
        // line 43
        echo "                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  355 => 43,  353 => 28,  350 => 27,  347 => 12,  338 => 11,  320 => 10,  302 => 9,  280 => 7,  181 => 45,  179 => 7,  176 => 6,  167 => 5,  145 => 3,  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_12_setter.html\">
    <div ng-controller=\"WizardParamSetter12Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block setter %}

            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{title}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.setter.titles.column_map.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.setter.column_map.table'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.setter.column_map.table.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.table\"
                                    ng-change=\"onColumnMapTableChange()\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"table in dataTableCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" ng-value=\"table.name\" ng-bind=\"table.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr class=\"formRowV\" ng-repeat=\"col in param.definition.cols\">{% endblock%}
                        {% block label %}{%verbatim%}{{col.label}}{%endverbatim%}{% endblock%}
                        {% block tooltip %}{%verbatim%}{{col.tooltip}}{%endverbatim%}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.columns[col.name]\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"column in dataTableCollectionService.getBy('name', output.table).columns | orderBy: 'name'\" ng-value=\"column.name\" ng-bind=\"column.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_12_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_12_setter.html.twig */
class __TwigTemplate_eddbcbe31d12c55f2a3ba949c580811aed32255dd9a60d5bbd0be9c5ed1122fe___1755005674 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", 12);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 14
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.column_map.table", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.column_map.table.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 17
        echo "                            <select ng-model=\"output.table\"
                                    ng-change=\"onColumnMapTableChange()\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                                <option ng-repeat=\"table in dataTableCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" ng-value=\"table.name\" ng-bind=\"table.name\"></option>
                            </select>
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  528 => 22,  521 => 17,  512 => 16,  494 => 15,  476 => 14,  454 => 12,  355 => 43,  353 => 28,  350 => 27,  347 => 12,  338 => 11,  320 => 10,  302 => 9,  280 => 7,  181 => 45,  179 => 7,  176 => 6,  167 => 5,  145 => 3,  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_12_setter.html\">
    <div ng-controller=\"WizardParamSetter12Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block setter %}

            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{title}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.setter.titles.column_map.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.setter.column_map.table'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.setter.column_map.table.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.table\"
                                    ng-change=\"onColumnMapTableChange()\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"table in dataTableCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" ng-value=\"table.name\" ng-bind=\"table.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr class=\"formRowV\" ng-repeat=\"col in param.definition.cols\">{% endblock%}
                        {% block label %}{%verbatim%}{{col.label}}{%endverbatim%}{% endblock%}
                        {% block tooltip %}{%verbatim%}{{col.tooltip}}{%endverbatim%}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.columns[col.name]\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"column in dataTableCollectionService.getBy('name', output.table).columns | orderBy: 'name'\" ng-value=\"column.name\" ng-bind=\"column.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_12_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_12_setter.html.twig */
class __TwigTemplate_eddbcbe31d12c55f2a3ba949c580811aed32255dd9a60d5bbd0be9c5ed1122fe___829929540 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'row' => [$this, 'block_row'],
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 28
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", 28);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 30
    public function block_row($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "row"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "row"));

        echo "<tr class=\"formRowV\" ng-repeat=\"col in param.definition.cols\">";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 31
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo "{{col.label}}";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 32
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo "{{col.tooltip}}";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 33
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 34
        echo "                            <select ng-model=\"output.columns[col.name]\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                                <option ng-repeat=\"column in dataTableCollectionService.getBy('name', output.table).columns | orderBy: 'name'\" ng-value=\"column.name\" ng-bind=\"column.name\"></option>
                            </select>
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  723 => 38,  717 => 34,  708 => 33,  690 => 32,  672 => 31,  654 => 30,  632 => 28,  528 => 22,  521 => 17,  512 => 16,  494 => 15,  476 => 14,  454 => 12,  355 => 43,  353 => 28,  350 => 27,  347 => 12,  338 => 11,  320 => 10,  302 => 9,  280 => 7,  181 => 45,  179 => 7,  176 => 6,  167 => 5,  145 => 3,  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_12_setter.html\">
    <div ng-controller=\"WizardParamSetter12Controller\">
    {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block setter %}

            {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                {% trans_default_domain \"TestWizard\" %}
                {% block legend %}{%verbatim%}{{title}}{%endverbatim%}{% endblock %}
                {% block legend_tooltip %}{{ 'param.setter.titles.column_map.tooltip'|trans }}{% endblock %}
                {% block elements %}
                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block label %}{{ 'param.setter.column_map.table'|trans }}{% endblock%}
                        {% block tooltip %}{{ 'param.setter.column_map.table.tooltip'|trans }}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.table\"
                                    ng-change=\"onColumnMapTableChange()\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"table in dataTableCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" ng-value=\"table.name\" ng-bind=\"table.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                        {% trans_default_domain \"TestWizard\" %}
                        {% block row %}<tr class=\"formRowV\" ng-repeat=\"col in param.definition.cols\">{% endblock%}
                        {% block label %}{%verbatim%}{{col.label}}{%endverbatim%}{% endblock%}
                        {% block tooltip %}{%verbatim%}{{col.tooltip}}{%endverbatim%}{% endblock%}
                        {% block control %}
                            <select ng-model=\"output.columns[col.name]\"
                                    style=\"width:100%;\"
                                    ng-disabled=\"!editable\"
                                    class='form-control'>
                                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                <option ng-repeat=\"column in dataTableCollectionService.getBy('name', output.table).columns | orderBy: 'name'\" ng-value=\"column.name\" ng-bind=\"column.name\"></option>
                            </select>
                        {% endblock %}
                    {% endembed %}
                {% endblock %}
            {% endembed %}
        {% endblock %}
    {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_12_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_12_setter.html.twig");
    }
}
