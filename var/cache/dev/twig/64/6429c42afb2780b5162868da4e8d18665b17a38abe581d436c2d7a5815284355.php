<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle::translation_injector.html.twig */
class __TwigTemplate_913b1379e5aab14dde2112c0c8e066241416e233563d83424bc991779eb52ef4 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::translation_injector.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::translation_injector.html.twig"));

        // line 2
        echo "
<script>
    function Paths() {
    }

    function Trans() {
    }

    Paths.CSS_PANEL_BUNDLE_1 = \"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/bootstrap/dist/css/bootstrap.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 10, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_2 = \"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/bootstrap/dist/css/bootstrap-theme.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 11, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_3 = \"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-ui-grid/ui-grid.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 12, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_4 = \"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-block-ui/dist/angular-block-ui.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 13, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_5 = \"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/lib/codemirror.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 14, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_6 = \"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/addon/display/fullscreen.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 15, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_7 = \"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/addon/hint/show-hint.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 16, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_9 = \"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/css/codemirror-auto-resize.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 17, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_11 = \"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/css/base.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 18, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_12 = \"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/css/helper.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 19, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_13 = \"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/css/flow.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 20, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_14 = \"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/css/file_manager.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 21, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_PANEL_BUNDLE_15 = \"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/jquery-ui/themes/base/jquery-ui.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 22, $this->source); })()))), "html", null, true);
        echo "\";


    Paths.CSS_TEST_BUNDLE_1 = \"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertotest/css/test.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 25, $this->source); })()))), "html", null, true);
        echo "\";
    Paths.CSS_TEST_BUNDLE_2 = \"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertotest/angularjs/bower_components/jquery-ui/themes/base/jquery-ui.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 26, $this->source); })()))), "html", null, true);
        echo "\";

    Paths.ENABLE_MFA = \"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("enable_mfa");
        echo "\";
    Paths.DISABLE_MFA = \"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("disable_mfa");
        echo "\";
    Paths.LOGOUT = \"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
        echo "\";
    Paths.LOCALE = \"";
        // line 31
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("locale_change", ["locale" => "{0}"]);
        echo "\";
    Paths.FILE_UPLOAD = \"";
        // line 32
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_upload");
        echo "\";
    Paths.BREADCRUMBS_TEMPLATE = \"";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("breadcrumbs_template");
        echo "\";
    Paths.FILE_UPLOAD_BROWSER = \"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_browser");
        echo "\";
    Paths.FILE_DELETE = \"";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_delete");
        echo "\";
    Paths.FILE_LIST = \"";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_list");
        echo "\";
    Paths.FILE_RENAME = \"";
        // line 37
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_rename");
        echo "\";
    Paths.FILE_COPY = \"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_copy");
        echo "\";
    Paths.FILE_MOVE = \"";
        // line 39
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_move");
        echo "\";
    Paths.FILE_EDIT = \"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_edit");
        echo "\";
    Paths.FILE_CONTENT = \"";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_content");
        echo "\";
    Paths.FILE_CREATE_DIRECTORY = \"";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_create_directory");
        echo "\";
    Paths.FILE_DOWNLOAD = \"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_download");
        echo "\";
    Paths.FILE_DOWNLOAD_MULTIPLE = \"";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_download_multiple");
        echo "\";
    Paths.FILE_COMPRESS = \"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_compress");
        echo "\";
    Paths.FILE_EXTRACT = \"";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_extract");
        echo "\";
    Paths.FILE_PERMISSIONS = \"";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("FileBrowser_permissions");
        echo "\";
    Paths.R_CACHE_DIRECTORY = \"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/concertopanel/rcache/"), "html", null, true);
        echo "\";

    Paths.TEST_WIZARD_COLLECTION = \"";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_collection");
        echo "\";
    Paths.TEST_WIZARD_DELETE = \"";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_ADD_FORM = \"";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_form", ["action" => "add"]);
        echo "\";
    Paths.TEST_WIZARD_FETCH_OBJECT = \"";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_object", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_SAVE = \"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_SAVE_NEW = \"";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_copy", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_IMPORT = \"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_import");
        echo "\";
    Paths.TEST_WIZARD_PRE_IMPORT_STATUS = \"";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_pre_import_status");
        echo "\";
    Paths.TEST_WIZARD_EXPORT_INSTRUCTIONS = \"";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_export_instructions", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_EXPORT = \"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_export", ["instructions" => "{0}", "format" => "{1}"]), "html", null, true);
        echo "\";
    Paths.TEST_WIZARD_STEP_COLLECTION = \"";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardStep_collection_by_wizard", ["wizard_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_STEP_DELETE_ALL = \"";
        // line 61
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardStep_clear", ["wizard_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_STEP_DELETE = \"";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardStep_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_STEP_SAVE = \"";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardStep_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_STEP_FETCH_OBJECT = \"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardStep_object", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_PARAM_COLLECTION = \"";
        // line 65
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardParam_collection_by_wizard", ["wizard_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_PARAM_TYPE_COLLECTION = \"";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardParam_collection_by_wizard_and_type", ["wizard_id" => "{0}", "type" => "{1}"]), "html", null, true);
        echo "\";
    Paths.TEST_WIZARD_PARAM_DELETE_ALL = \"";
        // line 67
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardParam_clear", ["wizard_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_PARAM_DELETE = \"";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardParam_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_PARAM_SAVE = \"";
        // line 69
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardParam_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_PARAM_FETCH_OBJECT = \"";
        // line 70
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizardParam_object", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_WIZARD_LOCK = \"";
        // line 71
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestWizard_toggleLock", ["object_id" => "{0}"]);
        echo "\";

    Paths.TEST_FLOW_NODE_COLLECTION = \"";
        // line 73
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNode_collection_by_flow_test", ["test_id" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_NODE_SAVE = \"";
        // line 74
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNode_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_NODE_ADD_COLLECTION = \"";
        // line 75
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_add_node", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_NODE_DELETE = \"";
        // line 76
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNode_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_NODE_MOVE = \"";
        // line 77
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_move_node");
        echo "\";
    Paths.TEST_FLOW_CONNECTION_COLLECTION = \"";
        // line 78
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNodeConnection_collection_by_flow_test", ["test_id" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_CONNECTION_SAVE = \"";
        // line 79
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNodeConnection_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_CONNECTION_ADD_COLLECTION = \"";
        // line 80
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_add_connection", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_CONNECTION_DELETE = \"";
        // line 81
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNodeConnection_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_PORT_SAVE = \"";
        // line 82
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNodePort_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_PORT_SAVE_COLLECTION = \"";
        // line 83
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNodePort_save_collection");
        echo "\";
    Paths.TEST_FLOW_NODE_PASTE_COLLECTION = \"";
        // line 84
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_paste_nodes", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_PORT_ADD_DYNAMIC = \"";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNode_add_dynamic_port", ["object_id" => "{0}", "type" => "{1}"]), "html", null, true);
        echo "\";
    Paths.TEST_FLOW_PORT_EXPOSE = \"";
        // line 86
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNode_expose_ports", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_FLOW_PORT_HIDE = \"";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestNodePort_hide", ["object_id" => "{0}"]);
        echo "\";

    Paths.TEST_COLLECTION = \"";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_collection");
        echo "\";
    Paths.TEST_DELETE = \"";
        // line 90
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_ADD_FORM = \"";
        // line 91
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_form", ["action" => "add"]);
        echo "\";
    Paths.TEST_FETCH_OBJECT = \"";
        // line 92
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_object", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_VARIABLE_FETCH_OBJECT = \"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestVariable_object", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_SAVE = \"";
        // line 94
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_SAVE_NEW = \"";
        // line 95
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_copy", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_IMPORT = \"";
        // line 96
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_import");
        echo "\";
    Paths.TEST_PRE_IMPORT_STATUS = \"";
        // line 97
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_pre_import_status");
        echo "\";
    Paths.TEST_EXPORT_INSTRUCTIONS = \"";
        // line 98
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_export_instructions", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_EXPORT = \"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_export", ["instructions" => "{0}", "format" => "{1}"]), "html", null, true);
        echo "\";
    Paths.TEST_LOG_COLLECTION = \"";
        // line 100
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestSessionLog_collection_by_test", ["test_id" => "{0}"]);
        echo "\";
    Paths.TEST_LOG_DELETE_ALL = \"";
        // line 101
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestSessionLog_clear", ["test_id" => "{0}"]);
        echo "\";
    Paths.TEST_LOG_DELETE = \"";
        // line 102
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestSessionLog_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_PARAMS_COLLECTION = \"";
        // line 103
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestVariable_parameters_collection", ["test_id" => "{0}"]);
        echo "\";
    Paths.TEST_RETURNS_COLLECTION = \"";
        // line 104
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestVariable_returns_collection", ["test_id" => "{0}"]);
        echo "\";
    Paths.TEST_BRANCHES_COLLECTION = \"";
        // line 105
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestVariable_branches_collection", ["test_id" => "{0}"]);
        echo "\";
    Paths.TEST_VARIABLE_BY_TEST_COLLECTION = \"";
        // line 106
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestVariable_by_test_collection", ["test_id" => "{0}"]);
        echo "\";
    Paths.TEST_VARIABLE_DELETE = \"";
        // line 107
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestVariable_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.TEST_VARIABLE_SAVE = \"";
        // line 108
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("TestVariable_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.TEST_RUN = \"";
        // line 109
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("test_runner_test_start", ["test_slug" => "{0}"]);
        echo "\";
    Paths.TEST_RUN_ADMIN = \"";
        // line 110
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("test_runner_protected_test_start", ["test_slug" => "{0}"]);
        echo "\";
    Paths.TEST_DEBUG = \"";
        // line 111
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("test_runner_test_start_debug", ["test_slug" => "{0}"]);
        echo "\";
    Paths.TEST_LOCK = \"";
        // line 112
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Test_toggleLock", ["object_id" => "{0}"]);
        echo "\";

    Paths.VIEW_TEMPLATE_COLLECTION = \"";
        // line 114
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_collection");
        echo "\";
    Paths.VIEW_TEMPLATE_DELETE = \"";
        // line 115
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.VIEW_TEMPLATE_ADD_FORM = \"";
        // line 116
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_form", ["action" => "add"]);
        echo "\";
    Paths.VIEW_TEMPLATE_FETCH_OBJECT = \"";
        // line 117
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_object", ["object_id" => "{0}"]);
        echo "\";
    Paths.VIEW_TEMPLATE_SAVE = \"";
        // line 118
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.VIEW_TEMPLATE_SAVE_NEW = \"";
        // line 119
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_copy", ["object_id" => "{0}"]);
        echo "\";
    Paths.VIEW_TEMPLATE_IMPORT = \"";
        // line 120
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_import");
        echo "\";
    Paths.VIEW_TEMPLATE_PRE_IMPORT_STATUS = \"";
        // line 121
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_pre_import_status");
        echo "\";
    Paths.VIEW_TEMPLATE_EXPORT_INSTRUCTIONS = \"";
        // line 122
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_export_instructions", ["object_ids" => "{0}"]);
        echo "\";
    Paths.VIEW_TEMPLATE_EXPORT = \"";
        // line 123
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_export", ["instructions" => "{0}", "format" => "{1}"]), "html", null, true);
        echo "\";
    Paths.VIEW_TEMPLATE_LOCK = \"";
        // line 124
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ViewTemplate_toggleLock", ["object_id" => "{0}"]);
        echo "\";

    Paths.DATA_TABLE_COLLECTION = \"";
        // line 126
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_collection");
        echo "\";
    Paths.DATA_TABLE_DELETE = \"";
        // line 127
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_ADD_FORM = \"";
        // line 128
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_form", ["action" => "add"]);
        echo "\";
    Paths.DATA_TABLE_FETCH_OBJECT = \"";
        // line 129
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_object", ["object_id" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_SAVE = \"";
        // line 130
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_save", ["object_id" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_SAVE_NEW = \"";
        // line 131
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_copy", ["object_id" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_IMPORT = \"";
        // line 132
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_import");
        echo "\";
    Paths.DATA_TABLE_PRE_IMPORT_STATUS = \"";
        // line 133
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_pre_import_status");
        echo "\";
    Paths.DATA_TABLE_EXPORT_INSTRUCTIONS = \"";
        // line 134
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_export_instructions", ["object_ids" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_EXPORT = \"";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_export", ["instructions" => "{0}", "format" => "{1}"]), "html", null, true);
        echo "\";
    Paths.DATA_TABLE_COLUMNS_COLLECTION = \"";
        // line 136
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_columns_collection", ["table_id" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_COLUMNS_DELETE = \"";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_column_delete", ["table_id" => "{0}", "column_names" => "{1}"]), "html", null, true);
        echo "\";
    Paths.DATA_TABLE_COLUMNS_FETCH_OBJECT = \"";
        // line 138
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_column_object", ["table_id" => "{0}", "column_name" => "{1}"]), "html", null, true);
        echo "\";
    Paths.DATA_TABLE_DATA_COLLECTION = \"";
        // line 139
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_data_collection", ["table_id" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_DATA_UPDATE = \"";
        // line 140
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_row_update", ["table_id" => "{0}", "row_id" => "{1}", "prefixed" => 0]), "html", null, true);
        echo "\";
    Paths.DATA_TABLE_DATA_INSERT = \"";
        // line 141
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_row_insert", ["table_id" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_DATA_DELETE = \"";
        // line 142
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_row_delete", ["table_id" => "{0}", "row_ids" => "{1}"]), "html", null, true);
        echo "\";
    Paths.DATA_TABLE_DATA_TRUNCATE = \"";
        // line 143
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_truncate", ["table_id" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_DATA_DELETE_ALL = \"";
        // line 144
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_deleteAll", ["table_id" => "{0}"]);
        echo "\";
    Paths.DATA_TABLE_IMPORT_CSV = \"";
        // line 145
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_csv_import", ["table_id" => "{0}", "restructure" => "{1}", "header" => "{2}", "delimiter" => "{3}", "enclosure" => "{4}"]), "html", null, true);
        echo "\";
    Paths.DATA_TABLE_COLUMNS_SAVE = \"";
        // line 146
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_column_save", ["table_id" => "{0}", "column_name" => "{1}"]), "html", null, true);
        echo "\";
    Paths.DATA_TABLE_DATA_ALL_CSV = \"";
        // line 147
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_data_collection_csv", ["table_id" => "{0}", "name" => "{1}"]), "html", null, true);
        echo "\";
    Paths.DATA_TABLE_LOCK = \"";
        // line 148
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("DataTable_toggleLock", ["object_id" => "{0}"]);
        echo "\";

    Paths.USER_COLLECTION = \"";
        // line 150
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_collection");
        echo "\";
    Paths.USER_DELETE = \"";
        // line 151
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.USER_ADD_FORM = \"";
        // line 152
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_form", ["action" => "add"]);
        echo "\";
    Paths.USER_FETCH_OBJECT = \"";
        // line 153
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_object", ["object_id" => "{0}"]);
        echo "\";
    Paths.USER_SAVE = \"";
        // line 154
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_save", ["object_id" => "{0}"]);
        echo "\";

    Paths.USERTEST_COLLECTION = \"";
        // line 156
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_test_collection");
        echo "\";
    Paths.USERTEST_DELETE = \"";
        // line 157
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_test_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.USERTEST_ADD_FORM = \"";
        // line 158
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_test_form", ["action" => "add"]);
        echo "\";
    Paths.USERTEST_FETCH_OBJECT = \"";
        // line 159
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_test_object", ["object_id" => "{0}"]);
        echo "\";
    Paths.USERTEST_SAVE = \"";
        // line 160
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("User_test_save", ["object_id" => "{0}"]);
        echo "\";

    Paths.DIALOG_TEMPLATE_ROOT = \"";
        // line 162
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Dialog_root");
        echo "\";

    Paths.ADMINISTRATION_SETTINGS_MAP = \"";
        // line 164
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("AdministrationSetting_map");
        echo "\";
    Paths.ADMINISTRATION_SETTINGS_MAP_UPDATE = \"";
        // line 165
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("AdministrationSetting_map_update");
        echo "\";
    Paths.ADMINISTRATION_SESSION_COUNT_COLLECTION = \"";
        // line 166
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_session_count_collection", ["filter" => "{0}"]);
        echo "\";
    Paths.ADMINISTRATION_SESSION_COUNT_CLEAR = \"";
        // line 167
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("AdministrationSetting_session_count_clear");
        echo "\";
    Paths.ADMINISTRATION_MESSAGES_COLLECTION = \"";
        // line 168
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_messages_collection");
        echo "\";
    Paths.ADMINISTRATION_MESSAGES_DELETE = \"";
        // line 169
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_messages_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.ADMINISTRATION_MESSAGES_CLEAR = \"";
        // line 170
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_messages_clear");
        echo "\";
    Paths.ADMINISTRATION_TASKS_COLLECTION = \"";
        // line 171
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_tasks_collection");
        echo "\";
    Paths.ADMINISTRATION_API_CLIENTS_COLLECTION = \"";
        // line 172
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_api_clients_collection");
        echo "\";
    Paths.ADMINISTRATION_API_CLIENTS_DELETE = \"";
        // line 173
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_api_clients_delete", ["object_ids" => "{0}"]);
        echo "\";
    Paths.ADMINISTRATION_API_CLIENTS_CLEAR = \"";
        // line 174
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_api_clients_clear");
        echo "\";
    Paths.ADMINISTRATION_API_CLIENTS_ADD = \"";
        // line 175
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_api_clients_add");
        echo "\";
    Paths.ADMINISTRATION_PACKAGES_STATUS = \"";
        // line 176
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_packages_status");
        echo "\";
    Paths.ADMINISTRATION_TASKS_PACKAGE_INSTALL = \"";
        // line 177
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_tasks_package_install");
        echo "\";
    Paths.ADMINISTRATION_TASKS_CONTENT_IMPORT = \"";
        // line 178
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_tasks_content_import");
        echo "\";
    Paths.ADMINISTRATION_CONTENT_EXPORT = \"";
        // line 179
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_content_export", ["instructions" => "{0}"]);
        echo "\";
    Paths.ADMINISTRATION_GET_AUTH_USER = \"";
        // line 180
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_user");
        echo "\";
    Paths.ADMINISTRATION_GIT_DISABLE = \"";
        // line 181
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_git_disable");
        echo "\";
    Paths.ADMINISTRATION_GIT_STATUS = \"";
        // line 182
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_git_status");
        echo "\";
    Paths.ADMINISTRATION_GIT_DIFF = \"";
        // line 183
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_git_diff", ["sha" => "{0}"]);
        echo "\";
    Paths.ADMINISTRATION_GIT_COMMIT = \"";
        // line 184
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_git_commit");
        echo "\";
    Paths.ADMINISTRATION_GIT_PUSH = \"";
        // line 185
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_git_push");
        echo "\";
    Paths.ADMINISTRATION_TASKS_GIT_PULL = \"";
        // line 186
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_tasks_git_pull");
        echo "\";
    Paths.ADMINISTRATION_TASKS_GIT_ENABLE = \"";
        // line 187
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_tasks_git_enable");
        echo "\";
    Paths.ADMINISTRATION_TASKS_GIT_UPDATE = \"";
        // line 188
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_tasks_git_update");
        echo "\";
    Paths.ADMINISTRATION_TASKS_GIT_RESET = \"";
        // line 189
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("Administration_tasks_git_reset");
        echo "\";

    Trans.LOCALE = \"";
        // line 191
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 191, $this->source); })()), "request", [], "any", false, false, false, 191), "locale", [], "any", false, false, false, 191), "html", null, true);
        echo "\";
    Trans.LANGUAGE = \"";
        // line 192
        echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 192, $this->source); })()), "request", [], "any", false, false, false, 192), "locale", [], "any", false, false, false, 192), 0, 2), "html", null, true);
        echo "\";
    Trans.PLEASE_WAIT = \"";
        // line 193
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("please.wait", [], "panel"), "html", null, true);
        echo "\";
    Trans.NONE = \"";
        // line 194
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none", [], "panel"), "html", null, true);
        echo "\";
    Trans.NONE_CHOOSEN = \"";
        // line 195
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "\";
    Trans.ACCESSIBILITY_PUBLIC = \"";
        // line 196
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.accessibility.public", [], "panel"), "html", null, true);
        echo "\";
    Trans.ACCESSIBILITY_GROUP = \"";
        // line 197
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.accessibility.group", [], "panel"), "html", null, true);
        echo "\";
    Trans.ACCESSIBILITY_PRIVATE = \"";
        // line 198
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.accessibility.private", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_ID = \"";
        // line 199
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.id", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_INFO = \"";
        // line 200
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.info", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_UPDATED_ON = \"";
        // line 201
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.updated.on", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_UPDATED_BY = \"";
        // line 202
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.updated.by", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_TYPE = \"";
        // line 203
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.type", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_EXISTS = \"";
        // line 204
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.exists", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_ACTION = \"";
        // line 205
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.action", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_DATA = \"";
        // line 206
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.data", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_DATA_NOT_APPLICABLE = \"";
        // line 207
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.data.not_applicable", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_DATA_LEAVE = \"";
        // line 208
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.data.leave", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_DATA_INCLUDE = \"";
        // line 209
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.data.include", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_DATA_IGNORE = \"";
        // line 210
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.data.ignore", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_DATA_REPLACE = \"";
        // line 211
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.data.replace", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_DATA_NUM = \"";
        // line 212
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.data_num", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_RENAME = \"";
        // line 213
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.rename", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_FIELD_SAFE = \"";
        // line 214
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.safe", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_EDIT = \"";
        // line 215
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.edit", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_EXPORT = \"";
        // line 216
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.export", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_DELETE = \"";
        // line 217
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.delete", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_BUTTONS_TOGGLE_FILTERS = \"";
        // line 218
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.toggle_filters", [], "panel"), "html", null, true);
        echo "\";
    Trans.LIST_LOCKED_TOOLTIP = \"";
        // line 219
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.locked.tooltip", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_TITLE_DELETE = \"";
        // line 220
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("dialog.title.delete", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_TITLE_SAVE = \"";
        // line 221
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("dialog.title.save", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_TITLE_CSV = \"";
        // line 222
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("dialog.title.csv", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_TITLE_LOCK = \"";
        // line 223
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("dialog.title.lock", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_MESSAGE_CONFIRM_DELETE = \"";
        // line 224
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("dialog.message.confirm.delete", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_MESSAGE_CONFIRM_UNSAFE_IMPORT = \"";
        // line 225
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("dialog.message.confirm.unsafe_import", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_MESSAGE_SAVED = \"";
        // line 226
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("dialog.message.saved", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_MESSAGE_FAILED = \"";
        // line 227
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("dialog.message.failed", [], "panel"), "html", null, true);
        echo "\";
    Trans.IMPORT_DIALOG_TITLE = \"";
        // line 228
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import.dialog.title", [], "panel"), "html", null, true);
        echo "\";
    Trans.EXPORT_DIALOG_TITLE = \"";
        // line 229
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("export.dialog.title", [], "panel"), "html", null, true);
        echo "\";
    Trans.EXPORT_DIALOG_EMPTY_LIST_ERROR_CONTENT = \"";
        // line 230
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("export.dialog.error.empty.content", [], "panel"), "html", null, true);
        echo "\";
    Trans.SAVE_NEW_DIALOG_TITLE = \"";
        // line 231
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("save_new.dialog.title", [], "panel"), "html", null, true);
        echo "\";
    Trans.SAVE_NEW_DIALOG_TITLE_MAIN = \"";
        // line 232
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("save_new.dialog.title_main", [], "panel"), "html", null, true);
        echo "\";
    Trans.SAVE_NEW_DIALOG_MESSAGE_COPIED = \"";
        // line 233
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("save_new.dialog.message.copied", [], "panel"), "html", null, true);
        echo "\";
    Trans.DESCRIPTION_DIALOG_TITLE = \"";
        // line 234
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("description.fieldset.legend", [], "panel"), "html", null, true);
        echo "\";
    Trans.DESCRIPTION_DIALOG_TOOLTIP = \"";
        // line 235
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("description.fieldset.legend.tooltip", [], "panel"), "html", null, true);
        echo "\";
    Trans.IMPORT_ACTION_NEW = \"";
        // line 236
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import.action.new", [], "panel"), "html", null, true);
        echo "\";
    Trans.IMPORT_ACTION_CONVERT = \"";
        // line 237
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import.action.convert", [], "panel"), "html", null, true);
        echo "\";
    Trans.IMPORT_ACTION_IGNORE = \"";
        // line 238
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import.action.ignore", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_DISABLING_MFA_TITLE = \"";
        // line 239
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("mfa.disable_dialog.title", [], "panel"), "html", null, true);
        echo "\";
    Trans.DIALOG_DISABLING_MFA_CONTENT = \"";
        // line 240
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("mfa.disable_dialog.content", [], "panel"), "html", null, true);
        echo "\";

    ";
        // line 243
        echo "    Trans.TEST_WIZARD_BREADCRUMB_LIST = \"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("breadcrumb.list", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_LIST_FIELD_NAME = \"";
        // line 244
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.name", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_LIST_FIELD_TEST = \"";
        // line 245
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.test", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_FORM_TITLE_ADD = \"";
        // line 246
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.add", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_FORM_TITLE_EDIT = \"";
        // line 247
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.edit", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_LIST_FIELD_ID = \"";
        // line 248
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.list.field.id", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_LIST_FIELD_INFO = \"";
        // line 249
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.list.field.info", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_LIST_FIELD_TITLE = \"";
        // line 250
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.list.field.title", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_LIST_FIELD_ORDER = \"";
        // line 251
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.list.field.order", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_LIST_BUTTON_DELETE = \"";
        // line 252
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.list.button.delete", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_LIST_BUTTON_EDIT = \"";
        // line 253
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.list.button.edit", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_DIALOG_TITLE_ADD = \"";
        // line 254
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.dialog.title.add", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_DIALOG_TITLE_EDIT = \"";
        // line 255
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.dialog.title.edit", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_DIALOG_TITLE_CLEAR = \"";
        // line 256
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.dialog.title.clear", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_DIALOG_MESSAGE_CLEAR_CONFIRM = \"";
        // line 257
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.dialog.message.clear.confirm", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_DIALOG_TITLE_DELETE = \"";
        // line 258
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.dialog.title.delete", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_STEP_DIALOG_MESSAGE_DELETE_CONFIRM = \"";
        // line 259
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.dialog.message.delete.confirm", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_ID = \"";
        // line 260
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.list.field.id", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_INFO = \"";
        // line 261
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.list.field.info", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_LABEL = \"";
        // line 262
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.list.field.label", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_STEP = \"";
        // line 263
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.list.field.step", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_VARIABLE = \"";
        // line 264
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.list.field.variable", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_TYPE = \"";
        // line 265
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.list.field.type", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_ORDER = \"";
        // line 266
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.list.field.order", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_BUTTON_DELETE = \"";
        // line 267
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.list.button.delete", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_BUTTON_EDIT = \"";
        // line 268
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.list.button.edit", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DIALOG_TITLE_ADD = \"";
        // line 269
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.dialog.title.add", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DIALOG_TITLE_EDIT = \"";
        // line 270
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.dialog.title.edit", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DIALOG_TITLE_CLEAR = \"";
        // line 271
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.dialog.title.clear", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DIALOG_MESSAGE_CLEAR_CONFIRM = \"";
        // line 272
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.dialog.message.clear.confirm", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DIALOG_TITLE_DELETE = \"";
        // line 273
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.dialog.title.delete", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DIALOG_MESSAGE_DELETE_CONFIRM = \"";
        // line 274
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.dialog.message.delete.confirm", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_SINGLE_LINE_TEXT = \"";
        // line 275
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.single_line_text", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_MULTI_LINE_TEXT = \"";
        // line 276
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.multi_line_text", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_HTML = \"";
        // line 277
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.html", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_SELECT = \"";
        // line 278
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.select", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_VIEW = \"";
        // line 279
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.view", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_TEST = \"";
        // line 280
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.test", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_TABLE = \"";
        // line 281
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.table", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_COLUMN = \"";
        // line 282
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.column", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_CHECKBOX = \"";
        // line 283
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.checkbox", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_GROUP = \"";
        // line 284
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.group", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_LIST = \"";
        // line 285
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.list", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_R = \"";
        // line 286
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.r", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_COLUMN_MAP = \"";
        // line 287
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.column_map", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_TYPE_WIZARD = \"";
        // line 288
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.type.wizard", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SELECT_LIST_FIELD_ORDER = \"";
        // line 289
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.list.field.order", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SELECT_LIST_FIELD_LABEL = \"";
        // line 290
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.list.field.label", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SELECT_LIST_FIELD_VALUE = \"";
        // line 291
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.list.field.value", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SELECT_LIST_BUTTON_DELETE = \"";
        // line 292
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.list.button.delete", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_COLUMN_MAP_LIST_FIELD_NAME = \"";
        // line 293
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.column_map.list.field.name", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_COLUMN_MAP_LIST_FIELD_LABEL = \"";
        // line 294
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.column_map.list.field.label", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_COLUMN_MAP_LIST_FIELD_TOOLTIP = \"";
        // line 295
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.column_map.list.field.tooltip", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_COLUMN_MAP_LIST_BUTTON_DELETE = \"";
        // line 296
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.column_map.list.button.delete", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_NAME = \"";
        // line 297
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.group.list.field.name", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_LABEL = \"";
        // line 298
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.group.list.field.label", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_TYPE = \"";
        // line 299
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.group.list.field.type", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_HIDE_CONDITION = \"";
        // line 300
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.group.list.field.hide_condition", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_DEFINITION = \"";
        // line 301
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.group.list.field.definition", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_ORDER = \"";
        // line 302
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.group.list.field.order", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_BUTTON_DELETE = \"";
        // line 303
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.list.button.delete", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINITION_ICON_TOOLTIP = \"";
        // line 304
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.definition.icon.tooltip", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_ELEMENT_DELETE = \"";
        // line 305
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.list.element.remove", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_LIST_COLUMN_ELEMENT = \"";
        // line 306
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.list.column.element", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_SELECT = \"";
        // line 307
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.select", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_GROUP = \"";
        // line 308
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.group", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_LIST = \"";
        // line 309
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.list", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_SINGLE_LINE = \"";
        // line 310
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.single_line", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_MULTI_LINE = \"";
        // line 311
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.multi_line", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_HTML = \"";
        // line 312
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.html", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_CHECKBOX = \"";
        // line 313
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.checkbox", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_TEST = \"";
        // line 314
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.test", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_TABLE = \"";
        // line 315
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.table", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_TEMPLATE = \"";
        // line 316
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.template", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_R_CODE = \"";
        // line 317
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.r_code", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_COLUMN_MAP = \"";
        // line 318
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.column_map", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_WIZARD = \"";
        // line 319
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.wizard", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_TEXTAREA = \"";
        // line 320
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.textarea", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_HTML = \"";
        // line 321
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.html", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_R = \"";
        // line 322
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.r", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_COLUMN = \"";
        // line 323
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.column", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_GROUP = \"";
        // line 324
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.group", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_LIST = \"";
        // line 325
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.list", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_COLUMN_MAP = \"";
        // line 326
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.column_map", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_WIZARD = \"";
        // line 327
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.wizard", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_SUMMARIES_SELECT = \"";
        // line 328
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.summaries.select", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_SUMMARIES_GROUP = \"";
        // line 329
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.summaries.group", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_SUMMARIES_LIST = \"";
        // line 330
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.summaries.list", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_DEFINER_SUMMARIES_COLUMN_MAP = \"";
        // line 331
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.summaries.column_map", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_TEXTAREA = \"";
        // line 332
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.summaries.textarea", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_HTML = \"";
        // line 333
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.summaries.html", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_COLUMN = \"";
        // line 334
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.summaries.column", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_GROUP = \"";
        // line 335
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.summaries.group", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_LIST = \"";
        // line 336
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.summaries.list", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_R = \"";
        // line 337
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.summaries.r", [], "TestWizard"), "html", null, true);
        echo "\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_COLUMN_MAP = \"";
        // line 338
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.summaries.column_map", [], "TestWizard"), "html", null, true);
        echo "\";

    ";
        // line 341
        echo "    Trans.TEST_BUTTON_RUN = \"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.run", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_BREADCRUMB_LIST = \"";
        // line 342
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("breadcrumb.list", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LIST_FIELD_NAME = \"";
        // line 343
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.name", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LIST_FIELD_SLUG = \"";
        // line 344
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.slug", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LIST_FIELD_WIZARD = \"";
        // line 345
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.wizard", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LIST_FIELD_WIZARD_SOURCE = \"";
        // line 346
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.wizard.source", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FORM_TITLE_ADD = \"";
        // line 347
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.add", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FORM_TITLE_EDIT = \"";
        // line 348
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.edit", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_LIST_FIELD_DATE = \"";
        // line 349
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.field.date", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_LIST_FIELD_BROWSER = \"";
        // line 350
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.field.browser", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_LIST_FIELD_IP = \"";
        // line 351
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.field.ip", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_LIST_FIELD_MESSAGE = \"";
        // line 352
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.field.message", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_LIST_FIELD_TYPE = \"";
        // line 353
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.field.type", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_LIST_FIELD_TYPE_R = \"";
        // line 354
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.field.type.R", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_LIST_FIELD_TYPE_JAVASCRIPT = \"";
        // line 355
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.field.type.javascript", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_LIST_BUTTON_DELETE = \"";
        // line 356
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.button.delete", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_DIALOG_TITLE_ADD = \"";
        // line 357
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.dialog.title.add", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_DIALOG_TITLE_EDIT = \"";
        // line 358
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.dialog.title.edit", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_DIALOG_TITLE_DELETE = \"";
        // line 359
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.dialog.title.delete", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_DIALOG_MESSAGE_DELETE_CONFIRM = \"";
        // line 360
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.dialog.message.delete.confirm", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_INFO = \"";
        // line 361
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.list.field.info", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_NAME = \"";
        // line 362
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.list.field.name", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_URL = \"";
        // line 363
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.list.field.url", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_URL_YES = \"";
        // line 364
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.list.field.url.yes", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_URL_NO = \"";
        // line 365
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.list.field.url.no", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_VALUE = \"";
        // line 366
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.list.field.value", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_LIST_EDIT = \"";
        // line 367
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.list.edit", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_PARAMS_LIST_DELETE = \"";
        // line 368
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.parameters.list.delete", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_RETURNS_DIALOG_TITLE_ADD = \"";
        // line 369
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.dialog.title.add", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_RETURNS_DIALOG_TITLE_EDIT = \"";
        // line 370
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.dialog.title.edit", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_RETURNS_DIALOG_TITLE_DELETE = \"";
        // line 371
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.dialog.title.delete", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_RETURNS_DIALOG_MESSAGE_DELETE_CONFIRM = \"";
        // line 372
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.dialog.message.delete.confirm", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_RETURNS_LIST_FIELD_INFO = \"";
        // line 373
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.list.field.info", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_RETURNS_LIST_FIELD_NAME = \"";
        // line 374
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.list.field.name", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_RETURNS_LIST_FIELD_VALUE = \"";
        // line 375
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.list.field.value", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_RETURNS_LIST_EDIT = \"";
        // line 376
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.list.edit", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_RETURNS_LIST_DELETE = \"";
        // line 377
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.list.delete", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_BRANCHES_DIALOG_TITLE_ADD = \"";
        // line 378
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.dialog.title.add", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_BRANCHES_DIALOG_TITLE_EDIT = \"";
        // line 379
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.dialog.title.edit", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_BRANCHES_DIALOG_TITLE_DELETE = \"";
        // line 380
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.dialog.title.delete", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_BRANCHES_DIALOG_MESSAGE_DELETE_CONFIRM = \"";
        // line 381
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.dialog.message.delete.confirm", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_BRANCHES_LIST_FIELD_INFO = \"";
        // line 382
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.list.field.info", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_BRANCHES_LIST_FIELD_NAME = \"";
        // line 383
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.list.field.name", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_BRANCHES_LIST_FIELD_VALUE = \"";
        // line 384
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.list.field.value", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_BRANCHES_LIST_EDIT = \"";
        // line 385
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.list.edit", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_VARS_BRANCHES_LIST_DELETE = \"";
        // line 386
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.list.delete", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FORM_FIELD_VISIBILITY_REGULAR = \"";
        // line 387
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.visibility.regular", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FORM_FIELD_VISIBILITY_FEATURED = \"";
        // line 388
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.visibility.featured", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FORM_FIELD_VISIBILITY_SUBTEST = \"";
        // line 389
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.visibility.subtest", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FORM_FIELD_TYPE_CODE = \"";
        // line 390
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.type.code", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FORM_FIELD_TYPE_WIZARD = \"";
        // line 391
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.type.wizard", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FORM_FIELD_TYPE_FLOW = \"";
        // line 392
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.type.flow", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_DIALOG_TITLE_CLEAR = \"";
        // line 393
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.dialog.title.clear", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_DIALOG_MESSAGE_CLEAR_CONFIRM = \"";
        // line 394
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.dialog.message.clear.confirm", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_DIALOG_TITLE_DELETE = \"";
        // line 395
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.dialog.title.delete", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOG_DIALOG_MESSAGE_DELETE_CONFIRM = \"";
        // line 396
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.dialog.message.delete.confirm", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOGIC_CONVERT_TITLE = \"";
        // line 397
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("logic.convert.title", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_LOGIC_CONVERT_CONFIRMATION = \"";
        // line 398
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("logic.convert.confirmation", [], "Test"), "html", null, true);
        echo "\";

    Trans.TEST_FLOW_NODE_NAME_START = \"";
        // line 400
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.names.test_start", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_NODE_NAME_END = \"";
        // line 401
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.names.test_end", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_NODE_DESCRIPTION_START = \"";
        // line 402
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.descriptions.test_start", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_NODE_DESCRIPTION_END = \"";
        // line 403
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.descriptions.test_end", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_NAME_IN = \"";
        // line 404
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.names.in", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DESCRIPTION_IN = \"";
        // line 405
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.descriptions.in", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_NAME_OUT = \"";
        // line 406
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.names.out", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DESCRIPTION_OUT = \"";
        // line 407
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.descriptions.out", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_ADD_INPUT = \"";
        // line 408
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.add.input", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_ADD_BRANCH = \"";
        // line 409
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.add.branch", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_ADD_RETURN = \"";
        // line 410
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.add.return", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_BUTTONS_NODE_MENU = \"";
        // line 411
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.buttons.node_menu", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_DIALOG_NODE_REMOVE_TITLE = \"";
        // line 412
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.dialog.delete.title", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_DIALOG_NODE_REMOVE_MESSAGE = \"";
        // line 413
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.dialog.delete.message", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_DIALOG_NODE_REMOVE_SELECTION_TITLE = \"";
        // line 414
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.dialog.selection_delete.title", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_DIALOG_NODE_REMOVE_SELECTION_MESSAGE = \"";
        // line 415
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.dialog.selection_delete.message", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_DIALOG_NODE_EDIT_TITLE_TITLE = \"";
        // line 416
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.dialog.title.title", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_DIALOG_NODE_EDIT_TITLE_TOOLTIP = \"";
        // line 417
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.dialog.title.tooltip", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_DIALOG_NODE_EDIT_TITLE = \"";
        // line 418
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.nodes.dialog.edit.title", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_DIALOG_NODE_INPUT_ADD_TITLE = \"";
        // line 419
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.dialog.titles.input.add", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DIALOG_TITLE_REMOVE_INPUT = \"";
        // line 420
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.dialog.titles.input.remove", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DIALOG_CONTENT_REMOVE_INPUT = \"";
        // line 421
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.dialog.content.input.remove", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DIALOG_TITLE_REMOVE_BRANCH = \"";
        // line 422
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.dialog.titles.branch.remove", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DIALOG_CONTENT_REMOVE_BRANCH = \"";
        // line 423
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.dialog.content.branch.remove", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DIALOG_TITLE_REMOVE_RETURN = \"";
        // line 424
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.dialog.titles.return.remove", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DIALOG_CONTENT_REMOVE_RETURN = \"";
        // line 425
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.dialog.content.return.remove", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DIALOG_TITLE_REMOVE_ALL_CONNECTIONS = \"";
        // line 426
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.dialog.title.port.remove_all_connections", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_PORT_DIALOG_CONTENT_REMOVE_ALL_CONNECTIONS = \"";
        // line 427
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.ports.dialog.content.port.remove_all_connections", [], "Test"), "html", null, true);
        echo "\";
    Trans.TEST_FLOW_DIALOG_CONNECTION_EDIT_TITLE = \"";
        // line 428
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.connections.dialog.edit.title", [], "Test"), "html", null, true);
        echo "\";

    ";
        // line 431
        echo "    Trans.VIEW_TEMPLATE_BREADCRUMB_LIST = \"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("breadcrumb.list", [], "ViewTemplate"), "html", null, true);
        echo "\";
    Trans.VIEW_TEMPLATE_LIST_FIELD_NAME = \"";
        // line 432
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.name", [], "ViewTemplate"), "html", null, true);
        echo "\";
    Trans.VIEW_TEMPLATE_FORM_TITLE_ADD = \"";
        // line 433
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.add", [], "ViewTemplate"), "html", null, true);
        echo "\";
    Trans.VIEW_TEMPLATE_FORM_TITLE_EDIT = \"";
        // line 434
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.edit", [], "ViewTemplate"), "html", null, true);
        echo "\";

    ";
        // line 437
        echo "    Trans.DATA_TABLE_BREADCRUMB_LIST = \"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("breadcrumb.list", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_LIST_FIELD_NAME = \"";
        // line 438
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.name", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_FORM_TITLE_ADD = \"";
        // line 439
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.add", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_FORM_TITLE_EDIT = \"";
        // line 440
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.edit", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_DATA_LIST_DELETE = \"";
        // line 441
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("data.list.delete", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_LIST_FIELD_NAME = \"";
        // line 442
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.list.field.name", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_LIST_FIELD_TYPE = \"";
        // line 443
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.list.field.type", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_LIST_FIELD_LENGTH = \"";
        // line 444
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.list.field.length", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_LIST_FIELD_NULLABLE = \"";
        // line 445
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.list.field.nullable", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_LIST_EDIT = \"";
        // line 446
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.list.edit", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_LIST_DELETE = \"";
        // line 447
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.list.delete", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_DATA_DIALOG_TITLE_DELETE = \"";
        // line 448
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("data.dialog.title.delete", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_DATA_DIALOG_TITLE_EDIT = \"";
        // line 449
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("data.dialog.title.edit", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_DATA_DIALOG_MESSAGE_CONFIRM_DELETE = \"";
        // line 450
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("data.dialog.message.confirm.delete", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_DIALOG_TITLE_DELETE = \"";
        // line 451
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.dialog.title.delete", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_DIALOG_MESSAGE_CONFIRM_DELETE = \"";
        // line 452
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.dialog.message.confirm.delete", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_IO_DIALOG_TITLE_IMPORT = \"";
        // line 453
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("io.dialog.title.import", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_IO_DIALOG_MESSAGE_IMPORTED = \"";
        // line 454
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("io.dialog.message.imported", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_IO_DIALOG_MESSAGE_ERROR = \"";
        // line 455
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("io.dialog.message.error", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_DIALOG_TITLE_ADD = \"";
        // line 456
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.dialog.title.add", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_STRUCTURE_DIALOG_TITLE_EDIT = \"";
        // line 457
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.dialog.title.edit", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_CELL_TEXT_EDIT_TITLE = \"";
        // line 458
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("data.cell.text.fieldset.legend", [], "DataTable"), "html", null, true);
        echo "\";
    Trans.DATA_TABLE_CELL_TEXT_EDIT_TOOLTIP = \"";
        // line 459
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("data.cell.text.fieldset.legend.tooltip", [], "DataTable"), "html", null, true);
        echo "\";

    ";
        // line 462
        echo "    Trans.USER_BREADCRUMB_LIST = \"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("breadcrumb.list", [], "User"), "html", null, true);
        echo "\";
    Trans.USER_LIST_FIELD_USERNAME = \"";
        // line 463
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.username", [], "User"), "html", null, true);
        echo "\";
    Trans.USER_LIST_FIELD_EMAIL = \"";
        // line 464
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.email", [], "User"), "html", null, true);
        echo "\";
    Trans.USER_FORM_TITLE_ADD = \"";
        // line 465
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.add", [], "User"), "html", null, true);
        echo "\";
    Trans.USER_FORM_TITLE_EDIT = \"";
        // line 466
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.edit", [], "User"), "html", null, true);
        echo "\";

    ";
        // line 469
        echo "    Trans.USERTEST_BREADCRUMB_LIST = \"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("breadcrumb.list", [], "UserTest"), "html", null, true);
        echo "\";
    Trans.USERTEST_LIST_FIELD_USERNAME = \"";
        // line 470
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.username", [], "UserTest"), "html", null, true);
        echo "\";
    Trans.USERTEST_LIST_FIELD_EMAIL = \"";
        // line 471
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.field.email", [], "UserTest"), "html", null, true);
        echo "\";
    Trans.USERTEST_FORM_TITLE_ADD = \"";
        // line 472
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.add", [], "UserTest"), "html", null, true);
        echo "\";
    Trans.USERTEST_FORM_TITLE_EDIT = \"";
        // line 473
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.title.edit", [], "UserTest"), "html", null, true);
        echo "\";

    ";
        // line 476
        echo "    Trans.FILE_BROWSER_BREADCRUMB_FILES = \"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("breadcrumb.files", [], "FileBrowser"), "html", null, true);
        echo "\";
    Trans.FILE_BROWSER_ALERT_UPLOAD_FAILED_TITLE = \"";
        // line 477
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("uploader.alerts.upload_failed.title", [], "FileBrowser"), "html", null, true);
        echo "\";
    Trans.FILE_BROWSER_ALERT_UPLOAD_FAILED_MESSAGE = \"";
        // line 478
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("uploader.alerts.upload_failed.message", [], "FileBrowser"), "html", null, true);
        echo "\";

    ";
        // line 481
        echo "    Trans.ADMINISTRATION_BREADCRUMB = \"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("breadcrumb", [], "Administration"), "html", null, true);
        echo "\";
    Trans.ADMINISTRATION_DIALOG_TITLE_CLEAR = \"";
        // line 482
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("usage_charts.dialog.title.clear", [], "Administration"), "html", null, true);
        echo "\";
    Trans.ADMINISTRATION_DIALOG_CONFIRM_CLEAR = \"";
        // line 483
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("usage_charts.dialog.confirm.clear", [], "Administration"), "html", null, true);
        echo "\";
    Trans.ADMINISTRATION_USAGE_DATA_FILTER_TODAY = \"";
        // line 484
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("usage_charts.filter.today", [], "Administration"), "html", null, true);
        echo "\";
    Trans.ADMINISTRATION_USAGE_DATA_FILTER_SPECIFIC_DATE = \"";
        // line 485
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("usage_charts.filter.specific_date", [], "Administration"), "html", null, true);
        echo "\";
    Trans.ADMINISTRATION_USAGE_DATA_FILTER_DATE_RANGE = \"";
        // line 486
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("usage_charts.filter.date_range", [], "Administration"), "html", null, true);
        echo "\";
    Trans.ADMINISTRATION_VERSION_NONE = \"";
        // line 487
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.version_none", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_FIELD_TIME = \"";
        // line 488
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.fields.time", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY = \"";
        // line 489
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.fields.category", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_SYSTEM = \"";
        // line 490
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.fields.category.system", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_GLOBAL = \"";
        // line 491
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.fields.category.global", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_LOCAL = \"";
        // line 492
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.fields.category.local", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_TEST = \"";
        // line 493
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.fields.category.test", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_CHANGELOG = \"";
        // line 494
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.fields.category.changelog", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_FIELD_SUBJECT = \"";
        // line 495
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.fields.subject", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_FIELD_MESSAGE = \"";
        // line 496
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.fields.message", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_LIST_BUTTONS_DELETE = \"";
        // line 497
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.list.buttons.delete", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_DIALOGS_TITLE_DELETE = \"";
        // line 498
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.dialogs.title.delete", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_DIALOGS_MESSAGE_DELETE = \"";
        // line 499
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.dialogs.message.delete", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_DIALOGS_TITLE_CLEAR = \"";
        // line 500
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.dialogs.title.clear", [], "Administration"), "html", null, true);
        echo "\";
    Trans.MESSAGES_DIALOGS_MESSAGE_CLEAR = \"";
        // line 501
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("messages.dialogs.message.clear", [], "Administration"), "html", null, true);
        echo "\";
    Trans.TASKS_LIST_FIELD_UPDATED = \"";
        // line 502
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.list.fields.updated", [], "Administration"), "html", null, true);
        echo "\";
    Trans.TASKS_LIST_FIELD_STATUS = \"";
        // line 503
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.list.fields.status", [], "Administration"), "html", null, true);
        echo "\";
    Trans.TASKS_LIST_FIELD_STATUS_PENDING = \"";
        // line 504
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.list.fields.status.pending", [], "Administration"), "html", null, true);
        echo "\";
    Trans.TASKS_LIST_FIELD_STATUS_ONGOING = \"";
        // line 505
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.list.fields.status.ongoing", [], "Administration"), "html", null, true);
        echo "\";
    Trans.TASKS_LIST_FIELD_STATUS_COMPLETED = \"";
        // line 506
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.list.fields.status.completed", [], "Administration"), "html", null, true);
        echo "\";
    Trans.TASKS_LIST_FIELD_STATUS_FAILED = \"";
        // line 507
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.list.fields.status.failed", [], "Administration"), "html", null, true);
        echo "\";
    Trans.TASKS_LIST_FIELD_STATUS_CANCELED = \"";
        // line 508
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.list.fields.status.canceled", [], "Administration"), "html", null, true);
        echo "\";
    Trans.TASKS_LIST_FIELD_DESCRIPTION = \"";
        // line 509
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.list.fields.description", [], "Administration"), "html", null, true);
        echo "\";
    Trans.TASKS_LIST_FIELD_OUTPUT = \"";
        // line 510
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tasks.list.fields.output", [], "Administration"), "html", null, true);
        echo "\";
    Trans.API_CLIENTS_LIST_FIELD_ID = \"";
        // line 511
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.list.fields.id", [], "Administration"), "html", null, true);
        echo "\";
    Trans.API_CLIENTS_LIST_FIELD_SECRET = \"";
        // line 512
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.list.fields.secret", [], "Administration"), "html", null, true);
        echo "\";
    Trans.API_CLIENTS_LIST_BUTTONS_DELETE = \"";
        // line 513
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.list.buttons.delete", [], "Administration"), "html", null, true);
        echo "\";
    Trans.API_CLIENTS_DIALOGS_TITLE_DELETE = \"";
        // line 514
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.dialogs.title.delete", [], "Administration"), "html", null, true);
        echo "\";
    Trans.API_CLIENTS_DIALOGS_MESSAGE_DELETE = \"";
        // line 515
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.dialogs.message.delete", [], "Administration"), "html", null, true);
        echo "\";
    Trans.API_CLIENTS_DIALOGS_TITLE_CLEAR = \"";
        // line 516
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.dialogs.title.clear", [], "Administration"), "html", null, true);
        echo "\";
    Trans.API_CLIENTS_DIALOGS_MESSAGE_CLEAR = \"";
        // line 517
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("api_clients.dialogs.message.clear", [], "Administration"), "html", null, true);
        echo "\";
    Trans.PACKAGES_DIALOG_TITLE_REPORT = \"";
        // line 518
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.dialog.title.report", [], "Administration"), "html", null, true);
        echo "\";
    Trans.PACKAGES_DIALOG_TITLE_REPORT_TOOLTIP = \"";
        // line 519
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.dialog.title.report.tooltip", [], "Administration"), "html", null, true);
        echo "\";
    Trans.PACKAGES_DIALOG_CONTENT_REPORT_FAILED = \"";
        // line 520
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.dialog.content.report_failed", [], "Administration"), "html", null, true);
        echo "\";
    Trans.PACKAGES_DIALOG_FIELDS_METHOD_LATEST = \"";
        // line 521
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.dialog.fields.method.latest", [], "Administration"), "html", null, true);
        echo "\";
    Trans.PACKAGES_DIALOG_FIELDS_METHOD_SPECIFIC = \"";
        // line 522
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.dialog.fields.method.specific", [], "Administration"), "html", null, true);
        echo "\";
    Trans.PACKAGES_DIALOG_TITLE_INSTALLATION_FAILED = \"";
        // line 523
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.dialog.title.installation_failed", [], "Administration"), "html", null, true);
        echo "\";
    Trans.CONTENT_IMPORT_FROM_FILE = \"";
        // line 524
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.import_from_file", [], "Administration"), "html", null, true);
        echo "\";
    Trans.CONTENT_IMPORT_FROM_URL = \"";
        // line 525
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.import_from_url", [], "Administration"), "html", null, true);
        echo "\";
    Trans.CONTENT_IMPORTING_CONTENT = \"";
        // line 526
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.importing_content", [], "Administration"), "html", null, true);
        echo "\";
    Trans.CONTENT_IMPORT_PROMPT = \"";
        // line 527
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.import_prompt", [], "Administration"), "html", null, true);
        echo "\";
    Trans.CONTENT_IMPORT_FAILURE = \"";
        // line 528
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.import_failure", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_ENABLE_TITLE = \"";
        // line 529
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.enable.title", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_DISABLE_TITLE = \"";
        // line 530
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.disable.title", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_DISABLE_CONFIRM = \"";
        // line 531
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.disable.confirm", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_DIFF_SHA = \"";
        // line 532
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.diff.sha", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_DIFF_LOCAL = \"";
        // line 533
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.diff.local", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_REFRESH_TITLE = \"";
        // line 534
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.refresh.title", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_COMMIT_SUCCESS = \"";
        // line 535
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.commit.success", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_COMMIT_FAILURE = \"";
        // line 536
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.commit.failure", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_RESET_TITLE = \"";
        // line 537
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.reset.title", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_RESET_CONFIRM = \"";
        // line 538
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.reset.confirm", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_PUSH_TITLE = \"";
        // line 539
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.push.title", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_PUSH_CONFIRM = \"";
        // line 540
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.push.confirm", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_PUSH_FAILURE = \"";
        // line 541
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.push.failure", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_PULL_TITLE = \"";
        // line 542
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.pull.title", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_PULL_CONFIRM = \"";
        // line 543
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.pull.confirm", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_PULL_FAILURE = \"";
        // line 544
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.pull.failure", [], "Administration"), "html", null, true);
        echo "\";
    Trans.GIT_UPDATE_TITLE = \"";
        // line 545
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.update.title", [], "Administration"), "html", null, true);
        echo "\";
</script>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle::translation_injector.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2101 => 545,  2097 => 544,  2093 => 543,  2089 => 542,  2085 => 541,  2081 => 540,  2077 => 539,  2073 => 538,  2069 => 537,  2065 => 536,  2061 => 535,  2057 => 534,  2053 => 533,  2049 => 532,  2045 => 531,  2041 => 530,  2037 => 529,  2033 => 528,  2029 => 527,  2025 => 526,  2021 => 525,  2017 => 524,  2013 => 523,  2009 => 522,  2005 => 521,  2001 => 520,  1997 => 519,  1993 => 518,  1989 => 517,  1985 => 516,  1981 => 515,  1977 => 514,  1973 => 513,  1969 => 512,  1965 => 511,  1961 => 510,  1957 => 509,  1953 => 508,  1949 => 507,  1945 => 506,  1941 => 505,  1937 => 504,  1933 => 503,  1929 => 502,  1925 => 501,  1921 => 500,  1917 => 499,  1913 => 498,  1909 => 497,  1905 => 496,  1901 => 495,  1897 => 494,  1893 => 493,  1889 => 492,  1885 => 491,  1881 => 490,  1877 => 489,  1873 => 488,  1869 => 487,  1865 => 486,  1861 => 485,  1857 => 484,  1853 => 483,  1849 => 482,  1844 => 481,  1839 => 478,  1835 => 477,  1830 => 476,  1825 => 473,  1821 => 472,  1817 => 471,  1813 => 470,  1808 => 469,  1803 => 466,  1799 => 465,  1795 => 464,  1791 => 463,  1786 => 462,  1781 => 459,  1777 => 458,  1773 => 457,  1769 => 456,  1765 => 455,  1761 => 454,  1757 => 453,  1753 => 452,  1749 => 451,  1745 => 450,  1741 => 449,  1737 => 448,  1733 => 447,  1729 => 446,  1725 => 445,  1721 => 444,  1717 => 443,  1713 => 442,  1709 => 441,  1705 => 440,  1701 => 439,  1697 => 438,  1692 => 437,  1687 => 434,  1683 => 433,  1679 => 432,  1674 => 431,  1669 => 428,  1665 => 427,  1661 => 426,  1657 => 425,  1653 => 424,  1649 => 423,  1645 => 422,  1641 => 421,  1637 => 420,  1633 => 419,  1629 => 418,  1625 => 417,  1621 => 416,  1617 => 415,  1613 => 414,  1609 => 413,  1605 => 412,  1601 => 411,  1597 => 410,  1593 => 409,  1589 => 408,  1585 => 407,  1581 => 406,  1577 => 405,  1573 => 404,  1569 => 403,  1565 => 402,  1561 => 401,  1557 => 400,  1552 => 398,  1548 => 397,  1544 => 396,  1540 => 395,  1536 => 394,  1532 => 393,  1528 => 392,  1524 => 391,  1520 => 390,  1516 => 389,  1512 => 388,  1508 => 387,  1504 => 386,  1500 => 385,  1496 => 384,  1492 => 383,  1488 => 382,  1484 => 381,  1480 => 380,  1476 => 379,  1472 => 378,  1468 => 377,  1464 => 376,  1460 => 375,  1456 => 374,  1452 => 373,  1448 => 372,  1444 => 371,  1440 => 370,  1436 => 369,  1432 => 368,  1428 => 367,  1424 => 366,  1420 => 365,  1416 => 364,  1412 => 363,  1408 => 362,  1404 => 361,  1400 => 360,  1396 => 359,  1392 => 358,  1388 => 357,  1384 => 356,  1380 => 355,  1376 => 354,  1372 => 353,  1368 => 352,  1364 => 351,  1360 => 350,  1356 => 349,  1352 => 348,  1348 => 347,  1344 => 346,  1340 => 345,  1336 => 344,  1332 => 343,  1328 => 342,  1323 => 341,  1318 => 338,  1314 => 337,  1310 => 336,  1306 => 335,  1302 => 334,  1298 => 333,  1294 => 332,  1290 => 331,  1286 => 330,  1282 => 329,  1278 => 328,  1274 => 327,  1270 => 326,  1266 => 325,  1262 => 324,  1258 => 323,  1254 => 322,  1250 => 321,  1246 => 320,  1242 => 319,  1238 => 318,  1234 => 317,  1230 => 316,  1226 => 315,  1222 => 314,  1218 => 313,  1214 => 312,  1210 => 311,  1206 => 310,  1202 => 309,  1198 => 308,  1194 => 307,  1190 => 306,  1186 => 305,  1182 => 304,  1178 => 303,  1174 => 302,  1170 => 301,  1166 => 300,  1162 => 299,  1158 => 298,  1154 => 297,  1150 => 296,  1146 => 295,  1142 => 294,  1138 => 293,  1134 => 292,  1130 => 291,  1126 => 290,  1122 => 289,  1118 => 288,  1114 => 287,  1110 => 286,  1106 => 285,  1102 => 284,  1098 => 283,  1094 => 282,  1090 => 281,  1086 => 280,  1082 => 279,  1078 => 278,  1074 => 277,  1070 => 276,  1066 => 275,  1062 => 274,  1058 => 273,  1054 => 272,  1050 => 271,  1046 => 270,  1042 => 269,  1038 => 268,  1034 => 267,  1030 => 266,  1026 => 265,  1022 => 264,  1018 => 263,  1014 => 262,  1010 => 261,  1006 => 260,  1002 => 259,  998 => 258,  994 => 257,  990 => 256,  986 => 255,  982 => 254,  978 => 253,  974 => 252,  970 => 251,  966 => 250,  962 => 249,  958 => 248,  954 => 247,  950 => 246,  946 => 245,  942 => 244,  937 => 243,  932 => 240,  928 => 239,  924 => 238,  920 => 237,  916 => 236,  912 => 235,  908 => 234,  904 => 233,  900 => 232,  896 => 231,  892 => 230,  888 => 229,  884 => 228,  880 => 227,  876 => 226,  872 => 225,  868 => 224,  864 => 223,  860 => 222,  856 => 221,  852 => 220,  848 => 219,  844 => 218,  840 => 217,  836 => 216,  832 => 215,  828 => 214,  824 => 213,  820 => 212,  816 => 211,  812 => 210,  808 => 209,  804 => 208,  800 => 207,  796 => 206,  792 => 205,  788 => 204,  784 => 203,  780 => 202,  776 => 201,  772 => 200,  768 => 199,  764 => 198,  760 => 197,  756 => 196,  752 => 195,  748 => 194,  744 => 193,  740 => 192,  736 => 191,  731 => 189,  727 => 188,  723 => 187,  719 => 186,  715 => 185,  711 => 184,  707 => 183,  703 => 182,  699 => 181,  695 => 180,  691 => 179,  687 => 178,  683 => 177,  679 => 176,  675 => 175,  671 => 174,  667 => 173,  663 => 172,  659 => 171,  655 => 170,  651 => 169,  647 => 168,  643 => 167,  639 => 166,  635 => 165,  631 => 164,  626 => 162,  621 => 160,  617 => 159,  613 => 158,  609 => 157,  605 => 156,  600 => 154,  596 => 153,  592 => 152,  588 => 151,  584 => 150,  579 => 148,  575 => 147,  571 => 146,  567 => 145,  563 => 144,  559 => 143,  555 => 142,  551 => 141,  547 => 140,  543 => 139,  539 => 138,  535 => 137,  531 => 136,  527 => 135,  523 => 134,  519 => 133,  515 => 132,  511 => 131,  507 => 130,  503 => 129,  499 => 128,  495 => 127,  491 => 126,  486 => 124,  482 => 123,  478 => 122,  474 => 121,  470 => 120,  466 => 119,  462 => 118,  458 => 117,  454 => 116,  450 => 115,  446 => 114,  441 => 112,  437 => 111,  433 => 110,  429 => 109,  425 => 108,  421 => 107,  417 => 106,  413 => 105,  409 => 104,  405 => 103,  401 => 102,  397 => 101,  393 => 100,  389 => 99,  385 => 98,  381 => 97,  377 => 96,  373 => 95,  369 => 94,  365 => 93,  361 => 92,  357 => 91,  353 => 90,  349 => 89,  344 => 87,  340 => 86,  336 => 85,  332 => 84,  328 => 83,  324 => 82,  320 => 81,  316 => 80,  312 => 79,  308 => 78,  304 => 77,  300 => 76,  296 => 75,  292 => 74,  288 => 73,  283 => 71,  279 => 70,  275 => 69,  271 => 68,  267 => 67,  263 => 66,  259 => 65,  255 => 64,  251 => 63,  247 => 62,  243 => 61,  239 => 60,  235 => 59,  231 => 58,  227 => 57,  223 => 56,  219 => 55,  215 => 54,  211 => 53,  207 => 52,  203 => 51,  199 => 50,  194 => 48,  190 => 47,  186 => 46,  182 => 45,  178 => 44,  174 => 43,  170 => 42,  166 => 41,  162 => 40,  158 => 39,  154 => 38,  150 => 37,  146 => 36,  142 => 35,  138 => 34,  134 => 33,  130 => 32,  126 => 31,  122 => 30,  118 => 29,  114 => 28,  109 => 26,  105 => 25,  99 => 22,  95 => 21,  91 => 20,  87 => 19,  83 => 18,  79 => 17,  75 => 16,  71 => 15,  67 => 14,  63 => 13,  59 => 12,  55 => 11,  51 => 10,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"panel\" %}

<script>
    function Paths() {
    }

    function Trans() {
    }

    Paths.CSS_PANEL_BUNDLE_1 = \"{{ asset('bundles/concertopanel/angularjs/bower_components/bootstrap/dist/css/bootstrap.min.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_2 = \"{{ asset('bundles/concertopanel/angularjs/bower_components/bootstrap/dist/css/bootstrap-theme.min.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_3 = \"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-ui-grid/ui-grid.min.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_4 = \"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-block-ui/dist/angular-block-ui.min.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_5 = \"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/lib/codemirror.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_6 = \"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/addon/display/fullscreen.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_7 = \"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/addon/hint/show-hint.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_9 = \"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/css/codemirror-auto-resize.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_11 = \"{{ asset('bundles/concertopanel/css/base.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_12 = \"{{ asset('bundles/concertopanel/css/helper.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_13 = \"{{ asset('bundles/concertopanel/css/flow.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_14 = \"{{ asset('bundles/concertopanel/css/file_manager.css?v=' ~ version) }}\";
    Paths.CSS_PANEL_BUNDLE_15 = \"{{ asset('bundles/concertopanel/angularjs/bower_components/jquery-ui/themes/base/jquery-ui.min.css?v=' ~ version) }}\";


    Paths.CSS_TEST_BUNDLE_1 = \"{{ asset('bundles/concertotest/css/test.css?v=' ~ version) }}\";
    Paths.CSS_TEST_BUNDLE_2 = \"{{ asset('bundles/concertotest/angularjs/bower_components/jquery-ui/themes/base/jquery-ui.min.css?v=' ~ version) }}\";

    Paths.ENABLE_MFA = \"{{ path(\"enable_mfa\") }}\";
    Paths.DISABLE_MFA = \"{{ path(\"disable_mfa\") }}\";
    Paths.LOGOUT = \"{{ path(\"logout\") }}\";
    Paths.LOCALE = \"{{ path(\"locale_change\", {'locale':\"{0}\"}) }}\";
    Paths.FILE_UPLOAD = \"{{ path(\"FileBrowser_upload\") }}\";
    Paths.BREADCRUMBS_TEMPLATE = \"{{ path(\"breadcrumbs_template\") }}\";
    Paths.FILE_UPLOAD_BROWSER = \"{{ path(\"FileBrowser_browser\") }}\";
    Paths.FILE_DELETE = \"{{ path(\"FileBrowser_delete\") }}\";
    Paths.FILE_LIST = \"{{ path(\"FileBrowser_list\") }}\";
    Paths.FILE_RENAME = \"{{ path(\"FileBrowser_rename\") }}\";
    Paths.FILE_COPY = \"{{ path(\"FileBrowser_copy\") }}\";
    Paths.FILE_MOVE = \"{{ path(\"FileBrowser_move\") }}\";
    Paths.FILE_EDIT = \"{{ path(\"FileBrowser_edit\") }}\";
    Paths.FILE_CONTENT = \"{{ path(\"FileBrowser_content\") }}\";
    Paths.FILE_CREATE_DIRECTORY = \"{{ path(\"FileBrowser_create_directory\") }}\";
    Paths.FILE_DOWNLOAD = \"{{ path(\"FileBrowser_download\") }}\";
    Paths.FILE_DOWNLOAD_MULTIPLE = \"{{ path(\"FileBrowser_download_multiple\") }}\";
    Paths.FILE_COMPRESS = \"{{ path(\"FileBrowser_compress\") }}\";
    Paths.FILE_EXTRACT = \"{{ path(\"FileBrowser_extract\") }}\";
    Paths.FILE_PERMISSIONS = \"{{ path(\"FileBrowser_permissions\") }}\";
    Paths.R_CACHE_DIRECTORY = \"{{ asset('bundles/concertopanel/rcache/') }}\";

    Paths.TEST_WIZARD_COLLECTION = \"{{ path(\"TestWizard_collection\") }}\";
    Paths.TEST_WIZARD_DELETE = \"{{ path(\"TestWizard_delete\", {'object_ids':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_ADD_FORM = \"{{ path(\"TestWizard_form\",{'action':\"add\"}) }}\";
    Paths.TEST_WIZARD_FETCH_OBJECT = \"{{ path(\"TestWizard_object\",{'object_id':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_SAVE = \"{{ path(\"TestWizard_save\",{'object_id':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_SAVE_NEW = \"{{ path(\"TestWizard_copy\",{'object_id':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_IMPORT = \"{{ path(\"TestWizard_import\") }}\";
    Paths.TEST_WIZARD_PRE_IMPORT_STATUS = \"{{ path(\"TestWizard_pre_import_status\") }}\";
    Paths.TEST_WIZARD_EXPORT_INSTRUCTIONS = \"{{ path(\"TestWizard_export_instructions\",{'object_ids':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_EXPORT = \"{{ path(\"TestWizard_export\",{'instructions':\"{0}\", 'format':\"{1}\"}) }}\";
    Paths.TEST_WIZARD_STEP_COLLECTION = \"{{ path(\"TestWizardStep_collection_by_wizard\", {'wizard_id':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_STEP_DELETE_ALL = \"{{ path(\"TestWizardStep_clear\",{'wizard_id':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_STEP_DELETE = \"{{ path(\"TestWizardStep_delete\",{'object_ids':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_STEP_SAVE = \"{{ path(\"TestWizardStep_save\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_WIZARD_STEP_FETCH_OBJECT = \"{{ path(\"TestWizardStep_object\",{'object_id':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_PARAM_COLLECTION = \"{{ path(\"TestWizardParam_collection_by_wizard\", {'wizard_id':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_PARAM_TYPE_COLLECTION = \"{{ path(\"TestWizardParam_collection_by_wizard_and_type\", {'wizard_id':\"{0}\", 'type':\"{1}\"}) }}\";
    Paths.TEST_WIZARD_PARAM_DELETE_ALL = \"{{ path(\"TestWizardParam_clear\",{'wizard_id':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_PARAM_DELETE = \"{{ path(\"TestWizardParam_delete\",{'object_ids':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_PARAM_SAVE = \"{{ path(\"TestWizardParam_save\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_WIZARD_PARAM_FETCH_OBJECT = \"{{ path(\"TestWizardParam_object\",{'object_id':\"{0}\"}) }}\";
    Paths.TEST_WIZARD_LOCK = \"{{ path(\"TestWizard_toggleLock\",{'object_id':\"{0}\"}) }}\";

    Paths.TEST_FLOW_NODE_COLLECTION = \"{{ path(\"TestNode_collection_by_flow_test\", {'test_id':\"{0}\"}) }}\";
    Paths.TEST_FLOW_NODE_SAVE = \"{{ path(\"TestNode_save\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_FLOW_NODE_ADD_COLLECTION = \"{{ path(\"Test_add_node\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_FLOW_NODE_DELETE = \"{{ path(\"TestNode_delete\", {\"object_ids\":\"{0}\"}) }}\";
    Paths.TEST_FLOW_NODE_MOVE = \"{{ path(\"Test_move_node\") }}\";
    Paths.TEST_FLOW_CONNECTION_COLLECTION = \"{{ path(\"TestNodeConnection_collection_by_flow_test\", {'test_id':\"{0}\"}) }}\";
    Paths.TEST_FLOW_CONNECTION_SAVE = \"{{ path(\"TestNodeConnection_save\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_FLOW_CONNECTION_ADD_COLLECTION = \"{{ path(\"Test_add_connection\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_FLOW_CONNECTION_DELETE = \"{{ path(\"TestNodeConnection_delete\", {\"object_ids\":\"{0}\"}) }}\";
    Paths.TEST_FLOW_PORT_SAVE = \"{{ path(\"TestNodePort_save\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_FLOW_PORT_SAVE_COLLECTION = \"{{ path(\"TestNodePort_save_collection\") }}\";
    Paths.TEST_FLOW_NODE_PASTE_COLLECTION = \"{{ path(\"Test_paste_nodes\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_FLOW_PORT_ADD_DYNAMIC = \"{{ path(\"TestNode_add_dynamic_port\", {\"object_id\":\"{0}\", \"type\":\"{1}\"}) }}\";
    Paths.TEST_FLOW_PORT_EXPOSE = \"{{ path(\"TestNode_expose_ports\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_FLOW_PORT_HIDE = \"{{ path(\"TestNodePort_hide\", {\"object_id\":\"{0}\"}) }}\";

    Paths.TEST_COLLECTION = \"{{ path(\"Test_collection\") }}\";
    Paths.TEST_DELETE = \"{{ path(\"Test_delete\", {'object_ids':\"{0}\"}) }}\";
    Paths.TEST_ADD_FORM = \"{{ path(\"Test_form\",{'action':\"add\"}) }}\";
    Paths.TEST_FETCH_OBJECT = \"{{ path(\"Test_object\",{'object_id':\"{0}\"}) }}\";
    Paths.TEST_VARIABLE_FETCH_OBJECT = \"{{ path(\"TestVariable_object\",{'object_id':\"{0}\"}) }}\";
    Paths.TEST_SAVE = \"{{ path(\"Test_save\",{'object_id':\"{0}\"}) }}\";
    Paths.TEST_SAVE_NEW = \"{{ path(\"Test_copy\",{'object_id':\"{0}\"}) }}\";
    Paths.TEST_IMPORT = \"{{ path(\"Test_import\") }}\";
    Paths.TEST_PRE_IMPORT_STATUS = \"{{ path(\"Test_pre_import_status\") }}\";
    Paths.TEST_EXPORT_INSTRUCTIONS = \"{{ path(\"Test_export_instructions\",{'object_ids':\"{0}\"}) }}\";
    Paths.TEST_EXPORT = \"{{ path(\"Test_export\",{'instructions':\"{0}\", 'format':\"{1}\"}) }}\";
    Paths.TEST_LOG_COLLECTION = \"{{ path(\"TestSessionLog_collection_by_test\", {'test_id':\"{0}\"}) }}\";
    Paths.TEST_LOG_DELETE_ALL = \"{{ path(\"TestSessionLog_clear\",{'test_id':\"{0}\"}) }}\";
    Paths.TEST_LOG_DELETE = \"{{ path(\"TestSessionLog_delete\",{'object_ids':\"{0}\"}) }}\";
    Paths.TEST_PARAMS_COLLECTION = \"{{ path(\"TestVariable_parameters_collection\",{'test_id':\"{0}\"}) }}\";
    Paths.TEST_RETURNS_COLLECTION = \"{{ path(\"TestVariable_returns_collection\",{'test_id':\"{0}\"}) }}\";
    Paths.TEST_BRANCHES_COLLECTION = \"{{ path(\"TestVariable_branches_collection\",{'test_id':\"{0}\"}) }}\";
    Paths.TEST_VARIABLE_BY_TEST_COLLECTION = \"{{ path(\"TestVariable_by_test_collection\", {'test_id':\"{0}\"}) }}\";
    Paths.TEST_VARIABLE_DELETE = \"{{ path(\"TestVariable_delete\", {\"object_ids\":\"{0}\"}) }}\";
    Paths.TEST_VARIABLE_SAVE = \"{{ path(\"TestVariable_save\", {\"object_id\":\"{0}\"}) }}\";
    Paths.TEST_RUN = \"{{ path(\"test_runner_test_start\",{\"test_slug\": \"{0}\" }) }}\";
    Paths.TEST_RUN_ADMIN = \"{{ path(\"test_runner_protected_test_start\",{\"test_slug\": \"{0}\" }) }}\";
    Paths.TEST_DEBUG = \"{{ path(\"test_runner_test_start_debug\",{\"test_slug\":\"{0}\"}) }}\";
    Paths.TEST_LOCK = \"{{ path(\"Test_toggleLock\",{'object_id':\"{0}\"}) }}\";

    Paths.VIEW_TEMPLATE_COLLECTION = \"{{ path(\"ViewTemplate_collection\") }}\";
    Paths.VIEW_TEMPLATE_DELETE = \"{{ path(\"ViewTemplate_delete\", {'object_ids':\"{0}\"}) }}\";
    Paths.VIEW_TEMPLATE_ADD_FORM = \"{{ path(\"ViewTemplate_form\",{'action':\"add\"}) }}\";
    Paths.VIEW_TEMPLATE_FETCH_OBJECT = \"{{ path(\"ViewTemplate_object\",{'object_id':\"{0}\"}) }}\";
    Paths.VIEW_TEMPLATE_SAVE = \"{{ path(\"ViewTemplate_save\",{'object_id':\"{0}\"}) }}\";
    Paths.VIEW_TEMPLATE_SAVE_NEW = \"{{ path(\"ViewTemplate_copy\",{'object_id':\"{0}\"}) }}\";
    Paths.VIEW_TEMPLATE_IMPORT = \"{{ path(\"ViewTemplate_import\") }}\";
    Paths.VIEW_TEMPLATE_PRE_IMPORT_STATUS = \"{{ path(\"ViewTemplate_pre_import_status\") }}\";
    Paths.VIEW_TEMPLATE_EXPORT_INSTRUCTIONS = \"{{ path(\"ViewTemplate_export_instructions\",{'object_ids':\"{0}\"}) }}\";
    Paths.VIEW_TEMPLATE_EXPORT = \"{{ path(\"ViewTemplate_export\",{'instructions':\"{0}\", 'format':\"{1}\"}) }}\";
    Paths.VIEW_TEMPLATE_LOCK = \"{{ path(\"ViewTemplate_toggleLock\",{'object_id':\"{0}\"}) }}\";

    Paths.DATA_TABLE_COLLECTION = \"{{ path(\"DataTable_collection\") }}\";
    Paths.DATA_TABLE_DELETE = \"{{ path(\"DataTable_delete\", {'object_ids':\"{0}\"}) }}\";
    Paths.DATA_TABLE_ADD_FORM = \"{{ path(\"DataTable_form\",{'action':\"add\"}) }}\";
    Paths.DATA_TABLE_FETCH_OBJECT = \"{{ path(\"DataTable_object\",{'object_id':\"{0}\"}) }}\";
    Paths.DATA_TABLE_SAVE = \"{{ path(\"DataTable_save\",{'object_id':\"{0}\"}) }}\";
    Paths.DATA_TABLE_SAVE_NEW = \"{{ path(\"DataTable_copy\",{'object_id':\"{0}\"}) }}\";
    Paths.DATA_TABLE_IMPORT = \"{{ path(\"DataTable_import\") }}\";
    Paths.DATA_TABLE_PRE_IMPORT_STATUS = \"{{ path(\"DataTable_pre_import_status\") }}\";
    Paths.DATA_TABLE_EXPORT_INSTRUCTIONS = \"{{ path(\"DataTable_export_instructions\",{'object_ids':\"{0}\"}) }}\";
    Paths.DATA_TABLE_EXPORT = \"{{ path(\"DataTable_export\",{'instructions':\"{0}\", 'format':\"{1}\"}) }}\";
    Paths.DATA_TABLE_COLUMNS_COLLECTION = \"{{ path(\"DataTable_columns_collection\", {'table_id':\"{0}\"}) }}\";
    Paths.DATA_TABLE_COLUMNS_DELETE = \"{{ path(\"DataTable_column_delete\", {\"table_id\":\"{0}\", \"column_names\":\"{1}\"}) }}\";
    Paths.DATA_TABLE_COLUMNS_FETCH_OBJECT = \"{{ path(\"DataTable_column_object\", {\"table_id\":\"{0}\", \"column_name\":\"{1}\"}) }}\";
    Paths.DATA_TABLE_DATA_COLLECTION = \"{{ path(\"DataTable_data_collection\", {'table_id':\"{0}\"}) }}\";
    Paths.DATA_TABLE_DATA_UPDATE = \"{{ path(\"DataTable_row_update\",{'table_id':\"{0}\",'row_id':\"{1}\", 'prefixed':0}) }}\";
    Paths.DATA_TABLE_DATA_INSERT = \"{{ path(\"DataTable_row_insert\",{'table_id':\"{0}\"}) }}\";
    Paths.DATA_TABLE_DATA_DELETE = \"{{ path(\"DataTable_row_delete\",{'table_id':\"{0}\",\"row_ids\":\"{1}\"}) }}\";
    Paths.DATA_TABLE_DATA_TRUNCATE = \"{{ path(\"DataTable_truncate\",{'table_id':\"{0}\"}) }}\";
    Paths.DATA_TABLE_DATA_DELETE_ALL = \"{{ path(\"DataTable_deleteAll\",{'table_id':\"{0}\"}) }}\";
    Paths.DATA_TABLE_IMPORT_CSV = \"{{ path(\"DataTable_csv_import\",{'table_id':\"{0}\", 'restructure':\"{1}\", 'header':\"{2}\", 'delimiter':\"{3}\", 'enclosure':\"{4}\"}) }}\";
    Paths.DATA_TABLE_COLUMNS_SAVE = \"{{ path(\"DataTable_column_save\", {\"table_id\":\"{0}\", \"column_name\":\"{1}\"}) }}\";
    Paths.DATA_TABLE_DATA_ALL_CSV = \"{{ path(\"DataTable_data_collection_csv\", {'table_id':\"{0}\", 'name':\"{1}\"}) }}\";
    Paths.DATA_TABLE_LOCK = \"{{ path(\"DataTable_toggleLock\",{'object_id':\"{0}\"}) }}\";

    Paths.USER_COLLECTION = \"{{ path(\"User_collection\") }}\";
    Paths.USER_DELETE = \"{{ path(\"User_delete\", {'object_ids':\"{0}\"}) }}\";
    Paths.USER_ADD_FORM = \"{{ path(\"User_form\",{'action':\"add\"}) }}\";
    Paths.USER_FETCH_OBJECT = \"{{ path(\"User_object\",{'object_id':\"{0}\"}) }}\";
    Paths.USER_SAVE = \"{{ path(\"User_save\",{'object_id':\"{0}\"}) }}\";

    Paths.USERTEST_COLLECTION = \"{{ path(\"User_test_collection\") }}\";
    Paths.USERTEST_DELETE = \"{{ path(\"User_test_delete\", {'object_ids':\"{0}\"}) }}\";
    Paths.USERTEST_ADD_FORM = \"{{ path(\"User_test_form\",{'action':\"add\"}) }}\";
    Paths.USERTEST_FETCH_OBJECT = \"{{ path(\"User_test_object\",{'object_id':\"{0}\"}) }}\";
    Paths.USERTEST_SAVE = \"{{ path(\"User_test_save\",{'object_id':\"{0}\"}) }}\";

    Paths.DIALOG_TEMPLATE_ROOT = \"{{ path(\"Dialog_root\") }}\";

    Paths.ADMINISTRATION_SETTINGS_MAP = \"{{ path(\"AdministrationSetting_map\") }}\";
    Paths.ADMINISTRATION_SETTINGS_MAP_UPDATE = \"{{ path(\"AdministrationSetting_map_update\") }}\";
    Paths.ADMINISTRATION_SESSION_COUNT_COLLECTION = \"{{ path(\"Administration_session_count_collection\", {'filter':\"{0}\"}) }}\";
    Paths.ADMINISTRATION_SESSION_COUNT_CLEAR = \"{{ path(\"AdministrationSetting_session_count_clear\") }}\";
    Paths.ADMINISTRATION_MESSAGES_COLLECTION = \"{{ path(\"Administration_messages_collection\") }}\";
    Paths.ADMINISTRATION_MESSAGES_DELETE = \"{{ path(\"Administration_messages_delete\", {'object_ids':\"{0}\"}) }}\";
    Paths.ADMINISTRATION_MESSAGES_CLEAR = \"{{ path(\"Administration_messages_clear\") }}\";
    Paths.ADMINISTRATION_TASKS_COLLECTION = \"{{ path(\"Administration_tasks_collection\") }}\";
    Paths.ADMINISTRATION_API_CLIENTS_COLLECTION = \"{{ path(\"Administration_api_clients_collection\") }}\";
    Paths.ADMINISTRATION_API_CLIENTS_DELETE = \"{{ path(\"Administration_api_clients_delete\", {'object_ids':\"{0}\"}) }}\";
    Paths.ADMINISTRATION_API_CLIENTS_CLEAR = \"{{ path(\"Administration_api_clients_clear\") }}\";
    Paths.ADMINISTRATION_API_CLIENTS_ADD = \"{{ path(\"Administration_api_clients_add\") }}\";
    Paths.ADMINISTRATION_PACKAGES_STATUS = \"{{ path(\"Administration_packages_status\") }}\";
    Paths.ADMINISTRATION_TASKS_PACKAGE_INSTALL = \"{{ path(\"Administration_tasks_package_install\") }}\";
    Paths.ADMINISTRATION_TASKS_CONTENT_IMPORT = \"{{ path(\"Administration_tasks_content_import\") }}\";
    Paths.ADMINISTRATION_CONTENT_EXPORT = \"{{ path(\"Administration_content_export\", {'instructions':\"{0}\"}) }}\";
    Paths.ADMINISTRATION_GET_AUTH_USER = \"{{ path(\"Administration_user\") }}\";
    Paths.ADMINISTRATION_GIT_DISABLE = \"{{ path(\"Administration_git_disable\") }}\";
    Paths.ADMINISTRATION_GIT_STATUS = \"{{ path(\"Administration_git_status\") }}\";
    Paths.ADMINISTRATION_GIT_DIFF = \"{{ path(\"Administration_git_diff\", {'sha':\"{0}\"}) }}\";
    Paths.ADMINISTRATION_GIT_COMMIT = \"{{ path(\"Administration_git_commit\") }}\";
    Paths.ADMINISTRATION_GIT_PUSH = \"{{ path(\"Administration_git_push\") }}\";
    Paths.ADMINISTRATION_TASKS_GIT_PULL = \"{{ path(\"Administration_tasks_git_pull\") }}\";
    Paths.ADMINISTRATION_TASKS_GIT_ENABLE = \"{{ path(\"Administration_tasks_git_enable\") }}\";
    Paths.ADMINISTRATION_TASKS_GIT_UPDATE = \"{{ path(\"Administration_tasks_git_update\") }}\";
    Paths.ADMINISTRATION_TASKS_GIT_RESET = \"{{ path(\"Administration_tasks_git_reset\") }}\";

    Trans.LOCALE = \"{{ app.request.locale }}\";
    Trans.LANGUAGE = \"{{ app.request.locale[:2] }}\";
    Trans.PLEASE_WAIT = \"{{ \"please.wait\"|trans }}\";
    Trans.NONE = \"{{ \"none\"|trans }}\";
    Trans.NONE_CHOOSEN = \"{{ \"none.choosen\"|trans }}\";
    Trans.ACCESSIBILITY_PUBLIC = \"{{ \"list.accessibility.public\"|trans }}\";
    Trans.ACCESSIBILITY_GROUP = \"{{ \"list.accessibility.group\"|trans }}\";
    Trans.ACCESSIBILITY_PRIVATE = \"{{ \"list.accessibility.private\"|trans }}\";
    Trans.LIST_FIELD_ID = \"{{ \"list.field.id\"|trans }}\";
    Trans.LIST_FIELD_INFO = \"{{ \"list.field.info\"|trans }}\";
    Trans.LIST_FIELD_UPDATED_ON = \"{{ \"list.field.updated.on\"|trans }}\";
    Trans.LIST_FIELD_UPDATED_BY = \"{{ \"list.field.updated.by\"|trans }}\";
    Trans.LIST_FIELD_TYPE = \"{{ \"list.field.type\"|trans }}\";
    Trans.LIST_FIELD_EXISTS = \"{{ \"list.field.exists\"|trans }}\";
    Trans.LIST_FIELD_ACTION = \"{{ \"list.field.action\"|trans }}\";
    Trans.LIST_FIELD_DATA = \"{{ \"list.field.data\"|trans }}\";
    Trans.LIST_FIELD_DATA_NOT_APPLICABLE = \"{{ \"list.field.data.not_applicable\"|trans }}\";
    Trans.LIST_FIELD_DATA_LEAVE = \"{{ \"list.field.data.leave\"|trans }}\";
    Trans.LIST_FIELD_DATA_INCLUDE = \"{{ \"list.field.data.include\"|trans }}\";
    Trans.LIST_FIELD_DATA_IGNORE = \"{{ \"list.field.data.ignore\"|trans }}\";
    Trans.LIST_FIELD_DATA_REPLACE = \"{{ \"list.field.data.replace\"|trans }}\";
    Trans.LIST_FIELD_DATA_NUM = \"{{ \"list.field.data_num\"|trans }}\";
    Trans.LIST_FIELD_RENAME = \"{{ \"list.field.rename\"|trans }}\";
    Trans.LIST_FIELD_SAFE = \"{{ \"list.field.safe\"|trans }}\";
    Trans.LIST_EDIT = \"{{ \"list.edit\"|trans }}\";
    Trans.LIST_EXPORT = \"{{ \"list.export\"|trans }}\";
    Trans.LIST_DELETE = \"{{ \"list.delete\"|trans }}\";
    Trans.LIST_BUTTONS_TOGGLE_FILTERS = \"{{ 'list.button.toggle_filters'|trans }}\";
    Trans.LIST_LOCKED_TOOLTIP = \"{{ 'list.locked.tooltip'|trans }}\";
    Trans.DIALOG_TITLE_DELETE = \"{{ 'dialog.title.delete'|trans }}\";
    Trans.DIALOG_TITLE_SAVE = \"{{ 'dialog.title.save'|trans }}\";
    Trans.DIALOG_TITLE_CSV = \"{{ 'dialog.title.csv'|trans }}\";
    Trans.DIALOG_TITLE_LOCK = \"{{ 'dialog.title.lock'|trans }}\";
    Trans.DIALOG_MESSAGE_CONFIRM_DELETE = \"{{ 'dialog.message.confirm.delete'|trans }}\";
    Trans.DIALOG_MESSAGE_CONFIRM_UNSAFE_IMPORT = \"{{ 'dialog.message.confirm.unsafe_import'|trans }}\";
    Trans.DIALOG_MESSAGE_SAVED = \"{{ 'dialog.message.saved'|trans }}\";
    Trans.DIALOG_MESSAGE_FAILED = \"{{ 'dialog.message.failed'|trans }}\";
    Trans.IMPORT_DIALOG_TITLE = \"{{ 'import.dialog.title'|trans }}\";
    Trans.EXPORT_DIALOG_TITLE = \"{{ 'export.dialog.title'|trans }}\";
    Trans.EXPORT_DIALOG_EMPTY_LIST_ERROR_CONTENT = \"{{ 'export.dialog.error.empty.content'|trans }}\";
    Trans.SAVE_NEW_DIALOG_TITLE = \"{{ 'save_new.dialog.title'|trans }}\";
    Trans.SAVE_NEW_DIALOG_TITLE_MAIN = \"{{ 'save_new.dialog.title_main'|trans }}\";
    Trans.SAVE_NEW_DIALOG_MESSAGE_COPIED = \"{{ 'save_new.dialog.message.copied'|trans }}\";
    Trans.DESCRIPTION_DIALOG_TITLE = \"{{ 'description.fieldset.legend'|trans }}\";
    Trans.DESCRIPTION_DIALOG_TOOLTIP = \"{{ 'description.fieldset.legend.tooltip'|trans }}\";
    Trans.IMPORT_ACTION_NEW = \"{{ 'import.action.new'|trans }}\";
    Trans.IMPORT_ACTION_CONVERT = \"{{ 'import.action.convert'|trans }}\";
    Trans.IMPORT_ACTION_IGNORE = \"{{ 'import.action.ignore'|trans }}\";
    Trans.DIALOG_DISABLING_MFA_TITLE = \"{{ 'mfa.disable_dialog.title'|trans }}\";
    Trans.DIALOG_DISABLING_MFA_CONTENT = \"{{ 'mfa.disable_dialog.content'|trans }}\";

    {% trans_default_domain \"TestWizard\" %}
    Trans.TEST_WIZARD_BREADCRUMB_LIST = \"{{ \"breadcrumb.list\"|trans }}\";
    Trans.TEST_WIZARD_LIST_FIELD_NAME = \"{{ \"list.field.name\"|trans }}\";
    Trans.TEST_WIZARD_LIST_FIELD_TEST = \"{{ \"list.field.test\"|trans }}\";
    Trans.TEST_WIZARD_FORM_TITLE_ADD = \"{{ 'form.title.add'|trans }}\";
    Trans.TEST_WIZARD_FORM_TITLE_EDIT = \"{{ 'form.title.edit'|trans }}\";
    Trans.TEST_WIZARD_STEP_LIST_FIELD_ID = \"{{ \"step.list.field.id\"|trans }}\";
    Trans.TEST_WIZARD_STEP_LIST_FIELD_INFO = \"{{ \"step.list.field.info\"|trans }}\";
    Trans.TEST_WIZARD_STEP_LIST_FIELD_TITLE = \"{{ \"step.list.field.title\"|trans }}\";
    Trans.TEST_WIZARD_STEP_LIST_FIELD_ORDER = \"{{ \"step.list.field.order\"|trans }}\";
    Trans.TEST_WIZARD_STEP_LIST_BUTTON_DELETE = \"{{ \"step.list.button.delete\"|trans }}\";
    Trans.TEST_WIZARD_STEP_LIST_BUTTON_EDIT = \"{{ \"step.list.button.edit\"|trans }}\";
    Trans.TEST_WIZARD_STEP_DIALOG_TITLE_ADD = \"{{ \"step.dialog.title.add\"|trans }}\";
    Trans.TEST_WIZARD_STEP_DIALOG_TITLE_EDIT = \"{{ \"step.dialog.title.edit\"|trans }}\";
    Trans.TEST_WIZARD_STEP_DIALOG_TITLE_CLEAR = \"{{ 'step.dialog.title.clear'|trans }}\";
    Trans.TEST_WIZARD_STEP_DIALOG_MESSAGE_CLEAR_CONFIRM = \"{{ 'step.dialog.message.clear.confirm'|trans }}\";
    Trans.TEST_WIZARD_STEP_DIALOG_TITLE_DELETE = \"{{ 'step.dialog.title.delete'|trans }}\";
    Trans.TEST_WIZARD_STEP_DIALOG_MESSAGE_DELETE_CONFIRM = \"{{ 'step.dialog.message.delete.confirm'|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_ID = \"{{ \"param.list.field.id\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_INFO = \"{{ \"param.list.field.info\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_LABEL = \"{{ \"param.list.field.label\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_STEP = \"{{ \"param.list.field.step\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_VARIABLE = \"{{ \"param.list.field.variable\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_TYPE = \"{{ \"param.list.field.type\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_FIELD_ORDER = \"{{ \"param.list.field.order\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_BUTTON_DELETE = \"{{ \"param.list.button.delete\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_BUTTON_EDIT = \"{{ \"param.list.button.edit\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DIALOG_TITLE_ADD = \"{{ \"param.dialog.title.add\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DIALOG_TITLE_EDIT = \"{{ \"param.dialog.title.edit\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DIALOG_TITLE_CLEAR = \"{{ 'param.dialog.title.clear'|trans }}\";
    Trans.TEST_WIZARD_PARAM_DIALOG_MESSAGE_CLEAR_CONFIRM = \"{{ 'param.dialog.message.clear.confirm'|trans }}\";
    Trans.TEST_WIZARD_PARAM_DIALOG_TITLE_DELETE = \"{{ 'param.dialog.title.delete'|trans }}\";
    Trans.TEST_WIZARD_PARAM_DIALOG_MESSAGE_DELETE_CONFIRM = \"{{ 'param.dialog.message.delete.confirm'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_SINGLE_LINE_TEXT = \"{{ 'param.form.field.type.single_line_text'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_MULTI_LINE_TEXT = \"{{ 'param.form.field.type.multi_line_text'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_HTML = \"{{ 'param.form.field.type.html'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_SELECT = \"{{ 'param.form.field.type.select'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_VIEW = \"{{ 'param.form.field.type.view'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_TEST = \"{{ 'param.form.field.type.test'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_TABLE = \"{{ 'param.form.field.type.table'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_COLUMN = \"{{ 'param.form.field.type.column'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_CHECKBOX = \"{{ 'param.form.field.type.checkbox'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_GROUP = \"{{ 'param.form.field.type.group'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_LIST = \"{{ 'param.form.field.type.list'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_R = \"{{ 'param.form.field.type.r'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_COLUMN_MAP = \"{{ 'param.form.field.type.column_map'|trans }}\";
    Trans.TEST_WIZARD_PARAM_TYPE_WIZARD = \"{{ 'param.form.field.type.wizard'|trans }}\";
    Trans.TEST_WIZARD_PARAM_SELECT_LIST_FIELD_ORDER = \"{{ \"param.definer.select.list.field.order\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SELECT_LIST_FIELD_LABEL = \"{{ \"param.definer.select.list.field.label\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SELECT_LIST_FIELD_VALUE = \"{{ \"param.definer.select.list.field.value\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SELECT_LIST_BUTTON_DELETE = \"{{ \"param.definer.select.list.button.delete\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_COLUMN_MAP_LIST_FIELD_NAME = \"{{ \"param.definer.column_map.list.field.name\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_COLUMN_MAP_LIST_FIELD_LABEL = \"{{ \"param.definer.column_map.list.field.label\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_COLUMN_MAP_LIST_FIELD_TOOLTIP = \"{{ \"param.definer.column_map.list.field.tooltip\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_COLUMN_MAP_LIST_BUTTON_DELETE = \"{{ \"param.definer.column_map.list.button.delete\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_NAME = \"{{ \"param.definer.group.list.field.name\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_LABEL = \"{{ \"param.definer.group.list.field.label\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_TYPE = \"{{ \"param.definer.group.list.field.type\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_HIDE_CONDITION = \"{{ \"param.definer.group.list.field.hide_condition\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_DEFINITION = \"{{ \"param.definer.group.list.field.definition\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_FIELD_ORDER = \"{{ \"param.definer.group.list.field.order\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_GROUP_LIST_BUTTON_DELETE = \"{{ \"param.definer.select.list.button.delete\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINITION_ICON_TOOLTIP = \"{{ \"param.form.field.definition.icon.tooltip\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_ELEMENT_DELETE = \"{{ \"param.setter.list.element.remove\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_LIST_COLUMN_ELEMENT = \"{{ \"param.setter.list.column.element\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_SELECT = \"{{ \"param.definer.titles.select\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_GROUP = \"{{ \"param.definer.titles.group\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_LIST = \"{{ \"param.definer.titles.list\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_SINGLE_LINE = \"{{ \"param.definer.titles.single_line\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_MULTI_LINE = \"{{ \"param.definer.titles.multi_line\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_HTML = \"{{ \"param.definer.titles.html\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_CHECKBOX = \"{{ \"param.definer.titles.checkbox\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_TEST = \"{{ \"param.definer.titles.test\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_TABLE = \"{{ \"param.definer.titles.table\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_TEMPLATE = \"{{ \"param.definer.titles.template\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_R_CODE = \"{{ \"param.definer.titles.r_code\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_COLUMN_MAP = \"{{ \"param.definer.titles.column_map\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_TITLES_WIZARD = \"{{ \"param.definer.titles.wizard\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_TEXTAREA = \"{{ \"param.setter.titles.textarea\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_HTML = \"{{ \"param.setter.titles.html\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_R = \"{{ \"param.setter.titles.r\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_COLUMN = \"{{ \"param.setter.titles.column\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_GROUP = \"{{ \"param.setter.titles.group\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_LIST = \"{{ \"param.setter.titles.list\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_COLUMN_MAP = \"{{ \"param.setter.titles.column_map\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_TITLES_WIZARD = \"{{ \"param.setter.titles.wizard\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_SUMMARIES_SELECT = \"{{ \"param.definer.summaries.select\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_SUMMARIES_GROUP = \"{{ \"param.definer.summaries.group\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_SUMMARIES_LIST = \"{{ \"param.definer.summaries.list\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_DEFINER_SUMMARIES_COLUMN_MAP = \"{{ \"param.definer.summaries.column_map\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_TEXTAREA = \"{{ \"param.setter.summaries.textarea\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_HTML = \"{{ \"param.setter.summaries.html\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_COLUMN = \"{{ \"param.setter.summaries.column\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_GROUP = \"{{ \"param.setter.summaries.group\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_LIST = \"{{ \"param.setter.summaries.list\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_R = \"{{ \"param.setter.summaries.r\"|trans }}\";
    Trans.TEST_WIZARD_PARAM_SETTER_SUMMARIES_COLUMN_MAP = \"{{ \"param.setter.summaries.column_map\"|trans }}\";

    {% trans_default_domain \"Test\" %}
    Trans.TEST_BUTTON_RUN = \"{{ \"form.button.run\"|trans }}\";
    Trans.TEST_BREADCRUMB_LIST = \"{{ \"breadcrumb.list\"|trans }}\";
    Trans.TEST_LIST_FIELD_NAME = \"{{ \"list.field.name\"|trans }}\";
    Trans.TEST_LIST_FIELD_SLUG = \"{{ \"list.field.slug\"|trans }}\";
    Trans.TEST_LIST_FIELD_WIZARD = \"{{ \"list.field.wizard\"|trans }}\";
    Trans.TEST_LIST_FIELD_WIZARD_SOURCE = \"{{ \"list.field.wizard.source\"|trans }}\";
    Trans.TEST_FORM_TITLE_ADD = \"{{ 'form.title.add'|trans }}\";
    Trans.TEST_FORM_TITLE_EDIT = \"{{ 'form.title.edit'|trans }}\";
    Trans.TEST_LOG_LIST_FIELD_DATE = \"{{ \"log.list.field.date\"|trans }}\";
    Trans.TEST_LOG_LIST_FIELD_BROWSER = \"{{ \"log.list.field.browser\"|trans }}\";
    Trans.TEST_LOG_LIST_FIELD_IP = \"{{ \"log.list.field.ip\"|trans }}\";
    Trans.TEST_LOG_LIST_FIELD_MESSAGE = \"{{ \"log.list.field.message\"|trans }}\";
    Trans.TEST_LOG_LIST_FIELD_TYPE = \"{{ \"log.list.field.type\"|trans }}\";
    Trans.TEST_LOG_LIST_FIELD_TYPE_R = \"{{ \"log.list.field.type.R\"|trans }}\";
    Trans.TEST_LOG_LIST_FIELD_TYPE_JAVASCRIPT = \"{{ \"log.list.field.type.javascript\"|trans }}\";
    Trans.TEST_LOG_LIST_BUTTON_DELETE = \"{{ \"log.list.button.delete\"|trans }}\";
    Trans.TEST_VARS_PARAMS_DIALOG_TITLE_ADD = \"{{ \"variables.input.parameters.dialog.title.add\"|trans }}\";
    Trans.TEST_VARS_PARAMS_DIALOG_TITLE_EDIT = \"{{ \"variables.input.parameters.dialog.title.edit\"|trans }}\";
    Trans.TEST_VARS_PARAMS_DIALOG_TITLE_DELETE = \"{{ 'variables.input.parameters.dialog.title.delete'|trans }}\";
    Trans.TEST_VARS_PARAMS_DIALOG_MESSAGE_DELETE_CONFIRM = \"{{ 'variables.input.parameters.dialog.message.delete.confirm'|trans }}\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_INFO = \"{{ 'variables.input.parameters.list.field.info'|trans }}\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_NAME = \"{{ 'variables.input.parameters.list.field.name'|trans }}\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_URL = \"{{ 'variables.input.parameters.list.field.url'|trans }}\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_URL_YES = \"{{ 'variables.input.parameters.list.field.url.yes'|trans }}\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_URL_NO = \"{{ 'variables.input.parameters.list.field.url.no'|trans }}\";
    Trans.TEST_VARS_PARAMS_LIST_FIELD_VALUE = \"{{ 'variables.input.parameters.list.field.value'|trans }}\";
    Trans.TEST_VARS_PARAMS_LIST_EDIT = \"{{ 'variables.input.parameters.list.edit'|trans }}\";
    Trans.TEST_VARS_PARAMS_LIST_DELETE = \"{{ 'variables.input.parameters.list.delete'|trans }}\";
    Trans.TEST_VARS_RETURNS_DIALOG_TITLE_ADD = \"{{ \"variables.output.returns.dialog.title.add\"|trans }}\";
    Trans.TEST_VARS_RETURNS_DIALOG_TITLE_EDIT = \"{{ \"variables.output.returns.dialog.title.edit\"|trans }}\";
    Trans.TEST_VARS_RETURNS_DIALOG_TITLE_DELETE = \"{{ 'variables.output.returns.dialog.title.delete'|trans }}\";
    Trans.TEST_VARS_RETURNS_DIALOG_MESSAGE_DELETE_CONFIRM = \"{{ 'variables.output.returns.dialog.message.delete.confirm'|trans }}\";
    Trans.TEST_VARS_RETURNS_LIST_FIELD_INFO = \"{{ 'variables.output.returns.list.field.info'|trans }}\";
    Trans.TEST_VARS_RETURNS_LIST_FIELD_NAME = \"{{ 'variables.output.returns.list.field.name'|trans }}\";
    Trans.TEST_VARS_RETURNS_LIST_FIELD_VALUE = \"{{ 'variables.output.returns.list.field.value'|trans }}\";
    Trans.TEST_VARS_RETURNS_LIST_EDIT = \"{{ 'variables.output.returns.list.edit'|trans }}\";
    Trans.TEST_VARS_RETURNS_LIST_DELETE = \"{{ 'variables.output.returns.list.delete'|trans }}\";
    Trans.TEST_VARS_BRANCHES_DIALOG_TITLE_ADD = \"{{ \"variables.output.branches.dialog.title.add\"|trans }}\";
    Trans.TEST_VARS_BRANCHES_DIALOG_TITLE_EDIT = \"{{ \"variables.output.branches.dialog.title.edit\"|trans }}\";
    Trans.TEST_VARS_BRANCHES_DIALOG_TITLE_DELETE = \"{{ 'variables.output.branches.dialog.title.delete'|trans }}\";
    Trans.TEST_VARS_BRANCHES_DIALOG_MESSAGE_DELETE_CONFIRM = \"{{ 'variables.output.branches.dialog.message.delete.confirm'|trans }}\";
    Trans.TEST_VARS_BRANCHES_LIST_FIELD_INFO = \"{{ 'variables.output.branches.list.field.info'|trans }}\";
    Trans.TEST_VARS_BRANCHES_LIST_FIELD_NAME = \"{{ 'variables.output.branches.list.field.name'|trans }}\";
    Trans.TEST_VARS_BRANCHES_LIST_FIELD_VALUE = \"{{ 'variables.output.branches.list.field.value'|trans }}\";
    Trans.TEST_VARS_BRANCHES_LIST_EDIT = \"{{ 'variables.output.branches.list.edit'|trans }}\";
    Trans.TEST_VARS_BRANCHES_LIST_DELETE = \"{{ 'variables.output.branches.list.delete'|trans }}\";
    Trans.TEST_FORM_FIELD_VISIBILITY_REGULAR = \"{{ 'form.field.visibility.regular'|trans }}\";
    Trans.TEST_FORM_FIELD_VISIBILITY_FEATURED = \"{{ 'form.field.visibility.featured'|trans }}\";
    Trans.TEST_FORM_FIELD_VISIBILITY_SUBTEST = \"{{ 'form.field.visibility.subtest'|trans }}\";
    Trans.TEST_FORM_FIELD_TYPE_CODE = \"{{ 'form.field.type.code'|trans }}\";
    Trans.TEST_FORM_FIELD_TYPE_WIZARD = \"{{ 'form.field.type.wizard'|trans }}\";
    Trans.TEST_FORM_FIELD_TYPE_FLOW = \"{{ 'form.field.type.flow'|trans }}\";
    Trans.TEST_LOG_DIALOG_TITLE_CLEAR = \"{{ 'log.dialog.title.clear'|trans }}\";
    Trans.TEST_LOG_DIALOG_MESSAGE_CLEAR_CONFIRM = \"{{ 'log.dialog.message.clear.confirm'|trans }}\";
    Trans.TEST_LOG_DIALOG_TITLE_DELETE = \"{{ 'log.dialog.title.delete'|trans }}\";
    Trans.TEST_LOG_DIALOG_MESSAGE_DELETE_CONFIRM = \"{{ 'log.dialog.message.delete.confirm'|trans }}\";
    Trans.TEST_LOGIC_CONVERT_TITLE = \"{{ \"logic.convert.title\"|trans }}\";
    Trans.TEST_LOGIC_CONVERT_CONFIRMATION = \"{{ \"logic.convert.confirmation\"|trans }}\";

    Trans.TEST_FLOW_NODE_NAME_START = \"{{ \"flow.nodes.names.test_start\"|trans }}\";
    Trans.TEST_FLOW_NODE_NAME_END = \"{{ \"flow.nodes.names.test_end\"|trans }}\";
    Trans.TEST_FLOW_NODE_DESCRIPTION_START = \"{{ \"flow.nodes.descriptions.test_start\"|trans }}\";
    Trans.TEST_FLOW_NODE_DESCRIPTION_END = \"{{ \"flow.nodes.descriptions.test_end\"|trans }}\";
    Trans.TEST_FLOW_PORT_NAME_IN = \"{{ \"flow.ports.names.in\"|trans }}\";
    Trans.TEST_FLOW_PORT_DESCRIPTION_IN = \"{{ \"flow.ports.descriptions.in\"|trans }}\";
    Trans.TEST_FLOW_PORT_NAME_OUT = \"{{ \"flow.ports.names.out\"|trans }}\";
    Trans.TEST_FLOW_PORT_DESCRIPTION_OUT = \"{{ \"flow.ports.descriptions.out\"|trans }}\";
    Trans.TEST_FLOW_PORT_ADD_INPUT = \"{{ \"flow.ports.add.input\"|trans }}\";
    Trans.TEST_FLOW_PORT_ADD_BRANCH = \"{{ \"flow.ports.add.branch\"|trans }}\";
    Trans.TEST_FLOW_PORT_ADD_RETURN = \"{{ \"flow.ports.add.return\"|trans }}\";
    Trans.TEST_FLOW_BUTTONS_NODE_MENU = \"{{ \"flow.buttons.node_menu\"|trans }}\";
    Trans.TEST_FLOW_DIALOG_NODE_REMOVE_TITLE = \"{{ \"flow.nodes.dialog.delete.title\"|trans }}\";
    Trans.TEST_FLOW_DIALOG_NODE_REMOVE_MESSAGE = \"{{ \"flow.nodes.dialog.delete.message\"|trans }}\";
    Trans.TEST_FLOW_DIALOG_NODE_REMOVE_SELECTION_TITLE = \"{{ \"flow.nodes.dialog.selection_delete.title\"|trans }}\";
    Trans.TEST_FLOW_DIALOG_NODE_REMOVE_SELECTION_MESSAGE = \"{{ \"flow.nodes.dialog.selection_delete.message\"|trans }}\";
    Trans.TEST_FLOW_DIALOG_NODE_EDIT_TITLE_TITLE = \"{{ \"flow.nodes.dialog.title.title\"|trans }}\";
    Trans.TEST_FLOW_DIALOG_NODE_EDIT_TITLE_TOOLTIP = \"{{ \"flow.nodes.dialog.title.tooltip\"|trans }}\";
    Trans.TEST_FLOW_DIALOG_NODE_EDIT_TITLE = \"{{ \"flow.nodes.dialog.edit.title\"|trans }}\";
    Trans.TEST_FLOW_DIALOG_NODE_INPUT_ADD_TITLE = \"{{ \"flow.ports.dialog.titles.input.add\"|trans }}\";
    Trans.TEST_FLOW_PORT_DIALOG_TITLE_REMOVE_INPUT = \"{{ \"flow.ports.dialog.titles.input.remove\"|trans }}\";
    Trans.TEST_FLOW_PORT_DIALOG_CONTENT_REMOVE_INPUT = \"{{ \"flow.ports.dialog.content.input.remove\"|trans }}\";
    Trans.TEST_FLOW_PORT_DIALOG_TITLE_REMOVE_BRANCH = \"{{ \"flow.ports.dialog.titles.branch.remove\"|trans }}\";
    Trans.TEST_FLOW_PORT_DIALOG_CONTENT_REMOVE_BRANCH = \"{{ \"flow.ports.dialog.content.branch.remove\"|trans }}\";
    Trans.TEST_FLOW_PORT_DIALOG_TITLE_REMOVE_RETURN = \"{{ \"flow.ports.dialog.titles.return.remove\"|trans }}\";
    Trans.TEST_FLOW_PORT_DIALOG_CONTENT_REMOVE_RETURN = \"{{ \"flow.ports.dialog.content.return.remove\"|trans }}\";
    Trans.TEST_FLOW_PORT_DIALOG_TITLE_REMOVE_ALL_CONNECTIONS = \"{{ \"flow.ports.dialog.title.port.remove_all_connections\"|trans }}\";
    Trans.TEST_FLOW_PORT_DIALOG_CONTENT_REMOVE_ALL_CONNECTIONS = \"{{ \"flow.ports.dialog.content.port.remove_all_connections\"|trans }}\";
    Trans.TEST_FLOW_DIALOG_CONNECTION_EDIT_TITLE = \"{{ \"flow.connections.dialog.edit.title\"|trans }}\";

    {% trans_default_domain \"ViewTemplate\" %}
    Trans.VIEW_TEMPLATE_BREADCRUMB_LIST = \"{{ \"breadcrumb.list\"|trans }}\";
    Trans.VIEW_TEMPLATE_LIST_FIELD_NAME = \"{{ \"list.field.name\"|trans }}\";
    Trans.VIEW_TEMPLATE_FORM_TITLE_ADD = \"{{ 'form.title.add'|trans }}\";
    Trans.VIEW_TEMPLATE_FORM_TITLE_EDIT = \"{{ 'form.title.edit'|trans }}\";

    {% trans_default_domain \"DataTable\" %}
    Trans.DATA_TABLE_BREADCRUMB_LIST = \"{{ \"breadcrumb.list\"|trans }}\";
    Trans.DATA_TABLE_LIST_FIELD_NAME = \"{{ \"list.field.name\"|trans }}\";
    Trans.DATA_TABLE_FORM_TITLE_ADD = \"{{ 'form.title.add'|trans }}\";
    Trans.DATA_TABLE_FORM_TITLE_EDIT = \"{{ 'form.title.edit'|trans }}\";
    Trans.DATA_TABLE_DATA_LIST_DELETE = \"{{ \"data.list.delete\"|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_LIST_FIELD_NAME = \"{{ 'structure.list.field.name'|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_LIST_FIELD_TYPE = \"{{ 'structure.list.field.type'|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_LIST_FIELD_LENGTH = \"{{ 'structure.list.field.length'|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_LIST_FIELD_NULLABLE = \"{{ 'structure.list.field.nullable'|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_LIST_EDIT = \"{{ 'structure.list.edit'|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_LIST_DELETE = \"{{ 'structure.list.delete'|trans }}\";
    Trans.DATA_TABLE_DATA_DIALOG_TITLE_DELETE = \"{{ 'data.dialog.title.delete'|trans }}\";
    Trans.DATA_TABLE_DATA_DIALOG_TITLE_EDIT = \"{{ 'data.dialog.title.edit'|trans }}\";
    Trans.DATA_TABLE_DATA_DIALOG_MESSAGE_CONFIRM_DELETE = \"{{ 'data.dialog.message.confirm.delete'|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_DIALOG_TITLE_DELETE = \"{{ 'structure.dialog.title.delete'|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_DIALOG_MESSAGE_CONFIRM_DELETE = \"{{ 'structure.dialog.message.confirm.delete'|trans }}\";
    Trans.DATA_TABLE_IO_DIALOG_TITLE_IMPORT = \"{{ 'io.dialog.title.import'|trans }}\";
    Trans.DATA_TABLE_IO_DIALOG_MESSAGE_IMPORTED = \"{{ 'io.dialog.message.imported'|trans }}\";
    Trans.DATA_TABLE_IO_DIALOG_MESSAGE_ERROR = \"{{ 'io.dialog.message.error'|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_DIALOG_TITLE_ADD = \"{{ 'structure.dialog.title.add'|trans }}\";
    Trans.DATA_TABLE_STRUCTURE_DIALOG_TITLE_EDIT = \"{{ 'structure.dialog.title.edit'|trans }}\";
    Trans.DATA_TABLE_CELL_TEXT_EDIT_TITLE = \"{{ 'data.cell.text.fieldset.legend'|trans }}\";
    Trans.DATA_TABLE_CELL_TEXT_EDIT_TOOLTIP = \"{{ 'data.cell.text.fieldset.legend.tooltip'|trans }}\";

    {% trans_default_domain \"User\" %}
    Trans.USER_BREADCRUMB_LIST = \"{{ \"breadcrumb.list\"|trans }}\";
    Trans.USER_LIST_FIELD_USERNAME = \"{{ \"list.field.username\"|trans }}\";
    Trans.USER_LIST_FIELD_EMAIL = \"{{ \"list.field.email\"|trans }}\";
    Trans.USER_FORM_TITLE_ADD = \"{{ 'form.title.add'|trans }}\";
    Trans.USER_FORM_TITLE_EDIT = \"{{ 'form.title.edit'|trans }}\";

    {% trans_default_domain \"UserTest\" %}
    Trans.USERTEST_BREADCRUMB_LIST = \"{{ \"breadcrumb.list\"|trans }}\";
    Trans.USERTEST_LIST_FIELD_USERNAME = \"{{ \"list.field.username\"|trans }}\";
    Trans.USERTEST_LIST_FIELD_EMAIL = \"{{ \"list.field.email\"|trans }}\";
    Trans.USERTEST_FORM_TITLE_ADD = \"{{ 'form.title.add'|trans }}\";
    Trans.USERTEST_FORM_TITLE_EDIT = \"{{ 'form.title.edit'|trans }}\";

    {% trans_default_domain \"FileBrowser\" %}
    Trans.FILE_BROWSER_BREADCRUMB_FILES = \"{{ \"breadcrumb.files\"|trans }}\";
    Trans.FILE_BROWSER_ALERT_UPLOAD_FAILED_TITLE = \"{{ \"uploader.alerts.upload_failed.title\"|trans }}\";
    Trans.FILE_BROWSER_ALERT_UPLOAD_FAILED_MESSAGE = \"{{ \"uploader.alerts.upload_failed.message\"|trans }}\";

    {% trans_default_domain \"Administration\" %}
    Trans.ADMINISTRATION_BREADCRUMB = \"{{ \"breadcrumb\"|trans }}\";
    Trans.ADMINISTRATION_DIALOG_TITLE_CLEAR = \"{{ \"usage_charts.dialog.title.clear\"|trans }}\";
    Trans.ADMINISTRATION_DIALOG_CONFIRM_CLEAR = \"{{ \"usage_charts.dialog.confirm.clear\"|trans }}\";
    Trans.ADMINISTRATION_USAGE_DATA_FILTER_TODAY = \"{{ \"usage_charts.filter.today\"|trans }}\";
    Trans.ADMINISTRATION_USAGE_DATA_FILTER_SPECIFIC_DATE = \"{{ \"usage_charts.filter.specific_date\"|trans }}\";
    Trans.ADMINISTRATION_USAGE_DATA_FILTER_DATE_RANGE = \"{{ \"usage_charts.filter.date_range\"|trans }}\";
    Trans.ADMINISTRATION_VERSION_NONE = \"{{ \"tasks.version_none\"|trans }}\";
    Trans.MESSAGES_LIST_FIELD_TIME = \"{{ \"messages.list.fields.time\"|trans }}\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY = \"{{ \"messages.list.fields.category\"|trans }}\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_SYSTEM = \"{{ \"messages.list.fields.category.system\"|trans }}\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_GLOBAL = \"{{ \"messages.list.fields.category.global\"|trans }}\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_LOCAL = \"{{ \"messages.list.fields.category.local\"|trans }}\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_TEST = \"{{ \"messages.list.fields.category.test\"|trans }}\";
    Trans.MESSAGES_LIST_FIELD_CATEGORY_CHANGELOG = \"{{ \"messages.list.fields.category.changelog\"|trans }}\";
    Trans.MESSAGES_LIST_FIELD_SUBJECT = \"{{ \"messages.list.fields.subject\"|trans }}\";
    Trans.MESSAGES_LIST_FIELD_MESSAGE = \"{{ \"messages.list.fields.message\"|trans }}\";
    Trans.MESSAGES_LIST_BUTTONS_DELETE = \"{{ \"messages.list.buttons.delete\"|trans }}\";
    Trans.MESSAGES_DIALOGS_TITLE_DELETE = \"{{ \"messages.dialogs.title.delete\"|trans }}\";
    Trans.MESSAGES_DIALOGS_MESSAGE_DELETE = \"{{ \"messages.dialogs.message.delete\"|trans }}\";
    Trans.MESSAGES_DIALOGS_TITLE_CLEAR = \"{{ \"messages.dialogs.title.clear\"|trans }}\";
    Trans.MESSAGES_DIALOGS_MESSAGE_CLEAR = \"{{ \"messages.dialogs.message.clear\"|trans }}\";
    Trans.TASKS_LIST_FIELD_UPDATED = \"{{ \"tasks.list.fields.updated\"|trans }}\";
    Trans.TASKS_LIST_FIELD_STATUS = \"{{ \"tasks.list.fields.status\"|trans }}\";
    Trans.TASKS_LIST_FIELD_STATUS_PENDING = \"{{ \"tasks.list.fields.status.pending\"|trans }}\";
    Trans.TASKS_LIST_FIELD_STATUS_ONGOING = \"{{ \"tasks.list.fields.status.ongoing\"|trans }}\";
    Trans.TASKS_LIST_FIELD_STATUS_COMPLETED = \"{{ \"tasks.list.fields.status.completed\"|trans }}\";
    Trans.TASKS_LIST_FIELD_STATUS_FAILED = \"{{ \"tasks.list.fields.status.failed\"|trans }}\";
    Trans.TASKS_LIST_FIELD_STATUS_CANCELED = \"{{ \"tasks.list.fields.status.canceled\"|trans }}\";
    Trans.TASKS_LIST_FIELD_DESCRIPTION = \"{{ \"tasks.list.fields.description\"|trans }}\";
    Trans.TASKS_LIST_FIELD_OUTPUT = \"{{ \"tasks.list.fields.output\"|trans }}\";
    Trans.API_CLIENTS_LIST_FIELD_ID = \"{{ \"api_clients.list.fields.id\"|trans }}\";
    Trans.API_CLIENTS_LIST_FIELD_SECRET = \"{{ \"api_clients.list.fields.secret\"|trans }}\";
    Trans.API_CLIENTS_LIST_BUTTONS_DELETE = \"{{ \"api_clients.list.buttons.delete\"|trans }}\";
    Trans.API_CLIENTS_DIALOGS_TITLE_DELETE = \"{{ \"api_clients.dialogs.title.delete\"|trans }}\";
    Trans.API_CLIENTS_DIALOGS_MESSAGE_DELETE = \"{{ \"api_clients.dialogs.message.delete\"|trans }}\";
    Trans.API_CLIENTS_DIALOGS_TITLE_CLEAR = \"{{ \"api_clients.dialogs.title.clear\"|trans }}\";
    Trans.API_CLIENTS_DIALOGS_MESSAGE_CLEAR = \"{{ \"api_clients.dialogs.message.clear\"|trans }}\";
    Trans.PACKAGES_DIALOG_TITLE_REPORT = \"{{ \"packages.dialog.title.report\"|trans }}\";
    Trans.PACKAGES_DIALOG_TITLE_REPORT_TOOLTIP = \"{{ \"packages.dialog.title.report.tooltip\"|trans }}\";
    Trans.PACKAGES_DIALOG_CONTENT_REPORT_FAILED = \"{{ \"packages.dialog.content.report_failed\"|trans }}\";
    Trans.PACKAGES_DIALOG_FIELDS_METHOD_LATEST = \"{{ \"packages.dialog.fields.method.latest\"|trans }}\";
    Trans.PACKAGES_DIALOG_FIELDS_METHOD_SPECIFIC = \"{{ \"packages.dialog.fields.method.specific\"|trans }}\";
    Trans.PACKAGES_DIALOG_TITLE_INSTALLATION_FAILED = \"{{ \"packages.dialog.title.installation_failed\"|trans }}\";
    Trans.CONTENT_IMPORT_FROM_FILE = \"{{ \"content.import_from_file\"|trans }}\";
    Trans.CONTENT_IMPORT_FROM_URL = \"{{ \"content.import_from_url\"|trans }}\";
    Trans.CONTENT_IMPORTING_CONTENT = \"{{ \"content.importing_content\"|trans }}\";
    Trans.CONTENT_IMPORT_PROMPT = \"{{ \"content.import_prompt\"|trans }}\";
    Trans.CONTENT_IMPORT_FAILURE = \"{{ \"content.import_failure\"|trans }}\";
    Trans.GIT_ENABLE_TITLE = \"{{ \"git.enable.title\"|trans }}\";
    Trans.GIT_DISABLE_TITLE = \"{{ \"git.disable.title\"|trans }}\";
    Trans.GIT_DISABLE_CONFIRM = \"{{ \"git.disable.confirm\"|trans }}\";
    Trans.GIT_DIFF_SHA = \"{{ \"git.diff.sha\"|trans }}\";
    Trans.GIT_DIFF_LOCAL = \"{{ \"git.diff.local\"|trans }}\";
    Trans.GIT_REFRESH_TITLE = \"{{ \"git.refresh.title\"|trans }}\";
    Trans.GIT_COMMIT_SUCCESS = \"{{ \"git.commit.success\"|trans }}\";
    Trans.GIT_COMMIT_FAILURE = \"{{ \"git.commit.failure\"|trans }}\";
    Trans.GIT_RESET_TITLE = \"{{ \"git.reset.title\"|trans }}\";
    Trans.GIT_RESET_CONFIRM = \"{{ \"git.reset.confirm\"|trans }}\";
    Trans.GIT_PUSH_TITLE = \"{{ \"git.push.title\"|trans }}\";
    Trans.GIT_PUSH_CONFIRM = \"{{ \"git.push.confirm\"|trans }}\";
    Trans.GIT_PUSH_FAILURE = \"{{ \"git.push.failure\"|trans }}\";
    Trans.GIT_PULL_TITLE = \"{{ \"git.pull.title\"|trans }}\";
    Trans.GIT_PULL_CONFIRM = \"{{ \"git.pull.confirm\"|trans }}\";
    Trans.GIT_PULL_FAILURE = \"{{ \"git.pull.failure\"|trans }}\";
    Trans.GIT_UPDATE_TITLE = \"{{ \"git.update.title\"|trans }}\";
</script>
", "ConcertoPanelBundle::translation_injector.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/translation_injector.html.twig");
    }
}
