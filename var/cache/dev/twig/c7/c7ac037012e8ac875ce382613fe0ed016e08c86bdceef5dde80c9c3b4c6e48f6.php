<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Administration:usage_charts_section.html.twig */
class __TwigTemplate_25a69bb26b22c41c49c5f7fd7304d60c66f0f4d48f10e5eb887d613f3056e9f9 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:usage_charts_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:usage_charts_section.html.twig"));

        // line 2
        echo "
<div class=\"center\">
    <table class=\"padded\">
        <tr>
            <td class=\"padded\"><select ng-model=\"chart.filter.id\" ng-options=\"f.id as f.label for f in usageChartFilters\" class='form-control'></select></td>
            <td class=\"padded\" ng-show=\"chart.filter.id > 1\"><input type=\"date\" ng-model=\"chart.filter.minDate\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' /></td>
            <td class=\"padded\" ng-show=\"chart.filter.id == 3\"><input type=\"date\" ng-model=\"chart.filter.maxDate\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' /></td>
            <td><button ng-click=\"refreshUsageChart();\" class=\"btn btn-default btn-sm btn-list-refresh\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("usage_charts.buttons.refresh", [], "Administration"), "html", null, true);
        echo "</button></td>
            <td><button ng-click=\"clearUsageDate();\" class=\"btn btn-danger btn-sm\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("usage_charts.buttons.clear", [], "Administration"), "html", null, true);
        echo "</button></td>
        </tr>
    </table>
</div>
<uib-alert type=\"warning\" ng-if=\"chart.data[0].length == 0\">
    <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
    ";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("alerts.no_usage_data", [], "Administration"), "html", null, true);
        echo "
</uib-alert>
<canvas class=\"chart chart-line\" chart-data=\"chart.data\" chart-options=\"chart.options\" chart-dataset-override=\"chart.datasets\" ng-if=\"chart.data[0].length > 0\">
</canvas>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:usage_charts_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 16,  54 => 10,  50 => 9,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Administration\" %}

<div class=\"center\">
    <table class=\"padded\">
        <tr>
            <td class=\"padded\"><select ng-model=\"chart.filter.id\" ng-options=\"f.id as f.label for f in usageChartFilters\" class='form-control'></select></td>
            <td class=\"padded\" ng-show=\"chart.filter.id > 1\"><input type=\"date\" ng-model=\"chart.filter.minDate\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' /></td>
            <td class=\"padded\" ng-show=\"chart.filter.id == 3\"><input type=\"date\" ng-model=\"chart.filter.maxDate\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' /></td>
            <td><button ng-click=\"refreshUsageChart();\" class=\"btn btn-default btn-sm btn-list-refresh\">{{ \"usage_charts.buttons.refresh\"|trans }}</button></td>
            <td><button ng-click=\"clearUsageDate();\" class=\"btn btn-danger btn-sm\">{{ \"usage_charts.buttons.clear\"|trans }}</button></td>
        </tr>
    </table>
</div>
<uib-alert type=\"warning\" ng-if=\"chart.data[0].length == 0\">
    <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
    {{ 'alerts.no_usage_data'|trans }}
</uib-alert>
<canvas class=\"chart chart-line\" chart-data=\"chart.data\" chart-options=\"chart.options\" chart-dataset-override=\"chart.datasets\" ng-if=\"chart.data[0].length > 0\">
</canvas>", "ConcertoPanelBundle:Administration:usage_charts_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/usage_charts_section.html.twig");
    }
}
