<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_11_setter.html.twig */
class __TwigTemplate_91f1bae57cdc926e386fefd4ae486d6a7d2f840b30f1638f89870d54bca1236a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig"));

        // line 1
        echo "<script type=\"text/ng-template\" id=\"type_11_setter.html\">
    <div ng-controller=\"WizardParamSetter11Controller\">
        ";
        // line 3
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_11_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig", 3, "1254294170")->display($context);
        // line 19
        echo "    </div>
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 19,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_11_setter.html\">
    <div ng-controller=\"WizardParamSetter11Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ title }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.setter.titles.r.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\", {mode: 'R'} ) }}
                        <ui-codemirror ng-model=\"output\" ui-codemirror-opts=\"codeEditorOptions\"
                                       ui-refresh='codemirrorForceRefresh'
                                       ng-change=\"onPrimitiveValueChange(output)\"></ui-codemirror>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_11_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_11_setter.html.twig */
class __TwigTemplate_91f1bae57cdc926e386fefd4ae486d6a7d2f840b30f1638f89870d54bca1236a___1254294170 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'setter' => [$this, 'block_setter'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_base_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_setter($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        // line 6
        echo "                ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_11_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig", 6, "631695323")->display($context);
        // line 17
        echo "            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 17,  148 => 6,  139 => 5,  117 => 3,  47 => 19,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_11_setter.html\">
    <div ng-controller=\"WizardParamSetter11Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ title }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.setter.titles.r.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\", {mode: 'R'} ) }}
                        <ui-codemirror ng-model=\"output\" ui-codemirror-opts=\"codeEditorOptions\"
                                       ui-refresh='codemirrorForceRefresh'
                                       ng-change=\"onPrimitiveValueChange(output)\"></ui-codemirror>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_11_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_11_setter.html.twig */
class __TwigTemplate_91f1bae57cdc926e386fefd4ae486d6a7d2f840b30f1638f89870d54bca1236a___631695323 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'legend_tooltip' => [$this, 'block_legend_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 6
        return "ConcertoPanelBundle::form_v_single.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_single.html.twig", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig", 6);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo "{{ title }}";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.r.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 11
        echo "                        ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle::code_mirror_header.html.twig", ["mode" => "R"]);
        echo "
                        <ui-codemirror ng-model=\"output\" ui-codemirror-opts=\"codeEditorOptions\"
                                       ui-refresh='codemirrorForceRefresh'
                                       ng-change=\"onPrimitiveValueChange(output)\"></ui-codemirror>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 11,  280 => 10,  262 => 9,  244 => 8,  222 => 6,  151 => 17,  148 => 6,  139 => 5,  117 => 3,  47 => 19,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_11_setter.html\">
    <div ng-controller=\"WizardParamSetter11Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ title }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.setter.titles.r.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        {{ include(\"ConcertoPanelBundle::code_mirror_header.html.twig\", {mode: 'R'} ) }}
                        <ui-codemirror ng-model=\"output\" ui-codemirror-opts=\"codeEditorOptions\"
                                       ui-refresh='codemirrorForceRefresh'
                                       ng-change=\"onPrimitiveValueChange(output)\"></ui-codemirror>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_11_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_11_setter.html.twig");
    }
}
