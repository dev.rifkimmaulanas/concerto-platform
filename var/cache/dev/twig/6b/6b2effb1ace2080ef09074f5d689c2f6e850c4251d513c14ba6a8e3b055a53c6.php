<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:DataTable:form.html.twig */
class __TwigTemplate_133b319e208d4c5e25cd172674fc46eb21a8fb5dac9847d8c35ae508fa0da6c8 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'top' => [$this, 'block_top'],
            'legend' => [$this, 'block_legend'],
            'elements' => [$this, 'block_elements'],
            'sections' => [$this, 'block_sections'],
            'floatingBarButtons' => [$this, 'block_floatingBarButtons'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "ConcertoPanelBundle::form_h.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        // line 2
        $context["class_name"] = "DataTable";
        // line 4
        $context["exportable"] = true;
        // line 1
        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_top($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        // line 7
        echo "    ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle::lock_info.html.twig");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        // line 10
        echo "    ";
        echo "{{formTitle}}";
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 13
        echo "    ";
        $this->loadTemplate("ConcertoPanelBundle:DataTable:form.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 13, "663100239")->display($context);
        // line 21
        echo "
    ";
        // line 22
        $this->loadTemplate("ConcertoPanelBundle:DataTable:form.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 22, "1870272779")->display($context);
        // line 30
        echo "    
    ";
        // line 31
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 31, $this->source); })()), "user", [], "any", false, false, false, 31) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
            // line 32
            echo "        ";
            $this->loadTemplate("ConcertoPanelBundle:DataTable:form.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 32, "256398622")->display($context);
            // line 41
            echo "
        ";
            // line 42
            $this->loadTemplate("ConcertoPanelBundle:DataTable:form.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 42, "1485350920")->display($context);
            // line 52
            echo "
        ";
            // line 53
            $this->loadTemplate("ConcertoPanelBundle:DataTable:form.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 53, "938993964")->display($context);
            // line 61
            echo "    ";
        }
        // line 62
        echo "    
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 65
    public function block_sections($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        // line 66
        echo "    <uib-accordion-group is-open=\"tabAccordion.structure.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.fieldset.legend.tooltip", [], "DataTable"), "html", null, true);
        echo "'\"></i>
            ";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("structure.fieldset.legend", [], "DataTable"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.structure.open, 'glyphicon-chevron-right': !tabAccordion.structure.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 72
        echo twig_include($this->env, $context, "ConcertoPanelBundle:DataTable:structure_section.html.twig", ["class_name" => "DataTable"]);
        echo "
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.data.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 77
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("data.fieldset.legend.tooltip", [], "DataTable"), "html", null, true);
        echo "'\"></i>
            ";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("data.fieldset.legend", [], "DataTable"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.data.open, 'glyphicon-chevron-right': !tabAccordion.data.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 81
        echo twig_include($this->env, $context, "ConcertoPanelBundle:DataTable:data_section.html.twig");
        echo "
    </uib-accordion-group>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 85
    public function block_floatingBarButtons($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarButtons"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarButtons"));

        // line 86
        echo "    <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.save", [], "panel"), "html", null, true);
        echo "</button>
    <button class=\"btn btn-success\" ng-click=\"importCsv()\" ng-disabled=\"!isEditable()\">";
        // line 87
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.import.csv", [], "DataTable"), "html", null, true);
        echo "</button>
    <button class='btn btn-warning' ng-click=\"cancel();\">";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.cancel", [], "panel"), "html", null, true);
        echo "</button>
    <button class='btn btn-success' ng-click=\"saveNew();\">";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.save.new", [], "panel"), "html", null, true);
        echo "</button>
    <button class=\"btn btn-default\" ng-click=\"exportObject();\">";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.export", [], "panel"), "html", null, true);
        echo "</button>
    <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.lock", [], "panel"), "html", null, true);
        echo "</button>
    <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>";
        // line 92
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.unlock", [], "panel"), "html", null, true);
        echo "</button>
    <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">";
        // line 93
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.delete", [], "panel"), "html", null, true);
        echo "</button>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:DataTable:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  250 => 93,  246 => 92,  242 => 91,  238 => 90,  234 => 89,  230 => 88,  226 => 87,  221 => 86,  212 => 85,  199 => 81,  193 => 78,  189 => 77,  181 => 72,  175 => 69,  171 => 68,  167 => 66,  158 => 65,  147 => 62,  144 => 61,  142 => 53,  139 => 52,  137 => 42,  134 => 41,  131 => 32,  129 => 31,  126 => 30,  124 => 22,  121 => 21,  118 => 13,  109 => 12,  96 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"DataTable\" %}
{% trans_default_domain \"DataTable\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\">
        {% endblock %}
    {% endembed %}
    
    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}
    
{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.structure.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'structure.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'structure.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.structure.open, 'glyphicon-chevron-right': !tabAccordion.structure.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:structure_section.html.twig\", {'class_name':\"DataTable\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.data.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'data.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'data.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.data.open, 'glyphicon-chevron-right': !tabAccordion.data.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:data_section.html.twig\") }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarButtons %}
    <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-success\" ng-click=\"importCsv()\" ng-disabled=\"!isEditable()\">{{ 'form.button.import.csv'|trans }}</button>
    <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
    <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
    <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:DataTable:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/DataTable/form.html.twig");
    }
}


/* ConcertoPanelBundle:DataTable:form.html.twig */
class __TwigTemplate_133b319e208d4c5e25cd172674fc46eb21a8fb5dac9847d8c35ae508fa0da6c8___663100239 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 13
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 13);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 15
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.name", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.name.tooltip", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 17
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 18
        echo "            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:DataTable:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  464 => 18,  455 => 17,  437 => 16,  419 => 15,  397 => 13,  250 => 93,  246 => 92,  242 => 91,  238 => 90,  234 => 89,  230 => 88,  226 => 87,  221 => 86,  212 => 85,  199 => 81,  193 => 78,  189 => 77,  181 => 72,  175 => 69,  171 => 68,  167 => 66,  158 => 65,  147 => 62,  144 => 61,  142 => 53,  139 => 52,  137 => 42,  134 => 41,  131 => 32,  129 => 31,  126 => 30,  124 => 22,  121 => 21,  118 => 13,  109 => 12,  96 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"DataTable\" %}
{% trans_default_domain \"DataTable\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\">
        {% endblock %}
    {% endembed %}
    
    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}
    
{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.structure.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'structure.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'structure.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.structure.open, 'glyphicon-chevron-right': !tabAccordion.structure.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:structure_section.html.twig\", {'class_name':\"DataTable\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.data.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'data.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'data.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.data.open, 'glyphicon-chevron-right': !tabAccordion.data.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:data_section.html.twig\") }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarButtons %}
    <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-success\" ng-click=\"importCsv()\" ng-disabled=\"!isEditable()\">{{ 'form.button.import.csv'|trans }}</button>
    <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
    <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
    <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:DataTable:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/DataTable/form.html.twig");
    }
}


/* ConcertoPanelBundle:DataTable:form.html.twig */
class __TwigTemplate_133b319e208d4c5e25cd172674fc46eb21a8fb5dac9847d8c35ae508fa0da6c8___1870272779 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 22
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 22);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 24
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived.tooltip", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 26
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 27
        echo "            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\">
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:DataTable:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  677 => 27,  668 => 26,  650 => 25,  632 => 24,  610 => 22,  464 => 18,  455 => 17,  437 => 16,  419 => 15,  397 => 13,  250 => 93,  246 => 92,  242 => 91,  238 => 90,  234 => 89,  230 => 88,  226 => 87,  221 => 86,  212 => 85,  199 => 81,  193 => 78,  189 => 77,  181 => 72,  175 => 69,  171 => 68,  167 => 66,  158 => 65,  147 => 62,  144 => 61,  142 => 53,  139 => 52,  137 => 42,  134 => 41,  131 => 32,  129 => 31,  126 => 30,  124 => 22,  121 => 21,  118 => 13,  109 => 12,  96 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"DataTable\" %}
{% trans_default_domain \"DataTable\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\">
        {% endblock %}
    {% endembed %}
    
    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}
    
{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.structure.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'structure.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'structure.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.structure.open, 'glyphicon-chevron-right': !tabAccordion.structure.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:structure_section.html.twig\", {'class_name':\"DataTable\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.data.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'data.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'data.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.data.open, 'glyphicon-chevron-right': !tabAccordion.data.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:data_section.html.twig\") }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarButtons %}
    <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-success\" ng-click=\"importCsv()\" ng-disabled=\"!isEditable()\">{{ 'form.button.import.csv'|trans }}</button>
    <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
    <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
    <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:DataTable:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/DataTable/form.html.twig");
    }
}


/* ConcertoPanelBundle:DataTable:form.html.twig */
class __TwigTemplate_133b319e208d4c5e25cd172674fc46eb21a8fb5dac9847d8c35ae508fa0da6c8___256398622 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 32
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 32);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 34
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 35
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility.tooltip", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 37
        echo "                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:DataTable:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  890 => 37,  881 => 36,  863 => 35,  845 => 34,  823 => 32,  677 => 27,  668 => 26,  650 => 25,  632 => 24,  610 => 22,  464 => 18,  455 => 17,  437 => 16,  419 => 15,  397 => 13,  250 => 93,  246 => 92,  242 => 91,  238 => 90,  234 => 89,  230 => 88,  226 => 87,  221 => 86,  212 => 85,  199 => 81,  193 => 78,  189 => 77,  181 => 72,  175 => 69,  171 => 68,  167 => 66,  158 => 65,  147 => 62,  144 => 61,  142 => 53,  139 => 52,  137 => 42,  134 => 41,  131 => 32,  129 => 31,  126 => 30,  124 => 22,  121 => 21,  118 => 13,  109 => 12,  96 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"DataTable\" %}
{% trans_default_domain \"DataTable\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\">
        {% endblock %}
    {% endembed %}
    
    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}
    
{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.structure.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'structure.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'structure.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.structure.open, 'glyphicon-chevron-right': !tabAccordion.structure.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:structure_section.html.twig\", {'class_name':\"DataTable\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.data.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'data.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'data.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.data.open, 'glyphicon-chevron-right': !tabAccordion.data.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:data_section.html.twig\") }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarButtons %}
    <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-success\" ng-click=\"importCsv()\" ng-disabled=\"!isEditable()\">{{ 'form.button.import.csv'|trans }}</button>
    <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
    <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
    <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:DataTable:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/DataTable/form.html.twig");
    }
}


/* ConcertoPanelBundle:DataTable:form.html.twig */
class __TwigTemplate_133b319e208d4c5e25cd172674fc46eb21a8fb5dac9847d8c35ae508fa0da6c8___1485350920 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 42
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 42);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 44
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 45
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner.tooltip", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 46
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 47
        echo "                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                </select>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:DataTable:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1107 => 48,  1104 => 47,  1095 => 46,  1077 => 45,  1059 => 44,  1037 => 42,  890 => 37,  881 => 36,  863 => 35,  845 => 34,  823 => 32,  677 => 27,  668 => 26,  650 => 25,  632 => 24,  610 => 22,  464 => 18,  455 => 17,  437 => 16,  419 => 15,  397 => 13,  250 => 93,  246 => 92,  242 => 91,  238 => 90,  234 => 89,  230 => 88,  226 => 87,  221 => 86,  212 => 85,  199 => 81,  193 => 78,  189 => 77,  181 => 72,  175 => 69,  171 => 68,  167 => 66,  158 => 65,  147 => 62,  144 => 61,  142 => 53,  139 => 52,  137 => 42,  134 => 41,  131 => 32,  129 => 31,  126 => 30,  124 => 22,  121 => 21,  118 => 13,  109 => 12,  96 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"DataTable\" %}
{% trans_default_domain \"DataTable\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\">
        {% endblock %}
    {% endembed %}
    
    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}
    
{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.structure.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'structure.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'structure.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.structure.open, 'glyphicon-chevron-right': !tabAccordion.structure.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:structure_section.html.twig\", {'class_name':\"DataTable\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.data.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'data.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'data.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.data.open, 'glyphicon-chevron-right': !tabAccordion.data.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:data_section.html.twig\") }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarButtons %}
    <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-success\" ng-click=\"importCsv()\" ng-disabled=\"!isEditable()\">{{ 'form.button.import.csv'|trans }}</button>
    <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
    <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
    <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:DataTable:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/DataTable/form.html.twig");
    }
}


/* ConcertoPanelBundle:DataTable:form.html.twig */
class __TwigTemplate_133b319e208d4c5e25cd172674fc46eb21a8fb5dac9847d8c35ae508fa0da6c8___938993964 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 53
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:DataTable:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:DataTable:form.html.twig", 53);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 55
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 56
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups.tooltip", [], "DataTable"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 57
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 58
        echo "                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:DataTable:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1322 => 58,  1313 => 57,  1295 => 56,  1277 => 55,  1255 => 53,  1107 => 48,  1104 => 47,  1095 => 46,  1077 => 45,  1059 => 44,  1037 => 42,  890 => 37,  881 => 36,  863 => 35,  845 => 34,  823 => 32,  677 => 27,  668 => 26,  650 => 25,  632 => 24,  610 => 22,  464 => 18,  455 => 17,  437 => 16,  419 => 15,  397 => 13,  250 => 93,  246 => 92,  242 => 91,  238 => 90,  234 => 89,  230 => 88,  226 => 87,  221 => 86,  212 => 85,  199 => 81,  193 => 78,  189 => 77,  181 => 72,  175 => 69,  171 => 68,  167 => 66,  158 => 65,  147 => 62,  144 => 61,  142 => 53,  139 => 52,  137 => 42,  134 => 41,  131 => 32,  129 => 31,  126 => 30,  124 => 22,  121 => 21,  118 => 13,  109 => 12,  96 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"DataTable\" %}
{% trans_default_domain \"DataTable\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"DataTable\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\">
        {% endblock %}
    {% endembed %}
    
    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"DataTable\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}
    
{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.structure.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'structure.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'structure.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.structure.open, 'glyphicon-chevron-right': !tabAccordion.structure.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:structure_section.html.twig\", {'class_name':\"DataTable\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.data.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'data.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'data.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.data.open, 'glyphicon-chevron-right': !tabAccordion.data.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:DataTable:data_section.html.twig\") }}
    </uib-accordion-group>
{% endblock %}

{% block floatingBarButtons %}
    <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-success\" ng-click=\"importCsv()\" ng-disabled=\"!isEditable()\">{{ 'form.button.import.csv'|trans }}</button>
    <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
    <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
    <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
    <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
{% endblock %}", "ConcertoPanelBundle:DataTable:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/DataTable/form.html.twig");
    }
}
