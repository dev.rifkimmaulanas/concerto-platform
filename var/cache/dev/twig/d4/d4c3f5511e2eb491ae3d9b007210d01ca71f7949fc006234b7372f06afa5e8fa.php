<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_1_definer.html.twig */
class __TwigTemplate_9fc79ac94e9a922db4cfc7d089ad1a5bc5f578d155e8eb123ced6ce341682ffc extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig"));

        // line 1
        echo "<script type=\"text/ng-template\" id=\"type_1_definer.html\">
    <div ng-controller=\"WizardParamDefiner1Controller\">
        ";
        // line 3
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_1_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig", 3, "572092949")->display($context);
        // line 19
        echo "    </div>
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 19,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_1_definer.html\">
    <div ng-controller=\"WizardParamDefiner1Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ testWizardParamService.getDefinerTitle(param) }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.definer.titles.multi_line.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <textarea ng-model=\"param.definition.defvalue\" class='form-control'
                                  ng-model-options='{ updateOn: \"blur\" }'
                                  ng-disabled=\"!editable\"
                                  ng-trim=\"false\"></textarea>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_1_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_1_definer.html.twig */
class __TwigTemplate_9fc79ac94e9a922db4cfc7d089ad1a5bc5f578d155e8eb123ced6ce341682ffc___572092949 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'definition' => [$this, 'block_definition'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "ConcertoPanelBundle:TestWizard:type_base_definer.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_base_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_definition($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "definition"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "definition"));

        // line 6
        echo "                ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_1_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig", 6, "1074165733")->display($context);
        // line 17
        echo "            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 17,  148 => 6,  139 => 5,  117 => 3,  47 => 19,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_1_definer.html\">
    <div ng-controller=\"WizardParamDefiner1Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ testWizardParamService.getDefinerTitle(param) }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.definer.titles.multi_line.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <textarea ng-model=\"param.definition.defvalue\" class='form-control'
                                  ng-model-options='{ updateOn: \"blur\" }'
                                  ng-disabled=\"!editable\"
                                  ng-trim=\"false\"></textarea>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_1_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_1_definer.html.twig */
class __TwigTemplate_9fc79ac94e9a922db4cfc7d089ad1a5bc5f578d155e8eb123ced6ce341682ffc___1074165733 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'legend_tooltip' => [$this, 'block_legend_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 6
        return "ConcertoPanelBundle::form_v_single.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_single.html.twig", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig", 6);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo "{{ testWizardParamService.getDefinerTitle(param) }}";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.multi_line.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 11
        echo "                        <textarea ng-model=\"param.definition.defvalue\" class='form-control'
                                  ng-model-options='{ updateOn: \"blur\" }'
                                  ng-disabled=\"!editable\"
                                  ng-trim=\"false\"></textarea>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 11,  280 => 10,  262 => 9,  244 => 8,  222 => 6,  151 => 17,  148 => 6,  139 => 5,  117 => 3,  47 => 19,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_1_definer.html\">
    <div ng-controller=\"WizardParamDefiner1Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ testWizardParamService.getDefinerTitle(param) }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.definer.titles.multi_line.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <textarea ng-model=\"param.definition.defvalue\" class='form-control'
                                  ng-model-options='{ updateOn: \"blur\" }'
                                  ng-disabled=\"!editable\"
                                  ng-trim=\"false\"></textarea>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_1_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_1_definer.html.twig");
    }
}
