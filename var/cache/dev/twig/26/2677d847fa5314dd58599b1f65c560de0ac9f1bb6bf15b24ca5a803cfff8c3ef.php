<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Test:log_section.html.twig */
class __TwigTemplate_ae621c8558e30ec1ab5aa14322c2f2f2e446f42320a83c4d9e2635e482789f31 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:log_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:log_section.html.twig"));

        // line 2
        $context["class_name"] = "Test";
        // line 1
        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle:Test:log_section.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div class=\"center\">
        <button ng-click=\"refreshLogs();\" class=\"btn btn-default btn-sm\">";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.button.refresh", [], "Test"), "html", null, true);
        echo "</button>
        <button ng-click=\"gridService.downloadList(logsGridApi);\" class=\"btn btn-default btn-sm\">";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.download", [], "panel"), "html", null, true);
        echo "</button>
        <button ng-click=\"deleteSelectedLogs();\" class=\"btn btn-danger btn-sm\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.button.delete.checked", [], "Test"), "html", null, true);
        echo "</button>
        <button ng-click=\"deleteAllLogs();\" class=\"btn btn-danger btn-sm\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.list.button.delete.all", [], "Test"), "html", null, true);
        echo "</button>
    </div>
    <div ui-grid=\"logsOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:log_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 10,  79 => 9,  75 => 8,  71 => 7,  68 => 6,  59 => 5,  48 => 1,  46 => 2,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}

{% block content %}
    <div class=\"center\">
        <button ng-click=\"refreshLogs();\" class=\"btn btn-default btn-sm\">{{ \"log.list.button.refresh\"|trans }}</button>
        <button ng-click=\"gridService.downloadList(logsGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans({},\"panel\") }}</button>
        <button ng-click=\"deleteSelectedLogs();\" class=\"btn btn-danger btn-sm\">{{ \"log.list.button.delete.checked\"|trans }}</button>
        <button ng-click=\"deleteAllLogs();\" class=\"btn btn-danger btn-sm\">{{ \"log.list.button.delete.all\"|trans }}</button>
    </div>
    <div ui-grid=\"logsOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
{% endblock %}
", "ConcertoPanelBundle:Test:log_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/log_section.html.twig");
    }
}
