<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:UserTest:list.html.twig */
class __TwigTemplate_bc2034b884701c92544e7b7367004ff9736fb9762dbedd2c752bc14467a3044e extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:UserTest:list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:UserTest:list.html.twig"));

        // line 1
        $context["class_name"] = "UserTest";
        // line 3
        $context["exportable"] = false;
        // line 4
        echo "

";
        // line 6
        $this->displayBlock('content', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "    ";
        // line 42
        echo "\t
\t<div class=\"center\">
        <button ng-click=\"fetchAllCollections();\" class=\"btn btn-default btn-sm btn-list-refresh\">";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.refresh", [], "UserTest"), "html", null, true);
        echo "</button>
        <button ng-click=\"add();\" class=\"btn btn-success btn-sm\">";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.add", [], "UserTest"), "html", null, true);
        echo "</button>
        ";
        // line 47
        echo "        ";
        // line 48
        echo "        ";
        // line 49
        echo "        ";
        // line 50
        echo "    </div>
\t";
        // line 57
        echo "\t<div ui-grid=\"collectionOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid collectionTable\"></div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:UserTest:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 57,  88 => 50,  86 => 49,  84 => 48,  82 => 47,  78 => 45,  74 => 44,  70 => 42,  68 => 7,  50 => 6,  46 => 4,  44 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set class_name = \"UserTest\" %}
{% trans_default_domain \"UserTest\" %}
{% set exportable = false %}


{% block content %}
    {# <div class=\"left\">
        <div class=\"row\">
            <div class=\"col-sm-12\" style=\"margin-bottom:2rem; padding-left:2rem;\">
                <div class=\"row\" style=\"padding-left:2rem; padding-right:2rem;\">
                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"UserTest\" %}
                        {% block label %}
                            {{ 'form.field.username'|trans }}
                        {% endblock %}
                        {% block tooltip %}
                            {{'form.field.username.tooltip'|trans}}
                        {% endblock %}
                        {% block control %}
                            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'/>
                        {% endblock %}
                    {% endembed %}
                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"UserTest\" %}
                        {% block label %}
                            {{ 'form.field.password'|trans }}
                        {% endblock %}
                        {% block tooltip %}
                            {{'form.field.password.tooltip'|trans}}
                        {% endblock %}
                        {% block control %}
                            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'/>
                        {% endblock %}
                    {% endembed %}
                    <div class=\"col-sm-4\">
                        <button style=\"margin-top:1rem;\" class=\"btn btn-md btn-success\" ng-click=\"save()\">{{ 'form.button.save'|trans }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div> #}
\t
\t<div class=\"center\">
        <button ng-click=\"fetchAllCollections();\" class=\"btn btn-default btn-sm btn-list-refresh\">{{ \"list.button.refresh\"|trans }}</button>
        <button ng-click=\"add();\" class=\"btn btn-success btn-sm\">{{ \"list.button.add\"|trans }}</button>
        {# <button ng-click=\"import();\" class=\"btn btn-success btn-sm\" ng-if=\"exportable\">{{ \"list.button.import\"|trans }}</button> #}
        {# <button ng-click=\"exportSelected();\" class=\"btn btn-default btn-sm\" ng-if=\"exportable\">{{ \"list.button.export.checked\"|trans }}</button> #}
        {# <button ng-click=\"gridService.downloadList(collectionGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans }}</button> #}
        {# <button ng-click=\"deleteSelected();\" class=\"btn btn-danger btn-sm\" ng-disabled=\"collectionFilter.starterContent && !administrationSettingsService.starterContentEditable\">{{ \"list.button.delete.checked\"|trans }}</button><br/> #}
    </div>
\t{# <div class=\"center\" style=\"margin-top: 16px;\">
\t        <div class=\"btn-group\">
\t            <label class=\"btn btn-default btn-sm\" ng-model=\"collectionFilter.starterContent\" uib-btn-radio=\"false\" ng-click=\"refreshGrid()\">{{ \"list.filter.user_made\"|trans }}</label>
\t            <label class=\"btn btn-default btn-sm\" ng-model=\"collectionFilter.starterContent\" uib-btn-radio=\"true\" ng-click=\"refreshGrid()\">{{ \"list.filter.starter_content\"|trans }}</label>
\t        </div>
\t    </div> #}
\t<div ui-grid=\"collectionOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid collectionTable\"></div>
{% endblock %}
", "ConcertoPanelBundle:UserTest:list.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/UserTest/list.html.twig");
    }
}
