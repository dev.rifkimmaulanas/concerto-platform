<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_base_setter.html.twig */
class __TwigTemplate_ff910b46a408db7140882867a8568d7e42530f83369f4fc8f1b46c005d698e3e extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'setter' => [$this, 'block_setter'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig"));

        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "    <!-- mode: dialog/outer/grid -->
    <div ng-if=\"testWizardParamService.isParamVisible(param, parent, grandParent, values)\" ng-class=\"{'bs-callout': mode == 'outer', 'bs-callout-warning': mode == 'outer'}\">
        <h4 ng-bind=\"param.label\" class=\"carouselParamHeader\" ng-if=\"mode == 'outer'\"></h4>
        <p ng-bind-html=\"param.description | trustedHtml\" ng-if=\"mode == 'outer'\"></p>

        <div ng-if=\"mode=='dialog' || !isSetterComplex\">";
        // line 10
        $this->displayBlock('setter', $context, $blocks);
        echo "</div>
        <div ng-if=\"mode!='dialog' && isSetterComplex\">
            <i class='glyphicon glyphicon-align-justify clickable' ng-click='launchSetterDialog()' uib-tooltip-html='\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.form.field.setter.icon.tooltip", [], "TestWizard"), "html", null, true);
        echo "\"' tooltip-append-to-body='true'></i>
            <span class=\"wizardParamSummary\">";
        // line 13
        echo "{{summary}}";
        echo "</span>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_setter($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 10,  82 => 13,  78 => 12,  73 => 10,  66 => 5,  57 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"TestWizard\" %}

{% block content %}
    <!-- mode: dialog/outer/grid -->
    <div ng-if=\"testWizardParamService.isParamVisible(param, parent, grandParent, values)\" ng-class=\"{'bs-callout': mode == 'outer', 'bs-callout-warning': mode == 'outer'}\">
        <h4 ng-bind=\"param.label\" class=\"carouselParamHeader\" ng-if=\"mode == 'outer'\"></h4>
        <p ng-bind-html=\"param.description | trustedHtml\" ng-if=\"mode == 'outer'\"></p>

        <div ng-if=\"mode=='dialog' || !isSetterComplex\">{% block setter %}{% endblock %}</div>
        <div ng-if=\"mode!='dialog' && isSetterComplex\">
            <i class='glyphicon glyphicon-align-justify clickable' ng-click='launchSetterDialog()' uib-tooltip-html='\"{{ 'param.form.field.setter.icon.tooltip'|trans }}\"' tooltip-append-to-body='true'></i>
            <span class=\"wizardParamSummary\">{%verbatim%}{{summary}}{%endverbatim%}</span>
        </div>
    </div>
{% endblock %}", "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_base_setter.html.twig");
    }
}
