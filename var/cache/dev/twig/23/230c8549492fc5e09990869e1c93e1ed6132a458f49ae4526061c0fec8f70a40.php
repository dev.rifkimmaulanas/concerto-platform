<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:UserTest:tab.html.twig */
class __TwigTemplate_ec103ca2e6504260a7ac3c7fc4d36ab0bfc0aff39993b6e72b2452cecb9fbf63 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:UserTest:tab.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:UserTest:tab.html.twig"));

        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle:UserTest:tab.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "    <div class=\"moduleList\" ng-if=\"tabSection == 'list'\">
        ";
        // line 6
        echo twig_include($this->env, $context, (("ConcertoPanelBundle:" . (isset($context["class_name"]) || array_key_exists("class_name", $context) ? $context["class_name"] : (function () { throw new RuntimeError('Variable "class_name" does not exist.', 6, $this->source); })())) . ":list.html.twig"), ["class_name" => (isset($context["class_name"]) || array_key_exists("class_name", $context) ? $context["class_name"] : (function () { throw new RuntimeError('Variable "class_name" does not exist.', 6, $this->source); })())]);
        echo "
    </div>

    <div class=\"moduleForm\" ng-if=\"tabSection == 'form'\">
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller((("ConcertoPanelBundle:" . (isset($context["class_name"]) || array_key_exists("class_name", $context) ? $context["class_name"] : (function () { throw new RuntimeError('Variable "class_name" does not exist.', 10, $this->source); })())) . ":form")));
        echo "
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:UserTest:tab.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 10,  68 => 6,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"panel\" %}

{% block content %}
    <div class=\"moduleList\" ng-if=\"tabSection == 'list'\">
        {{ include(\"ConcertoPanelBundle:\"~ class_name ~\":list.html.twig\", {'class_name':class_name}) }}
    </div>

    <div class=\"moduleForm\" ng-if=\"tabSection == 'form'\">
        {{ render(controller(\"ConcertoPanelBundle:\"~ class_name ~\":form\")) }}
    </div>
{% endblock %}", "ConcertoPanelBundle:UserTest:tab.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/UserTest/tab.html.twig");
    }
}
