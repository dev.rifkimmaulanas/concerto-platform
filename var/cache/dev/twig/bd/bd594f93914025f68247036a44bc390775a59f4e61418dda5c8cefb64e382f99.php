<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_8_setter.html.twig */
class __TwigTemplate_075eb135bf7842a378a66a09defd7a3a908db023d8cb08cc8f1fcfa8ba988980 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig"));

        // line 1
        echo "<script type=\"text/ng-template\" id=\"type_8_setter.html\">
    <div ng-controller=\"WizardParamSetter8Controller\">
        ";
        // line 3
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_8_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig", 3, "260110109")->display($context);
        // line 14
        echo "    </div>
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 14,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_8_setter.html\">
    <div ng-controller=\"WizardParamSetter8Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                <select ng-model=\"output\" ng-change=\"onPrimitiveValueChange(output)\"
                        ng-disabled=\"!editable\"
                        class='form-control'>
                    <option value=\"\">{{ \"none.choosen\"|trans({},\"panel\") }}</option>
                    <option ng-repeat=\"test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" ng-value=\"test.name\" ng-bind=\"test.name\"></option>
                </select>
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_8_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_8_setter.html.twig */
class __TwigTemplate_075eb135bf7842a378a66a09defd7a3a908db023d8cb08cc8f1fcfa8ba988980___260110109 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'setter' => [$this, 'block_setter'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_base_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_setter($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        // line 6
        echo "                <select ng-model=\"output\" ng-change=\"onPrimitiveValueChange(output)\"
                        ng-disabled=\"!editable\"
                        class='form-control'>
                    <option value=\"\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                    <option ng-repeat=\"test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" ng-value=\"test.name\" ng-bind=\"test.name\"></option>
                </select>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 9,  143 => 6,  134 => 5,  112 => 3,  47 => 14,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_8_setter.html\">
    <div ng-controller=\"WizardParamSetter8Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                <select ng-model=\"output\" ng-change=\"onPrimitiveValueChange(output)\"
                        ng-disabled=\"!editable\"
                        class='form-control'>
                    <option value=\"\">{{ \"none.choosen\"|trans({},\"panel\") }}</option>
                    <option ng-repeat=\"test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" ng-value=\"test.name\" ng-bind=\"test.name\"></option>
                </select>
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_8_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_8_setter.html.twig");
    }
}
