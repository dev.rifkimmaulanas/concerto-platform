<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Test:output_section.html.twig */
class __TwigTemplate_6c11d9d35e17ade2c5434a67df52edb5370dfa53b6bd20638a34676cdd058619 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:output_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:output_section.html.twig"));

        // line 2
        $context["class_name"] = "Test";
        // line 1
        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle:Test:output_section.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <table style=\"width:100%;\">
        <tr>
            <td style=\"width:50%;\">
                <div class=\"center\">
                    <h4 align=\"center\">
                        <i class=\"glyphicon glyphicon-question-sign\" uib-tooltip-html=\"'";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.legend.tooltip", [], "Test"), "html", null, true);
        echo "'\"></i>
                        ";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.legend", [], "Test"), "html", null, true);
        echo "
                    </h4>
                    <div class=\"divButtonContainer center\">
                        <button ng-disabled=\"!isEditable()\" class=\"btn btn-success btn-sm\" ng-click=\"addVariable(1)\">";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.list.add", [], "Test"), "html", null, true);
        echo "</button>
                        <button ng-click=\"gridService.downloadList(returnsGridApi);\" class=\"btn btn-default btn-sm\">";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.download", [], "panel"), "html", null, true);
        echo "</button>
                        <button ng-disabled=\"!isEditable()\" class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedVariables(1)\">";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.returns.list.delete.checked", [], "Test"), "html", null, true);
        echo "</button>
                    </div>
                </div>
                <div ui-grid=\"returnsOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
            </td>
            <td style=\"width:50%;\">
                <div class=\"center\">
                    <h4 align=\"center\">
                        <i class=\"glyphicon glyphicon-question-sign\" uib-tooltip-html=\"'";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.legend.tooltip", [], "Test"), "html", null, true);
        echo "'\"></i>
                        ";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.legend", [], "Test"), "html", null, true);
        echo "
                    </h4>
                    <div class=\"divButtonContainer center\">
                        <button ng-disabled=\"!isEditable()\" class=\"btn btn-success btn-sm\" ng-click=\"addVariable(2)\">";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.list.add", [], "Test"), "html", null, true);
        echo "</button>
                        <button ng-click=\"gridService.downloadList(branchesGridApi);\" class=\"btn btn-default btn-sm\">";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.download", [], "panel"), "html", null, true);
        echo "</button>
                        <button ng-disabled=\"!isEditable()\" class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedVariables(2)\">";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.branches.list.delete.checked", [], "Test"), "html", null, true);
        echo "</button>
                    </div>
                </div>
                <div ui-grid=\"branchesOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
            </td>
        </tr>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:output_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 31,  118 => 30,  114 => 29,  108 => 26,  104 => 25,  93 => 17,  89 => 16,  85 => 15,  79 => 12,  75 => 11,  68 => 6,  59 => 5,  48 => 1,  46 => 2,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}

{% block content %}
    <table style=\"width:100%;\">
        <tr>
            <td style=\"width:50%;\">
                <div class=\"center\">
                    <h4 align=\"center\">
                        <i class=\"glyphicon glyphicon-question-sign\" uib-tooltip-html=\"'{{ 'variables.output.returns.legend.tooltip'|trans }}'\"></i>
                        {{ 'variables.output.returns.legend'|trans }}
                    </h4>
                    <div class=\"divButtonContainer center\">
                        <button ng-disabled=\"!isEditable()\" class=\"btn btn-success btn-sm\" ng-click=\"addVariable(1)\">{{ 'variables.output.returns.list.add'|trans }}</button>
                        <button ng-click=\"gridService.downloadList(returnsGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans({},\"panel\") }}</button>
                        <button ng-disabled=\"!isEditable()\" class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedVariables(1)\">{{ 'variables.output.returns.list.delete.checked'|trans }}</button>
                    </div>
                </div>
                <div ui-grid=\"returnsOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
            </td>
            <td style=\"width:50%;\">
                <div class=\"center\">
                    <h4 align=\"center\">
                        <i class=\"glyphicon glyphicon-question-sign\" uib-tooltip-html=\"'{{ 'variables.output.branches.legend.tooltip'|trans }}'\"></i>
                        {{ 'variables.output.branches.legend'|trans }}
                    </h4>
                    <div class=\"divButtonContainer center\">
                        <button ng-disabled=\"!isEditable()\" class=\"btn btn-success btn-sm\" ng-click=\"addVariable(2)\">{{ 'variables.output.branches.list.add'|trans }}</button>
                        <button ng-click=\"gridService.downloadList(branchesGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans({},\"panel\") }}</button>
                        <button ng-disabled=\"!isEditable()\" class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedVariables(2)\">{{ 'variables.output.branches.list.delete.checked'|trans }}</button>
                    </div>
                </div>
                <div ui-grid=\"branchesOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
            </td>
        </tr>
    </table>
{% endblock %}", "ConcertoPanelBundle:Test:output_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/output_section.html.twig");
    }
}
