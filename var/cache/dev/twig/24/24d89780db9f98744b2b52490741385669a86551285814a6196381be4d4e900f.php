<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:carousel.html.twig */
class __TwigTemplate_58ad9db6031d4dcc4e9af45cc962ab089f6a8dece05e9cd04ac59f9eed266a41 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:carousel.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:carousel.html.twig"));

        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle:TestWizard:carousel.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "    <uib-alert type=\"warning\" ng-if=\"object.steps == 0\">
        <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
        ";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("alerts.no_steps", [], "TestWizard"), "html", null, true);
        echo "
    </uib-alert>

    <uib-tabset ng-if=\"object.steps != 0\">
        <uib-tab ng-repeat=\"step in object.steps\">
            <uib-tab-heading>";
        // line 12
        echo "{{ step.title }}";
        echo "</uib-tab-heading>
            <div ng-controller=\"TestWizardCarouselStepController\">
                <div class=\"bs-callout bs-callout-info\" align=\"left\">
                    <h3 ng-bind=\"step.title\"></h3>
                    <p ng-bind-html=\"step.description | trustedHtml\"></p>
                </div>
                <uib-alert type=\"warning\" ng-if=\"(step.params|filter:filterByGuiEligible).length == 0\">
                    <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                    ";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("alerts.no_params", [], "TestWizard"), "html", null, true);
        echo "
                </uib-alert>
                <div ng-repeat=\"param in step.params | filter:filterByGuiEligible | orderBy: 'order' track by \$index\">
                    <wizard-param-setter editable=\"isEditable()\"
                                         param=\"param\"
                                         parent=\"null\"
                                         grand-parent=\"null\"
                                         output=\"param.output\"
                                         mode=\"outer\"
                                         wizard-mode=\"";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["wizard_mode"]) || array_key_exists("wizard_mode", $context) ? $context["wizard_mode"] : (function () { throw new RuntimeError('Variable "wizard_mode" does not exist.', 29, $this->source); })()), "html", null, true);
        echo "\"
                                         values=\"values\"
                                         wizard-object=\"object\"
                                         under-list=\"false\"></wizard-param-setter>
                </div>
            </div>
        </uib-tab>
    </uib-tabset>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:carousel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 29,  88 => 20,  77 => 12,  69 => 7,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"TestWizard\" %}

{% block content %}
    <uib-alert type=\"warning\" ng-if=\"object.steps == 0\">
        <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
        {{ 'alerts.no_steps'|trans }}
    </uib-alert>

    <uib-tabset ng-if=\"object.steps != 0\">
        <uib-tab ng-repeat=\"step in object.steps\">
            <uib-tab-heading>{% verbatim %}{{ step.title }}{% endverbatim %}</uib-tab-heading>
            <div ng-controller=\"TestWizardCarouselStepController\">
                <div class=\"bs-callout bs-callout-info\" align=\"left\">
                    <h3 ng-bind=\"step.title\"></h3>
                    <p ng-bind-html=\"step.description | trustedHtml\"></p>
                </div>
                <uib-alert type=\"warning\" ng-if=\"(step.params|filter:filterByGuiEligible).length == 0\">
                    <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                    {{ 'alerts.no_params'|trans }}
                </uib-alert>
                <div ng-repeat=\"param in step.params | filter:filterByGuiEligible | orderBy: 'order' track by \$index\">
                    <wizard-param-setter editable=\"isEditable()\"
                                         param=\"param\"
                                         parent=\"null\"
                                         grand-parent=\"null\"
                                         output=\"param.output\"
                                         mode=\"outer\"
                                         wizard-mode=\"{{ wizard_mode }}\"
                                         values=\"values\"
                                         wizard-object=\"object\"
                                         under-list=\"false\"></wizard-param-setter>
                </div>
            </div>
        </uib-tab>
    </uib-tabset>

{% endblock %}", "ConcertoPanelBundle:TestWizard:carousel.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/carousel.html.twig");
    }
}
