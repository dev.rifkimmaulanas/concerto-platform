<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_3_definer.html.twig */
class __TwigTemplate_0b4ffbc7f6f817e1fc1bf07fc460e8cb59b583ad3ef2776735768641191824b4 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        // line 1
        echo "<script type=\"text/ng-template\" id=\"type_3_definer.html\">
    <div ng-controller=\"WizardParamDefiner3Controller\">
        ";
        // line 3
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", 3, "923704682")->display($context);
        // line 47
        echo "    </div>
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_3_definer.html\">
    <div ng-controller=\"WizardParamDefiner3Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}
                {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ testWizardParamService.getDefinerTitle(param) }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.definer.titles.select.tooltip'|trans }}{% endblock %}

                    {% block elements %}
                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.options'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.options.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <div class=\"center\">
                                    <button class=\"btn btn-success btn-sm\" ng-click=\"addOption()\">{{ 'param.definer.select.option.add'|trans }}</button>
                                    <button ng-click=\"gridService.uploadList(selectGridApi);\" class=\"btn btn-success btn-sm\">{{ \"list.button.upload\"|trans({},\"panel\") }}</button>
                                    <button ng-click=\"gridService.downloadList(selectGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans({},\"panel\") }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedOptions()\">{{ 'param.definer.select.option.remove.selected'|trans }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteAllOptions()\">{{ 'param.definer.select.option.remove.all'|trans }}</button>
                                </div>
                                <div ui-grid=\"selectOptions\" ui-grid-edit ui-grid-row-edit ui-grid-auto-resize
                                     ui-grid-cellNav ui-grid-resize-columns ui-grid-exporter ui-grid-importer
                                     ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.default'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.default.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <select ng-model=\"param.definition.defvalue\"
                                        ng-disabled=\"!editable\"
                                        class='form-control'
                                        ng-options=\"opt.value as opt.label for opt in param.definition.options | orderBy:'order'\">
                                    <option value=\"\">{{ \"none.choosen\"|trans({},\"panel\") }}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_3_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_3_definer.html.twig */
class __TwigTemplate_0b4ffbc7f6f817e1fc1bf07fc460e8cb59b583ad3ef2776735768641191824b4___923704682 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'definition' => [$this, 'block_definition'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "ConcertoPanelBundle:TestWizard:type_base_definer.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_base_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_definition($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "definition"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "definition"));

        // line 6
        echo "                ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", 6, "1959884417")->display($context);
        // line 45
        echo "            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 45,  176 => 6,  167 => 5,  145 => 3,  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_3_definer.html\">
    <div ng-controller=\"WizardParamDefiner3Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}
                {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ testWizardParamService.getDefinerTitle(param) }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.definer.titles.select.tooltip'|trans }}{% endblock %}

                    {% block elements %}
                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.options'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.options.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <div class=\"center\">
                                    <button class=\"btn btn-success btn-sm\" ng-click=\"addOption()\">{{ 'param.definer.select.option.add'|trans }}</button>
                                    <button ng-click=\"gridService.uploadList(selectGridApi);\" class=\"btn btn-success btn-sm\">{{ \"list.button.upload\"|trans({},\"panel\") }}</button>
                                    <button ng-click=\"gridService.downloadList(selectGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans({},\"panel\") }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedOptions()\">{{ 'param.definer.select.option.remove.selected'|trans }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteAllOptions()\">{{ 'param.definer.select.option.remove.all'|trans }}</button>
                                </div>
                                <div ui-grid=\"selectOptions\" ui-grid-edit ui-grid-row-edit ui-grid-auto-resize
                                     ui-grid-cellNav ui-grid-resize-columns ui-grid-exporter ui-grid-importer
                                     ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.default'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.default.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <select ng-model=\"param.definition.defvalue\"
                                        ng-disabled=\"!editable\"
                                        class='form-control'
                                        ng-options=\"opt.value as opt.label for opt in param.definition.options | orderBy:'order'\">
                                    <option value=\"\">{{ \"none.choosen\"|trans({},\"panel\") }}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_3_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_3_definer.html.twig */
class __TwigTemplate_0b4ffbc7f6f817e1fc1bf07fc460e8cb59b583ad3ef2776735768641191824b4___1959884417 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'legend_tooltip' => [$this, 'block_legend_tooltip'],
            'elements' => [$this, 'block_elements'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 6
        return "ConcertoPanelBundle::form_v_panel.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_panel.html.twig", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", 6);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo "{{ testWizardParamService.getDefinerTitle(param) }}";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.titles.select.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 12
        echo "                        ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", 12, "603534139")->display($context);
        // line 29
        echo "
                        ";
        // line 30
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", 30, "654879289")->display($context);
        // line 43
        echo "                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  353 => 43,  351 => 30,  348 => 29,  345 => 12,  336 => 11,  318 => 9,  300 => 8,  278 => 6,  179 => 45,  176 => 6,  167 => 5,  145 => 3,  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_3_definer.html\">
    <div ng-controller=\"WizardParamDefiner3Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}
                {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ testWizardParamService.getDefinerTitle(param) }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.definer.titles.select.tooltip'|trans }}{% endblock %}

                    {% block elements %}
                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.options'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.options.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <div class=\"center\">
                                    <button class=\"btn btn-success btn-sm\" ng-click=\"addOption()\">{{ 'param.definer.select.option.add'|trans }}</button>
                                    <button ng-click=\"gridService.uploadList(selectGridApi);\" class=\"btn btn-success btn-sm\">{{ \"list.button.upload\"|trans({},\"panel\") }}</button>
                                    <button ng-click=\"gridService.downloadList(selectGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans({},\"panel\") }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedOptions()\">{{ 'param.definer.select.option.remove.selected'|trans }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteAllOptions()\">{{ 'param.definer.select.option.remove.all'|trans }}</button>
                                </div>
                                <div ui-grid=\"selectOptions\" ui-grid-edit ui-grid-row-edit ui-grid-auto-resize
                                     ui-grid-cellNav ui-grid-resize-columns ui-grid-exporter ui-grid-importer
                                     ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.default'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.default.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <select ng-model=\"param.definition.defvalue\"
                                        ng-disabled=\"!editable\"
                                        class='form-control'
                                        ng-options=\"opt.value as opt.label for opt in param.definition.options | orderBy:'order'\">
                                    <option value=\"\">{{ \"none.choosen\"|trans({},\"panel\") }}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_3_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_3_definer.html.twig */
class __TwigTemplate_0b4ffbc7f6f817e1fc1bf07fc460e8cb59b583ad3ef2776735768641191824b4___603534139 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", 12);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 14
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.options", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.options.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 17
        echo "                                <div class=\"center\">
                                    <button class=\"btn btn-success btn-sm\" ng-click=\"addOption()\">";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.option.add", [], "TestWizard"), "html", null, true);
        echo "</button>
                                    <button ng-click=\"gridService.uploadList(selectGridApi);\" class=\"btn btn-success btn-sm\">";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.upload", [], "panel"), "html", null, true);
        echo "</button>
                                    <button ng-click=\"gridService.downloadList(selectGridApi);\" class=\"btn btn-default btn-sm\">";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.download", [], "panel"), "html", null, true);
        echo "</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedOptions()\">";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.option.remove.selected", [], "TestWizard"), "html", null, true);
        echo "</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteAllOptions()\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.option.remove.all", [], "TestWizard"), "html", null, true);
        echo "</button>
                                </div>
                                <div ui-grid=\"selectOptions\" ui-grid-edit ui-grid-row-edit ui-grid-auto-resize
                                     ui-grid-cellNav ui-grid-resize-columns ui-grid-exporter ui-grid-importer
                                     ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  538 => 22,  534 => 21,  530 => 20,  526 => 19,  522 => 18,  519 => 17,  510 => 16,  492 => 15,  474 => 14,  452 => 12,  353 => 43,  351 => 30,  348 => 29,  345 => 12,  336 => 11,  318 => 9,  300 => 8,  278 => 6,  179 => 45,  176 => 6,  167 => 5,  145 => 3,  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_3_definer.html\">
    <div ng-controller=\"WizardParamDefiner3Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}
                {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ testWizardParamService.getDefinerTitle(param) }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.definer.titles.select.tooltip'|trans }}{% endblock %}

                    {% block elements %}
                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.options'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.options.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <div class=\"center\">
                                    <button class=\"btn btn-success btn-sm\" ng-click=\"addOption()\">{{ 'param.definer.select.option.add'|trans }}</button>
                                    <button ng-click=\"gridService.uploadList(selectGridApi);\" class=\"btn btn-success btn-sm\">{{ \"list.button.upload\"|trans({},\"panel\") }}</button>
                                    <button ng-click=\"gridService.downloadList(selectGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans({},\"panel\") }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedOptions()\">{{ 'param.definer.select.option.remove.selected'|trans }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteAllOptions()\">{{ 'param.definer.select.option.remove.all'|trans }}</button>
                                </div>
                                <div ui-grid=\"selectOptions\" ui-grid-edit ui-grid-row-edit ui-grid-auto-resize
                                     ui-grid-cellNav ui-grid-resize-columns ui-grid-exporter ui-grid-importer
                                     ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.default'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.default.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <select ng-model=\"param.definition.defvalue\"
                                        ng-disabled=\"!editable\"
                                        class='form-control'
                                        ng-options=\"opt.value as opt.label for opt in param.definition.options | orderBy:'order'\">
                                    <option value=\"\">{{ \"none.choosen\"|trans({},\"panel\") }}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_3_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_3_definer.html.twig */
class __TwigTemplate_0b4ffbc7f6f817e1fc1bf07fc460e8cb59b583ad3ef2776735768641191824b4___654879289 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 30
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", 30);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 32
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.default", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 33
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.definer.select.default.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 34
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 35
        echo "                                <select ng-model=\"param.definition.defvalue\"
                                        ng-disabled=\"!editable\"
                                        class='form-control'
                                        ng-options=\"opt.value as opt.label for opt in param.definition.options | orderBy:'order'\">
                                    <option value=\"\">";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                                </select>
                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  716 => 39,  710 => 35,  701 => 34,  683 => 33,  665 => 32,  643 => 30,  538 => 22,  534 => 21,  530 => 20,  526 => 19,  522 => 18,  519 => 17,  510 => 16,  492 => 15,  474 => 14,  452 => 12,  353 => 43,  351 => 30,  348 => 29,  345 => 12,  336 => 11,  318 => 9,  300 => 8,  278 => 6,  179 => 45,  176 => 6,  167 => 5,  145 => 3,  47 => 47,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_3_definer.html\">
    <div ng-controller=\"WizardParamDefiner3Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}
                {% embed \"ConcertoPanelBundle::form_v_panel.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ testWizardParamService.getDefinerTitle(param) }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.definer.titles.select.tooltip'|trans }}{% endblock %}

                    {% block elements %}
                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.options'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.options.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <div class=\"center\">
                                    <button class=\"btn btn-success btn-sm\" ng-click=\"addOption()\">{{ 'param.definer.select.option.add'|trans }}</button>
                                    <button ng-click=\"gridService.uploadList(selectGridApi);\" class=\"btn btn-success btn-sm\">{{ \"list.button.upload\"|trans({},\"panel\") }}</button>
                                    <button ng-click=\"gridService.downloadList(selectGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans({},\"panel\") }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteSelectedOptions()\">{{ 'param.definer.select.option.remove.selected'|trans }}</button>
                                    <button class=\"btn btn-danger btn-sm\" ng-click=\"deleteAllOptions()\">{{ 'param.definer.select.option.remove.all'|trans }}</button>
                                </div>
                                <div ui-grid=\"selectOptions\" ui-grid-edit ui-grid-row-edit ui-grid-auto-resize
                                     ui-grid-cellNav ui-grid-resize-columns ui-grid-exporter ui-grid-importer
                                     ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"TestWizard\" %}
                            {% block label %}{{ 'param.definer.select.default'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'param.definer.select.default.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <select ng-model=\"param.definition.defvalue\"
                                        ng-disabled=\"!editable\"
                                        class='form-control'
                                        ng-options=\"opt.value as opt.label for opt in param.definition.options | orderBy:'order'\">
                                    <option value=\"\">{{ \"none.choosen\"|trans({},\"panel\") }}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_3_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_3_definer.html.twig");
    }
}
