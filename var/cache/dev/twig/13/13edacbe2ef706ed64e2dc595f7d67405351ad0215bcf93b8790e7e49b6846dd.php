<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:form.html.twig */
class __TwigTemplate_338c04ad8f3a2e51ce06be1d64c5998aad376e7a0c48b394dedcc28bce71e389 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'top' => [$this, 'block_top'],
            'legend' => [$this, 'block_legend'],
            'elements' => [$this, 'block_elements'],
            'sections' => [$this, 'block_sections'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "ConcertoPanelBundle::form_h.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        // line 2
        $context["class_name"] = "TestWizard";
        // line 4
        $context["exportable"] = true;
        // line 1
        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_top($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        // line 7
        echo "    ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle::lock_info.html.twig");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo " 
    <span ng-bind-html=\"formTitle\"></span>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 13
        echo "    ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:form.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 13, "229900911")->display($context);
        // line 21
        echo "
    ";
        // line 22
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:form.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 22, "1771663787")->display($context);
        // line 30
        echo "
    ";
        // line 31
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:form.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 31, "1601084649")->display($context);
        // line 47
        echo "
    ";
        // line 48
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 48, $this->source); })()), "user", [], "any", false, false, false, 48) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
            // line 49
            echo "        ";
            $this->loadTemplate("ConcertoPanelBundle:TestWizard:form.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 49, "1324790251")->display($context);
            // line 58
            echo "
        ";
            // line 59
            $this->loadTemplate("ConcertoPanelBundle:TestWizard:form.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 59, "1254892268")->display($context);
            // line 69
            echo "
        ";
            // line 70
            $this->loadTemplate("ConcertoPanelBundle:TestWizard:form.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 70, "428721045")->display($context);
            // line 78
            echo "    ";
        }
        // line 79
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 82
    public function block_sections($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        // line 83
        echo "    <uib-accordion-group is-open=\"tabAccordion.step.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.fieldset.legend.tooltip", [], "TestWizard"), "html", null, true);
        echo "'\"></i>
            ";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.fieldset.legend", [], "TestWizard"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.step.open, 'glyphicon-chevron-right': !tabAccordion.step.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 89
        echo twig_include($this->env, $context, "ConcertoPanelBundle:TestWizard:step_section.html.twig", ["class_name" => "TestWizard"]);
        echo "
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.param.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.fieldset.legend.tooltip", [], "TestWizard"), "html", null, true);
        echo "'\"></i>
            ";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.fieldset.legend", [], "TestWizard"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.param.open, 'glyphicon-chevron-right': !tabAccordion.param.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 98
        echo twig_include($this->env, $context, "ConcertoPanelBundle:TestWizard:param_section.html.twig", ["class_name" => "TestWizard"]);
        echo "
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.preview.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("preview.fieldset.legend.tooltip", [], "TestWizard"), "html", null, true);
        echo "'\"></i>
            ";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("preview.fieldset.legend", [], "TestWizard"), "html", null, true);
        echo "
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.preview.open, 'glyphicon-chevron-right': !tabAccordion.preview.open}\"></i>
        </uib-accordion-heading>
        ";
        // line 107
        echo twig_include($this->env, $context, "ConcertoPanelBundle:TestWizard:preview_section.html.twig", ["class_name" => "TestWizard"]);
        echo "
    </uib-accordion-group>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 107,  213 => 104,  209 => 103,  201 => 98,  195 => 95,  191 => 94,  183 => 89,  177 => 86,  173 => 85,  169 => 83,  160 => 82,  149 => 79,  146 => 78,  144 => 70,  141 => 69,  139 => 59,  136 => 58,  133 => 49,  131 => 48,  128 => 47,  126 => 31,  123 => 30,  121 => 22,  118 => 21,  115 => 13,  106 => 12,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"TestWizard\" %}
{% trans_default_domain \"TestWizard\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    <span ng-bind-html=\"formTitle\"></span>
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.test\" style=\"width:100%;\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    ng-options=\"test.id as test.name for test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
            </select>
        {% endblock %}
        {% block extra_info %}
            <a ng-href=\"#/tests/{% verbatim %}{{object.test}}{% endverbatim %}\" ng-show=\"object.test !== null\">
                <i class=\"glyphicon glyphicon-link\"></i>
            </a>
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.step.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'step.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'step.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.step.open, 'glyphicon-chevron-right': !tabAccordion.step.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:step_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.param.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'param.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'param.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.param.open, 'glyphicon-chevron-right': !tabAccordion.param.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:param_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.preview.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'preview.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'preview.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.preview.open, 'glyphicon-chevron-right': !tabAccordion.preview.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:preview_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>
{% endblock %}", "ConcertoPanelBundle:TestWizard:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/form.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:form.html.twig */
class __TwigTemplate_338c04ad8f3a2e51ce06be1d64c5998aad376e7a0c48b394dedcc28bce71e389___229900911 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 13
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 13);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 15
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.name", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.name.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 17
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 18
        echo "            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  449 => 18,  440 => 17,  422 => 16,  404 => 15,  382 => 13,  219 => 107,  213 => 104,  209 => 103,  201 => 98,  195 => 95,  191 => 94,  183 => 89,  177 => 86,  173 => 85,  169 => 83,  160 => 82,  149 => 79,  146 => 78,  144 => 70,  141 => 69,  139 => 59,  136 => 58,  133 => 49,  131 => 48,  128 => 47,  126 => 31,  123 => 30,  121 => 22,  118 => 21,  115 => 13,  106 => 12,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"TestWizard\" %}
{% trans_default_domain \"TestWizard\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    <span ng-bind-html=\"formTitle\"></span>
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.test\" style=\"width:100%;\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    ng-options=\"test.id as test.name for test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
            </select>
        {% endblock %}
        {% block extra_info %}
            <a ng-href=\"#/tests/{% verbatim %}{{object.test}}{% endverbatim %}\" ng-show=\"object.test !== null\">
                <i class=\"glyphicon glyphicon-link\"></i>
            </a>
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.step.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'step.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'step.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.step.open, 'glyphicon-chevron-right': !tabAccordion.step.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:step_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.param.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'param.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'param.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.param.open, 'glyphicon-chevron-right': !tabAccordion.param.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:param_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.preview.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'preview.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'preview.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.preview.open, 'glyphicon-chevron-right': !tabAccordion.preview.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:preview_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>
{% endblock %}", "ConcertoPanelBundle:TestWizard:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/form.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:form.html.twig */
class __TwigTemplate_338c04ad8f3a2e51ce06be1d64c5998aad376e7a0c48b394dedcc28bce71e389___1771663787 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 22
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 22);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 24
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 26
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 27
        echo "            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  677 => 27,  668 => 26,  650 => 25,  632 => 24,  610 => 22,  449 => 18,  440 => 17,  422 => 16,  404 => 15,  382 => 13,  219 => 107,  213 => 104,  209 => 103,  201 => 98,  195 => 95,  191 => 94,  183 => 89,  177 => 86,  173 => 85,  169 => 83,  160 => 82,  149 => 79,  146 => 78,  144 => 70,  141 => 69,  139 => 59,  136 => 58,  133 => 49,  131 => 48,  128 => 47,  126 => 31,  123 => 30,  121 => 22,  118 => 21,  115 => 13,  106 => 12,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"TestWizard\" %}
{% trans_default_domain \"TestWizard\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    <span ng-bind-html=\"formTitle\"></span>
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.test\" style=\"width:100%;\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    ng-options=\"test.id as test.name for test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
            </select>
        {% endblock %}
        {% block extra_info %}
            <a ng-href=\"#/tests/{% verbatim %}{{object.test}}{% endverbatim %}\" ng-show=\"object.test !== null\">
                <i class=\"glyphicon glyphicon-link\"></i>
            </a>
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.step.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'step.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'step.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.step.open, 'glyphicon-chevron-right': !tabAccordion.step.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:step_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.param.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'param.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'param.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.param.open, 'glyphicon-chevron-right': !tabAccordion.param.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:param_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.preview.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'preview.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'preview.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.preview.open, 'glyphicon-chevron-right': !tabAccordion.preview.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:preview_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>
{% endblock %}", "ConcertoPanelBundle:TestWizard:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/form.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:form.html.twig */
class __TwigTemplate_338c04ad8f3a2e51ce06be1d64c5998aad376e7a0c48b394dedcc28bce71e389___1601084649 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
            'extra_info' => [$this, 'block_extra_info'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 31
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 31);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 33
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.test", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 34
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.test.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 35
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 36
        echo "            <select ng-model=\"object.test\" style=\"width:100%;\" ";
        if (((isset($context["isAddDialog"]) || array_key_exists("isAddDialog", $context) ? $context["isAddDialog"] : (function () { throw new RuntimeError('Variable "isAddDialog" does not exist.', 36, $this->source); })()) == false)) {
            echo "ng-disabled=\"true\"";
        } else {
            echo "ng-disabled=\"!isEditable()\"";
        }
        // line 37
        echo "                    ng-options=\"test.id as test.name for test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                <option value=\"\">";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
            </select>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 41
    public function block_extra_info($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_info"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_info"));

        // line 42
        echo "            <a ng-href=\"#/tests/";
        echo "{{object.test}}";
        echo "\" ng-show=\"object.test !== null\">
                <i class=\"glyphicon glyphicon-link\"></i>
            </a>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  938 => 42,  929 => 41,  916 => 38,  913 => 37,  906 => 36,  897 => 35,  879 => 34,  861 => 33,  839 => 31,  677 => 27,  668 => 26,  650 => 25,  632 => 24,  610 => 22,  449 => 18,  440 => 17,  422 => 16,  404 => 15,  382 => 13,  219 => 107,  213 => 104,  209 => 103,  201 => 98,  195 => 95,  191 => 94,  183 => 89,  177 => 86,  173 => 85,  169 => 83,  160 => 82,  149 => 79,  146 => 78,  144 => 70,  141 => 69,  139 => 59,  136 => 58,  133 => 49,  131 => 48,  128 => 47,  126 => 31,  123 => 30,  121 => 22,  118 => 21,  115 => 13,  106 => 12,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"TestWizard\" %}
{% trans_default_domain \"TestWizard\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    <span ng-bind-html=\"formTitle\"></span>
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.test\" style=\"width:100%;\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    ng-options=\"test.id as test.name for test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
            </select>
        {% endblock %}
        {% block extra_info %}
            <a ng-href=\"#/tests/{% verbatim %}{{object.test}}{% endverbatim %}\" ng-show=\"object.test !== null\">
                <i class=\"glyphicon glyphicon-link\"></i>
            </a>
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.step.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'step.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'step.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.step.open, 'glyphicon-chevron-right': !tabAccordion.step.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:step_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.param.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'param.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'param.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.param.open, 'glyphicon-chevron-right': !tabAccordion.param.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:param_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.preview.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'preview.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'preview.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.preview.open, 'glyphicon-chevron-right': !tabAccordion.preview.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:preview_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>
{% endblock %}", "ConcertoPanelBundle:TestWizard:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/form.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:form.html.twig */
class __TwigTemplate_338c04ad8f3a2e51ce06be1d64c5998aad376e7a0c48b394dedcc28bce71e389___1324790251 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 49
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 49);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 51
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 52
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 53
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 54
        echo "                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1170 => 54,  1161 => 53,  1143 => 52,  1125 => 51,  1103 => 49,  938 => 42,  929 => 41,  916 => 38,  913 => 37,  906 => 36,  897 => 35,  879 => 34,  861 => 33,  839 => 31,  677 => 27,  668 => 26,  650 => 25,  632 => 24,  610 => 22,  449 => 18,  440 => 17,  422 => 16,  404 => 15,  382 => 13,  219 => 107,  213 => 104,  209 => 103,  201 => 98,  195 => 95,  191 => 94,  183 => 89,  177 => 86,  173 => 85,  169 => 83,  160 => 82,  149 => 79,  146 => 78,  144 => 70,  141 => 69,  139 => 59,  136 => 58,  133 => 49,  131 => 48,  128 => 47,  126 => 31,  123 => 30,  121 => 22,  118 => 21,  115 => 13,  106 => 12,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"TestWizard\" %}
{% trans_default_domain \"TestWizard\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    <span ng-bind-html=\"formTitle\"></span>
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.test\" style=\"width:100%;\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    ng-options=\"test.id as test.name for test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
            </select>
        {% endblock %}
        {% block extra_info %}
            <a ng-href=\"#/tests/{% verbatim %}{{object.test}}{% endverbatim %}\" ng-show=\"object.test !== null\">
                <i class=\"glyphicon glyphicon-link\"></i>
            </a>
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.step.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'step.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'step.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.step.open, 'glyphicon-chevron-right': !tabAccordion.step.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:step_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.param.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'param.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'param.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.param.open, 'glyphicon-chevron-right': !tabAccordion.param.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:param_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.preview.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'preview.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'preview.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.preview.open, 'glyphicon-chevron-right': !tabAccordion.preview.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:preview_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>
{% endblock %}", "ConcertoPanelBundle:TestWizard:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/form.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:form.html.twig */
class __TwigTemplate_338c04ad8f3a2e51ce06be1d64c5998aad376e7a0c48b394dedcc28bce71e389___1254892268 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 59
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 59);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 61
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 62
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 63
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 64
        echo "                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                </select>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1402 => 65,  1399 => 64,  1390 => 63,  1372 => 62,  1354 => 61,  1332 => 59,  1170 => 54,  1161 => 53,  1143 => 52,  1125 => 51,  1103 => 49,  938 => 42,  929 => 41,  916 => 38,  913 => 37,  906 => 36,  897 => 35,  879 => 34,  861 => 33,  839 => 31,  677 => 27,  668 => 26,  650 => 25,  632 => 24,  610 => 22,  449 => 18,  440 => 17,  422 => 16,  404 => 15,  382 => 13,  219 => 107,  213 => 104,  209 => 103,  201 => 98,  195 => 95,  191 => 94,  183 => 89,  177 => 86,  173 => 85,  169 => 83,  160 => 82,  149 => 79,  146 => 78,  144 => 70,  141 => 69,  139 => 59,  136 => 58,  133 => 49,  131 => 48,  128 => 47,  126 => 31,  123 => 30,  121 => 22,  118 => 21,  115 => 13,  106 => 12,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"TestWizard\" %}
{% trans_default_domain \"TestWizard\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    <span ng-bind-html=\"formTitle\"></span>
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.test\" style=\"width:100%;\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    ng-options=\"test.id as test.name for test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
            </select>
        {% endblock %}
        {% block extra_info %}
            <a ng-href=\"#/tests/{% verbatim %}{{object.test}}{% endverbatim %}\" ng-show=\"object.test !== null\">
                <i class=\"glyphicon glyphicon-link\"></i>
            </a>
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.step.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'step.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'step.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.step.open, 'glyphicon-chevron-right': !tabAccordion.step.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:step_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.param.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'param.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'param.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.param.open, 'glyphicon-chevron-right': !tabAccordion.param.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:param_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.preview.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'preview.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'preview.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.preview.open, 'glyphicon-chevron-right': !tabAccordion.preview.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:preview_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>
{% endblock %}", "ConcertoPanelBundle:TestWizard:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/form.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:form.html.twig */
class __TwigTemplate_338c04ad8f3a2e51ce06be1d64c5998aad376e7a0c48b394dedcc28bce71e389___428721045 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 70
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:TestWizard:form.html.twig", 70);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 72
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 73
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 74
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 75
        echo "                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1632 => 75,  1623 => 74,  1605 => 73,  1587 => 72,  1565 => 70,  1402 => 65,  1399 => 64,  1390 => 63,  1372 => 62,  1354 => 61,  1332 => 59,  1170 => 54,  1161 => 53,  1143 => 52,  1125 => 51,  1103 => 49,  938 => 42,  929 => 41,  916 => 38,  913 => 37,  906 => 36,  897 => 35,  879 => 34,  861 => 33,  839 => 31,  677 => 27,  668 => 26,  650 => 25,  632 => 24,  610 => 22,  449 => 18,  440 => 17,  422 => 16,  404 => 15,  382 => 13,  219 => 107,  213 => 104,  209 => 103,  201 => 98,  195 => 95,  191 => 94,  183 => 89,  177 => 86,  173 => 85,  169 => 83,  160 => 82,  149 => 79,  146 => 78,  144 => 70,  141 => 69,  139 => 59,  136 => 58,  133 => 49,  131 => 48,  128 => 47,  126 => 31,  123 => 30,  121 => 22,  118 => 21,  115 => 13,  106 => 12,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"TestWizard\" %}
{% trans_default_domain \"TestWizard\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    <span ng-bind-html=\"formTitle\"></span>
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"TestWizard\" %}
        {% block label %}{{ 'form.field.test'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.test.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.test\" style=\"width:100%;\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    ng-options=\"test.id as test.name for test in testCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
            </select>
        {% endblock %}
        {% block extra_info %}
            <a ng-href=\"#/tests/{% verbatim %}{{object.test}}{% endverbatim %}\" ng-show=\"object.test !== null\">
                <i class=\"glyphicon glyphicon-link\"></i>
            </a>
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    <uib-accordion-group is-open=\"tabAccordion.step.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'step.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'step.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.step.open, 'glyphicon-chevron-right': !tabAccordion.step.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:step_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.param.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'param.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'param.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.param.open, 'glyphicon-chevron-right': !tabAccordion.param.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:param_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>

    <uib-accordion-group is-open=\"tabAccordion.preview.open\">
        <uib-accordion-heading>
            <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'preview.fieldset.legend.tooltip'|trans }}'\"></i>
            {{ 'preview.fieldset.legend'|trans }}
            <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.preview.open, 'glyphicon-chevron-right': !tabAccordion.preview.open}\"></i>
        </uib-accordion-heading>
        {{ include(\"ConcertoPanelBundle:TestWizard:preview_section.html.twig\", {'class_name':\"TestWizard\"}) }}
    </uib-accordion-group>
{% endblock %}", "ConcertoPanelBundle:TestWizard:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/form.html.twig");
    }
}
