<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'elements' => [$this, 'block_elements'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "ConcertoPanelBundle::form_h.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        // line 2
        $context["class_name"] = "User";
        // line 4
        $context["exportable"] = false;
        // line 1
        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h.html.twig", "ConcertoPanelBundle:User:form.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        // line 7
        echo "    ";
        echo "{{formTitle}}";
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 10
        echo "    ";
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 10, "1840056352")->display($context);
        // line 18
        echo "    ";
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 18, "1988208907")->display($context);
        // line 26
        echo "    ";
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 26, "2119960322")->display($context);
        // line 34
        echo "    ";
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 34, "1491990241")->display($context);
        // line 42
        echo "
    ";
        // line 43
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 43, "751631792")->display($context);
        // line 52
        echo "
    ";
        // line 53
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 53, "2118267966")->display($context);
        // line 61
        echo "
    ";
        // line 62
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 62, "1931290274")->display($context);
        // line 70
        echo "
<!-- @TODO element shouldn't be cut in half -->
    ";
        // line 72
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 72, "1924765372")->display($context);
        // line 81
        echo "
        ";
        // line 82
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 82, "694449008")->display($context);
        // line 91
        echo "
            ";
        // line 92
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 92, "696356752")->display($context);
        // line 101
        echo "
                ";
        // line 102
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 102, "845863180")->display($context);
        // line 111
        echo "
                    ";
        // line 112
        $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 112, "888599786")->display($context);
        // line 121
        echo "
                    ";
        // line 122
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 122, $this->source); })()), "user", [], "any", false, false, false, 122) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
            // line 123
            echo "
                        ";
            // line 124
            $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 124, "2103364102")->display($context);
            // line 134
            echo "
                        ";
            // line 135
            $this->loadTemplate("ConcertoPanelBundle:User:form.html.twig", "ConcertoPanelBundle:User:form.html.twig", 135, "708858276")->display($context);
            // line 143
            echo "                    ";
        }
        // line 144
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___1840056352 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 10
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 10);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 12
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.login", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.login.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 15
        echo "            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___1988208907 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 18
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 18);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 20
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.password", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 21
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.password.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 22
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 23
        echo "            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___2119960322 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 26
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 26);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 28
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.passwordConfirmation", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 29
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.passwordConfirmation.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 30
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 31
        echo "            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___1491990241 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 34
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 34);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 36
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.email", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 37
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.email.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 38
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 39
        echo "            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___751631792 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 43
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 43);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 45
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 46
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 47
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 48
        echo "            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___2118267966 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 53
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 53);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 55
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 56
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 57
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 58
        echo "            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1755 => 58,  1746 => 57,  1728 => 56,  1710 => 55,  1688 => 53,  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___1931290274 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 62
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 62);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 64
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_super_admin", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 65
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_super_admin.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 66
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 67
        echo "            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2020 => 67,  2011 => 66,  1993 => 65,  1975 => 64,  1953 => 62,  1755 => 58,  1746 => 57,  1728 => 56,  1710 => 55,  1688 => 53,  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___1924765372 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'div' => [$this, 'block_div'],
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 72
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 72);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 74
    public function block_div($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        echo "<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 75
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_test", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 76
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_test.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 77
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 78
        echo "                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2304 => 78,  2295 => 77,  2277 => 76,  2259 => 75,  2241 => 74,  2219 => 72,  2020 => 67,  2011 => 66,  1993 => 65,  1975 => 64,  1953 => 62,  1755 => 58,  1746 => 57,  1728 => 56,  1710 => 55,  1688 => 53,  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___694449008 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'div' => [$this, 'block_div'],
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 82
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 82);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 84
    public function block_div($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        echo "<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 85
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_template", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 86
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_template.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 87
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 88
        echo "                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2588 => 88,  2579 => 87,  2561 => 86,  2543 => 85,  2525 => 84,  2503 => 82,  2304 => 78,  2295 => 77,  2277 => 76,  2259 => 75,  2241 => 74,  2219 => 72,  2020 => 67,  2011 => 66,  1993 => 65,  1975 => 64,  1953 => 62,  1755 => 58,  1746 => 57,  1728 => 56,  1710 => 55,  1688 => 53,  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___696356752 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'div' => [$this, 'block_div'],
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 92
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 92);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 94
    public function block_div($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        echo "<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 95
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_table", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 96
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_table.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 97
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 98
        echo "                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2872 => 98,  2863 => 97,  2845 => 96,  2827 => 95,  2809 => 94,  2787 => 92,  2588 => 88,  2579 => 87,  2561 => 86,  2543 => 85,  2525 => 84,  2503 => 82,  2304 => 78,  2295 => 77,  2277 => 76,  2259 => 75,  2241 => 74,  2219 => 72,  2020 => 67,  2011 => 66,  1993 => 65,  1975 => 64,  1953 => 62,  1755 => 58,  1746 => 57,  1728 => 56,  1710 => 55,  1688 => 53,  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___845863180 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'div' => [$this, 'block_div'],
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 102
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 102);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 104
    public function block_div($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        echo "<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 105
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_file", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 106
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_file.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 107
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 108
        echo "                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  3156 => 108,  3147 => 107,  3129 => 106,  3111 => 105,  3093 => 104,  3071 => 102,  2872 => 98,  2863 => 97,  2845 => 96,  2827 => 95,  2809 => 94,  2787 => 92,  2588 => 88,  2579 => 87,  2561 => 86,  2543 => 85,  2525 => 84,  2503 => 82,  2304 => 78,  2295 => 77,  2277 => 76,  2259 => 75,  2241 => 74,  2219 => 72,  2020 => 67,  2011 => 66,  1993 => 65,  1975 => 64,  1953 => 62,  1755 => 58,  1746 => 57,  1728 => 56,  1710 => 55,  1688 => 53,  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___888599786 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'div' => [$this, 'block_div'],
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 112
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 112);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 114
    public function block_div($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        echo "<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 115
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_wizard", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 116
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.role_wizard.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 117
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 118
        echo "                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  3440 => 118,  3431 => 117,  3413 => 116,  3395 => 115,  3377 => 114,  3355 => 112,  3156 => 108,  3147 => 107,  3129 => 106,  3111 => 105,  3093 => 104,  3071 => 102,  2872 => 98,  2863 => 97,  2845 => 96,  2827 => 95,  2809 => 94,  2787 => 92,  2588 => 88,  2579 => 87,  2561 => 86,  2543 => 85,  2525 => 84,  2503 => 82,  2304 => 78,  2295 => 77,  2277 => 76,  2259 => 75,  2241 => 74,  2219 => 72,  2020 => 67,  2011 => 66,  1993 => 65,  1975 => 64,  1953 => 62,  1755 => 58,  1746 => 57,  1728 => 56,  1710 => 55,  1688 => 53,  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___2103364102 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 124
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 124);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 126
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 127
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 128
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 129
        echo "                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                                </select>
                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  3708 => 130,  3705 => 129,  3696 => 128,  3678 => 127,  3660 => 126,  3638 => 124,  3440 => 118,  3431 => 117,  3413 => 116,  3395 => 115,  3377 => 114,  3355 => 112,  3156 => 108,  3147 => 107,  3129 => 106,  3111 => 105,  3093 => 104,  3071 => 102,  2872 => 98,  2863 => 97,  2845 => 96,  2827 => 95,  2809 => 94,  2787 => 92,  2588 => 88,  2579 => 87,  2561 => 86,  2543 => 85,  2525 => 84,  2503 => 82,  2304 => 78,  2295 => 77,  2277 => 76,  2259 => 75,  2241 => 74,  2219 => 72,  2020 => 67,  2011 => 66,  1993 => 65,  1975 => 64,  1953 => 62,  1755 => 58,  1746 => 57,  1728 => 56,  1710 => 55,  1688 => 53,  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}


/* ConcertoPanelBundle:User:form.html.twig */
class __TwigTemplate_cf93c86a16cd86c254cc05722755ba88462d44a1c53a11deea59ca3028a35bdc___708858276 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 135
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:User:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:User:form.html.twig", 135);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 137
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 138
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups.tooltip", [], "User"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 139
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 140
        echo "                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  3975 => 140,  3966 => 139,  3948 => 138,  3930 => 137,  3908 => 135,  3708 => 130,  3705 => 129,  3696 => 128,  3678 => 127,  3660 => 126,  3638 => 124,  3440 => 118,  3431 => 117,  3413 => 116,  3395 => 115,  3377 => 114,  3355 => 112,  3156 => 108,  3147 => 107,  3129 => 106,  3111 => 105,  3093 => 104,  3071 => 102,  2872 => 98,  2863 => 97,  2845 => 96,  2827 => 95,  2809 => 94,  2787 => 92,  2588 => 88,  2579 => 87,  2561 => 86,  2543 => 85,  2525 => 84,  2503 => 82,  2304 => 78,  2295 => 77,  2277 => 76,  2259 => 75,  2241 => 74,  2219 => 72,  2020 => 67,  2011 => 66,  1993 => 65,  1975 => 64,  1953 => 62,  1755 => 58,  1746 => 57,  1728 => 56,  1710 => 55,  1688 => 53,  1489 => 48,  1480 => 47,  1462 => 46,  1444 => 45,  1422 => 43,  1224 => 39,  1215 => 38,  1197 => 37,  1179 => 36,  1157 => 34,  959 => 31,  950 => 30,  932 => 29,  914 => 28,  892 => 26,  694 => 23,  685 => 22,  667 => 21,  649 => 20,  627 => 18,  429 => 15,  420 => 14,  402 => 13,  384 => 12,  362 => 10,  164 => 144,  161 => 143,  159 => 135,  156 => 134,  154 => 124,  151 => 123,  149 => 122,  146 => 121,  144 => 112,  141 => 111,  139 => 102,  136 => 101,  134 => 92,  131 => 91,  129 => 82,  126 => 81,  124 => 72,  120 => 70,  118 => 62,  115 => 61,  113 => 53,  110 => 52,  108 => 43,  105 => 42,  102 => 34,  99 => 26,  96 => 18,  93 => 10,  84 => 9,  71 => 7,  62 => 6,  51 => 1,  49 => 4,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"User\" %}
{% trans_default_domain \"User\" %}
{% set exportable = false %}

{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.login'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.login.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.username\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.password'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.password.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.password\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.passwordConfirmation'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.passwordConfirmation.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.passwordConfirmation\" type=\"password\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.email'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.email.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.email\" type=\"text\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block label %}{{ 'form.field.role_super_admin'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.role_super_admin.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-model=\"object.role_super_admin\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

<!-- @TODO element shouldn't be cut in half -->
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"User\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
            {% block label %}{{ 'form.field.role_test'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.role_test.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-model=\"object.role_test\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"User\" %}
            {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                {% block label %}{{ 'form.field.role_template'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.role_template.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-model=\"object.role_template\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"User\" %}
                {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_table'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_table.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_table\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                    {% trans_default_domain \"User\" %}
                    {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                    {% block label %}{{ 'form.field.role_file'|trans }}{% endblock %}
                    {% block tooltip %}{{'form.field.role_file.tooltip'|trans}}{% endblock %}
                    {% block control %}
                        <input ng-model=\"object.role_file\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                    {% endblock %}
                {% endembed %}

                    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                        {% trans_default_domain \"User\" %}
                        {% block div %}<div class=\"horizontalElement center\" ng-hide=\"object.role_super_admin == '1'\">{% endblock %}
                        {% block label %}{{ 'form.field.role_wizard'|trans }}{% endblock %}
                        {% block tooltip %}{{'form.field.role_wizard.tooltip'|trans}}{% endblock %}
                        {% block control %}
                            <input ng-model=\"object.role_wizard\" type=\"checkbox\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
                        {% endblock %}
                    {% endembed %}

                    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <select ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                                </select>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                            {% trans_default_domain \"User\" %}
                            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                            {% block control %}
                                <input type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                            {% endblock %}
                        {% endembed %}
                    {% endif %}

{% endblock %}
", "ConcertoPanelBundle:User:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/User/form.html.twig");
    }
}
