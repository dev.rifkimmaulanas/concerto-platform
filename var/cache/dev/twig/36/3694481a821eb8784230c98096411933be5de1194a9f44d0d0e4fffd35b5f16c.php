<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle::content.html.twig */
class __TwigTemplate_615b02600e039980b762806c76bfeb751d22ca05d32bd091db9f61dd68df4b95 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::content.html.twig"));

        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle::content.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "    <div ng-controller=\"MainController\" style=\"height: 100%;\">
        <div ui-i18n=\"'";
        // line 7
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "request", [], "any", false, false, false, 7), "locale", [], "any", false, false, false, 7) == "en_GB")) {
            echo "en";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 8
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "request", [], "any", false, false, false, 8), "locale", [], "any", false, false, false, 8) == "zh_CN")) {
            echo "zh-cn";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 9
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 9, $this->source); })()), "request", [], "any", false, false, false, 9), "locale", [], "any", false, false, false, 9) == "fr_FR")) {
            echo "fr";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 10
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 10, $this->source); })()), "request", [], "any", false, false, false, 10), "locale", [], "any", false, false, false, 10) == "pl_PL")) {
            echo "pl";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 11
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 11, $this->source); })()), "request", [], "any", false, false, false, 11), "locale", [], "any", false, false, false, 11) == "tr_TR")) {
            echo "tr";
        }
        // line 12
        echo "'\" style=\"height: 100%;\">

            <uib-tabset active=\"tab.activeIndex\" style=\"height: 100%;\">
                ";
        // line 15
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 15, $this->source); })()), "user", [], "any", false, false, false, 15) && ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN") || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEST")))) {
            // line 16
            echo "                    <uib-tab ng-click=\"goToTab(0)\" index=\"0\" ng-init=\"setFirstActiveTab(0)\">
                        <uib-tab-heading uib-tooltip-html=\"'";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.tests.tooltip", [], "panel"), "html", null, true);
            echo "'\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.tests", [], "panel"), "html", null, true);
            echo "</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 0\" ng-controller=\"TestController\" id=\"test-tab\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewTest\" class=\"center\"></div>
                            ";
            // line 22
            echo twig_include($this->env, $context, "ConcertoPanelBundle::tab.html.twig", ["class_name" => "Test"]);
            echo "
                        </div>
                    </uib-tab>
                ";
        }
        // line 26
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 26, $this->source); })()), "user", [], "any", false, false, false, 26) && ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN") || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_WIZARD")))) {
            // line 27
            echo "                    <uib-tab ng-click=\"goToTab(5)\" index=\"5\" ng-init=\"setFirstActiveTab(5)\">
                        <uib-tab-heading uib-tooltip-html=\"'";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.wizards.tooltip", [], "panel"), "html", null, true);
            echo "'\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.wizards", [], "panel"), "html", null, true);
            echo "</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 5\" ng-controller=\"TestWizardController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewTestWizard\" class=\"center\"></div>
                            ";
            // line 33
            echo twig_include($this->env, $context, "ConcertoPanelBundle::tab.html.twig", ["class_name" => "TestWizard"]);
            echo "
                        </div>
                    </uib-tab>
                ";
        }
        // line 37
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 37, $this->source); })()), "user", [], "any", false, false, false, 37) && ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN") || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TEMPLATE")))) {
            // line 38
            echo "                    <uib-tab ng-click=\"goToTab(1)\" index=\"1\" ng-init=\"setFirstActiveTab(1)\">
                        <uib-tab-heading uib-tooltip-html=\"'";
            // line 39
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.templates.tooltip", [], "panel"), "html", null, true);
            echo "'\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.templates", [], "panel"), "html", null, true);
            echo "</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 1\" ng-controller=\"ViewTemplateController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewViewTemplate\" class=\"center\"></div>
                            ";
            // line 44
            echo twig_include($this->env, $context, "ConcertoPanelBundle::tab.html.twig", ["class_name" => "ViewTemplate"]);
            echo "
                        </div>
                    </uib-tab>
                ";
        }
        // line 48
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 48, $this->source); })()), "user", [], "any", false, false, false, 48) && ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN") || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_TABLE")))) {
            // line 49
            echo "                    <uib-tab ng-click=\"goToTab(2)\" index=\"2\" ng-init=\"setFirstActiveTab(2)\">
                        <uib-tab-heading uib-tooltip-html=\"'";
            // line 50
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.tables.tooltip", [], "panel"), "html", null, true);
            echo "'\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.tables", [], "panel"), "html", null, true);
            echo "</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 2\" ng-controller=\"DataTableController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewDataTable\" class=\"center\"></div>
                            ";
            // line 55
            echo twig_include($this->env, $context, "ConcertoPanelBundle::tab.html.twig", ["class_name" => "DataTable"]);
            echo "
                        </div>
                    </uib-tab>
                ";
        }
        // line 59
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 59, $this->source); })()), "user", [], "any", false, false, false, 59) && ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN") || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_FILE")))) {
            // line 60
            echo "                    <uib-tab ng-click=\"goToTab(3)\" index=\"3\" ng-init=\"setFirstActiveTab(3)\">
                        <uib-tab-heading uib-tooltip-html=\"'";
            // line 61
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.files.tooltip", [], "panel"), "html", null, true);
            echo "'\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.files", [], "panel"), "html", null, true);
            echo "</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 3\" ng-controller=\"FileBrowserController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewFile\" class=\"center\"></div>
                            ";
            // line 66
            echo twig_include($this->env, $context, "ConcertoPanelBundle:FileBrowser:file_browser_content.html.twig");
            echo "
                        </div>
                    </uib-tab>
                ";
        }
        // line 70
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 70, $this->source); })()), "user", [], "any", false, false, false, 70) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
            // line 71
            echo "                    <uib-tab ng-click=\"goToTab(4)\" index=\"4\" ng-init=\"setFirstActiveTab(4)\">
                        <uib-tab-heading uib-tooltip-html=\"'";
            // line 72
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.users.tooltip", [], "panel"), "html", null, true);
            echo "'\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.users", [], "panel"), "html", null, true);
            echo "</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 4\" ng-controller=\"UserController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewUser\" class=\"center\"></div>
                            ";
            // line 77
            echo twig_include($this->env, $context, "ConcertoPanelBundle::tab.html.twig", ["class_name" => "User"]);
            echo "
                        </div>
                    </uib-tab>
                ";
        }
        // line 81
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 81, $this->source); })()), "user", [], "any", false, false, false, 81) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
            // line 82
            echo "                    <uib-tab ng-click=\"goToTab(6)\" index=\"6\" ng-init=\"setFirstActiveTab(6)\">
                        <uib-tab-heading uib-tooltip-html=\"'";
            // line 83
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.administration.tooltip", [], "panel"), "html", null, true);
            echo "'\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.administration", [], "panel"), "html", null, true);
            echo "</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 6\" ng-controller=\"AdministrationController\" id=\"administration-tab\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewAdministration\" class=\"center\"></div>
                            ";
            // line 88
            echo twig_include($this->env, $context, "ConcertoPanelBundle:Administration:form.html.twig");
            echo "
                        </div>
                    </uib-tab>
                ";
        }
        // line 92
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 92, $this->source); })()), "user", [], "any", false, false, false, 92) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
            // line 93
            echo "                    <uib-tab ng-click=\"goToTab(7)\" index=\"7\" ng-init=\"setFirstActiveTab(7)\">
                        <uib-tab-heading uib-tooltip-html=\"'";
            // line 94
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.usertest.tooltip", [], "panel"), "html", null, true);
            echo "'\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("tab.usertest", [], "panel"), "html", null, true);
            echo "</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 7\" ng-controller=\"UserTestController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewUserTest\" class=\"center\"></div>
                            ";
            // line 99
            echo twig_include($this->env, $context, "ConcertoPanelBundle:UserTest:tab.html.twig", ["class_name" => "UserTest"]);
            echo "
                        </div>
                    </uib-tab>
                ";
        }
        // line 103
        echo "            </uib-tabset>

            <div ng-if=\"RDocumentation.html && RDocumentation.active\" class=\"codemirrorRDocsWindow\">
                <div ng-bind-html=\"RDocumentation.html\"></div>
            </div>

            <!-- wizard param setters -->
            ";
        // line 110
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 13));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 111
            echo "                ";
            echo twig_include($this->env, $context, (("ConcertoPanelBundle:TestWizard:type_" . $context["i"]) . "_setter.html.twig"));
            echo "
                ";
            // line 112
            echo twig_include($this->env, $context, (("ConcertoPanelBundle:TestWizard:type_" . $context["i"]) . "_definer.html.twig"));
            echo "
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 114
        echo "
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle::content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  319 => 114,  303 => 112,  298 => 111,  281 => 110,  272 => 103,  265 => 99,  255 => 94,  252 => 93,  249 => 92,  242 => 88,  232 => 83,  229 => 82,  226 => 81,  219 => 77,  209 => 72,  206 => 71,  203 => 70,  196 => 66,  186 => 61,  183 => 60,  180 => 59,  173 => 55,  163 => 50,  160 => 49,  157 => 48,  150 => 44,  140 => 39,  137 => 38,  134 => 37,  127 => 33,  117 => 28,  114 => 27,  111 => 26,  104 => 22,  94 => 17,  91 => 16,  89 => 15,  84 => 12,  80 => 11,  77 => 10,  74 => 9,  71 => 8,  68 => 7,  65 => 5,  56 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% trans_default_domain \"panel\" %}

{% block content %}
    <div ng-controller=\"MainController\" style=\"height: 100%;\">
        <div ui-i18n=\"'{% 
if app.request.locale == \"en_GB\" %}en{% 
elseif app.request.locale == \"zh_CN\" %}zh-cn{% 
elseif app.request.locale == \"fr_FR\" %}fr{% 
elseif app.request.locale == \"pl_PL\" %}pl{% 
elseif app.request.locale == \"tr_TR\" %}tr{% 
endif %}'\" style=\"height: 100%;\">

            <uib-tabset active=\"tab.activeIndex\" style=\"height: 100%;\">
                {% if app.user and (is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_TEST')) %}
                    <uib-tab ng-click=\"goToTab(0)\" index=\"0\" ng-init=\"setFirstActiveTab(0)\">
                        <uib-tab-heading uib-tooltip-html=\"'{{ \"tab.tests.tooltip\"|trans }}'\">{{ \"tab.tests\"|trans }}</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 0\" ng-controller=\"TestController\" id=\"test-tab\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewTest\" class=\"center\"></div>
                            {{ include(\"ConcertoPanelBundle::tab.html.twig\", {'class_name':\"Test\"}) }}
                        </div>
                    </uib-tab>
                {% endif %}
                {% if app.user and (is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_WIZARD')) %}
                    <uib-tab ng-click=\"goToTab(5)\" index=\"5\" ng-init=\"setFirstActiveTab(5)\">
                        <uib-tab-heading uib-tooltip-html=\"'{{ \"tab.wizards.tooltip\"|trans }}'\">{{ \"tab.wizards\"|trans }}</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 5\" ng-controller=\"TestWizardController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewTestWizard\" class=\"center\"></div>
                            {{ include(\"ConcertoPanelBundle::tab.html.twig\", {'class_name':\"TestWizard\"}) }}
                        </div>
                    </uib-tab>
                {% endif %}
                {% if app.user and (is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_TEMPLATE')) %}
                    <uib-tab ng-click=\"goToTab(1)\" index=\"1\" ng-init=\"setFirstActiveTab(1)\">
                        <uib-tab-heading uib-tooltip-html=\"'{{ \"tab.templates.tooltip\"|trans }}'\">{{ \"tab.templates\"|trans }}</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 1\" ng-controller=\"ViewTemplateController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewViewTemplate\" class=\"center\"></div>
                            {{ include(\"ConcertoPanelBundle::tab.html.twig\", {'class_name':\"ViewTemplate\"}) }}
                        </div>
                    </uib-tab>
                {% endif %}
                {% if app.user and (is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_TABLE')) %}
                    <uib-tab ng-click=\"goToTab(2)\" index=\"2\" ng-init=\"setFirstActiveTab(2)\">
                        <uib-tab-heading uib-tooltip-html=\"'{{ \"tab.tables.tooltip\"|trans }}'\">{{ \"tab.tables\"|trans }}</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 2\" ng-controller=\"DataTableController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewDataTable\" class=\"center\"></div>
                            {{ include(\"ConcertoPanelBundle::tab.html.twig\", {'class_name':\"DataTable\"}) }}
                        </div>
                    </uib-tab>
                {% endif %}
                {% if app.user and (is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_FILE')) %}
                    <uib-tab ng-click=\"goToTab(3)\" index=\"3\" ng-init=\"setFirstActiveTab(3)\">
                        <uib-tab-heading uib-tooltip-html=\"'{{ \"tab.files.tooltip\"|trans }}'\">{{ \"tab.files\"|trans }}</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 3\" ng-controller=\"FileBrowserController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewFile\" class=\"center\"></div>
                            {{ include(\"ConcertoPanelBundle:FileBrowser:file_browser_content.html.twig\") }}
                        </div>
                    </uib-tab>
                {% endif %}
                {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
                    <uib-tab ng-click=\"goToTab(4)\" index=\"4\" ng-init=\"setFirstActiveTab(4)\">
                        <uib-tab-heading uib-tooltip-html=\"'{{ \"tab.users.tooltip\"|trans }}'\">{{ \"tab.users\"|trans }}</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 4\" ng-controller=\"UserController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewUser\" class=\"center\"></div>
                            {{ include(\"ConcertoPanelBundle::tab.html.twig\", {'class_name':\"User\"}) }}
                        </div>
                    </uib-tab>
                {% endif %}
                {% if app.user and (is_granted('ROLE_SUPER_ADMIN')) %}
                    <uib-tab ng-click=\"goToTab(6)\" index=\"6\" ng-init=\"setFirstActiveTab(6)\">
                        <uib-tab-heading uib-tooltip-html=\"'{{ \"tab.administration.tooltip\"|trans }}'\">{{ \"tab.administration\"|trans }}</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 6\" ng-controller=\"AdministrationController\" id=\"administration-tab\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewAdministration\" class=\"center\"></div>
                            {{ include(\"ConcertoPanelBundle:Administration:form.html.twig\") }}
                        </div>
                    </uib-tab>
                {% endif %}
                {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
                    <uib-tab ng-click=\"goToTab(7)\" index=\"7\" ng-init=\"setFirstActiveTab(7)\">
                        <uib-tab-heading uib-tooltip-html=\"'{{ \"tab.usertest.tooltip\"|trans }}'\">{{ \"tab.usertest\"|trans }}</uib-tab-heading>

                        <div ng-if=\"tab.activeIndex == 7\" ng-controller=\"UserTestController\">
                            <div class=\"breadcrumb c-breadcrumb\" ncy-breadcrumb style=\"text-align: left;\"></div>
                            <div ui-view=\"tabViewUserTest\" class=\"center\"></div>
                            {{ include(\"ConcertoPanelBundle:UserTest:tab.html.twig\", {'class_name':\"UserTest\"}) }}
                        </div>
                    </uib-tab>
                {% endif %}
            </uib-tabset>

            <div ng-if=\"RDocumentation.html && RDocumentation.active\" class=\"codemirrorRDocsWindow\">
                <div ng-bind-html=\"RDocumentation.html\"></div>
            </div>

            <!-- wizard param setters -->
            {% for i in 0..13 %}
                {{ include(\"ConcertoPanelBundle:TestWizard:type_\" ~ i ~ \"_setter.html.twig\") }}
                {{ include(\"ConcertoPanelBundle:TestWizard:type_\" ~ i ~ \"_definer.html.twig\") }}
            {% endfor %}

        </div>
    </div>

{% endblock %}
", "ConcertoPanelBundle::content.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/content.html.twig");
    }
}
