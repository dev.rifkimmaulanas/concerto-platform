<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:step_section.html.twig */
class __TwigTemplate_31e7c3a057cb04fbfb7837f213391ab0b91fc56c305dd766a5bb08651812af8a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base_include.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:step_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:step_section.html.twig"));

        // line 2
        $context["class_name"] = "TestWizard";
        // line 1
        $this->parent = $this->loadTemplate("::base_include.html.twig", "ConcertoPanelBundle:TestWizard:step_section.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div class=\"center\">
        <button ng-disabled=\"!isEditable()\" ng-click=\"addStep()\" class=\"btn btn-success btn-sm\">";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.list.button.add", [], "TestWizard"), "html", null, true);
        echo "</button>
        <button ng-click=\"gridService.downloadList(stepsGridApi);\" class=\"btn btn-default btn-sm\">";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("list.button.download", [], "panel"), "html", null, true);
        echo "</button>
        <button ng-disabled=\"!isEditable()\" ng-click=\"deleteSelectedSteps();\" class=\"btn btn-danger btn-sm\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.list.button.delete.checked", [], "TestWizard"), "html", null, true);
        echo "</button>
        <button ng-disabled=\"!isEditable()\" ng-click=\"deleteAllSteps();\" class=\"btn btn-danger btn-sm\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("step.list.button.delete.all", [], "TestWizard"), "html", null, true);
        echo "</button>
    </div>
    <div ui-grid=\"stepsOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:step_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 10,  79 => 9,  75 => 8,  71 => 7,  68 => 6,  59 => 5,  48 => 1,  46 => 2,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base_include.html.twig\" %}
{% set class_name = \"TestWizard\" %}
{% trans_default_domain \"TestWizard\" %}

{% block content %}
    <div class=\"center\">
        <button ng-disabled=\"!isEditable()\" ng-click=\"addStep()\" class=\"btn btn-success btn-sm\">{{ 'step.list.button.add'|trans }}</button>
        <button ng-click=\"gridService.downloadList(stepsGridApi);\" class=\"btn btn-default btn-sm\">{{ \"list.button.download\"|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" ng-click=\"deleteSelectedSteps();\" class=\"btn btn-danger btn-sm\">{{ \"step.list.button.delete.checked\"|trans }}</button>
        <button ng-disabled=\"!isEditable()\" ng-click=\"deleteAllSteps();\" class=\"btn btn-danger btn-sm\">{{ \"step.list.button.delete.all\"|trans }}</button>
    </div>
    <div ui-grid=\"stepsOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid\"></div>
{% endblock %}", "ConcertoPanelBundle:TestWizard:step_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/step_section.html.twig");
    }
}
