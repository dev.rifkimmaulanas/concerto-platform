<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_9_setter.html.twig */
class __TwigTemplate_aa886e0f758cf587c6ce55918304f9407d73b0372811996441f1f64665af04a7 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig"));

        // line 1
        echo "<script type=\"text/ng-template\" id=\"type_9_setter.html\">
    <div ng-controller=\"WizardParamSetter9Controller\">
        ";
        // line 3
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_9_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig", 3, "1387150549")->display($context);
        // line 27
        echo "    </div>
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 27,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_9_setter.html\">
    <div ng-controller=\"WizardParamSetter9Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ title }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.setter.titles.group.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <div ng-repeat=\"field in param.definition.fields | orderBy: 'order' track by \$index\">
                            <wizard-param-setter editable=\"editable\"
                                                 param=\"field\"
                                                 output=\"output[field.name]\"
                                                 mode=\"outer\"
                                                 values=\"values\"
                                                 parent=\"output\"
                                                 grand-parent=\"parent\"
                                                 under-list=\"underList\"
                                                 wizard-object=\"wizardObject\"
                                                 wizard-mode=\"{% verbatim %}{{ wizardMode }}{% endverbatim %}\"></wizard-param-setter>
                        </div>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_9_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_9_setter.html.twig */
class __TwigTemplate_aa886e0f758cf587c6ce55918304f9407d73b0372811996441f1f64665af04a7___1387150549 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'setter' => [$this, 'block_setter'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_base_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_setter($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        // line 6
        echo "                ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_9_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig", 6, "1624601297")->display($context);
        // line 25
        echo "            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 25,  156 => 6,  147 => 5,  125 => 3,  47 => 27,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_9_setter.html\">
    <div ng-controller=\"WizardParamSetter9Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ title }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.setter.titles.group.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <div ng-repeat=\"field in param.definition.fields | orderBy: 'order' track by \$index\">
                            <wizard-param-setter editable=\"editable\"
                                                 param=\"field\"
                                                 output=\"output[field.name]\"
                                                 mode=\"outer\"
                                                 values=\"values\"
                                                 parent=\"output\"
                                                 grand-parent=\"parent\"
                                                 under-list=\"underList\"
                                                 wizard-object=\"wizardObject\"
                                                 wizard-mode=\"{% verbatim %}{{ wizardMode }}{% endverbatim %}\"></wizard-param-setter>
                        </div>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_9_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_9_setter.html.twig */
class __TwigTemplate_aa886e0f758cf587c6ce55918304f9407d73b0372811996441f1f64665af04a7___1624601297 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'legend_tooltip' => [$this, 'block_legend_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 6
        return "ConcertoPanelBundle::form_v_single.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_single.html.twig", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig", 6);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo "{{ title }}";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.group.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 11
        echo "                        <div ng-repeat=\"field in param.definition.fields | orderBy: 'order' track by \$index\">
                            <wizard-param-setter editable=\"editable\"
                                                 param=\"field\"
                                                 output=\"output[field.name]\"
                                                 mode=\"outer\"
                                                 values=\"values\"
                                                 parent=\"output\"
                                                 grand-parent=\"parent\"
                                                 under-list=\"underList\"
                                                 wizard-object=\"wizardObject\"
                                                 wizard-mode=\"";
        // line 21
        echo "{{ wizardMode }}";
        echo "\"></wizard-param-setter>
                        </div>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  317 => 21,  305 => 11,  296 => 10,  278 => 9,  260 => 8,  238 => 6,  159 => 25,  156 => 6,  147 => 5,  125 => 3,  47 => 27,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_9_setter.html\">
    <div ng-controller=\"WizardParamSetter9Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ title }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.setter.titles.group.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <div ng-repeat=\"field in param.definition.fields | orderBy: 'order' track by \$index\">
                            <wizard-param-setter editable=\"editable\"
                                                 param=\"field\"
                                                 output=\"output[field.name]\"
                                                 mode=\"outer\"
                                                 values=\"values\"
                                                 parent=\"output\"
                                                 grand-parent=\"parent\"
                                                 under-list=\"underList\"
                                                 wizard-object=\"wizardObject\"
                                                 wizard-mode=\"{% verbatim %}{{ wizardMode }}{% endverbatim %}\"></wizard-param-setter>
                        </div>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_9_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_9_setter.html.twig");
    }
}
