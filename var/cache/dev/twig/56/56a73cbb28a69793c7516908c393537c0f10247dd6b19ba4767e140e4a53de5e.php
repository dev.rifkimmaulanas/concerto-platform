<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle::base_panel.html.twig */
class __TwigTemplate_c6dcd4e71688abdb583f3d5ad7824b81c28e1a5bc7a0cf17b19168e9a549c38d extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheetsInclude' => [$this, 'block_stylesheetsInclude'],
            'javascriptsInclude' => [$this, 'block_javascriptsInclude'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::base_panel.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle::base_panel.html.twig"));

        $this->parent = $this->loadTemplate("::base.html.twig", "ConcertoPanelBundle::base_panel.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_stylesheetsInclude($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheetsInclude"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheetsInclude"));

        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/bootstrap/dist/css/bootstrap.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 4, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/bootstrap/dist/css/bootstrap-theme.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 5, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-ui-grid/ui-grid.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 6, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-block-ui/dist/angular-block-ui.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 7, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/lib/codemirror.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 8, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/addon/display/fullscreen.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 9, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/addon/hint/show-hint.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 10, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/css/codemirror-auto-resize.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 11, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/jquery-ui/themes/base/jquery-ui.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 12, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/js/angular-filemanager/angular-filemanager.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 13, $this->source); })()))), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/css/base.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 14, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/css/helper.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 15, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/css/flow.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 16, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/css/file_manager.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 17, $this->source); })()))), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/diff2html/dist/diff2html.min.css?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 18, $this->source); })()))), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 21
    public function block_javascriptsInclude($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascriptsInclude"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascriptsInclude"));

        // line 22
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/diff2html/dist/diff2html.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 22, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/jquery/dist/jquery.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 23, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/jquery.metadata/jquery.metadata.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 24, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/concertopanel/angularjs/bower_components/jquery-ui/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/jquery-mousewheel/jquery.mousewheel.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 26, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/common/js/misc/helpers.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 27, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular/angular.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 28, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-animate/angular-animate.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 29, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-translate/angular-translate.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 30, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/ng-file-upload/ng-file-upload.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 31, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-sanitize/angular-sanitize.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 32, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-ui-grid/ui-grid.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 33, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/bootstrap/dist/js/bootstrap.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 34, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-bootstrap/ui-bootstrap.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 35, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 36, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-block-ui/dist/angular-block-ui.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 37, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/ckeditor/ckeditor.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 38, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/lib/codemirror.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 39, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/addon/display/fullscreen.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 40, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/misc/extended-show-hint.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 41, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/misc/codemirror-r-completion.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 42, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/mode/xml/xml.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 43, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/mode/javascript/javascript.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 44, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/mode/css/css.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 45, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/mode/htmlmixed/htmlmixed.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 46, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/mode/r/r.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 47, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/codemirror/mode/sql/sql.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 48, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-ui-codemirror/ui-codemirror.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 49, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-file-upload/dist/angular-file-upload.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 50, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-ui-sortable/sortable.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 51, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-ui-router/release/angular-ui-router.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 52, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-breadcrumb/release/angular-breadcrumb.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 53, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/directives/ng-html.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 54, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/jsPlumb/dist/js/dom.jsPlumb-1.7.6-min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 55, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/ng-context-menu-concerto/dist/ng-context-menu.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 56, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/pdfmake/build/pdfmake.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 57, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/csv-js/csv.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 58, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/misc/vfs_fonts.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 59, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/chart.js/dist/Chart.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 60, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-chart.js/dist/angular-chart.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 61, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/qrcode/lib/qrcode.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 62, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/bower_components/angular-qr/angular-qr.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 63, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/js/angular-filemanager/angular-filemanager.min.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 64, $this->source); })()))), "html", null, true);
        echo "\"></script>
    ";
        // line 65
        echo twig_include($this->env, $context, "ConcertoPanelBundle::translation_injector.html.twig");
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/misc/defaults.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 66, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/app.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 67, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/main_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 68, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/alert_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 69, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/content_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 70, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/confirm_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 71, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/connection_return_function_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 72, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/port_value_edit_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 73, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/port_add_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 74, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/textarea_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 75, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/node_wizard_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 76, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/import_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 77, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/export_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 78, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/download_list_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 79, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/upload_list_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 80, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/header_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 81, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/save_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 82, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/save_new_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 83, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/base_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 84, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 85, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_step_save_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 86, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_param_definer_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 87, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_param_setter_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 88, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_param_save_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 89, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_carousel_step_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 90, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 91, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_variables_save_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 92, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/default_r_completion_wizard_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 93, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/r_documentation_generation_help_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 94, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/view_template_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 95, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/data_table_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 96, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/data_table_import_csv_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 97, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/data_table_structure_save_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 98, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/ckeditor_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 99, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/user_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 100, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/user_test_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 101, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/state_view_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 102, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/administration_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 103, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/administration_content_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 104, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/r_package_install_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 105, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/pre_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 106, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/mfa_enabled_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 107, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/ongoing_scheduled_task_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 108, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_0_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 109, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_1_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 110, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_2_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 111, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_3_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 112, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_4_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 113, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_5_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 114, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_6_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 115, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_7_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 116, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_8_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 117, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_9_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 118, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_10_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 119, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_11_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 120, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_12_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 121, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_13_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 122, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_0_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 123, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_1_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 124, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_2_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 125, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_3_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 126, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_4_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 127, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_5_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 128, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_6_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 129, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_7_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 130, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_8_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 131, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_9_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 132, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_10_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 133, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_11_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 134, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_12_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 135, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_13_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 136, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/git_enable_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 137, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/git_commit_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 138, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/grid_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 139, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/r_documentation_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 140, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/base_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 141, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/test_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 142, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/test_wizard_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 143, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/view_template_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 144, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/data_table_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 145, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/user_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 146, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/user_test_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 147, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/session_count_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 148, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/test_wizard_param_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 149, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/administration_settings_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 150, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/messages_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 151, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/api_clients_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 152, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/scheduled_tasks_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 153, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/dialogs_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 154, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/auth_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 155, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/services/all_collection_service.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 156, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/file_browser_controller.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 157, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/directives/flow_logic_directive.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 158, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/directives/wizard_param_definer_directive.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 159, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/directives/wizard_param_setter_directive.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 160, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/directives/ng_ckeditor.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 161, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/filters/trusted_html.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 162, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/filters/logical.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 163, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/filters/capitalize.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 164, $this->source); })()))), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("bundles/concertopanel/angularjs/app/concerto_panel/js/filters/task_status_label.js?v=" . (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 165, $this->source); })()))), "html", null, true);
        echo "\"></script>

    <script>
        if (typeof CKEDITOR != \"undefined\") {
            CKEDITOR.plugins.addExternal('cmsource', '";
        // line 169
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/concertopanel/angularjs/app/concerto_panel/js/misc/cmsource/"), "html", null, true);
        echo "', 'plugin.js?v=";
        echo twig_escape_filter($this->env, (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 169, $this->source); })()), "html", null, true);
        echo "');
            CKEDITOR.plugins.addExternal('autogrow', '";
        // line 170
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/concertopanel/angularjs/app/concerto_panel/js/misc/autogrow/"), "html", null, true);
        echo "', 'plugin.js?v=";
        echo twig_escape_filter($this->env, (isset($context["version"]) || array_key_exists("version", $context) ? $context["version"] : (function () { throw new RuntimeError('Variable "version" does not exist.', 170, $this->source); })()), "html", null, true);
        echo "');

            \$.each(CKEDITOR.dtd.\$removeEmpty, function (i, value) {
                CKEDITOR.dtd.\$removeEmpty[i] = false;
            });
        }
    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle::base_panel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  730 => 170,  724 => 169,  717 => 165,  713 => 164,  709 => 163,  705 => 162,  701 => 161,  697 => 160,  693 => 159,  689 => 158,  685 => 157,  681 => 156,  677 => 155,  673 => 154,  669 => 153,  665 => 152,  661 => 151,  657 => 150,  653 => 149,  649 => 148,  645 => 147,  641 => 146,  637 => 145,  633 => 144,  629 => 143,  625 => 142,  621 => 141,  617 => 140,  613 => 139,  609 => 138,  605 => 137,  601 => 136,  597 => 135,  593 => 134,  589 => 133,  585 => 132,  581 => 131,  577 => 130,  573 => 129,  569 => 128,  565 => 127,  561 => 126,  557 => 125,  553 => 124,  549 => 123,  545 => 122,  541 => 121,  537 => 120,  533 => 119,  529 => 118,  525 => 117,  521 => 116,  517 => 115,  513 => 114,  509 => 113,  505 => 112,  501 => 111,  497 => 110,  493 => 109,  489 => 108,  485 => 107,  481 => 106,  477 => 105,  473 => 104,  469 => 103,  465 => 102,  461 => 101,  457 => 100,  453 => 99,  449 => 98,  445 => 97,  441 => 96,  437 => 95,  433 => 94,  429 => 93,  425 => 92,  421 => 91,  417 => 90,  413 => 89,  409 => 88,  405 => 87,  401 => 86,  397 => 85,  393 => 84,  389 => 83,  385 => 82,  381 => 81,  377 => 80,  373 => 79,  369 => 78,  365 => 77,  361 => 76,  357 => 75,  353 => 74,  349 => 73,  345 => 72,  341 => 71,  337 => 70,  333 => 69,  329 => 68,  325 => 67,  321 => 66,  317 => 65,  313 => 64,  309 => 63,  305 => 62,  301 => 61,  297 => 60,  293 => 59,  289 => 58,  285 => 57,  281 => 56,  277 => 55,  273 => 54,  269 => 53,  265 => 52,  261 => 51,  257 => 50,  253 => 49,  249 => 48,  245 => 47,  241 => 46,  237 => 45,  233 => 44,  229 => 43,  225 => 42,  221 => 41,  217 => 40,  213 => 39,  209 => 38,  205 => 37,  201 => 36,  197 => 35,  193 => 34,  189 => 33,  185 => 32,  181 => 31,  177 => 30,  173 => 29,  169 => 28,  165 => 27,  161 => 26,  157 => 25,  153 => 24,  149 => 23,  144 => 22,  135 => 21,  123 => 18,  119 => 17,  115 => 16,  111 => 15,  107 => 14,  103 => 13,  99 => 12,  95 => 11,  91 => 10,  87 => 9,  83 => 8,  79 => 7,  75 => 6,  71 => 5,  66 => 4,  57 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"::base.html.twig\" %}

{% block stylesheetsInclude %}
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/angularjs/bower_components/bootstrap/dist/css/bootstrap.min.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/angularjs/bower_components/bootstrap/dist/css/bootstrap-theme.min.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-ui-grid/ui-grid.min.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-block-ui/dist/angular-block-ui.min.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/lib/codemirror.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/addon/display/fullscreen.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/addon/hint/show-hint.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/css/codemirror-auto-resize.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/angularjs/bower_components/jquery-ui/themes/base/jquery-ui.min.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/js/angular-filemanager/angular-filemanager.min.css?v=' ~ version) }}\">
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/css/base.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/css/helper.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/css/flow.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/concertopanel/css/file_manager.css?v=' ~ version) }}\"/>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('bundles/concertopanel/angularjs/bower_components/diff2html/dist/diff2html.min.css?v=' ~ version) }}\">
{% endblock %}

{% block javascriptsInclude %}
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/diff2html/dist/diff2html.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/jquery/dist/jquery.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/jquery.metadata/jquery.metadata.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/jquery-ui/jquery-ui.min.js') }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/jquery-mousewheel/jquery.mousewheel.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/common/js/misc/helpers.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular/angular.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-animate/angular-animate.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-translate/angular-translate.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/ng-file-upload/ng-file-upload.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-sanitize/angular-sanitize.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-ui-grid/ui-grid.min.js?v=' ~ version) }}\"></script>
    <script src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/bootstrap/dist/js/bootstrap.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-bootstrap/ui-bootstrap.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-block-ui/dist/angular-block-ui.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/ckeditor/ckeditor.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/lib/codemirror.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/addon/display/fullscreen.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/misc/extended-show-hint.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/misc/codemirror-r-completion.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/mode/xml/xml.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/mode/javascript/javascript.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/mode/css/css.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/mode/htmlmixed/htmlmixed.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/mode/r/r.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/codemirror/mode/sql/sql.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-ui-codemirror/ui-codemirror.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-file-upload/dist/angular-file-upload.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-ui-sortable/sortable.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-ui-router/release/angular-ui-router.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-breadcrumb/release/angular-breadcrumb.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/directives/ng-html.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/jsPlumb/dist/js/dom.jsPlumb-1.7.6-min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/ng-context-menu-concerto/dist/ng-context-menu.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/pdfmake/build/pdfmake.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/csv-js/csv.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/misc/vfs_fonts.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/chart.js/dist/Chart.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-chart.js/dist/angular-chart.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/qrcode/lib/qrcode.min.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/bower_components/angular-qr/angular-qr.min.js?v=' ~ version) }}\"></script>
    <script src=\"{{ asset('bundles/concertopanel/js/angular-filemanager/angular-filemanager.min.js?v=' ~ version) }}\"></script>
    {{ include(\"ConcertoPanelBundle::translation_injector.html.twig\") }}
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/misc/defaults.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/app.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/main_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/alert_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/content_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/confirm_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/connection_return_function_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/port_value_edit_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/port_add_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/textarea_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/node_wizard_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/import_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/export_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/download_list_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/upload_list_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/header_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/save_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/save_new_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/base_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_step_save_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_param_definer_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_param_setter_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_param_save_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_wizard_carousel_step_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/test_variables_save_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/default_r_completion_wizard_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/r_documentation_generation_help_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/view_template_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/data_table_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/data_table_import_csv_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/data_table_structure_save_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/ckeditor_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/user_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/user_test_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/state_view_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/administration_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/administration_content_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/r_package_install_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/pre_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/mfa_enabled_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/ongoing_scheduled_task_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_0_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_1_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_2_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_3_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_4_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_5_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_6_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_7_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_8_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_9_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_10_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_11_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_12_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_definer_13_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_0_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_1_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_2_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_3_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_4_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_5_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_6_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_7_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_8_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_9_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_10_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_11_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_12_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/wizard_param_setter_13_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/git_enable_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/git_commit_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/grid_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/r_documentation_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/base_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/test_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/test_wizard_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/view_template_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/data_table_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/user_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/user_test_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/session_count_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/test_wizard_param_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/administration_settings_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/messages_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/api_clients_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/scheduled_tasks_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/dialogs_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/auth_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/services/all_collection_service.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/controllers/file_browser_controller.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/directives/flow_logic_directive.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/directives/wizard_param_definer_directive.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/directives/wizard_param_setter_directive.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/directives/ng_ckeditor.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/filters/trusted_html.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/filters/logical.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/filters/capitalize.js?v=' ~ version) }}\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/filters/task_status_label.js?v=' ~ version) }}\"></script>

    <script>
        if (typeof CKEDITOR != \"undefined\") {
            CKEDITOR.plugins.addExternal('cmsource', '{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/misc/cmsource/') }}', 'plugin.js?v={{ version }}');
            CKEDITOR.plugins.addExternal('autogrow', '{{ asset('bundles/concertopanel/angularjs/app/concerto_panel/js/misc/autogrow/') }}', 'plugin.js?v={{ version }}');

            \$.each(CKEDITOR.dtd.\$removeEmpty, function (i, value) {
                CKEDITOR.dtd.\$removeEmpty[i] = false;
            });
        }
    </script>

{% endblock %}
", "ConcertoPanelBundle::base_panel.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/base_panel.html.twig");
    }
}
