<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_13_setter.html.twig */
class __TwigTemplate_c02ed5f160b13ffb348528eb491f3f57141b1c2adddf41a9a347a5f3d607139a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig"));

        // line 1
        echo "<script type=\"text/ng-template\" id=\"type_13_setter.html\">
    <div ng-controller=\"WizardParamSetter13Controller\">
        ";
        // line 3
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_13_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig", 3, "1528636961")->display($context);
        // line 46
        echo "    </div>
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 46,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_13_setter.html\">
    <div ng-controller=\"WizardParamSetter13Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ title }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.setter.titles.wizard.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <uib-alert type=\"warning\" ng-if=\"object.steps == 0\">
                            <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                            {{ 'alerts.no_steps'|trans }}
                        </uib-alert>

                        <uib-tabset ng-if=\"object.steps != 0\">
                            <uib-tab ng-repeat=\"step in object.steps\">
                                <uib-tab-heading>{% verbatim %}{{ step.title }}{% endverbatim %}</uib-tab-heading>
                                <div ng-controller=\"TestWizardCarouselStepController\">
                                    <div class=\"bs-callout bs-callout-info\" align=\"left\">
                                        <h3 ng-bind=\"step.title\"></h3>
                                        <p ng-bind-html=\"step.description | trustedHtml\"></p>
                                    </div>
                                    <uib-alert type=\"warning\" ng-if=\"step.params == 0\">
                                        <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                                        {{ 'alerts.no_params'|trans }}
                                    </uib-alert>
                                    <div ng-repeat=\"param in step.params | orderBy: 'order' track by \$index\">
                                        <wizard-param-setter editable=\"editable\"
                                                             param=\"param\"
                                                             parent=\"output\"
                                                             grand-parent=\"parent\"
                                                             output=\"output[param.name]\"
                                                             mode=\"outer\"
                                                             wizard-mode=\"prod\" values=\"values\"
                                                             wizard-object=\"object\"
                                                             under-list=\"false\"></wizard-param-setter>
                                    </div>
                                </div>
                            </uib-tab>
                        </uib-tabset>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_13_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_13_setter.html.twig */
class __TwigTemplate_c02ed5f160b13ffb348528eb491f3f57141b1c2adddf41a9a347a5f3d607139a___1528636961 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'setter' => [$this, 'block_setter'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "ConcertoPanelBundle:TestWizard:type_base_setter.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_base_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_setter($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "setter"));

        // line 6
        echo "                ";
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_13_setter.html.twig", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig", 6, "571224475")->display($context);
        // line 44
        echo "            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 44,  175 => 6,  166 => 5,  144 => 3,  47 => 46,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_13_setter.html\">
    <div ng-controller=\"WizardParamSetter13Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ title }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.setter.titles.wizard.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <uib-alert type=\"warning\" ng-if=\"object.steps == 0\">
                            <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                            {{ 'alerts.no_steps'|trans }}
                        </uib-alert>

                        <uib-tabset ng-if=\"object.steps != 0\">
                            <uib-tab ng-repeat=\"step in object.steps\">
                                <uib-tab-heading>{% verbatim %}{{ step.title }}{% endverbatim %}</uib-tab-heading>
                                <div ng-controller=\"TestWizardCarouselStepController\">
                                    <div class=\"bs-callout bs-callout-info\" align=\"left\">
                                        <h3 ng-bind=\"step.title\"></h3>
                                        <p ng-bind-html=\"step.description | trustedHtml\"></p>
                                    </div>
                                    <uib-alert type=\"warning\" ng-if=\"step.params == 0\">
                                        <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                                        {{ 'alerts.no_params'|trans }}
                                    </uib-alert>
                                    <div ng-repeat=\"param in step.params | orderBy: 'order' track by \$index\">
                                        <wizard-param-setter editable=\"editable\"
                                                             param=\"param\"
                                                             parent=\"output\"
                                                             grand-parent=\"parent\"
                                                             output=\"output[param.name]\"
                                                             mode=\"outer\"
                                                             wizard-mode=\"prod\" values=\"values\"
                                                             wizard-object=\"object\"
                                                             under-list=\"false\"></wizard-param-setter>
                                    </div>
                                </div>
                            </uib-tab>
                        </uib-tabset>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_13_setter.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_13_setter.html.twig */
class __TwigTemplate_c02ed5f160b13ffb348528eb491f3f57141b1c2adddf41a9a347a5f3d607139a___571224475 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'legend' => [$this, 'block_legend'],
            'legend_tooltip' => [$this, 'block_legend_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 6
        return "ConcertoPanelBundle::form_v_single.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_single.html.twig", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig", 6);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo "{{ title }}";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend_tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("param.setter.titles.wizard.tooltip", [], "TestWizard"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 11
        echo "                        <uib-alert type=\"warning\" ng-if=\"object.steps == 0\">
                            <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                            ";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("alerts.no_steps", [], "TestWizard"), "html", null, true);
        echo "
                        </uib-alert>

                        <uib-tabset ng-if=\"object.steps != 0\">
                            <uib-tab ng-repeat=\"step in object.steps\">
                                <uib-tab-heading>";
        // line 18
        echo "{{ step.title }}";
        echo "</uib-tab-heading>
                                <div ng-controller=\"TestWizardCarouselStepController\">
                                    <div class=\"bs-callout bs-callout-info\" align=\"left\">
                                        <h3 ng-bind=\"step.title\"></h3>
                                        <p ng-bind-html=\"step.description | trustedHtml\"></p>
                                    </div>
                                    <uib-alert type=\"warning\" ng-if=\"step.params == 0\">
                                        <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                                        ";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("alerts.no_params", [], "TestWizard"), "html", null, true);
        echo "
                                    </uib-alert>
                                    <div ng-repeat=\"param in step.params | orderBy: 'order' track by \$index\">
                                        <wizard-param-setter editable=\"editable\"
                                                             param=\"param\"
                                                             parent=\"output\"
                                                             grand-parent=\"parent\"
                                                             output=\"output[param.name]\"
                                                             mode=\"outer\"
                                                             wizard-mode=\"prod\" values=\"values\"
                                                             wizard-object=\"object\"
                                                             under-list=\"false\"></wizard-param-setter>
                                    </div>
                                </div>
                            </uib-tab>
                        </uib-tabset>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  366 => 26,  355 => 18,  347 => 13,  343 => 11,  334 => 10,  316 => 9,  298 => 8,  276 => 6,  178 => 44,  175 => 6,  166 => 5,  144 => 3,  47 => 46,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_13_setter.html\">
    <div ng-controller=\"WizardParamSetter13Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_setter.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block setter %}
                {% embed \"ConcertoPanelBundle::form_v_single.html.twig\" %}
                    {% trans_default_domain \"TestWizard\" %}
                    {% block legend %}{% verbatim %}{{ title }}{% endverbatim %}{% endblock %}
                    {% block legend_tooltip %}{{ 'param.setter.titles.wizard.tooltip'|trans }}{% endblock %}
                    {% block control %}
                        <uib-alert type=\"warning\" ng-if=\"object.steps == 0\">
                            <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                            {{ 'alerts.no_steps'|trans }}
                        </uib-alert>

                        <uib-tabset ng-if=\"object.steps != 0\">
                            <uib-tab ng-repeat=\"step in object.steps\">
                                <uib-tab-heading>{% verbatim %}{{ step.title }}{% endverbatim %}</uib-tab-heading>
                                <div ng-controller=\"TestWizardCarouselStepController\">
                                    <div class=\"bs-callout bs-callout-info\" align=\"left\">
                                        <h3 ng-bind=\"step.title\"></h3>
                                        <p ng-bind-html=\"step.description | trustedHtml\"></p>
                                    </div>
                                    <uib-alert type=\"warning\" ng-if=\"step.params == 0\">
                                        <i class=\"glyphicon glyphicon-exclamation-sign\"></i>
                                        {{ 'alerts.no_params'|trans }}
                                    </uib-alert>
                                    <div ng-repeat=\"param in step.params | orderBy: 'order' track by \$index\">
                                        <wizard-param-setter editable=\"editable\"
                                                             param=\"param\"
                                                             parent=\"output\"
                                                             grand-parent=\"parent\"
                                                             output=\"output[param.name]\"
                                                             mode=\"outer\"
                                                             wizard-mode=\"prod\" values=\"values\"
                                                             wizard-object=\"object\"
                                                             under-list=\"false\"></wizard-param-setter>
                                    </div>
                                </div>
                            </uib-tab>
                        </uib-tabset>
                    {% endblock %}
                {% endembed %}
            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_13_setter.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_13_setter.html.twig");
    }
}
