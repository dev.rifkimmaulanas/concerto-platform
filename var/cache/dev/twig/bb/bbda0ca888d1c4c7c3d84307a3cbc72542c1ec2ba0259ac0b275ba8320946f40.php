<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Administration:packages_section.html.twig */
class __TwigTemplate_d362d8ac394b36a25e54cb80eacf1df4f5cee52111b5e6a651e8b8c3eea18308 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:packages_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:packages_section.html.twig"));

        // line 2
        echo "
<div class=\"center\">
    <button ng-click=\"packagesReport();\" class=\"btn btn-default btn-sm\">";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.list.buttons.report", [], "Administration"), "html", null, true);
        echo "</button>
    <button ng-click=\"refreshAllTaskRelated();\" class=\"btn btn-default btn-sm btn-list-refresh\">";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.list.buttons.refresh", [], "Administration"), "html", null, true);
        echo "</button>
    <button ng-click=\"installPackage();\" class=\"btn btn-success btn-sm\">";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("packages.list.buttons.install", [], "Administration"), "html", null, true);
        echo "</button>
</div>
<div ui-grid=\"packagesTasksOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid collectionTable\"></div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:packages_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 6,  49 => 5,  45 => 4,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Administration\" %}

<div class=\"center\">
    <button ng-click=\"packagesReport();\" class=\"btn btn-default btn-sm\">{{ \"packages.list.buttons.report\"|trans }}</button>
    <button ng-click=\"refreshAllTaskRelated();\" class=\"btn btn-default btn-sm btn-list-refresh\">{{ \"packages.list.buttons.refresh\"|trans }}</button>
    <button ng-click=\"installPackage();\" class=\"btn btn-success btn-sm\">{{ \"packages.list.buttons.install\"|trans }}</button>
</div>
<div ui-grid=\"packagesTasksOptions\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-exporter ui-grid-selection ui-grid-move-columns class=\"grid collectionTable\"></div>", "ConcertoPanelBundle:Administration:packages_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/packages_section.html.twig");
    }
}
