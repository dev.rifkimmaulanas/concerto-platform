<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'top' => [$this, 'block_top'],
            'legend' => [$this, 'block_legend'],
            'elements' => [$this, 'block_elements'],
            'sections' => [$this, 'block_sections'],
            'floatingBarButtons' => [$this, 'block_floatingBarButtons'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "ConcertoPanelBundle::form_h.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        // line 2
        $context["class_name"] = "Test";
        // line 4
        $context["exportable"] = true;
        // line 1
        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_top($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        // line 7
        echo "    ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle::lock_info.html.twig");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        echo " 
    ";
        // line 10
        echo "{{formTitle}}";
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 13
        echo "    ";
        $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 13, "291626582")->display($context);
        // line 21
        echo "
    ";
        // line 22
        $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 22, "718674015")->display($context);
        // line 30
        echo "
    ";
        // line 31
        $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 31, "704024557")->display($context);
        // line 39
        echo "
    ";
        // line 40
        $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 40, "239413296")->display($context);
        // line 49
        echo "
    ";
        // line 50
        $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 50, "279246251")->display($context);
        // line 60
        echo "
    ";
        // line 61
        $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 61, "1853393791")->display($context);
        // line 78
        echo "
        ";
        // line 79
        $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 79, "1675363917")->display($context);
        // line 87
        echo "
        ";
        // line 88
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 88, $this->source); })()), "user", [], "any", false, false, false, 88) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
            // line 89
            echo "            ";
            $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 89, "909961383")->display($context);
            // line 98
            echo "
            ";
            // line 99
            $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 99, "1524497865")->display($context);
            // line 109
            echo "
            ";
            // line 110
            $this->loadTemplate("ConcertoPanelBundle:Test:form.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 110, "665590510")->display($context);
            // line 118
            echo "        ";
        }
        // line 119
        echo "
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 122
    public function block_sections($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        // line 123
        echo "        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 125
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("templates.fieldset.legend.tooltip", [], "Test"), "html", null, true);
        echo "'\"></i>
                ";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("templates.fieldset.legend", [], "Test"), "html", null, true);
        echo "
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            ";
        // line 129
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Test:templates_section.html.twig");
        echo "
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 134
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.fieldset.legend.tooltip", [], "Test"), "html", null, true);
        echo "'\"></i>
                ";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.input.fieldset.legend", [], "Test"), "html", null, true);
        echo "
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            ";
        // line 138
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Test:input_section.html.twig");
        echo "
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 143
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("logic.code.fieldset.legend.tooltip", [], "Test"), "html", null, true);
        echo "'\"></i>
                ";
        // line 144
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("logic.code.fieldset.legend", [], "Test"), "html", null, true);
        echo "
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            ";
        // line 147
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Test:logic_section.html.twig");
        echo "
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("logic.code.fieldset.legend.tooltip", [], "Test"), "html", null, true);
        echo "'\"></i>
                ";
        // line 153
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("logic.code.fieldset.legend", [], "Test"), "html", null, true);
        echo "
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            ";
        // line 156
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Test:wizard_section.html.twig");
        echo "
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 161
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.fieldset.legend.tooltip", [], "Test"), "html", null, true);
        echo "'\"></i>
                ";
        // line 162
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("flow.fieldset.legend", [], "Test"), "html", null, true);
        echo "
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            ";
        // line 165
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Test:flow_section.html.twig");
        echo "
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 170
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.fieldset.legend.tooltip", [], "Test"), "html", null, true);
        echo "'\"></i>
                ";
        // line 171
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("variables.output.fieldset.legend", [], "Test"), "html", null, true);
        echo "
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            ";
        // line 174
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Test:output_section.html.twig");
        echo "
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'";
        // line 179
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.fieldset.legend.tooltip", [], "Test"), "html", null, true);
        echo "'\"></i>
                ";
        // line 180
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("log.fieldset.legend", [], "Test"), "html", null, true);
        echo "
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            ";
        // line 183
        echo twig_include($this->env, $context, "ConcertoPanelBundle:Test:log_section.html.twig");
        echo "
        </uib-accordion-group>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 187
    public function block_floatingBarButtons($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarButtons"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "floatingBarButtons"));

        // line 188
        echo "        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.save", [], "panel"), "html", null, true);
        echo "</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">";
        // line 189
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.run", [], "Test"), "html", null, true);
        echo "</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">";
        // line 190
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.debug", [], "Test"), "html", null, true);
        echo "</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">";
        // line 191
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.cancel", [], "panel"), "html", null, true);
        echo "</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">";
        // line 192
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.save.new", [], "panel"), "html", null, true);
        echo "</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">";
        // line 193
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.export", [], "panel"), "html", null, true);
        echo "</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>";
        // line 194
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.lock", [], "panel"), "html", null, true);
        echo "</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>";
        // line 195
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.unlock", [], "panel"), "html", null, true);
        echo "</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">";
        // line 196
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.button.delete", [], "panel"), "html", null, true);
        echo "</button>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___291626582 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 13
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 13);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 15
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.name", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.name.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 17
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 18
        echo "            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___718674015 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 22
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 22);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 24
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 26
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 27
        echo "            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1003 => 27,  994 => 26,  976 => 25,  958 => 24,  936 => 22,  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___704024557 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 31
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 31);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 33
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.slug", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 34
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.slug.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 35
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 36
        echo "            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1319 => 36,  1310 => 35,  1292 => 34,  1274 => 33,  1252 => 31,  1003 => 27,  994 => 26,  976 => 25,  958 => 24,  936 => 22,  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___239413296 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 40
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 40);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 42
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.visibility", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 43
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.visibility.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 44
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 45
        echo "            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1635 => 45,  1626 => 44,  1608 => 43,  1590 => 42,  1568 => 40,  1319 => 36,  1310 => 35,  1292 => 34,  1274 => 33,  1252 => 31,  1003 => 27,  994 => 26,  976 => 25,  958 => 24,  936 => 22,  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___279246251 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 50
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 50);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 52
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.type", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 53
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.type.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 54
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 55
        echo "            <select ng-model=\"object.type\" ";
        if (((isset($context["isAddDialog"]) || array_key_exists("isAddDialog", $context) ? $context["isAddDialog"] : (function () { throw new RuntimeError('Variable "isAddDialog" does not exist.', 55, $this->source); })()) == false)) {
            echo "ng-disabled=\"true\"";
        } else {
            echo "ng-disabled=\"!isEditable()\"";
        }
        // line 56
        echo "                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1959 => 56,  1952 => 55,  1943 => 54,  1925 => 53,  1907 => 52,  1885 => 50,  1635 => 45,  1626 => 44,  1608 => 43,  1590 => 42,  1568 => 40,  1319 => 36,  1310 => 35,  1292 => 34,  1274 => 33,  1252 => 31,  1003 => 27,  994 => 26,  976 => 25,  958 => 24,  936 => 22,  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___1853393791 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'div' => [$this, 'block_div'],
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
            'extra_info' => [$this, 'block_extra_info'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 61
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 61);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 63
    public function block_div($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "div"));

        echo "<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 64
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.wizard", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 65
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.wizard.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 66
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 67
        echo "                <select ng-model=\"object.sourceWizard\" ";
        if (((isset($context["isAddDialog"]) || array_key_exists("isAddDialog", $context) ? $context["isAddDialog"] : (function () { throw new RuntimeError('Variable "isAddDialog" does not exist.', 67, $this->source); })()) == false)) {
            echo "ng-disabled=\"true\"";
        } else {
            echo "ng-disabled=\"!isEditable()\"";
        }
        // line 68
        echo "                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.wizard.none", [], "Test"), "html", null, true);
        echo "</option>
                </select>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 72
    public function block_extra_info($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_info"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_info"));

        // line 73
        echo "                <a ng-href=\"#/wizards/";
        echo "{{object.sourceWizard}}";
        echo "\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2328 => 73,  2319 => 72,  2306 => 69,  2303 => 68,  2296 => 67,  2287 => 66,  2269 => 65,  2251 => 64,  2233 => 63,  2211 => 61,  1959 => 56,  1952 => 55,  1943 => 54,  1925 => 53,  1907 => 52,  1885 => 50,  1635 => 45,  1626 => 44,  1608 => 43,  1590 => 42,  1568 => 40,  1319 => 36,  1310 => 35,  1292 => 34,  1274 => 33,  1252 => 31,  1003 => 27,  994 => 26,  976 => 25,  958 => 24,  936 => 22,  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___1675363917 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 79
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 79);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 81
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.protected", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 82
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.protected.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 83
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 84
        echo "                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2648 => 84,  2639 => 83,  2621 => 82,  2603 => 81,  2581 => 79,  2328 => 73,  2319 => 72,  2306 => 69,  2303 => 68,  2296 => 67,  2287 => 66,  2269 => 65,  2251 => 64,  2233 => 63,  2211 => 61,  1959 => 56,  1952 => 55,  1943 => 54,  1925 => 53,  1907 => 52,  1885 => 50,  1635 => 45,  1626 => 44,  1608 => 43,  1590 => 42,  1568 => 40,  1319 => 36,  1310 => 35,  1292 => 34,  1274 => 33,  1252 => 31,  1003 => 27,  994 => 26,  976 => 25,  958 => 24,  936 => 22,  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___909961383 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 89
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 89);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 91
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 92
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 93
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 94
        echo "                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2964 => 94,  2955 => 93,  2937 => 92,  2919 => 91,  2897 => 89,  2648 => 84,  2639 => 83,  2621 => 82,  2603 => 81,  2581 => 79,  2328 => 73,  2319 => 72,  2306 => 69,  2303 => 68,  2296 => 67,  2287 => 66,  2269 => 65,  2251 => 64,  2233 => 63,  2211 => 61,  1959 => 56,  1952 => 55,  1943 => 54,  1925 => 53,  1907 => 52,  1885 => 50,  1635 => 45,  1626 => 44,  1608 => 43,  1590 => 42,  1568 => 40,  1319 => 36,  1310 => 35,  1292 => 34,  1274 => 33,  1252 => 31,  1003 => 27,  994 => 26,  976 => 25,  958 => 24,  936 => 22,  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___1524497865 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 99
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 99);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 101
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 102
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 103
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 104
        echo "                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                    </select>
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  3284 => 105,  3281 => 104,  3272 => 103,  3254 => 102,  3236 => 101,  3214 => 99,  2964 => 94,  2955 => 93,  2937 => 92,  2919 => 91,  2897 => 89,  2648 => 84,  2639 => 83,  2621 => 82,  2603 => 81,  2581 => 79,  2328 => 73,  2319 => 72,  2306 => 69,  2303 => 68,  2296 => 67,  2287 => 66,  2269 => 65,  2251 => 64,  2233 => 63,  2211 => 61,  1959 => 56,  1952 => 55,  1943 => 54,  1925 => 53,  1907 => 52,  1885 => 50,  1635 => 45,  1626 => 44,  1608 => 43,  1590 => 42,  1568 => 40,  1319 => 36,  1310 => 35,  1292 => 34,  1274 => 33,  1252 => 31,  1003 => 27,  994 => 26,  976 => 25,  958 => 24,  936 => 22,  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}


/* ConcertoPanelBundle:Test:form.html.twig */
class __TwigTemplate_c7b28e2e05e70342f5d178aa979d5e393bc95339d9a4c126e376da054111d9c1___665590510 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 110
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Test:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:Test:form.html.twig", 110);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 112
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 113
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups.tooltip", [], "Test"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 114
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 115
        echo "                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Test:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  3602 => 115,  3593 => 114,  3575 => 113,  3557 => 112,  3535 => 110,  3284 => 105,  3281 => 104,  3272 => 103,  3254 => 102,  3236 => 101,  3214 => 99,  2964 => 94,  2955 => 93,  2937 => 92,  2919 => 91,  2897 => 89,  2648 => 84,  2639 => 83,  2621 => 82,  2603 => 81,  2581 => 79,  2328 => 73,  2319 => 72,  2306 => 69,  2303 => 68,  2296 => 67,  2287 => 66,  2269 => 65,  2251 => 64,  2233 => 63,  2211 => 61,  1959 => 56,  1952 => 55,  1943 => 54,  1925 => 53,  1907 => 52,  1885 => 50,  1635 => 45,  1626 => 44,  1608 => 43,  1590 => 42,  1568 => 40,  1319 => 36,  1310 => 35,  1292 => 34,  1274 => 33,  1252 => 31,  1003 => 27,  994 => 26,  976 => 25,  958 => 24,  936 => 22,  687 => 18,  678 => 17,  660 => 16,  642 => 15,  620 => 13,  370 => 196,  366 => 195,  362 => 194,  358 => 193,  354 => 192,  350 => 191,  346 => 190,  342 => 189,  337 => 188,  328 => 187,  315 => 183,  309 => 180,  305 => 179,  297 => 174,  291 => 171,  287 => 170,  279 => 165,  273 => 162,  269 => 161,  261 => 156,  255 => 153,  251 => 152,  243 => 147,  237 => 144,  233 => 143,  225 => 138,  219 => 135,  215 => 134,  207 => 129,  201 => 126,  197 => 125,  193 => 123,  184 => 122,  173 => 119,  170 => 118,  168 => 110,  165 => 109,  163 => 99,  160 => 98,  157 => 89,  155 => 88,  152 => 87,  150 => 79,  147 => 78,  145 => 61,  142 => 60,  140 => 50,  137 => 49,  135 => 40,  132 => 39,  130 => 31,  127 => 30,  125 => 22,  122 => 21,  119 => 13,  110 => 12,  98 => 10,  87 => 9,  74 => 7,  65 => 6,  54 => 1,  52 => 4,  50 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"Test\" %}
{% trans_default_domain \"Test\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %} 
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.slug'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.slug.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.slug\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.visibility'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.visibility.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-disabled=\"!isEditable()\" ng-model=\"object.visibility\" style=\"width:100%;\" ng-options=\"visibility.value as visibility.label for visibility in visibilities\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block label %}{{ 'form.field.type'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.type.tooltip'|trans}}{% endblock %}
        {% block control %}
            <select ng-model=\"object.type\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                    style=\"width:100%;\" ng-options=\"type.value as type.label for type in types\" class='form-control'>
            </select>
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"Test\" %}
        {% block div %}<div class=\"horizontalElement center\" ng-show=\"object.type == 1\">{% endblock %}
            {% block label %}{{ 'form.field.wizard'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.wizard.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-model=\"object.sourceWizard\" {% if isAddDialog == false %}ng-disabled=\"true\"{% else %}ng-disabled=\"!isEditable()\"{% endif %}
                        style=\"width:100%;\" ng-options=\"wizard.id as wizard.name for wizard in testWizardCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'name'\" class='form-control'>
                    <option value=\"\">{{ \"form.field.wizard.none\"|trans }}</option>
                </select>
            {% endblock %}
            {% block extra_info %}
                <a ng-href=\"#/wizards/{% verbatim %}{{object.sourceWizard}}{% endverbatim %}\" ng-show=\"object.sourceWizard !== null\">
                    <i class=\"glyphicon glyphicon-link\"></i>
                </a>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"Test\" %}
            {% block label %}{{ 'form.field.protected'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.protected.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.protected\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
            {% endblock %}
        {% endembed %}

        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username'\" class='form-control'>
                        <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                    </select>
                {% endblock %}
            {% endembed %}

            {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
                {% trans_default_domain \"Test\" %}
                {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
                {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
                {% block control %}
                    <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}

    {% block sections %}
        <uib-accordion-group is-open=\"tabAccordion.templates.open\" ng-show=\"object.visibility != 2\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'templates.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'templates.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.templates.open, 'glyphicon-chevron-right': !tabAccordion.templates.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:templates_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.input.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.input.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.input.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.input.open, 'glyphicon-chevron-right': !tabAccordion.input.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:input_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 0\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:logic_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'logic.code.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'logic.code.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:wizard_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.logic.open\" ng-show=\"object.type == 2\" id=\"accordion-group-test-flow\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'flow.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'flow.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.logic.open, 'glyphicon-chevron-right': !tabAccordion.logic.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:flow_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.output.open\" ng-show=\"object.type != 1\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'variables.output.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'variables.output.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.output.open, 'glyphicon-chevron-right': !tabAccordion.output.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:output_section.html.twig\") }}
        </uib-accordion-group>

        <uib-accordion-group is-open=\"tabAccordion.log.open\">
            <uib-accordion-heading>
                <i class=\"glyphicon glyphicon-info-sign\" uib-tooltip-html=\"'{{ 'log.fieldset.legend.tooltip'|trans }}'\"></i>
                {{ 'log.fieldset.legend'|trans }}
                <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': tabAccordion.log.open, 'glyphicon-chevron-right': !tabAccordion.log.open}\"></i>
            </uib-accordion-heading>
            {{ include(\"ConcertoPanelBundle:Test:log_section.html.twig\") }}
        </uib-accordion-group>
    {% endblock %}

    {% block floatingBarButtons %}
        <button ng-disabled=\"!isEditable()\" class='btn btn-success' ng-click=\"persist();\">{{ 'form.button.save'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-primary\" ng-click=\"startTest()\">{{ 'form.button.run'|trans }}</button>
        <button class=\"btn btn-primary\" ng-click=\"debugTest()\">{{ 'form.button.debug'|trans }}</button>
        <button class='btn btn-warning' ng-click=\"cancel();\">{{ 'form.button.cancel'|trans({},\"panel\") }}</button>
        <button class='btn btn-success' ng-click=\"saveNew();\">{{ 'form.button.save.new'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-click=\"exportObject();\">{{ 'form.button.export'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"!isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.lock'|trans({},\"panel\") }}</button>
        <button class=\"btn btn-default\" ng-show=\"isLocked()\" ng-disabled=\"!isEditable()\" ng-click=\"toggleLock()\"><i class=\"glyphicon glyphicon-lock\"></i>{{ 'form.button.unlock'|trans({},\"panel\") }}</button>
        <button ng-disabled=\"!isEditable()\" class='btn btn-danger' ng-click=\"deleteObject();\">{{ 'form.button.delete'|trans({},\"panel\") }}</button>
    {% endblock %}", "ConcertoPanelBundle:Test:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Test/form.html.twig");
    }
}
