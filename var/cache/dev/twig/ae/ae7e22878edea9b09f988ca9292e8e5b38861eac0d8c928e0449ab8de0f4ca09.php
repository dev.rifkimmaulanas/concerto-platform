<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:TestWizard:type_7_definer.html.twig */
class __TwigTemplate_a99f800686aaa30bb74f76c02a1a77d96e5d48f0c61219c7c08538d823e15ea5 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig"));

        // line 1
        echo "<script type=\"text/ng-template\" id=\"type_7_definer.html\">
    <div ng-controller=\"WizardParamDefiner7Controller\">
        ";
        // line 3
        $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_7_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig", 3, "1578440203")->display($context);
        // line 9
        echo "    </div>
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 9,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_7_definer.html\">
    <div ng-controller=\"WizardParamDefiner7Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}

            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_7_definer.html.twig");
    }
}


/* ConcertoPanelBundle:TestWizard:type_7_definer.html.twig */
class __TwigTemplate_a99f800686aaa30bb74f76c02a1a77d96e5d48f0c61219c7c08538d823e15ea5___1578440203 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'definition' => [$this, 'block_definition'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "ConcertoPanelBundle:TestWizard:type_base_definer.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle:TestWizard:type_base_definer.html.twig", "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_definition($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "definition"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "definition"));

        // line 6
        echo "
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 6,  129 => 5,  107 => 3,  47 => 9,  45 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/ng-template\" id=\"type_7_definer.html\">
    <div ng-controller=\"WizardParamDefiner7Controller\">
        {% embed \"ConcertoPanelBundle:TestWizard:type_base_definer.html.twig\" %}
            {% trans_default_domain \"TestWizard\" %}
            {% block definition %}

            {% endblock %}
        {% endembed %}
    </div>
</script>", "ConcertoPanelBundle:TestWizard:type_7_definer.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/TestWizard/type_7_definer.html.twig");
    }
}
