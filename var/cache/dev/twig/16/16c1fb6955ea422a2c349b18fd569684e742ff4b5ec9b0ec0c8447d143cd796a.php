<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:Administration:content_section.html.twig */
class __TwigTemplate_079f06dc9d0f3a08696614afece27821dbe7233d4a3ce94a669c5742afa30213 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        // line 2
        echo "
<div ng-controller=\"AdministrationContentController\">

    <div style=\"display: table; width: 100%;\">
        <div style=\"display: table-cell; width: 50%; border-right: solid #ddd 1px; padding-right: 16px;\">
            <div class=\"alert alert-info\">
                <table>
                    <tr>
                        <td style=\"padding-right: 5px;\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.last_import_time", [], "Administration"), "html", null, true);
        echo "</td>
                        <td><strong>";
        // line 11
        echo "{{ internalSettingsMap.last_import_time ? internalSettingsMap.last_import_time : \"-\" }}";
        echo "</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.enable.url", [], "Administration"), "html", null, true);
        echo "</td>
                        <td><strong>";
        // line 15
        echo "{{exposedSettingsMap.git_url}}";
        echo "</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.enable.branch", [], "Administration"), "html", null, true);
        echo "</td>
                        <td><strong>";
        // line 19
        echo "{{exposedSettingsMap.git_branch}}";
        echo "</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.enable.login", [], "Administration"), "html", null, true);
        echo "</td>
                        <td><strong>";
        // line 23
        echo "{{exposedSettingsMap.git_login}}";
        echo "</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.status.behind", [], "Administration"), "html", null, true);
        echo "</td>
                        <td><strong>";
        // line 27
        echo "{{gitStatus.behind}}";
        echo "</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.status.ahead", [], "Administration"), "html", null, true);
        echo "</td>
                        <td><strong>";
        // line 31
        echo "{{gitStatus.ahead}}";
        echo "</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.status.local_changes", [], "Administration"), "html", null, true);
        echo "</td>
                        <td>
                            <span ng-if=\"gitStatus.diff !== '?'\">
                                <strong ng-if=\"!hasUncommittedChanges()\">";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.status.local_changes.no_changes", [], "Administration"), "html", null, true);
        echo "</strong>
                                <strong ng-if=\"hasUncommittedChanges()\" class=\"clickable\" ng-click=\"showLocalDiff()\">";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.status.local_changes.uncommitted_changes", [], "Administration"), "html", null, true);
        echo "</strong>
                                ";
        // line 39
        echo "{{ internalSettingsMap.last_git_update_time ? \" (\" + internalSettingsMap.last_git_update_time + \")\" : \"\" }}";
        echo "
                            </span>
                            <span ng-if=\"gitStatus.diff === '?'\"><strong>?</strong></span>
                        </td>
                    </tr>
                </table>
            </div>

            <div>
                ";
        // line 48
        $this->loadTemplate("ConcertoPanelBundle:Administration:content_section.html.twig", "ConcertoPanelBundle:Administration:content_section.html.twig", 48, "203095222")->display(twig_array_merge($context, ["no_header" => true]));
        // line 85
        echo "            </div>

            <div class=\"center\">
                <button ng-click=\"importUrl()\" class=\"btn btn-success btn-sm\" ng-disabled=\"!canImport()\">";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.import", [], "Administration"), "html", null, true);
        echo "</button>
                <button ng-click=\"exportUrl()\" class=\"btn btn-default btn-sm\">";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.export", [], "Administration"), "html", null, true);
        echo "</button>
                <button ng-click=\"push()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPush()\" style=\"margin-left: 16px;\">";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.push.button", [], "Administration"), "html", null, true);
        echo "</button>
                <button ng-click=\"pull()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPull()\">";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.pull.button", [], "Administration"), "html", null, true);
        echo "</button>
                <button ng-click=\"commit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canCommit()\">";
        // line 92
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.commit.button", [], "Administration"), "html", null, true);
        echo "</button>
                <button ng-click=\"reset()\" class=\"btn btn-danger btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canReset()\">";
        // line 93
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.reset.button", [], "Administration"), "html", null, true);
        echo "</button>
                <button ng-click=\"update()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canUpdate()\">";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.update.button", [], "Administration"), "html", null, true);
        echo "</button>
                <button ng-click=\"refreshGitStatus()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\">";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.refresh.button", [], "Administration"), "html", null, true);
        echo "</button>
                <button ng-click=\"enableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"!isGitEnabled()\" ng-disabled=\"!canEnableGit()\">";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.enable.button", [], "Administration"), "html", null, true);
        echo "</button>
                <button ng-click=\"disableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canDisableGit()\">";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.disable.button", [], "Administration"), "html", null, true);
        echo "</button>
            </div>

            <div ng-if=\"isGitEnabled()\">
                <h4 style=\"text-align: center; margin-top: 32px;\">";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.latest_operation", [], "Administration"), "html", null, true);
        echo "</h4>

                <div class=\"alert alert-warning\" ng-if=\"gitStatus.latestTask == null\">";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.no_git_task", [], "Administration"), "html", null, true);
        echo "</div>
                <table class=\"table-lite\" ng-if=\"gitStatus.latestTask != null\">
                    <tr>
                        <th>";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.task.started", [], "Administration"), "html", null, true);
        echo "</th>
                        <th>";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.task.description", [], "Administration"), "html", null, true);
        echo "</th>
                        <th>";
        // line 108
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.task.output", [], "Administration"), "html", null, true);
        echo "</th>
                        <th>";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.task.status", [], "Administration"), "html", null, true);
        echo "</th>
                    </tr>
                    <tr>
                        <td ng-bind=\"gitStatus.latestTask.created\"></td>
                        <td ng-bind=\"gitStatus.latestTask.description\"></td>
                        <td>
                            <i class=\"glyphicon glyphicon-align-justify clickable\" ng-click=\"showLatestTaskOutput()\"></i>
                        </td>
                        <td>";
        // line 117
        echo "{{ gitStatus.latestTask.status|taskStatusLabel }}";
        echo "</td>
                    </tr>
                </table>
            </div>
        </div>

        <div style=\"display: table-cell; width: 50%; padding-left: 16px;\" ng-if=\"isGitEnabled()\">
            <h4 style=\"text-align: center;\">";
        // line 124
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.history.title", [], "Administration"), "html", null, true);
        echo "</h4>

            <div class=\"alert alert-warning\" ng-if=\"gitStatus.history.length == 0\">";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.history.no_commits", [], "Administration"), "html", null, true);
        echo "</div>

            <table class=\"table-lite\" ng-if=\"gitStatus.history.length > 0\">
                <tr>
                    <th>";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.history.list.sha", [], "Administration"), "html", null, true);
        echo "</th>
                    <th>";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.history.list.committer", [], "Administration"), "html", null, true);
        echo "</th>
                    <th>";
        // line 132
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.history.list.time_ago", [], "Administration"), "html", null, true);
        echo "</th>
                    <th>";
        // line 133
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.history.list.subject", [], "Administration"), "html", null, true);
        echo "</th>
                    <th>";
        // line 134
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("git.history.list.ref", [], "Administration"), "html", null, true);
        echo "</th>
                </tr>
                <tr ng-repeat=\"commit in gitStatus.history track by \$index\" ng-class=\"{'git-history-head': commit.ref.indexOf('HEAD -> master') !== -1, 'clickable': canDiff(commit.sha)}\" ng-click=\"showDiff(commit.sha)\">
                    <td ng-bind=\"commit.sha\" class=\"git-history-commit\"></td>
                    <td ng-bind=\"commit.committer\"></td>
                    <td ng-bind=\"commit.timeAgo\"></td>
                    <td ng-bind=\"commit.subject\"></td>
                    <td ng-bind=\"commit.ref\"></td>
                </tr>
            </table>
        </div>
    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:content_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 134,  255 => 133,  251 => 132,  247 => 131,  243 => 130,  236 => 126,  231 => 124,  221 => 117,  210 => 109,  206 => 108,  202 => 107,  198 => 106,  192 => 103,  187 => 101,  180 => 97,  176 => 96,  172 => 95,  168 => 94,  164 => 93,  160 => 92,  156 => 91,  152 => 90,  148 => 89,  144 => 88,  139 => 85,  137 => 48,  125 => 39,  121 => 38,  117 => 37,  111 => 34,  105 => 31,  101 => 30,  95 => 27,  91 => 26,  85 => 23,  81 => 22,  75 => 19,  71 => 18,  65 => 15,  61 => 14,  55 => 11,  51 => 10,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Administration\" %}

<div ng-controller=\"AdministrationContentController\">

    <div style=\"display: table; width: 100%;\">
        <div style=\"display: table-cell; width: 50%; border-right: solid #ddd 1px; padding-right: 16px;\">
            <div class=\"alert alert-info\">
                <table>
                    <tr>
                        <td style=\"padding-right: 5px;\">{{ 'content.last_import_time'|trans }}</td>
                        <td><strong>{% verbatim %}{{ internalSettingsMap.last_import_time ? internalSettingsMap.last_import_time : \"-\" }}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.url\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_url}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.branch\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_branch}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.login\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_login}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.behind\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.behind}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.ahead\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.ahead}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.local_changes\"|trans }}</td>
                        <td>
                            <span ng-if=\"gitStatus.diff !== '?'\">
                                <strong ng-if=\"!hasUncommittedChanges()\">{{ \"git.status.local_changes.no_changes\"|trans }}</strong>
                                <strong ng-if=\"hasUncommittedChanges()\" class=\"clickable\" ng-click=\"showLocalDiff()\">{{ \"git.status.local_changes.uncommitted_changes\"|trans }}</strong>
                                {% verbatim %}{{ internalSettingsMap.last_git_update_time ? \" (\" + internalSettingsMap.last_git_update_time + \")\" : \"\" }}{% endverbatim %}
                            </span>
                            <span ng-if=\"gitStatus.diff === '?'\"><strong>?</strong></span>
                        </td>
                    </tr>
                </table>
            </div>

            <div>
                {% embed \"ConcertoPanelBundle::form_v.html.twig\" with {'no_header': true} %}
                    {% trans_default_domain \"Administration\" %}
                    {% block elements %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.url'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.url.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <input ng-disabled=\"exposedSettingsMap.content_url_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.content_url\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'/>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.file'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.file.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <form id=\"form-file-content-import-url\">
                                    <input type=\"file\" nv-file-select uploader=\"uploader\" ng-show=\"uploadItem==null && !uploader.isUploading\"/></span>
                                    <uib-progressbar class=\"progress-striped active\" value=\"uploader.progress\" ng-show=\"uploadItem==null && uploader.isUploading\"></uib-progressbar>
                                    <span ng-show=\"uploadItem!=null\">{% verbatim %}{{ uploadItem.file.name }}{% endverbatim %} <button class=\"btn btn-xs btn-danger\" ng-click=\"resetFile()\">{{ 'content.clear'|trans }}</button></span>
                                </form>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ \"content.transfer_options\"|trans }}{% endblock %}
                            {% block tooltip %}{{ \"content.transfer_options.tooltip\"|trans }}{% endblock %}
                            {% block control %}
                                <textarea ng-disabled=\"exposedSettingsMap.content_transfer_options_overridable === 'false'\" ng-model=\"exposedSettingsMap.content_transfer_options\" style=\"width:100%; resize: vertical;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'></textarea>
                            {% endblock %}
                        {% endembed %}

                    {% endblock %}
                {% endembed %}
            </div>

            <div class=\"center\">
                <button ng-click=\"importUrl()\" class=\"btn btn-success btn-sm\" ng-disabled=\"!canImport()\">{{ 'content.import'|trans }}</button>
                <button ng-click=\"exportUrl()\" class=\"btn btn-default btn-sm\">{{ 'content.export'|trans }}</button>
                <button ng-click=\"push()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPush()\" style=\"margin-left: 16px;\">{{ \"git.push.button\"|trans }}</button>
                <button ng-click=\"pull()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPull()\">{{ \"git.pull.button\"|trans }}</button>
                <button ng-click=\"commit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canCommit()\">{{ \"git.commit.button\"|trans }}</button>
                <button ng-click=\"reset()\" class=\"btn btn-danger btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canReset()\">{{ \"git.reset.button\"|trans }}</button>
                <button ng-click=\"update()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canUpdate()\">{{ \"git.update.button\"|trans }}</button>
                <button ng-click=\"refreshGitStatus()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\">{{ \"git.refresh.button\"|trans }}</button>
                <button ng-click=\"enableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"!isGitEnabled()\" ng-disabled=\"!canEnableGit()\">{{ \"git.enable.button\"|trans }}</button>
                <button ng-click=\"disableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canDisableGit()\">{{ \"git.disable.button\"|trans }}</button>
            </div>

            <div ng-if=\"isGitEnabled()\">
                <h4 style=\"text-align: center; margin-top: 32px;\">{{ \"git.latest_operation\"|trans }}</h4>

                <div class=\"alert alert-warning\" ng-if=\"gitStatus.latestTask == null\">{{ \"git.no_git_task\"|trans }}</div>
                <table class=\"table-lite\" ng-if=\"gitStatus.latestTask != null\">
                    <tr>
                        <th>{{ \"git.task.started\"|trans }}</th>
                        <th>{{ \"git.task.description\"|trans }}</th>
                        <th>{{ \"git.task.output\"|trans }}</th>
                        <th>{{ \"git.task.status\"|trans }}</th>
                    </tr>
                    <tr>
                        <td ng-bind=\"gitStatus.latestTask.created\"></td>
                        <td ng-bind=\"gitStatus.latestTask.description\"></td>
                        <td>
                            <i class=\"glyphicon glyphicon-align-justify clickable\" ng-click=\"showLatestTaskOutput()\"></i>
                        </td>
                        <td>{% verbatim %}{{ gitStatus.latestTask.status|taskStatusLabel }}{% endverbatim %}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div style=\"display: table-cell; width: 50%; padding-left: 16px;\" ng-if=\"isGitEnabled()\">
            <h4 style=\"text-align: center;\">{{ \"git.history.title\"|trans }}</h4>

            <div class=\"alert alert-warning\" ng-if=\"gitStatus.history.length == 0\">{{ \"git.history.no_commits\"|trans }}</div>

            <table class=\"table-lite\" ng-if=\"gitStatus.history.length > 0\">
                <tr>
                    <th>{{ \"git.history.list.sha\"|trans }}</th>
                    <th>{{ \"git.history.list.committer\"|trans }}</th>
                    <th>{{ \"git.history.list.time_ago\"|trans }}</th>
                    <th>{{ \"git.history.list.subject\"|trans }}</th>
                    <th>{{ \"git.history.list.ref\"|trans }}</th>
                </tr>
                <tr ng-repeat=\"commit in gitStatus.history track by \$index\" ng-class=\"{'git-history-head': commit.ref.indexOf('HEAD -> master') !== -1, 'clickable': canDiff(commit.sha)}\" ng-click=\"showDiff(commit.sha)\">
                    <td ng-bind=\"commit.sha\" class=\"git-history-commit\"></td>
                    <td ng-bind=\"commit.committer\"></td>
                    <td ng-bind=\"commit.timeAgo\"></td>
                    <td ng-bind=\"commit.subject\"></td>
                    <td ng-bind=\"commit.ref\"></td>
                </tr>
            </table>
        </div>
    </div>
</div>", "ConcertoPanelBundle:Administration:content_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/content_section.html.twig");
    }
}


/* ConcertoPanelBundle:Administration:content_section.html.twig */
class __TwigTemplate_079f06dc9d0f3a08696614afece27821dbe7233d4a3ce94a669c5742afa30213___203095222 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'elements' => [$this, 'block_elements'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 48
        return "ConcertoPanelBundle::form_v.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v.html.twig", "ConcertoPanelBundle:Administration:content_section.html.twig", 48);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 50
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 51
        echo "
                        ";
        // line 52
        $this->loadTemplate("ConcertoPanelBundle:Administration:content_section.html.twig", "ConcertoPanelBundle:Administration:content_section.html.twig", 52, "72298467")->display($context);
        // line 60
        echo "
                        ";
        // line 61
        $this->loadTemplate("ConcertoPanelBundle:Administration:content_section.html.twig", "ConcertoPanelBundle:Administration:content_section.html.twig", 61, "1621397769")->display($context);
        // line 73
        echo "
                        ";
        // line 74
        $this->loadTemplate("ConcertoPanelBundle:Administration:content_section.html.twig", "ConcertoPanelBundle:Administration:content_section.html.twig", 74, "1404922092")->display($context);
        // line 82
        echo "
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:content_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  513 => 82,  511 => 74,  508 => 73,  506 => 61,  503 => 60,  501 => 52,  498 => 51,  489 => 50,  467 => 48,  259 => 134,  255 => 133,  251 => 132,  247 => 131,  243 => 130,  236 => 126,  231 => 124,  221 => 117,  210 => 109,  206 => 108,  202 => 107,  198 => 106,  192 => 103,  187 => 101,  180 => 97,  176 => 96,  172 => 95,  168 => 94,  164 => 93,  160 => 92,  156 => 91,  152 => 90,  148 => 89,  144 => 88,  139 => 85,  137 => 48,  125 => 39,  121 => 38,  117 => 37,  111 => 34,  105 => 31,  101 => 30,  95 => 27,  91 => 26,  85 => 23,  81 => 22,  75 => 19,  71 => 18,  65 => 15,  61 => 14,  55 => 11,  51 => 10,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Administration\" %}

<div ng-controller=\"AdministrationContentController\">

    <div style=\"display: table; width: 100%;\">
        <div style=\"display: table-cell; width: 50%; border-right: solid #ddd 1px; padding-right: 16px;\">
            <div class=\"alert alert-info\">
                <table>
                    <tr>
                        <td style=\"padding-right: 5px;\">{{ 'content.last_import_time'|trans }}</td>
                        <td><strong>{% verbatim %}{{ internalSettingsMap.last_import_time ? internalSettingsMap.last_import_time : \"-\" }}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.url\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_url}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.branch\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_branch}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.login\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_login}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.behind\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.behind}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.ahead\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.ahead}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.local_changes\"|trans }}</td>
                        <td>
                            <span ng-if=\"gitStatus.diff !== '?'\">
                                <strong ng-if=\"!hasUncommittedChanges()\">{{ \"git.status.local_changes.no_changes\"|trans }}</strong>
                                <strong ng-if=\"hasUncommittedChanges()\" class=\"clickable\" ng-click=\"showLocalDiff()\">{{ \"git.status.local_changes.uncommitted_changes\"|trans }}</strong>
                                {% verbatim %}{{ internalSettingsMap.last_git_update_time ? \" (\" + internalSettingsMap.last_git_update_time + \")\" : \"\" }}{% endverbatim %}
                            </span>
                            <span ng-if=\"gitStatus.diff === '?'\"><strong>?</strong></span>
                        </td>
                    </tr>
                </table>
            </div>

            <div>
                {% embed \"ConcertoPanelBundle::form_v.html.twig\" with {'no_header': true} %}
                    {% trans_default_domain \"Administration\" %}
                    {% block elements %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.url'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.url.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <input ng-disabled=\"exposedSettingsMap.content_url_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.content_url\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'/>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.file'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.file.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <form id=\"form-file-content-import-url\">
                                    <input type=\"file\" nv-file-select uploader=\"uploader\" ng-show=\"uploadItem==null && !uploader.isUploading\"/></span>
                                    <uib-progressbar class=\"progress-striped active\" value=\"uploader.progress\" ng-show=\"uploadItem==null && uploader.isUploading\"></uib-progressbar>
                                    <span ng-show=\"uploadItem!=null\">{% verbatim %}{{ uploadItem.file.name }}{% endverbatim %} <button class=\"btn btn-xs btn-danger\" ng-click=\"resetFile()\">{{ 'content.clear'|trans }}</button></span>
                                </form>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ \"content.transfer_options\"|trans }}{% endblock %}
                            {% block tooltip %}{{ \"content.transfer_options.tooltip\"|trans }}{% endblock %}
                            {% block control %}
                                <textarea ng-disabled=\"exposedSettingsMap.content_transfer_options_overridable === 'false'\" ng-model=\"exposedSettingsMap.content_transfer_options\" style=\"width:100%; resize: vertical;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'></textarea>
                            {% endblock %}
                        {% endembed %}

                    {% endblock %}
                {% endembed %}
            </div>

            <div class=\"center\">
                <button ng-click=\"importUrl()\" class=\"btn btn-success btn-sm\" ng-disabled=\"!canImport()\">{{ 'content.import'|trans }}</button>
                <button ng-click=\"exportUrl()\" class=\"btn btn-default btn-sm\">{{ 'content.export'|trans }}</button>
                <button ng-click=\"push()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPush()\" style=\"margin-left: 16px;\">{{ \"git.push.button\"|trans }}</button>
                <button ng-click=\"pull()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPull()\">{{ \"git.pull.button\"|trans }}</button>
                <button ng-click=\"commit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canCommit()\">{{ \"git.commit.button\"|trans }}</button>
                <button ng-click=\"reset()\" class=\"btn btn-danger btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canReset()\">{{ \"git.reset.button\"|trans }}</button>
                <button ng-click=\"update()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canUpdate()\">{{ \"git.update.button\"|trans }}</button>
                <button ng-click=\"refreshGitStatus()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\">{{ \"git.refresh.button\"|trans }}</button>
                <button ng-click=\"enableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"!isGitEnabled()\" ng-disabled=\"!canEnableGit()\">{{ \"git.enable.button\"|trans }}</button>
                <button ng-click=\"disableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canDisableGit()\">{{ \"git.disable.button\"|trans }}</button>
            </div>

            <div ng-if=\"isGitEnabled()\">
                <h4 style=\"text-align: center; margin-top: 32px;\">{{ \"git.latest_operation\"|trans }}</h4>

                <div class=\"alert alert-warning\" ng-if=\"gitStatus.latestTask == null\">{{ \"git.no_git_task\"|trans }}</div>
                <table class=\"table-lite\" ng-if=\"gitStatus.latestTask != null\">
                    <tr>
                        <th>{{ \"git.task.started\"|trans }}</th>
                        <th>{{ \"git.task.description\"|trans }}</th>
                        <th>{{ \"git.task.output\"|trans }}</th>
                        <th>{{ \"git.task.status\"|trans }}</th>
                    </tr>
                    <tr>
                        <td ng-bind=\"gitStatus.latestTask.created\"></td>
                        <td ng-bind=\"gitStatus.latestTask.description\"></td>
                        <td>
                            <i class=\"glyphicon glyphicon-align-justify clickable\" ng-click=\"showLatestTaskOutput()\"></i>
                        </td>
                        <td>{% verbatim %}{{ gitStatus.latestTask.status|taskStatusLabel }}{% endverbatim %}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div style=\"display: table-cell; width: 50%; padding-left: 16px;\" ng-if=\"isGitEnabled()\">
            <h4 style=\"text-align: center;\">{{ \"git.history.title\"|trans }}</h4>

            <div class=\"alert alert-warning\" ng-if=\"gitStatus.history.length == 0\">{{ \"git.history.no_commits\"|trans }}</div>

            <table class=\"table-lite\" ng-if=\"gitStatus.history.length > 0\">
                <tr>
                    <th>{{ \"git.history.list.sha\"|trans }}</th>
                    <th>{{ \"git.history.list.committer\"|trans }}</th>
                    <th>{{ \"git.history.list.time_ago\"|trans }}</th>
                    <th>{{ \"git.history.list.subject\"|trans }}</th>
                    <th>{{ \"git.history.list.ref\"|trans }}</th>
                </tr>
                <tr ng-repeat=\"commit in gitStatus.history track by \$index\" ng-class=\"{'git-history-head': commit.ref.indexOf('HEAD -> master') !== -1, 'clickable': canDiff(commit.sha)}\" ng-click=\"showDiff(commit.sha)\">
                    <td ng-bind=\"commit.sha\" class=\"git-history-commit\"></td>
                    <td ng-bind=\"commit.committer\"></td>
                    <td ng-bind=\"commit.timeAgo\"></td>
                    <td ng-bind=\"commit.subject\"></td>
                    <td ng-bind=\"commit.ref\"></td>
                </tr>
            </table>
        </div>
    </div>
</div>", "ConcertoPanelBundle:Administration:content_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/content_section.html.twig");
    }
}


/* ConcertoPanelBundle:Administration:content_section.html.twig */
class __TwigTemplate_079f06dc9d0f3a08696614afece27821dbe7233d4a3ce94a669c5742afa30213___72298467 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 52
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:Administration:content_section.html.twig", 52);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 54
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.url", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 55
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.url.tooltip", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 56
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 57
        echo "                                <input ng-disabled=\"exposedSettingsMap.content_url_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.content_url\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'/>
                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:content_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  778 => 57,  769 => 56,  751 => 55,  733 => 54,  711 => 52,  513 => 82,  511 => 74,  508 => 73,  506 => 61,  503 => 60,  501 => 52,  498 => 51,  489 => 50,  467 => 48,  259 => 134,  255 => 133,  251 => 132,  247 => 131,  243 => 130,  236 => 126,  231 => 124,  221 => 117,  210 => 109,  206 => 108,  202 => 107,  198 => 106,  192 => 103,  187 => 101,  180 => 97,  176 => 96,  172 => 95,  168 => 94,  164 => 93,  160 => 92,  156 => 91,  152 => 90,  148 => 89,  144 => 88,  139 => 85,  137 => 48,  125 => 39,  121 => 38,  117 => 37,  111 => 34,  105 => 31,  101 => 30,  95 => 27,  91 => 26,  85 => 23,  81 => 22,  75 => 19,  71 => 18,  65 => 15,  61 => 14,  55 => 11,  51 => 10,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Administration\" %}

<div ng-controller=\"AdministrationContentController\">

    <div style=\"display: table; width: 100%;\">
        <div style=\"display: table-cell; width: 50%; border-right: solid #ddd 1px; padding-right: 16px;\">
            <div class=\"alert alert-info\">
                <table>
                    <tr>
                        <td style=\"padding-right: 5px;\">{{ 'content.last_import_time'|trans }}</td>
                        <td><strong>{% verbatim %}{{ internalSettingsMap.last_import_time ? internalSettingsMap.last_import_time : \"-\" }}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.url\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_url}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.branch\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_branch}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.login\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_login}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.behind\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.behind}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.ahead\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.ahead}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.local_changes\"|trans }}</td>
                        <td>
                            <span ng-if=\"gitStatus.diff !== '?'\">
                                <strong ng-if=\"!hasUncommittedChanges()\">{{ \"git.status.local_changes.no_changes\"|trans }}</strong>
                                <strong ng-if=\"hasUncommittedChanges()\" class=\"clickable\" ng-click=\"showLocalDiff()\">{{ \"git.status.local_changes.uncommitted_changes\"|trans }}</strong>
                                {% verbatim %}{{ internalSettingsMap.last_git_update_time ? \" (\" + internalSettingsMap.last_git_update_time + \")\" : \"\" }}{% endverbatim %}
                            </span>
                            <span ng-if=\"gitStatus.diff === '?'\"><strong>?</strong></span>
                        </td>
                    </tr>
                </table>
            </div>

            <div>
                {% embed \"ConcertoPanelBundle::form_v.html.twig\" with {'no_header': true} %}
                    {% trans_default_domain \"Administration\" %}
                    {% block elements %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.url'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.url.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <input ng-disabled=\"exposedSettingsMap.content_url_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.content_url\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'/>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.file'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.file.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <form id=\"form-file-content-import-url\">
                                    <input type=\"file\" nv-file-select uploader=\"uploader\" ng-show=\"uploadItem==null && !uploader.isUploading\"/></span>
                                    <uib-progressbar class=\"progress-striped active\" value=\"uploader.progress\" ng-show=\"uploadItem==null && uploader.isUploading\"></uib-progressbar>
                                    <span ng-show=\"uploadItem!=null\">{% verbatim %}{{ uploadItem.file.name }}{% endverbatim %} <button class=\"btn btn-xs btn-danger\" ng-click=\"resetFile()\">{{ 'content.clear'|trans }}</button></span>
                                </form>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ \"content.transfer_options\"|trans }}{% endblock %}
                            {% block tooltip %}{{ \"content.transfer_options.tooltip\"|trans }}{% endblock %}
                            {% block control %}
                                <textarea ng-disabled=\"exposedSettingsMap.content_transfer_options_overridable === 'false'\" ng-model=\"exposedSettingsMap.content_transfer_options\" style=\"width:100%; resize: vertical;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'></textarea>
                            {% endblock %}
                        {% endembed %}

                    {% endblock %}
                {% endembed %}
            </div>

            <div class=\"center\">
                <button ng-click=\"importUrl()\" class=\"btn btn-success btn-sm\" ng-disabled=\"!canImport()\">{{ 'content.import'|trans }}</button>
                <button ng-click=\"exportUrl()\" class=\"btn btn-default btn-sm\">{{ 'content.export'|trans }}</button>
                <button ng-click=\"push()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPush()\" style=\"margin-left: 16px;\">{{ \"git.push.button\"|trans }}</button>
                <button ng-click=\"pull()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPull()\">{{ \"git.pull.button\"|trans }}</button>
                <button ng-click=\"commit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canCommit()\">{{ \"git.commit.button\"|trans }}</button>
                <button ng-click=\"reset()\" class=\"btn btn-danger btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canReset()\">{{ \"git.reset.button\"|trans }}</button>
                <button ng-click=\"update()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canUpdate()\">{{ \"git.update.button\"|trans }}</button>
                <button ng-click=\"refreshGitStatus()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\">{{ \"git.refresh.button\"|trans }}</button>
                <button ng-click=\"enableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"!isGitEnabled()\" ng-disabled=\"!canEnableGit()\">{{ \"git.enable.button\"|trans }}</button>
                <button ng-click=\"disableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canDisableGit()\">{{ \"git.disable.button\"|trans }}</button>
            </div>

            <div ng-if=\"isGitEnabled()\">
                <h4 style=\"text-align: center; margin-top: 32px;\">{{ \"git.latest_operation\"|trans }}</h4>

                <div class=\"alert alert-warning\" ng-if=\"gitStatus.latestTask == null\">{{ \"git.no_git_task\"|trans }}</div>
                <table class=\"table-lite\" ng-if=\"gitStatus.latestTask != null\">
                    <tr>
                        <th>{{ \"git.task.started\"|trans }}</th>
                        <th>{{ \"git.task.description\"|trans }}</th>
                        <th>{{ \"git.task.output\"|trans }}</th>
                        <th>{{ \"git.task.status\"|trans }}</th>
                    </tr>
                    <tr>
                        <td ng-bind=\"gitStatus.latestTask.created\"></td>
                        <td ng-bind=\"gitStatus.latestTask.description\"></td>
                        <td>
                            <i class=\"glyphicon glyphicon-align-justify clickable\" ng-click=\"showLatestTaskOutput()\"></i>
                        </td>
                        <td>{% verbatim %}{{ gitStatus.latestTask.status|taskStatusLabel }}{% endverbatim %}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div style=\"display: table-cell; width: 50%; padding-left: 16px;\" ng-if=\"isGitEnabled()\">
            <h4 style=\"text-align: center;\">{{ \"git.history.title\"|trans }}</h4>

            <div class=\"alert alert-warning\" ng-if=\"gitStatus.history.length == 0\">{{ \"git.history.no_commits\"|trans }}</div>

            <table class=\"table-lite\" ng-if=\"gitStatus.history.length > 0\">
                <tr>
                    <th>{{ \"git.history.list.sha\"|trans }}</th>
                    <th>{{ \"git.history.list.committer\"|trans }}</th>
                    <th>{{ \"git.history.list.time_ago\"|trans }}</th>
                    <th>{{ \"git.history.list.subject\"|trans }}</th>
                    <th>{{ \"git.history.list.ref\"|trans }}</th>
                </tr>
                <tr ng-repeat=\"commit in gitStatus.history track by \$index\" ng-class=\"{'git-history-head': commit.ref.indexOf('HEAD -> master') !== -1, 'clickable': canDiff(commit.sha)}\" ng-click=\"showDiff(commit.sha)\">
                    <td ng-bind=\"commit.sha\" class=\"git-history-commit\"></td>
                    <td ng-bind=\"commit.committer\"></td>
                    <td ng-bind=\"commit.timeAgo\"></td>
                    <td ng-bind=\"commit.subject\"></td>
                    <td ng-bind=\"commit.ref\"></td>
                </tr>
            </table>
        </div>
    </div>
</div>", "ConcertoPanelBundle:Administration:content_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/content_section.html.twig");
    }
}


/* ConcertoPanelBundle:Administration:content_section.html.twig */
class __TwigTemplate_079f06dc9d0f3a08696614afece27821dbe7233d4a3ce94a669c5742afa30213___1621397769 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 61
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:Administration:content_section.html.twig", 61);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 63
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.file", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 64
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.file.tooltip", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 65
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 66
        echo "                                <form id=\"form-file-content-import-url\">
                                    <input type=\"file\" nv-file-select uploader=\"uploader\" ng-show=\"uploadItem==null && !uploader.isUploading\"/></span>
                                    <uib-progressbar class=\"progress-striped active\" value=\"uploader.progress\" ng-show=\"uploadItem==null && uploader.isUploading\"></uib-progressbar>
                                    <span ng-show=\"uploadItem!=null\">";
        // line 69
        echo "{{ uploadItem.file.name }}";
        echo " <button class=\"btn btn-xs btn-danger\" ng-click=\"resetFile()\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.clear", [], "Administration"), "html", null, true);
        echo "</button></span>
                                </form>
                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:content_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1048 => 69,  1043 => 66,  1034 => 65,  1016 => 64,  998 => 63,  976 => 61,  778 => 57,  769 => 56,  751 => 55,  733 => 54,  711 => 52,  513 => 82,  511 => 74,  508 => 73,  506 => 61,  503 => 60,  501 => 52,  498 => 51,  489 => 50,  467 => 48,  259 => 134,  255 => 133,  251 => 132,  247 => 131,  243 => 130,  236 => 126,  231 => 124,  221 => 117,  210 => 109,  206 => 108,  202 => 107,  198 => 106,  192 => 103,  187 => 101,  180 => 97,  176 => 96,  172 => 95,  168 => 94,  164 => 93,  160 => 92,  156 => 91,  152 => 90,  148 => 89,  144 => 88,  139 => 85,  137 => 48,  125 => 39,  121 => 38,  117 => 37,  111 => 34,  105 => 31,  101 => 30,  95 => 27,  91 => 26,  85 => 23,  81 => 22,  75 => 19,  71 => 18,  65 => 15,  61 => 14,  55 => 11,  51 => 10,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Administration\" %}

<div ng-controller=\"AdministrationContentController\">

    <div style=\"display: table; width: 100%;\">
        <div style=\"display: table-cell; width: 50%; border-right: solid #ddd 1px; padding-right: 16px;\">
            <div class=\"alert alert-info\">
                <table>
                    <tr>
                        <td style=\"padding-right: 5px;\">{{ 'content.last_import_time'|trans }}</td>
                        <td><strong>{% verbatim %}{{ internalSettingsMap.last_import_time ? internalSettingsMap.last_import_time : \"-\" }}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.url\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_url}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.branch\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_branch}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.login\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_login}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.behind\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.behind}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.ahead\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.ahead}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.local_changes\"|trans }}</td>
                        <td>
                            <span ng-if=\"gitStatus.diff !== '?'\">
                                <strong ng-if=\"!hasUncommittedChanges()\">{{ \"git.status.local_changes.no_changes\"|trans }}</strong>
                                <strong ng-if=\"hasUncommittedChanges()\" class=\"clickable\" ng-click=\"showLocalDiff()\">{{ \"git.status.local_changes.uncommitted_changes\"|trans }}</strong>
                                {% verbatim %}{{ internalSettingsMap.last_git_update_time ? \" (\" + internalSettingsMap.last_git_update_time + \")\" : \"\" }}{% endverbatim %}
                            </span>
                            <span ng-if=\"gitStatus.diff === '?'\"><strong>?</strong></span>
                        </td>
                    </tr>
                </table>
            </div>

            <div>
                {% embed \"ConcertoPanelBundle::form_v.html.twig\" with {'no_header': true} %}
                    {% trans_default_domain \"Administration\" %}
                    {% block elements %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.url'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.url.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <input ng-disabled=\"exposedSettingsMap.content_url_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.content_url\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'/>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.file'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.file.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <form id=\"form-file-content-import-url\">
                                    <input type=\"file\" nv-file-select uploader=\"uploader\" ng-show=\"uploadItem==null && !uploader.isUploading\"/></span>
                                    <uib-progressbar class=\"progress-striped active\" value=\"uploader.progress\" ng-show=\"uploadItem==null && uploader.isUploading\"></uib-progressbar>
                                    <span ng-show=\"uploadItem!=null\">{% verbatim %}{{ uploadItem.file.name }}{% endverbatim %} <button class=\"btn btn-xs btn-danger\" ng-click=\"resetFile()\">{{ 'content.clear'|trans }}</button></span>
                                </form>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ \"content.transfer_options\"|trans }}{% endblock %}
                            {% block tooltip %}{{ \"content.transfer_options.tooltip\"|trans }}{% endblock %}
                            {% block control %}
                                <textarea ng-disabled=\"exposedSettingsMap.content_transfer_options_overridable === 'false'\" ng-model=\"exposedSettingsMap.content_transfer_options\" style=\"width:100%; resize: vertical;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'></textarea>
                            {% endblock %}
                        {% endembed %}

                    {% endblock %}
                {% endembed %}
            </div>

            <div class=\"center\">
                <button ng-click=\"importUrl()\" class=\"btn btn-success btn-sm\" ng-disabled=\"!canImport()\">{{ 'content.import'|trans }}</button>
                <button ng-click=\"exportUrl()\" class=\"btn btn-default btn-sm\">{{ 'content.export'|trans }}</button>
                <button ng-click=\"push()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPush()\" style=\"margin-left: 16px;\">{{ \"git.push.button\"|trans }}</button>
                <button ng-click=\"pull()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPull()\">{{ \"git.pull.button\"|trans }}</button>
                <button ng-click=\"commit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canCommit()\">{{ \"git.commit.button\"|trans }}</button>
                <button ng-click=\"reset()\" class=\"btn btn-danger btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canReset()\">{{ \"git.reset.button\"|trans }}</button>
                <button ng-click=\"update()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canUpdate()\">{{ \"git.update.button\"|trans }}</button>
                <button ng-click=\"refreshGitStatus()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\">{{ \"git.refresh.button\"|trans }}</button>
                <button ng-click=\"enableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"!isGitEnabled()\" ng-disabled=\"!canEnableGit()\">{{ \"git.enable.button\"|trans }}</button>
                <button ng-click=\"disableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canDisableGit()\">{{ \"git.disable.button\"|trans }}</button>
            </div>

            <div ng-if=\"isGitEnabled()\">
                <h4 style=\"text-align: center; margin-top: 32px;\">{{ \"git.latest_operation\"|trans }}</h4>

                <div class=\"alert alert-warning\" ng-if=\"gitStatus.latestTask == null\">{{ \"git.no_git_task\"|trans }}</div>
                <table class=\"table-lite\" ng-if=\"gitStatus.latestTask != null\">
                    <tr>
                        <th>{{ \"git.task.started\"|trans }}</th>
                        <th>{{ \"git.task.description\"|trans }}</th>
                        <th>{{ \"git.task.output\"|trans }}</th>
                        <th>{{ \"git.task.status\"|trans }}</th>
                    </tr>
                    <tr>
                        <td ng-bind=\"gitStatus.latestTask.created\"></td>
                        <td ng-bind=\"gitStatus.latestTask.description\"></td>
                        <td>
                            <i class=\"glyphicon glyphicon-align-justify clickable\" ng-click=\"showLatestTaskOutput()\"></i>
                        </td>
                        <td>{% verbatim %}{{ gitStatus.latestTask.status|taskStatusLabel }}{% endverbatim %}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div style=\"display: table-cell; width: 50%; padding-left: 16px;\" ng-if=\"isGitEnabled()\">
            <h4 style=\"text-align: center;\">{{ \"git.history.title\"|trans }}</h4>

            <div class=\"alert alert-warning\" ng-if=\"gitStatus.history.length == 0\">{{ \"git.history.no_commits\"|trans }}</div>

            <table class=\"table-lite\" ng-if=\"gitStatus.history.length > 0\">
                <tr>
                    <th>{{ \"git.history.list.sha\"|trans }}</th>
                    <th>{{ \"git.history.list.committer\"|trans }}</th>
                    <th>{{ \"git.history.list.time_ago\"|trans }}</th>
                    <th>{{ \"git.history.list.subject\"|trans }}</th>
                    <th>{{ \"git.history.list.ref\"|trans }}</th>
                </tr>
                <tr ng-repeat=\"commit in gitStatus.history track by \$index\" ng-class=\"{'git-history-head': commit.ref.indexOf('HEAD -> master') !== -1, 'clickable': canDiff(commit.sha)}\" ng-click=\"showDiff(commit.sha)\">
                    <td ng-bind=\"commit.sha\" class=\"git-history-commit\"></td>
                    <td ng-bind=\"commit.committer\"></td>
                    <td ng-bind=\"commit.timeAgo\"></td>
                    <td ng-bind=\"commit.subject\"></td>
                    <td ng-bind=\"commit.ref\"></td>
                </tr>
            </table>
        </div>
    </div>
</div>", "ConcertoPanelBundle:Administration:content_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/content_section.html.twig");
    }
}


/* ConcertoPanelBundle:Administration:content_section.html.twig */
class __TwigTemplate_079f06dc9d0f3a08696614afece27821dbe7233d4a3ce94a669c5742afa30213___1404922092 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 74
        return "ConcertoPanelBundle::form_v_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:Administration:content_section.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_v_element.html.twig", "ConcertoPanelBundle:Administration:content_section.html.twig", 74);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 76
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.transfer_options", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 77
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("content.transfer_options.tooltip", [], "Administration"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 78
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 79
        echo "                                <textarea ng-disabled=\"exposedSettingsMap.content_transfer_options_overridable === 'false'\" ng-model=\"exposedSettingsMap.content_transfer_options\" style=\"width:100%; resize: vertical;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'></textarea>
                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:Administration:content_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1317 => 79,  1308 => 78,  1290 => 77,  1272 => 76,  1250 => 74,  1048 => 69,  1043 => 66,  1034 => 65,  1016 => 64,  998 => 63,  976 => 61,  778 => 57,  769 => 56,  751 => 55,  733 => 54,  711 => 52,  513 => 82,  511 => 74,  508 => 73,  506 => 61,  503 => 60,  501 => 52,  498 => 51,  489 => 50,  467 => 48,  259 => 134,  255 => 133,  251 => 132,  247 => 131,  243 => 130,  236 => 126,  231 => 124,  221 => 117,  210 => 109,  206 => 108,  202 => 107,  198 => 106,  192 => 103,  187 => 101,  180 => 97,  176 => 96,  172 => 95,  168 => 94,  164 => 93,  160 => 92,  156 => 91,  152 => 90,  148 => 89,  144 => 88,  139 => 85,  137 => 48,  125 => 39,  121 => 38,  117 => 37,  111 => 34,  105 => 31,  101 => 30,  95 => 27,  91 => 26,  85 => 23,  81 => 22,  75 => 19,  71 => 18,  65 => 15,  61 => 14,  55 => 11,  51 => 10,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain \"Administration\" %}

<div ng-controller=\"AdministrationContentController\">

    <div style=\"display: table; width: 100%;\">
        <div style=\"display: table-cell; width: 50%; border-right: solid #ddd 1px; padding-right: 16px;\">
            <div class=\"alert alert-info\">
                <table>
                    <tr>
                        <td style=\"padding-right: 5px;\">{{ 'content.last_import_time'|trans }}</td>
                        <td><strong>{% verbatim %}{{ internalSettingsMap.last_import_time ? internalSettingsMap.last_import_time : \"-\" }}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.url\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_url}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.branch\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_branch}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.enable.login\"|trans }}</td>
                        <td><strong>{% verbatim %}{{exposedSettingsMap.git_login}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.behind\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.behind}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.ahead\"|trans }}</td>
                        <td><strong>{% verbatim %}{{gitStatus.ahead}}{% endverbatim %}</strong></td>
                    </tr>
                    <tr ng-if=\"isGitEnabled()\">
                        <td style=\"padding-right: 5px;\">{{ \"git.status.local_changes\"|trans }}</td>
                        <td>
                            <span ng-if=\"gitStatus.diff !== '?'\">
                                <strong ng-if=\"!hasUncommittedChanges()\">{{ \"git.status.local_changes.no_changes\"|trans }}</strong>
                                <strong ng-if=\"hasUncommittedChanges()\" class=\"clickable\" ng-click=\"showLocalDiff()\">{{ \"git.status.local_changes.uncommitted_changes\"|trans }}</strong>
                                {% verbatim %}{{ internalSettingsMap.last_git_update_time ? \" (\" + internalSettingsMap.last_git_update_time + \")\" : \"\" }}{% endverbatim %}
                            </span>
                            <span ng-if=\"gitStatus.diff === '?'\"><strong>?</strong></span>
                        </td>
                    </tr>
                </table>
            </div>

            <div>
                {% embed \"ConcertoPanelBundle::form_v.html.twig\" with {'no_header': true} %}
                    {% trans_default_domain \"Administration\" %}
                    {% block elements %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.url'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.url.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <input ng-disabled=\"exposedSettingsMap.content_url_overridable === 'false'\" type=\"text\" ng-model=\"exposedSettingsMap.content_url\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'/>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ 'content.file'|trans }}{% endblock %}
                            {% block tooltip %}{{ 'content.file.tooltip'|trans }}{% endblock %}
                            {% block control %}
                                <form id=\"form-file-content-import-url\">
                                    <input type=\"file\" nv-file-select uploader=\"uploader\" ng-show=\"uploadItem==null && !uploader.isUploading\"/></span>
                                    <uib-progressbar class=\"progress-striped active\" value=\"uploader.progress\" ng-show=\"uploadItem==null && uploader.isUploading\"></uib-progressbar>
                                    <span ng-show=\"uploadItem!=null\">{% verbatim %}{{ uploadItem.file.name }}{% endverbatim %} <button class=\"btn btn-xs btn-danger\" ng-click=\"resetFile()\">{{ 'content.clear'|trans }}</button></span>
                                </form>
                            {% endblock %}
                        {% endembed %}

                        {% embed \"ConcertoPanelBundle::form_v_element.html.twig\" %}
                            {% trans_default_domain \"Administration\" %}
                            {% block label %}{{ \"content.transfer_options\"|trans }}{% endblock %}
                            {% block tooltip %}{{ \"content.transfer_options.tooltip\"|trans }}{% endblock %}
                            {% block control %}
                                <textarea ng-disabled=\"exposedSettingsMap.content_transfer_options_overridable === 'false'\" ng-model=\"exposedSettingsMap.content_transfer_options\" style=\"width:100%; resize: vertical;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }'></textarea>
                            {% endblock %}
                        {% endembed %}

                    {% endblock %}
                {% endembed %}
            </div>

            <div class=\"center\">
                <button ng-click=\"importUrl()\" class=\"btn btn-success btn-sm\" ng-disabled=\"!canImport()\">{{ 'content.import'|trans }}</button>
                <button ng-click=\"exportUrl()\" class=\"btn btn-default btn-sm\">{{ 'content.export'|trans }}</button>
                <button ng-click=\"push()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPush()\" style=\"margin-left: 16px;\">{{ \"git.push.button\"|trans }}</button>
                <button ng-click=\"pull()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canPull()\">{{ \"git.pull.button\"|trans }}</button>
                <button ng-click=\"commit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canCommit()\">{{ \"git.commit.button\"|trans }}</button>
                <button ng-click=\"reset()\" class=\"btn btn-danger btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canReset()\">{{ \"git.reset.button\"|trans }}</button>
                <button ng-click=\"update()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canUpdate()\">{{ \"git.update.button\"|trans }}</button>
                <button ng-click=\"refreshGitStatus()\" class=\"btn btn-default btn-sm\" ng-if=\"isGitEnabled()\">{{ \"git.refresh.button\"|trans }}</button>
                <button ng-click=\"enableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"!isGitEnabled()\" ng-disabled=\"!canEnableGit()\">{{ \"git.enable.button\"|trans }}</button>
                <button ng-click=\"disableGit()\" class=\"btn btn-success btn-sm\" ng-if=\"isGitEnabled()\" ng-disabled=\"!canDisableGit()\">{{ \"git.disable.button\"|trans }}</button>
            </div>

            <div ng-if=\"isGitEnabled()\">
                <h4 style=\"text-align: center; margin-top: 32px;\">{{ \"git.latest_operation\"|trans }}</h4>

                <div class=\"alert alert-warning\" ng-if=\"gitStatus.latestTask == null\">{{ \"git.no_git_task\"|trans }}</div>
                <table class=\"table-lite\" ng-if=\"gitStatus.latestTask != null\">
                    <tr>
                        <th>{{ \"git.task.started\"|trans }}</th>
                        <th>{{ \"git.task.description\"|trans }}</th>
                        <th>{{ \"git.task.output\"|trans }}</th>
                        <th>{{ \"git.task.status\"|trans }}</th>
                    </tr>
                    <tr>
                        <td ng-bind=\"gitStatus.latestTask.created\"></td>
                        <td ng-bind=\"gitStatus.latestTask.description\"></td>
                        <td>
                            <i class=\"glyphicon glyphicon-align-justify clickable\" ng-click=\"showLatestTaskOutput()\"></i>
                        </td>
                        <td>{% verbatim %}{{ gitStatus.latestTask.status|taskStatusLabel }}{% endverbatim %}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div style=\"display: table-cell; width: 50%; padding-left: 16px;\" ng-if=\"isGitEnabled()\">
            <h4 style=\"text-align: center;\">{{ \"git.history.title\"|trans }}</h4>

            <div class=\"alert alert-warning\" ng-if=\"gitStatus.history.length == 0\">{{ \"git.history.no_commits\"|trans }}</div>

            <table class=\"table-lite\" ng-if=\"gitStatus.history.length > 0\">
                <tr>
                    <th>{{ \"git.history.list.sha\"|trans }}</th>
                    <th>{{ \"git.history.list.committer\"|trans }}</th>
                    <th>{{ \"git.history.list.time_ago\"|trans }}</th>
                    <th>{{ \"git.history.list.subject\"|trans }}</th>
                    <th>{{ \"git.history.list.ref\"|trans }}</th>
                </tr>
                <tr ng-repeat=\"commit in gitStatus.history track by \$index\" ng-class=\"{'git-history-head': commit.ref.indexOf('HEAD -> master') !== -1, 'clickable': canDiff(commit.sha)}\" ng-click=\"showDiff(commit.sha)\">
                    <td ng-bind=\"commit.sha\" class=\"git-history-commit\"></td>
                    <td ng-bind=\"commit.committer\"></td>
                    <td ng-bind=\"commit.timeAgo\"></td>
                    <td ng-bind=\"commit.subject\"></td>
                    <td ng-bind=\"commit.ref\"></td>
                </tr>
            </table>
        </div>
    </div>
</div>", "ConcertoPanelBundle:Administration:content_section.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/Administration/content_section.html.twig");
    }
}
