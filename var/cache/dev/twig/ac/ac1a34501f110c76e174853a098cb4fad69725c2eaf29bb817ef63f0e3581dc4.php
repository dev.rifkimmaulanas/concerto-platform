<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ConcertoPanelBundle:ViewTemplate:form.html.twig */
class __TwigTemplate_ce31adb630b329d671e9614705998d4f8ae63288b2fb8fab67c8d4c2701736d0 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'top' => [$this, 'block_top'],
            'legend' => [$this, 'block_legend'],
            'elements' => [$this, 'block_elements'],
            'sections' => [$this, 'block_sections'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "ConcertoPanelBundle::form_h.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        // line 2
        $context["class_name"] = "ViewTemplate";
        // line 4
        $context["exportable"] = true;
        // line 1
        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_top($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        // line 7
        echo "    ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle::lock_info.html.twig");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_legend($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "legend"));

        // line 10
        echo "    ";
        echo "{{formTitle}}";
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_elements($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "elements"));

        // line 13
        echo "    ";
        $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:form.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 13, "1664855887")->display($context);
        // line 21
        echo "
    ";
        // line 22
        $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:form.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 22, "756888298")->display($context);
        // line 30
        echo "
    ";
        // line 31
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 31, $this->source); })()), "user", [], "any", false, false, false, 31) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
            // line 32
            echo "        ";
            $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:form.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 32, "566632212")->display($context);
            // line 41
            echo "
        ";
            // line 42
            $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:form.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 42, "946790283")->display($context);
            // line 52
            echo "
        ";
            // line 53
            $this->loadTemplate("ConcertoPanelBundle:ViewTemplate:form.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 53, "111930241")->display($context);
            // line 61
            echo "    ";
        }
        // line 62
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 65
    public function block_sections($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sections"));

        // line 66
        echo "    ";
        echo twig_include($this->env, $context, "ConcertoPanelBundle:ViewTemplate:source_section.html.twig");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 66,  157 => 65,  146 => 62,  143 => 61,  141 => 53,  138 => 52,  136 => 42,  133 => 41,  130 => 32,  128 => 31,  125 => 30,  123 => 22,  120 => 21,  117 => 13,  108 => 12,  95 => 10,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"ViewTemplate\" %}
{% trans_default_domain \"ViewTemplate\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"text\" ng-disabled=\"!isEditable()\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    {{ include(\"ConcertoPanelBundle:ViewTemplate:source_section.html.twig\") }}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/form.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:form.html.twig */
class __TwigTemplate_ce31adb630b329d671e9614705998d4f8ae63288b2fb8fab67c8d4c2701736d0___1664855887 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 13
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 13);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 15
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.name", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.name.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 17
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 18
        echo "            <input type=\"text\" ng-disabled=\"!isEditable()\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  354 => 18,  345 => 17,  327 => 16,  309 => 15,  287 => 13,  166 => 66,  157 => 65,  146 => 62,  143 => 61,  141 => 53,  138 => 52,  136 => 42,  133 => 41,  130 => 32,  128 => 31,  125 => 30,  123 => 22,  120 => 21,  117 => 13,  108 => 12,  95 => 10,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"ViewTemplate\" %}
{% trans_default_domain \"ViewTemplate\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"text\" ng-disabled=\"!isEditable()\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    {{ include(\"ConcertoPanelBundle:ViewTemplate:source_section.html.twig\") }}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/form.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:form.html.twig */
class __TwigTemplate_ce31adb630b329d671e9614705998d4f8ae63288b2fb8fab67c8d4c2701736d0___756888298 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 22
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 22);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 24
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.archived.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 26
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 27
        echo "            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  540 => 27,  531 => 26,  513 => 25,  495 => 24,  473 => 22,  354 => 18,  345 => 17,  327 => 16,  309 => 15,  287 => 13,  166 => 66,  157 => 65,  146 => 62,  143 => 61,  141 => 53,  138 => 52,  136 => 42,  133 => 41,  130 => 32,  128 => 31,  125 => 30,  123 => 22,  120 => 21,  117 => 13,  108 => 12,  95 => 10,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"ViewTemplate\" %}
{% trans_default_domain \"ViewTemplate\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"text\" ng-disabled=\"!isEditable()\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    {{ include(\"ConcertoPanelBundle:ViewTemplate:source_section.html.twig\") }}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/form.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:form.html.twig */
class __TwigTemplate_ce31adb630b329d671e9614705998d4f8ae63288b2fb8fab67c8d4c2701736d0___566632212 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 32
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 32);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 34
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 35
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.accessibility.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 37
        echo "                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  726 => 37,  717 => 36,  699 => 35,  681 => 34,  659 => 32,  540 => 27,  531 => 26,  513 => 25,  495 => 24,  473 => 22,  354 => 18,  345 => 17,  327 => 16,  309 => 15,  287 => 13,  166 => 66,  157 => 65,  146 => 62,  143 => 61,  141 => 53,  138 => 52,  136 => 42,  133 => 41,  130 => 32,  128 => 31,  125 => 30,  123 => 22,  120 => 21,  117 => 13,  108 => 12,  95 => 10,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"ViewTemplate\" %}
{% trans_default_domain \"ViewTemplate\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"text\" ng-disabled=\"!isEditable()\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    {{ include(\"ConcertoPanelBundle:ViewTemplate:source_section.html.twig\") }}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/form.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:form.html.twig */
class __TwigTemplate_ce31adb630b329d671e9614705998d4f8ae63288b2fb8fab67c8d4c2701736d0___946790283 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 42
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 42);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 44
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 45
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.owner.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 46
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 47
        echo "                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("none.choosen", [], "panel"), "html", null, true);
        echo "</option>
                </select>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  916 => 48,  913 => 47,  904 => 46,  886 => 45,  868 => 44,  846 => 42,  726 => 37,  717 => 36,  699 => 35,  681 => 34,  659 => 32,  540 => 27,  531 => 26,  513 => 25,  495 => 24,  473 => 22,  354 => 18,  345 => 17,  327 => 16,  309 => 15,  287 => 13,  166 => 66,  157 => 65,  146 => 62,  143 => 61,  141 => 53,  138 => 52,  136 => 42,  133 => 41,  130 => 32,  128 => 31,  125 => 30,  123 => 22,  120 => 21,  117 => 13,  108 => 12,  95 => 10,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"ViewTemplate\" %}
{% trans_default_domain \"ViewTemplate\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"text\" ng-disabled=\"!isEditable()\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    {{ include(\"ConcertoPanelBundle:ViewTemplate:source_section.html.twig\") }}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/form.html.twig");
    }
}


/* ConcertoPanelBundle:ViewTemplate:form.html.twig */
class __TwigTemplate_ce31adb630b329d671e9614705998d4f8ae63288b2fb8fab67c8d4c2701736d0___111930241 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'label' => [$this, 'block_label'],
            'tooltip' => [$this, 'block_tooltip'],
            'control' => [$this, 'block_control'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 53
        return "ConcertoPanelBundle::form_h_element.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ConcertoPanelBundle:ViewTemplate:form.html.twig"));

        $this->parent = $this->loadTemplate("ConcertoPanelBundle::form_h_element.html.twig", "ConcertoPanelBundle:ViewTemplate:form.html.twig", 53);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 55
    public function block_label($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "label"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 56
    public function block_tooltip($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "tooltip"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form.field.groups.tooltip", [], "ViewTemplate"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 57
    public function block_control($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "control"));

        // line 58
        echo "                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ConcertoPanelBundle:ViewTemplate:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1104 => 58,  1095 => 57,  1077 => 56,  1059 => 55,  1037 => 53,  916 => 48,  913 => 47,  904 => 46,  886 => 45,  868 => 44,  846 => 42,  726 => 37,  717 => 36,  699 => 35,  681 => 34,  659 => 32,  540 => 27,  531 => 26,  513 => 25,  495 => 24,  473 => 22,  354 => 18,  345 => 17,  327 => 16,  309 => 15,  287 => 13,  166 => 66,  157 => 65,  146 => 62,  143 => 61,  141 => 53,  138 => 52,  136 => 42,  133 => 41,  130 => 32,  128 => 31,  125 => 30,  123 => 22,  120 => 21,  117 => 13,  108 => 12,  95 => 10,  86 => 9,  73 => 7,  64 => 6,  53 => 1,  51 => 4,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"ConcertoPanelBundle::form_h.html.twig\" %}
{% set class_name = \"ViewTemplate\" %}
{% trans_default_domain \"ViewTemplate\" %}
{% set exportable = true %}

{% block top %}
    {{ include(\"ConcertoPanelBundle::lock_info.html.twig\") }}
{% endblock %}
{% block legend %}
    {% verbatim %}{{formTitle}}{% endverbatim %}
{% endblock %}
{% block elements %}
    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.name'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.name.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"text\" ng-disabled=\"!isEditable()\" ng-model=\"object.name\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
        {% endblock %}
    {% endembed %}

    {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
        {% trans_default_domain \"ViewTemplate\" %}
        {% block label %}{{ 'form.field.archived'|trans }}{% endblock %}
        {% block tooltip %}{{'form.field.archived.tooltip'|trans}}{% endblock %}
        {% block control %}
            <input type=\"checkbox\" ng-disabled=\"!isEditable()\"  ng-model=\"object.archived\" ng-true-value=\"'1'\" ng-false-value=\"'0'\" />
        {% endblock %}
    {% endembed %}

    {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.accessibility'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.accessibility.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.accessibility\" style=\"width:100%;\" ng-options=\"accessibility.value as accessibility.label for accessibility in accessibilities\" class='form-control'>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.owner'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.owner.tooltip'|trans}}{% endblock %}
            {% block control %}
                <select ng-disabled=\"!isEditable()\" ng-model=\"object.owner\" style=\"width:100%;\" ng-options=\"owner.id as owner.username for owner in userCollectionService.collection | filter : {'archived':'!1'} | orderBy: 'username' \" class='form-control'>
                    <option value=\"\">{{\"none.choosen\"|trans({},\"panel\")}}</option>
                </select>
            {% endblock %}
        {% endembed %}

        {% embed \"ConcertoPanelBundle::form_h_element.html.twig\" %}
            {% trans_default_domain \"ViewTemplate\" %}
            {% block label %}{{ 'form.field.groups'|trans }}{% endblock %}
            {% block tooltip %}{{'form.field.groups.tooltip'|trans}}{% endblock %}
            {% block control %}
                <input ng-disabled=\"!isEditable()\" type=\"text\" ng-model=\"object.groups\" style=\"width:100%;\" class='form-control' ng-model-options='{ updateOn: \"blur\" }' />
            {% endblock %}
        {% endembed %}
    {% endif %}

{% endblock %}

{% block sections %}
    {{ include(\"ConcertoPanelBundle:ViewTemplate:source_section.html.twig\") }}
{% endblock %}", "ConcertoPanelBundle:ViewTemplate:form.html.twig", "D:\\project\\concerto\\src\\Concerto\\PanelBundle/Resources/views/ViewTemplate/form.html.twig");
    }
}
