function UserTestController($scope, $uibModal, $http, $filter, $state, $sce, $timeout, uiGridConstants, GridService, DialogsService, DataTableCollectionService, TestCollectionService, TestWizardCollectionService, UserCollectionService, UserTestCollectionService, ViewTemplateCollectionService, AdministrationSettingsService, AuthService, ScheduledTasksCollectionService) {
    $scope.tabStateName = "usertest";
    // console.log("usertest")
    BaseController.call(this, $scope, $uibModal, $http, $filter, $state, $timeout, uiGridConstants, GridService, DialogsService, UserCollectionService, DataTableCollectionService, TestCollectionService, TestWizardCollectionService, UserCollectionService, UserTestCollectionService, ViewTemplateCollectionService, AdministrationSettingsService, AuthService, ScheduledTasksCollectionService);
    $scope.exportable = false;
    $scope.reloadOnModification = true;

    $scope.deletePath = Paths.USERTEST_DELETE;
    $scope.addFormPath = Paths.USERTEST_ADD_FORM;
    $scope.fetchObjectPath = Paths.USERTEST_FETCH_OBJECT;
    $scope.savePath = Paths.USERTEST_SAVE;

    $scope.formTitleAddLabel = Trans.USERTEST_FORM_TITLE_ADD;
    $scope.formTitleEditLabel = Trans.USERTEST_FORM_TITLE_EDIT;
    $scope.formTitle = $scope.formTitleAddLabel;
    $scope.additionalColumnsDef = [
        {
            displayName: Trans.USERTEST_LIST_FIELD_USERNAME,
            field: "username"
        }, {
            displayName: Trans.USERTEST_LIST_FIELD_EMAIL,
            field: "email"
        }];

    $scope.resetObject = function () {
        $scope.object = {
            id: 0,
            accessibility: 0,
            username: "",
            email: ""
        };
    };

    $scope.logIn = function () {
        $("#formLogin").submit();
    };

    $scope.resetObject();
    $scope.initializeColumnDefs();
}

concertoPanel.controller('UserTestController', ["$scope", "$uibModal", "$http", "$filter", "$state", "$sce", "$timeout", "uiGridConstants", "GridService", "DialogsService", "DataTableCollectionService", "TestCollectionService", "TestWizardCollectionService", "UserCollectionService", "UserTestCollectionService", "ViewTemplateCollectionService", "AdministrationSettingsService", "AuthService", "ScheduledTasksCollectionService", UserTestController]);