function MainController($scope, i18nService, RDocumentation, AdministrationSettingsService, DataTableCollectionService, TestCollectionService, TestWizardCollectionService, UserCollectionService, UserTestCollectionService, ViewTemplateCollectionService, AuthService) {
    $scope.lang = "pl";
    $scope.RDocumentation = RDocumentation;

    AuthService.fetchAuthUser(function () {
        AdministrationSettingsService.fetchSettingsMap();
        DataTableCollectionService.fetchObjectCollection();
        TestCollectionService.fetchObjectCollection();
        TestWizardCollectionService.fetchObjectCollection();
        ViewTemplateCollectionService.fetchObjectCollection();
        UserCollectionService.fetchObjectCollection();
        UserTestCollectionService.fetchObjectCollection();
    });
}

concertoPanel.controller('MainController', ["$scope", "i18nService", "RDocumentation", "AdministrationSettingsService", "DataTableCollectionService", "TestCollectionService", "TestWizardCollectionService", "UserCollectionService", "UserTestCollectionService", "ViewTemplateCollectionService", "AuthService", MainController]);