<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'a68a6de9c4a3e4d49bc11e07cd8337e797fb8feb',
        'name' => 'campsych/concerto-platform',
        'dev' => true,
    ),
    'versions' => array(
        'beberlei/assert' => array(
            'pretty_version' => 'v3.3.2',
            'version' => '3.3.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../beberlei/assert',
            'aliases' => array(),
            'reference' => 'cb70015c04be1baee6f5f5c953703347c0ac1655',
            'dev_requirement' => false,
        ),
        'campsych/concerto-platform' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'a68a6de9c4a3e4d49bc11e07cd8337e797fb8feb',
            'dev_requirement' => false,
        ),
        'cocur/slugify' => array(
            'pretty_version' => 'v1.4.1',
            'version' => '1.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../cocur/slugify',
            'aliases' => array(),
            'reference' => '16cdd7e792657d524cde931ea666436623b23301',
            'dev_requirement' => false,
        ),
        'composer/package-versions-deprecated' => array(
            'pretty_version' => '1.11.99.5',
            'version' => '1.11.99.5',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/./package-versions-deprecated',
            'aliases' => array(),
            'reference' => 'b4f54f74ef3453349c24a845d22392cd31e65f1d',
            'dev_requirement' => false,
        ),
        'doctrine/annotations' => array(
            'pretty_version' => '1.13.2',
            'version' => '1.13.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/annotations',
            'aliases' => array(),
            'reference' => '5b668aef16090008790395c02c893b1ba13f7e08',
            'dev_requirement' => false,
        ),
        'doctrine/cache' => array(
            'pretty_version' => '1.12.1',
            'version' => '1.12.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/cache',
            'aliases' => array(),
            'reference' => '4cf401d14df219fa6f38b671f5493449151c9ad8',
            'dev_requirement' => false,
        ),
        'doctrine/collections' => array(
            'pretty_version' => '1.6.8',
            'version' => '1.6.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/collections',
            'aliases' => array(),
            'reference' => '1958a744696c6bb3bb0d28db2611dc11610e78af',
            'dev_requirement' => false,
        ),
        'doctrine/common' => array(
            'pretty_version' => '2.13.3',
            'version' => '2.13.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/common',
            'aliases' => array(),
            'reference' => 'f3812c026e557892c34ef37f6ab808a6b567da7f',
            'dev_requirement' => false,
        ),
        'doctrine/dbal' => array(
            'pretty_version' => '2.13.8',
            'version' => '2.13.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/dbal',
            'aliases' => array(),
            'reference' => 'dc9b3c3c8592c935a6e590441f9abc0f9eba335b',
            'dev_requirement' => false,
        ),
        'doctrine/deprecations' => array(
            'pretty_version' => 'v0.5.3',
            'version' => '0.5.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/deprecations',
            'aliases' => array(),
            'reference' => '9504165960a1f83cc1480e2be1dd0a0478561314',
            'dev_requirement' => false,
        ),
        'doctrine/doctrine-bundle' => array(
            'pretty_version' => '1.12.13',
            'version' => '1.12.13.0',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../doctrine/doctrine-bundle',
            'aliases' => array(),
            'reference' => '85460b85edd8f61a16ad311e7ffc5d255d3c937c',
            'dev_requirement' => false,
        ),
        'doctrine/doctrine-cache-bundle' => array(
            'pretty_version' => '1.4.0',
            'version' => '1.4.0.0',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../doctrine/doctrine-cache-bundle',
            'aliases' => array(),
            'reference' => '6bee2f9b339847e8a984427353670bad4e7bdccb',
            'dev_requirement' => false,
        ),
        'doctrine/event-manager' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/event-manager',
            'aliases' => array(),
            'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
            'dev_requirement' => false,
        ),
        'doctrine/inflector' => array(
            'pretty_version' => '1.3.1',
            'version' => '1.3.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'reference' => 'ec3a55242203ffa6a4b27c58176da97ff0a7aec1',
            'dev_requirement' => false,
        ),
        'doctrine/instantiator' => array(
            'pretty_version' => '1.4.1',
            'version' => '1.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/instantiator',
            'aliases' => array(),
            'reference' => '10dcfce151b967d20fde1b34ae6640712c3891bc',
            'dev_requirement' => false,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '1.2.3',
            'version' => '1.2.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'reference' => 'c268e882d4dbdd85e36e4ad69e02dc284f89d229',
            'dev_requirement' => false,
        ),
        'doctrine/orm' => array(
            'pretty_version' => '2.7.5',
            'version' => '2.7.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/orm',
            'aliases' => array(),
            'reference' => '01187c9260cd085529ddd1273665217cae659640',
            'dev_requirement' => false,
        ),
        'doctrine/persistence' => array(
            'pretty_version' => '1.3.8',
            'version' => '1.3.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/persistence',
            'aliases' => array(),
            'reference' => '7a6eac9fb6f61bba91328f15aa7547f4806ca288',
            'dev_requirement' => false,
        ),
        'doctrine/reflection' => array(
            'pretty_version' => '1.2.2',
            'version' => '1.2.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/reflection',
            'aliases' => array(),
            'reference' => 'fa587178be682efe90d005e3a322590d6ebb59a5',
            'dev_requirement' => false,
        ),
        'fig/link-util' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../fig/link-util',
            'aliases' => array(),
            'reference' => '5d7b8d04ed3393b4b59968ca1e906fb7186d81e8',
            'dev_requirement' => false,
        ),
        'friendsofsymfony/oauth-server-bundle' => array(
            'pretty_version' => '1.6.2',
            'version' => '1.6.2.0',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../friendsofsymfony/oauth-server-bundle',
            'aliases' => array(),
            'reference' => 'fcaa25cc49474bdb0db7894f880976fe76ffed23',
            'dev_requirement' => false,
        ),
        'friendsofsymfony/oauth2-php' => array(
            'pretty_version' => '1.3.1',
            'version' => '1.3.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../friendsofsymfony/oauth2-php',
            'aliases' => array(),
            'reference' => '546f869d68fb79b284752e6787263d797165dba4',
            'dev_requirement' => false,
        ),
        'incenteev/composer-parameter-handler' => array(
            'pretty_version' => 'v2.1.4',
            'version' => '2.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../incenteev/composer-parameter-handler',
            'aliases' => array(),
            'reference' => '084befb11ec21faeadcddefb88b66132775ff59b',
            'dev_requirement' => false,
        ),
        'jdorn/sql-formatter' => array(
            'pretty_version' => 'v1.2.17',
            'version' => '1.2.17.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jdorn/sql-formatter',
            'aliases' => array(),
            'reference' => '64990d96e0959dff8e059dfcdc1af130728d92bc',
            'dev_requirement' => false,
        ),
        'lcobucci/jwt' => array(
            'pretty_version' => '3.4.6',
            'version' => '3.4.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lcobucci/jwt',
            'aliases' => array(),
            'reference' => '3ef8657a78278dfeae7707d51747251db4176240',
            'dev_requirement' => false,
        ),
        'lexik/jwt-authentication-bundle' => array(
            'pretty_version' => 'v2.10.7',
            'version' => '2.10.7.0',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../lexik/jwt-authentication-bundle',
            'aliases' => array(),
            'reference' => '79ba5af396c4f4e64fe9c8b9af65f8441fdb44cf',
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => '1.27.0',
            'version' => '1.27.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(),
            'reference' => '52ebd235c1f7e0d5e1b16464b695a28335f8e44a',
            'dev_requirement' => false,
        ),
        'namshi/jose' => array(
            'pretty_version' => '7.2.3',
            'version' => '7.2.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../namshi/jose',
            'aliases' => array(),
            'reference' => '89a24d7eb3040e285dd5925fcad992378b82bcff',
            'dev_requirement' => false,
        ),
        'ocramius/package-versions' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.11.99',
            ),
        ),
        'onelogin/php-saml' => array(
            'pretty_version' => '3.6.1',
            'version' => '3.6.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../onelogin/php-saml',
            'aliases' => array(),
            'reference' => 'a7328b11887660ad248ea10952dd67a5aa73ba3b',
            'dev_requirement' => false,
        ),
        'paragonie/constant_time_encoding' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/constant_time_encoding',
            'aliases' => array(),
            'reference' => '9229e15f2e6ba772f0c55dd6986c563b937170a8',
            'dev_requirement' => false,
        ),
        'paragonie/random_compat' => array(
            'pretty_version' => 'v2.0.21',
            'version' => '2.0.21.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/random_compat',
            'aliases' => array(),
            'reference' => '96c132c7f2f7bc3230723b66e89f8f150b29d5ae',
            'dev_requirement' => false,
        ),
        'php-http/async-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'psr/cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
            'dev_requirement' => false,
        ),
        'psr/cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
            'dev_requirement' => false,
        ),
        'psr/container-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/link' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/link',
            'aliases' => array(),
            'reference' => 'eea8e8662d5cd3ae4517c9b864493f59fca95562',
            'dev_requirement' => false,
        ),
        'psr/link-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0.0',
                1 => '1.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'dev_requirement' => false,
        ),
        'psr/simple-cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'robrichards/xmlseclibs' => array(
            'pretty_version' => '3.1.1',
            'version' => '3.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../robrichards/xmlseclibs',
            'aliases' => array(),
            'reference' => 'f8f19e58f26cdb42c54b214ff8a820760292f8df',
            'dev_requirement' => false,
        ),
        'scheb/two-factor-bundle' => array(
            'pretty_version' => 'v4.18.4',
            'version' => '4.18.4.0',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../scheb/two-factor-bundle',
            'aliases' => array(),
            'reference' => '78f5832d59ec49491ef27edc0fa03a3110139f5c',
            'dev_requirement' => false,
        ),
        'sensio/distribution-bundle' => array(
            'pretty_version' => 'v5.0.25',
            'version' => '5.0.25.0',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../sensio/distribution-bundle',
            'aliases' => array(),
            'reference' => '80a38234bde8321fb92aa0b8c27978a272bb4baf',
            'dev_requirement' => false,
        ),
        'sensio/framework-extra-bundle' => array(
            'pretty_version' => 'v5.1.6',
            'version' => '5.1.6.0',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../sensio/framework-extra-bundle',
            'aliases' => array(),
            'reference' => 'bf4940572e43af679aaa13be98f3446a1c237bd8',
            'dev_requirement' => false,
        ),
        'sensiolabs/security-checker' => array(
            'pretty_version' => 'v6.0.3',
            'version' => '6.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sensiolabs/security-checker',
            'aliases' => array(),
            'reference' => 'a576c01520d9761901f269c4934ba55448be4a54',
            'dev_requirement' => false,
        ),
        'spomky-labs/otphp' => array(
            'pretty_version' => 'v10.0.3',
            'version' => '10.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../spomky-labs/otphp',
            'aliases' => array(),
            'reference' => '9784d9f7c790eed26e102d6c78f12c754036c366',
            'dev_requirement' => false,
        ),
        'symfony/asset' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/browser-kit' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/class-loader' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/config' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/console' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/css-selector' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/debug' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/debug-bundle' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/dependency-injection' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
            'dev_requirement' => false,
        ),
        'symfony/doctrine-bridge' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/dom-crawler' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/dotenv' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/event-dispatcher' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/expression-language' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/filesystem' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/finder' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/form' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/framework-bundle' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/http-client' => array(
            'pretty_version' => 'v5.4.5',
            'version' => '5.4.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client',
            'aliases' => array(),
            'reference' => 'fab84798694e45b4571d305125215699eb2b1f73',
            'dev_requirement' => false,
        ),
        'symfony/http-client-contracts' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client-contracts',
            'aliases' => array(),
            'reference' => 'ec82e57b5b714dbb69300d348bd840b345e24166',
            'dev_requirement' => false,
        ),
        'symfony/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.4',
            ),
        ),
        'symfony/http-foundation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/http-kernel' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/inflector' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/intl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/ldap' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/lock' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/mime' => array(
            'pretty_version' => 'v5.4.3',
            'version' => '5.4.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/mime',
            'aliases' => array(),
            'reference' => 'e1503cfb5c9a225350f549d3bb99296f4abfb80f',
            'dev_requirement' => false,
        ),
        'symfony/monolog-bridge' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/monolog-bundle' => array(
            'pretty_version' => 'v3.4.0',
            'version' => '3.4.0.0',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../symfony/monolog-bundle',
            'aliases' => array(),
            'reference' => '7fbecb371c1c614642c93c6b2cbcdf723ae8809d',
            'dev_requirement' => false,
        ),
        'symfony/options-resolver' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/phpunit-bridge' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'type' => 'symfony-bridge',
            'install_path' => __DIR__ . '/../symfony/phpunit-bridge',
            'aliases' => array(),
            'reference' => '120273ad5d03a8deee08ca9260e2598f288f2bac',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-apcu' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-apcu',
            'aliases' => array(),
            'reference' => '80f7fb64c5b64ebcba76f40215e63808a2062a18',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '30885182c981ab175d4d034db0f6f469898070ab',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-icu' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-icu',
            'aliases' => array(),
            'reference' => 'c023a439b8551e320cc3c8433b198e408a623af1',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'reference' => '749045c69efb97c70d25d7463abba812e91f3a44',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php56' => array(
            'pretty_version' => 'v1.20.0',
            'version' => '1.20.0.0',
            'type' => 'metapackage',
            'install_path' => NULL,
            'aliases' => array(),
            'reference' => '54b8cd7e6c1643d78d011f3be89f3ef1f9f4c675',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php70' => array(
            'pretty_version' => 'v1.20.0',
            'version' => '1.20.0.0',
            'type' => 'metapackage',
            'install_path' => NULL,
            'aliases' => array(),
            'reference' => '5f03a781d984aae42cebd18e7912fa80f02ee644',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'reference' => 'cc5db0e22b3cb4111010e48785a97f670b350ca5',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '4407588e0d3f1f52efb65fbe92babe41f37fe50c',
            'dev_requirement' => false,
        ),
        'symfony/process' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/property-access' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/property-info' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/proxy-manager-bridge' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/routing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/security' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/security-bundle' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/security-core' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/security-csrf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/security-guard' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/security-http' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/serializer' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'reference' => '1ab11b933cd6bc5464b08e81e2c5b07dec58b0fc',
            'dev_requirement' => false,
        ),
        'symfony/stopwatch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/symfony' => array(
            'pretty_version' => 'v3.4.49',
            'version' => '3.4.49.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/symfony',
            'aliases' => array(),
            'reference' => 'ba0e346e3ad11de4a307fe4fa2452a3656dcc17b',
            'dev_requirement' => false,
        ),
        'symfony/templating' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/translation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/twig-bridge' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/twig-bundle' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/validator' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/var-dumper' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/web-link' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/web-profiler-bundle' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/web-server-bundle' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/workflow' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'symfony/yaml' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.4.49',
            ),
        ),
        'thecodingmachine/safe' => array(
            'pretty_version' => 'v1.3.3',
            'version' => '1.3.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../thecodingmachine/safe',
            'aliases' => array(),
            'reference' => 'a8ab0876305a4cdaef31b2350fcb9811b5608dbc',
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'v2.10.0',
            'version' => '2.10.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twig/twig',
            'aliases' => array(),
            'reference' => '5240e21982885b76629552d83b4ebb6d41ccde6b',
            'dev_requirement' => false,
        ),
    ),
);
